package ch.simonste.jasstafel;


import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import ch.simonste.jasstafel.common.ListRow;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.scrollTo;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.isEnabled;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.instanceOf;

public abstract class ListFragmentUITest extends UITest {

    protected int[] totals = new int[8];
    protected boolean rounded = false;
    protected boolean landscape = false;

    protected void deletePoints() {
        onView(allOf(withId(R.id.action_reset), withContentDescription(R.string.reset), isDisplayed())).perform(click());

        onView(withId(android.R.id.message)).check(matches(withText(R.string.resetConfirm)));

        onView(allOf(withId(android.R.id.button1), withText(android.R.string.ok))).perform(click());
        totals = new int[8];
    }

    protected void addRound(LinkedHashMap<Integer, Integer> map) {
        onView(allOf(withId(R.id.addButton), withContentDescription(R.string.round),
                withParent(withId(R.id.FABcontainter)),
                isDisplayed())).perform(click());

        Iterator it = ((LinkedHashMap<Integer, Integer>) map.clone()).entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            if (landscape) {
                onView(allOf(withId(R.id.edit), withParent(withId((int) pair.getKey())))).perform(scrollTo());
            }
            if (!landscape || pair.getValue() == null) {
                onView(allOf(withId(R.id.edit), withParent(withId((int) pair.getKey())), isDisplayed())).perform(click());
            }
            if (pair.getValue() != null) {
                onView(allOf(withId(R.id.edit), withParent(withId((int) pair.getKey())), isDisplayed()))
                        .perform(replaceText(pair.getValue().toString()), closeSoftKeyboard());
                if (rounded) {
                    int roundedPts = Math.round(Integer.parseInt(pair.getValue().toString()) / 10.0f);
                    onView(allOf(withId(R.id.pts), withParent(withId((int) pair.getKey())))).check(matches(withText(Integer.toString(roundedPts))));
                } else {
                    onView(allOf(withId(R.id.pts), withParent(withId((int) pair.getKey())))).check(matches(withText("")));
                }
            } else {
                onView(allOf(withId(R.id.edit), withParent(withId((int) pair.getKey())), isDisplayed())).perform(closeSoftKeyboard());
            }
            it.remove();
        }
        onView(allOf(withId(R.id.button), withText(android.R.string.ok), isDisplayed(), isEnabled())).perform(click());
    }

    protected void checkTotal() {
        int[] resId = {R.id.total1, R.id.total2, R.id.total3, R.id.total4, R.id.total5, R.id.total6, R.id.total7, R.id.total8};

        for (int i = 0; i < totals.length; i++) {
            int pts = totals[i];
            if (rounded) {
                pts = (int) Math.round(0.1 * pts);
            }
            onView(allOf(withId(resId[i]), childAtPosition(childAtPosition(withId(R.id.footer), 0), i + 2)))
                    .check(matches(withText(Integer.toString(pts))));
        }
    }

    protected void checkRoundNo(int row, Integer roundNo) {
        if (roundNo == null) {
            onData(instanceOf(ListRow.class)).atPosition(row).check(matches(hasDescendant(allOf(withId(R.id.roundNo), withText("")))));
        } else {
            onData(instanceOf(ListRow.class)).atPosition(row).check(matches(hasDescendant(allOf(withId(R.id.roundNo), withText(String.valueOf(roundNo))))));
        }
    }

    @Override
    protected void setWhoIsNext(int roundNo, int plId, int prevPlId) {
        onView(withId(R.id.footer)).perform(click());
        onView(allOf(withId(R.id.action_whobegins), withContentDescription(R.string.whoBegins), isDisplayed())).perform(click());
        super.setWhoIsNext(roundNo, plId, prevPlId);
    }
}