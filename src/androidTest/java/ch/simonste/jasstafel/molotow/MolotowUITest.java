package ch.simonste.jasstafel.molotow;


import android.content.Context;
import android.graphics.Point;
import android.os.SystemClock;
import android.view.Display;
import android.view.Gravity;
import android.view.WindowManager;

import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.filters.FlakyTest;

import org.junit.Before;
import org.junit.Test;

import java.util.LinkedHashMap;

import ch.simonste.helpers.UndoBar;
import ch.simonste.jasstafel.ListFragmentUITest;
import ch.simonste.jasstafel.OrientationChangeAction;
import ch.simonste.jasstafel.R;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.longClick;
import static androidx.test.espresso.action.ViewActions.pressImeActionButton;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.swipeDown;
import static androidx.test.espresso.action.ViewActions.swipeUp;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.PreferenceMatchers.withTitle;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.isRoot;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.not;

public class MolotowUITest extends ListFragmentUITest {

    private void addHandweis(int plId, int ptsId) {
        onView(allOf(withId(R.id.handweis), withContentDescription(R.string.handweis),
                withParent(withId(R.id.FABcontainter)),
                isDisplayed())).perform(click());

        onView(allOf(childAtPosition(withId(R.id.toolbar), 0))).check(matches(withText(R.string.handweis)));
        onView(withId(plId)).perform(click());
        onView(withId(ptsId)).perform(click());
    }

    private void addTischweis(int plId, int ptsId, boolean twice) {
        onView(allOf(withId(R.id.tischweis), withContentDescription(R.string.tischweis),
                withParent(withId(R.id.FABcontainter)),
                isDisplayed())).perform(click());

        onView(allOf(childAtPosition(withId(R.id.toolbar), 0))).check(matches(withText(R.string.tischweis)));
        if (twice) {
            onView(withId(R.id.action_fremdweis)).perform(click());
            onView(withId(R.id.w20)).check(matches(withText("2x 20")));
        } else {
            onView(withId(R.id.w20)).check(matches(withText("20")));
        }
        onView(withId(plId)).perform(click());
        onView(withId(ptsId)).perform(click());
    }

    @Before
    public void initPreferences() {
        super.initPreferences("Molotow");
        rounded = true;
    }

    @Test
    public void testMolotowGame() {
        onView(allOf(withId(R.id.startmolotow), withText(R.string.molotow))).perform(click());

        addHandweis(R.id.player2, R.id.w20);
        totals[1] -= 20;
        addTischweis(R.id.player3, R.id.w50, true);
        totals[2] += 100;

        onView(withId(R.id.footer)).perform(click());
        String text = String.format(getInstrumentation().getTargetContext().getString(R.string.duration), 0);
        onView(allOf(withId(R.id.text), isDisplayed())).check(matches(withText(text)));
        pressBack();

        checkTotal();
        checkRoundNo(0, null);

        LinkedHashMap<Integer, Integer> map = new LinkedHashMap<>();
        map.put(R.id.p2, 34);
        map.put(R.id.p1, 44);
        map.put(R.id.p4, 0);
        map.put(R.id.p3, null);
        addRound(map);
        totals[0] += 44;
        totals[1] += 34;
        totals[2] += 79;
        totals[3] += 0;

        checkTotal();
        checkRoundNo(1, 1);

        onView(allOf(withId(R.id.action_settings), withContentDescription(R.string.settings), isDisplayed())).perform(click());
        onData(withTitle(R.string.denominator10)).perform(click());
        pressBack();
        rounded = false;

        checkTotal();

        onView(allOf(withId(R.id.action_settings), withContentDescription(R.string.settings), isDisplayed())).perform(click());
        onData(withTitle(R.string.denominator10)).perform(click());
        onView(withId(R.id.action_close)).perform(click());
        rounded = true;

        addTischweis(R.id.player4, R.id.w50, false);
        totals[3] += 50;
        addTischweis(R.id.player4, R.id.w20, false);
        totals[3] += 20;
        checkTotal();
        checkRoundNo(2, null);
        checkRoundNo(3, null);

        map.clear();
        map.put(R.id.p1, 78);
        map.put(R.id.p4, 66);
        map.put(R.id.p3, 13);
        map.put(R.id.p2, null);
        addRound(map);
        totals[0] += 78;
        totals[1] += 0;
        totals[2] += 13;
        totals[3] += 66;

        checkTotal();
        checkRoundNo(4, 2);

        pressBack();
        onView(allOf(withId(R.id.startmolotow), withText(R.string.molotow))).perform(click());
        checkTotal();

        // TODO: fails on landscape
        onView(childAtPosition(withId(R.id.list), 4)).perform(click()); // nothing happens
        onView(childAtPosition(withId(R.id.list), 4)).perform(longClick());
        onView(allOf(withId(R.id.edit), withText("78"), withParent(withId(R.id.p1))))
                .perform(replaceText("70"));
        onView(allOf(withId(R.id.edit), withText("0"), withParent(withId(R.id.p2))))
                .perform(replaceText("8"));
        onView(allOf(withId(R.id.button), withText(android.R.string.ok), isDisplayed())).perform(closeSoftKeyboard(), click());
        totals[0] -= 8;
        totals[1] += 8;
        checkTotal();

        setWhoIsNext(2, R.id.r1c1, 0);

        onView(childAtPosition(withId(R.id.list), 0)).perform(longClick());
        onView(allOf(withId(R.id.edit), withText("100"), withParent(withId(R.id.p3))))
                .perform(replaceText(""));
        onView(allOf(withId(R.id.button), withText(android.R.string.ok), isDisplayed())).perform(closeSoftKeyboard(), click());
        totals[2] -= 100;
        checkTotal();

        addTischweis(R.id.player3, R.id.w50, false);
        onView(withId(R.id.undoAction)).perform(click());
        checkTotal();

        deletePoints();
        checkTotal();
    }

    @Test
    public void testWhoBeginsSwap() {
        onView(allOf(withId(R.id.startmolotow), withText(R.string.molotow))).perform(click());

        onView(allOf(withId(R.id.action_settings), withContentDescription(R.string.settings), isDisplayed())).perform(click());
        onData(withTitle(R.string.diff_players)).perform(click());
        onView(withText("5")).perform(click());
        pressBack();

        onView(withId(R.id.footer)).perform(click());
        onView(allOf(withId(R.id.action_whobegins), withContentDescription(R.string.whoBegins), isDisplayed())).perform(click());
        onView(withId(R.id.r1c2)).perform(swipeTo(R.id.r1c1));
        onView(withId(R.id.r1c2)).perform(longClick());
        pressBack();

        LinkedHashMap<Integer, Integer> map = new LinkedHashMap<>();
        map.put(R.id.p2, 157);
        addRound(map);
        setWhoIsNext(1, R.id.r1c1, R.id.r1c1);

        addTischweis(R.id.player4, R.id.w50, false);

        addRound(map);
        setWhoIsNext(2, R.id.r3c1, R.id.r2c1);

        addRound(map);
        setWhoIsNext(3, R.id.r1c1, R.id.r2c2);

        onView(withId(R.id.footer)).perform(click());
        onView(allOf(withId(R.id.action_whobegins), withContentDescription(R.string.whoBegins), isDisplayed())).perform(click());
        onView(withId(R.id.r2c2)).perform(swipeTo(R.id.r1c1));
        onView(withId(R.id.r2c2)).check(matches(withText(R.string.defpl5)));
        onView(withId(R.id.r1c1)).check(matches(withText(R.string.defpl4)));
        pressBack();

        setWhoIsNext(3, R.id.r1c1, R.id.r1c1);
        addRound(map);
        setWhoIsNext(4, R.id.r2c1, R.id.r2c1);
    }

    @Test
    public void testNoOfPlayers() {
        onView(allOf(withId(R.id.startmolotow), withText(R.string.molotow))).perform(click());

        onView(allOf(withId(R.id.action_settings), withContentDescription(R.string.settings), isDisplayed())).perform(click());
        onData(withTitle(R.string.diff_players)).perform(click());
        onView(withText("2")).perform(click());
        pressBack();

        addTischweis(R.id.player1, R.id.w50, false);
        totals[0] += 50;

        LinkedHashMap<Integer, Integer> map = new LinkedHashMap<>();
        map.put(R.id.p2, 34);
        map.put(R.id.p1, null);
        addRound(map);
        totals[0] += 157 - 34;
        totals[1] += 34;
        checkTotal();

        addTischweis(R.id.player2, R.id.w20, false);

        onView(allOf(withId(R.id.action_settings), withContentDescription(R.string.settings), isDisplayed())).perform(click());
        onData(withTitle(R.string.diff_players)).perform(click());
        onView(withText("6")).perform(click());
        pressBack();

        addTischweis(R.id.player5, R.id.w50, false);

        map = new LinkedHashMap<>();
        map.put(R.id.p6, 54);
        map.put(R.id.p2, 0);
        map.put(R.id.p5, 14);
        map.put(R.id.p1, 30);
        map.put(R.id.p3, 0);
        map.put(R.id.p4, null);
        addRound(map);

        checkRoundNo(0, null);
        checkRoundNo(1, 1);
        checkRoundNo(2, null);
        checkRoundNo(3, 2);

        onView(allOf(withParent(withParent(childAtPosition(withId(R.id.list), 1))), withId(R.id.p3))).check(matches(withText("")));
        onView(allOf(withParent(withParent(childAtPosition(withId(R.id.list), 3))), withId(R.id.p3))).check(matches(withText("0")));
    }

    @Test
    public void testRenamePlayers() {
        onView(allOf(withId(R.id.startmolotow), withText(R.string.molotow))).perform(click());

        onView(withId(R.id.player2)).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("P1"), pressImeActionButton());
        onView(withId(R.id.player2)).check(matches(withText("P1")));

        onView(withId(R.id.player3)).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("Hans"), pressImeActionButton());
        onView(withId(R.id.player3)).check(matches(withText("Hans")));

        addHandweis(R.id.player3, R.id.w50);
    }

    @Test
    @FlakyTest
    public void testFABPositions() {
        onView(allOf(withId(R.id.startmolotow), withText(R.string.molotow))).perform(click());

        onView(allOf(withId(R.id.action_settings), withContentDescription(R.string.settings), isDisplayed())).perform(click());
        onData(withTitle(R.string.screenOrientation)).perform(click());
        onView(withText(R.string.portrait)).perform(click());
        pressBack();

        WindowManager wm = (WindowManager) getInstrumentation().getTargetContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        final int height = size.y - 300;
        final int columnheight = 75;
        final int columns = (height / columnheight) - 5;
        for (int i = 0; i < columns; i++) {
            addTischweis(R.id.player1, R.id.w20, false);
        }

        onView(withId(R.id.FABcontainter)).check(matches(gravity(Gravity.BOTTOM)));
        onView(isRoot()).perform(OrientationChangeAction.orientationLandscape());
        //SystemClock.sleep(1000);

        onView(withId(R.id.FABcontainter)).check(matches(gravity(Gravity.TOP)));
        onView(isRoot()).perform(OrientationChangeAction.orientationPortrait());
        //SystemClock.sleep(1000);
        clickUntilGone(withId(R.id.action_settings));
        //waitView(withId(R.id.FABcontainter));
        ///onView(allOf(withId(R.id.action_settings), withContentDescription(R.string.settings), isDisplayed())).perform(click());
        onData(withTitle(R.string.screenOrientation)).perform(click());
        onView(withText(R.string.landscape)).perform(click());
        pressBack();
        onView(withId(R.id.FABcontainter)).check(matches(gravity(Gravity.TOP)));
        onView(withId(R.id.list)).perform(swipeDown());
        onView(withId(R.id.FABcontainter)).check(matches(not(isDisplayed())));
        onView(withId(R.id.FABcontainter)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
        onView(withId(R.id.list)).perform(swipeUp());
        onView(withId(R.id.FABcontainter)).check(matches(isDisplayed()));

        onView(allOf(withId(R.id.action_settings), withContentDescription(R.string.settings), isDisplayed())).perform(click());
        onData(withTitle(R.string.screenOrientation)).perform(click());
        onView(withText(R.string.portrait)).perform(click());
        pressBack();
        onView(withId(R.id.FABcontainter)).check(matches(gravity(Gravity.BOTTOM)));
    }

    @Test
    public void testPointsPerRound() {
        onView(allOf(withId(R.id.startmolotow), withText(R.string.molotow))).perform(click());

        onView(allOf(withId(R.id.action_settings), withContentDescription(R.string.settings), isDisplayed())).perform(click());
        onData(withTitle(R.string.pointsperround)).perform(click());
        onView(withId(android.R.id.edit)).perform(replaceText(""), pressImeActionButton());
        onView(allOf(withId(android.R.id.button1), withText(android.R.string.ok))).perform(click());
        onData(withTitle(R.string.pointsperround)).perform(click());
        onView(withId(android.R.id.edit)).check(matches(withText("157")));
        onView(withId(android.R.id.edit)).perform(replaceText("111"), pressImeActionButton());
        onView(allOf(withId(android.R.id.button1), withText(android.R.string.ok))).perform(click());
        onView(settingsSummary(R.string.pointsperround)).check(matches(withText("111")));
        pressBack();

        LinkedHashMap<Integer, Integer> map = new LinkedHashMap<>();
        map.put(R.id.p1, 14);
        map.put(R.id.p4, 66);
        map.put(R.id.p3, 10);
        map.put(R.id.p2, null);
        addRound(map);
        totals[0] += 14;
        totals[1] += 21;
        totals[2] += 10;
        totals[3] += 66;

        checkTotal();
    }

    @Test
    public void testUndo() {
        onView(allOf(withId(R.id.startmolotow), withText(R.string.molotow))).perform(click());

        addTischweis(R.id.player1, R.id.w50, false);
        onView(withId(R.id.undoAction)).perform(click());
        checkTotal();

        addTischweis(R.id.player1, R.id.w50, false);
        addTischweis(R.id.player1, R.id.w50, false);
        onView(withId(R.id.undoAction)).perform(click());
        totals[0] += 50;
        checkTotal();

        // hide undo bar when points deleted
        addTischweis(R.id.player3, R.id.w20, false);
        deletePoints();
        onView(withId(R.id.undoAction)).check(matches(not(isDisplayed())));
    }

    @Test
    public void testMolotowProfile() {
        onView(allOf(withId(R.id.startmolotow), withText(R.string.molotow))).perform(click());
        addTischweis(R.id.player1, R.id.w200, false);

        onView(withId(R.id.player1)).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("One"), pressImeActionButton());
        onView(allOf(withId(R.id.action_settings), withContentDescription(R.string.settings), isDisplayed())).perform(click());
        onData(withTitle(R.string.profiles)).perform(click());
        onView(withId(R.id.addButton)).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("8 Spieler gerundet"), pressImeActionButton());
        onView(withText("8 Spieler gerundet")).perform(click());
        pressBack();

        onData(withTitle(R.string.diff_players)).perform(click());
        onView(withText("8")).perform(click());
        pressBack();

        onView(withId(R.id.player1)).check(matches(withText("One"))); // name when profile was created (not default)
        onView(withId(R.id.player1)).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText(getInstrumentation().getTargetContext().getString(R.string.defpl1)), pressImeActionButton());
        onView(withId(R.id.player1)).check(matches(withText(R.string.defpl1)));
        addTischweis(R.id.player7, R.id.w50, false);
        totals[6] = 50;
        checkTotal();

        onView(allOf(withId(R.id.action_settings), withContentDescription(R.string.settings), isDisplayed())).perform(click());
        onData(withTitle(R.string.profiles)).perform(click());
        onView(withText("Standard")).perform(click());
        pressBack();
        onData(withTitle(R.string.denominator10)).perform(click());
        rounded = false;
        pressBack();
        onView(withId(R.id.player1)).check(matches(withText("One")));
        totals[0] = 200;
        totals[6] = 0;
        checkTotal();

        deleteProfile("8 Spieler gerundet");
    }

    @Test
    public void testProfileRotateDevice() {
        onView(allOf(withId(R.id.startmolotow), withText(R.string.molotow))).perform(click());
        onView(allOf(withId(R.id.action_settings), withContentDescription(R.string.settings), isDisplayed())).perform(click());
        onData(withTitle(R.string.profiles)).perform(click());

        onView(withId(R.id.addButton)).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("New profile"));
        onView(withId(R.id.inputField)).perform(pressImeActionButton());
        onView(withText("New profile")).perform(longClick());
        onView(withId(R.id.action_delete)).perform(click());

        onView(withId(R.id.addButton)).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("New profile"));

        onView(isRoot()).perform(OrientationChangeAction.orientationLandscape());
        onView(withId(R.id.inputField)).perform(pressImeActionButton());

        onView(withText("New profile")).perform(click());
        pressBack();

        onView(isRoot()).perform(OrientationChangeAction.orientationPortrait());
        onData(withTitle(R.string.profiles)).perform(click());
    }

    @Test
    @FlakyTest
    public void testRotateDevice() {
        onView(allOf(withId(R.id.startmolotow), withText(R.string.molotow))).perform(click());

        onView(withId(R.id.player2)).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("Spieler xy"));
        onView(isRoot()).perform(OrientationChangeAction.orientationLandscape());
        onView(withId(R.id.inputField)).check(matches(withText("Spieler xy")));
        onView(withId(R.id.inputField)).perform(pressImeActionButton());
        onView(withId(R.id.player2)).check(matches(withText("Spieler xy")));

        onView(isRoot()).perform(OrientationChangeAction.orientationPortrait());
        onView(allOf(withId(R.id.tischweis), withContentDescription(R.string.tischweis),
                withParent(withId(R.id.FABcontainter)),
                isDisplayed())).perform(click());
        onView(withId(R.id.player1)).perform(click());
        onView(isRoot()).perform(OrientationChangeAction.orientationLandscape());
        //waitView(withId(R.id.w50));
        SystemClock.sleep(2000);
        onView(withId(R.id.w50)).perform(click());
    }

    @Test
    public void testRoundNo_ChangingPlayers() {
        UndoBar.DURATION = 1; // do show undo bar very short to not hide controls
        super.initPreferences("Molotow");

        onView(allOf(withId(R.id.startmolotow), withText(R.string.molotow))).perform(click());
        onView(allOf(withId(R.id.action_settings), withContentDescription(R.string.settings), isDisplayed())).perform(click());
        onData(withTitle(R.string.denominator10)).perform(click());
        pressBack();
        rounded = false;
        addHandweis(R.id.player2, R.id.w20);
        addHandweis(R.id.player4, R.id.w100);
        checkRoundNo(0, null);

        LinkedHashMap<Integer, Integer> map = new LinkedHashMap<>();
        map.put(R.id.p2, 34);
        map.put(R.id.p1, 44);
        map.put(R.id.p4, 20);
        map.put(R.id.p3, null);

        for (int i = 1; i < 10; ++i) {
            addTischweis(R.id.player4, R.id.w20, false);
            addRound(map);
            checkRoundNo(i * 2, i);
        }

        onView(childAtPosition(withId(R.id.list), 4)).perform(longClick());
        onView(allOf(withId(R.id.edit), withText("44"))).perform(replaceText("52"));
        onView(allOf(withId(R.id.button), withText(android.R.string.ok), isDisplayed())).perform(closeSoftKeyboard(), click());

        onView(allOf(withId(R.id.action_settings), withContentDescription(R.string.settings), isDisplayed())).perform(click());
        onData(withTitle(R.string.diff_players)).perform(click());
        onView(withText("3")).perform(click());
        pressBack();

        addHandweis(R.id.player1, R.id.w20);
        addHandweis(R.id.player3, R.id.w20);
        addTischweis(R.id.player2, R.id.w50, false);

        map = new LinkedHashMap<>();
        map.put(R.id.p3, 74);
        map.put(R.id.p1, 44);
        map.put(R.id.p2, null);

        for (int i = 1; i < 4; ++i) {
            addRound(map);
            checkRoundNo(i + 19, i + 9);
        }

        onView(allOf(withId(R.id.action_settings), withContentDescription(R.string.settings), isDisplayed())).perform(click());
        onData(withTitle(R.string.diff_players)).perform(click());
        onView(withText("5")).perform(click());
        pressBack();

        // check points for negative match is counted as "round"
        map = new LinkedHashMap<>();
        map.put(R.id.p3, -157);
        addRound(map);
        checkRoundNo(23, 13);

        UndoBar.DURATION = 3000; // undo bar duration back to normal
    }
}
