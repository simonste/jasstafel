package ch.simonste.jasstafel.punktetafel;

import androidx.test.espresso.NoMatchingViewException;

import org.junit.Before;
import org.junit.Test;

import java.util.LinkedHashMap;

import ch.simonste.jasstafel.ListFragmentUITest;
import ch.simonste.jasstafel.OrientationChangeAction;
import ch.simonste.jasstafel.R;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.longClick;
import static androidx.test.espresso.action.ViewActions.pressImeActionButton;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.PreferenceMatchers.withTitle;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.isEnabled;
import static androidx.test.espresso.matcher.ViewMatchers.isRoot;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withResourceName;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.not;

public class PunkteTafelUITest extends ListFragmentUITest {

    @Before
    public void initPreferences() {
        super.initPreferences("PunkteTafel");
    }

    @Test
    public void testNoGoal() {
        onView(allOf(withId(R.id.startpunktetafel), withText(R.string.punktetafel))).perform(click());

        LinkedHashMap<Integer, Integer> map = new LinkedHashMap<>();
        map.put(R.id.p2, 24);
        map.put(R.id.p1, 74);
        map.put(R.id.p4, 0);
        map.put(R.id.p3, null);
        addRound(map);

        totals[0] += 74;
        totals[1] += 24;
        totals[2] += 59;
        totals[3] += 0;
        checkTotal();

        deletePoints();
        checkTotal();
    }

    @Test
    public void testGoalPoints() {
        onView(allOf(withId(R.id.startpunktetafel), withText(R.string.punktetafel))).perform(click());
        onView(allOf(withId(R.id.action_settings), withContentDescription(R.string.settings), isDisplayed())).perform(click());
        onData(withTitle(R.string.entergoalpoints)).check(matches(not(isEnabled())));
        onData(withTitle(R.string.goal)).perform(click());
        onView(withText(R.string.goalpoints)).perform(click());
        onData(withTitle(R.string.entergoalpoints)).perform(click());
        onView(withId(android.R.id.edit)).perform(replaceText("101"), pressImeActionButton());
        onView(allOf(withId(android.R.id.button1), withText(android.R.string.ok))).perform(click());
        onData(withTitle(R.string.pointsperround)).perform(click());
        onView(withId(android.R.id.edit)).perform(replaceText("120"), pressImeActionButton());
        onView(allOf(withId(android.R.id.button1), withText(android.R.string.ok))).perform(click());
        pressBack();

        LinkedHashMap<Integer, Integer> map = new LinkedHashMap<>();
        map.put(R.id.p2, 24);
        map.put(R.id.p1, 74);
        map.put(R.id.p4, 0);
        map.put(R.id.p3, null);
        addRound(map);

        map = new LinkedHashMap<>();
        map.put(R.id.p2, 54);
        map.put(R.id.p1, 27);
        map.put(R.id.p4, 20);
        map.put(R.id.p3, null);
        addRound(map);

        String text = String.format(getInstrumentation().getTargetContext().getString(R.string.winnerrounds), getInstrumentation().getTargetContext().getString(R.string.defpl1));
        onView(withId(android.R.id.message)).check(matches(withText(text)));
        onView(withText(android.R.string.ok)).perform(click());
    }

    @Test
    public void testGoalRounds() {
        onView(allOf(withId(R.id.startpunktetafel), withText(R.string.punktetafel))).perform(click());
        onView(allOf(withId(R.id.action_settings), withContentDescription(R.string.settings), isDisplayed())).perform(click());
        onData(withTitle(R.string.diff_players)).perform(click());
        onView(withText("3")).perform(click());
        onData(withTitle(R.string.goal)).perform(click());
        onView(withText(R.string.rounds)).perform(click());
        onData(withTitle(R.string.rounds)).perform(click());
        onView(withResourceName("alertTitle")).check(matches(withText(R.string.rounds)));
        onView(withId(android.R.id.edit)).perform(replaceText("3"), pressImeActionButton());
        onView(allOf(withId(android.R.id.button1), withText(android.R.string.ok))).perform(click());
        pressBack();

        onView(withId(R.id.player3)).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("Hans"), pressImeActionButton());
        onView(withId(R.id.player3)).check(matches(withText("Hans")));

        LinkedHashMap<Integer, Integer> map = new LinkedHashMap<>();
        map.put(R.id.p2, 24);
        map.put(R.id.p3, 74);
        map.put(R.id.p1, null);
        addRound(map);

        map = new LinkedHashMap<>();
        map.put(R.id.p2, 44);
        map.put(R.id.p1, 0);
        map.put(R.id.p3, null);
        addRound(map);

        map = new LinkedHashMap<>();
        map.put(R.id.p1, 60);
        map.put(R.id.p2, 34);
        map.put(R.id.p3, null);
        addRound(map);

        String text = String.format(getInstrumentation().getTargetContext().getString(R.string.winnerrounds), "Hans");
        onView(withId(android.R.id.message)).check(matches(withText(text)));
        onView(withText(android.R.string.ok)).perform(click());
    }

    @Test
    public void testWinnerDialog() {
        onView(allOf(withId(R.id.startpunktetafel), withText(R.string.punktetafel))).perform(click());
        onView(allOf(withId(R.id.action_settings), withContentDescription(R.string.settings), isDisplayed())).perform(click());
        onData(withTitle(R.string.diff_players)).perform(click());
        onView(withText("2")).perform(click());
        onData(withTitle(R.string.entergoalpoints)).check(matches(not(isEnabled())));
        onData(withTitle(R.string.goal)).perform(click());
        onView(withText(R.string.goalpoints)).perform(click());
        onData(withTitle(R.string.entergoalpoints)).perform(click());
        onView(withId(android.R.id.edit)).perform(replaceText("10"), pressImeActionButton());
        onView(allOf(withId(android.R.id.button1), withText(android.R.string.ok))).perform(click());
        onData(withTitle(R.string.pointsperround)).perform(click());
        onView(withId(android.R.id.edit)).perform(replaceText("10"), pressImeActionButton());
        onView(allOf(withId(android.R.id.button1), withText(android.R.string.ok))).perform(click());
        pressBack();

        LinkedHashMap<Integer, Integer> map = new LinkedHashMap<>();
        map.put(R.id.p2, 4);
        map.put(R.id.p1, null);
        addRound(map);

        map = new LinkedHashMap<>();
        map.put(R.id.p2, 4);
        map.put(R.id.p1, null);
        addRound(map);

        String text = String.format(getInstrumentation().getTargetContext().getString(R.string.winnerrounds), getInstrumentation().getTargetContext().getString(R.string.defpl1));
        onView(withId(android.R.id.message)).check(matches(withText(text)));
        onView(withText(android.R.string.ok)).perform(click());

        pressBack();
        onView(allOf(withId(R.id.startpunktetafel), withText(R.string.punktetafel))).perform(click());

        onView(allOf(withId(R.id.action_settings), withContentDescription(R.string.settings), isDisplayed())).perform(click());
        onData(withTitle(R.string.entergoalpoints)).perform(click());
        onView(withId(android.R.id.edit)).perform(replaceText("15"), pressImeActionButton());
        onView(allOf(withId(android.R.id.button1), withText(android.R.string.ok))).perform(click());
        pressBack();

        map = new LinkedHashMap<>();
        map.put(R.id.p2, 8);
        map.put(R.id.p1, null);
        addRound(map);
        text = String.format(getInstrumentation().getTargetContext().getString(R.string.winnerrounds), getInstrumentation().getTargetContext().getString(R.string.defpl2));
        onView(withId(android.R.id.message)).check(matches(withText(text)));
        onView(withText(android.R.string.ok)).perform(click());

        // TODO: fails on landscape
        onView(childAtPosition(withId(R.id.list), 2)).perform(longClick());
        onView(allOf(withId(R.id.edit), withText("8"), withParent(withId(R.id.p2)))).perform(replaceText("4"));
        onView(allOf(withId(R.id.edit), withText("2"), withParent(withId(R.id.p1)))).perform(replaceText("6"));
        // onView(allOf(withId(R.id.edit), withParent(withId(R.id.p2)), isDisplayed())).perform(click());
        onView(allOf(withId(R.id.button), withText(android.R.string.ok), isDisplayed())).perform(closeSoftKeyboard(), click());

        text = String.format(getInstrumentation().getTargetContext().getString(R.string.winnerrounds), getInstrumentation().getTargetContext().getString(R.string.defpl1));
        onView(withId(android.R.id.message)).check(matches(withText(text)));
        onView(withText(android.R.string.ok)).perform(click());
    }


    @Test
    public void testFewestPointsWinner() {
        onView(allOf(withId(R.id.startpunktetafel), withText(R.string.punktetafel))).perform(click());
        onView(allOf(withId(R.id.action_settings), withContentDescription(R.string.settings), isDisplayed())).perform(click());
        onData(withTitle(R.string.diff_players)).perform(click());
        onView(withText("2")).perform(click());
        onData(withTitle(R.string.goalpoints)).check(matches(not(isEnabled())));
        onData(withTitle(R.string.goal)).perform(click());
        onView(withText(R.string.rounds)).perform(click());
        onData(withTitle(R.string.rounds)).perform(click());
        onView(withId(android.R.id.edit)).perform(replaceText("3"), pressImeActionButton());
        onView(allOf(withId(android.R.id.button1), withText(android.R.string.ok))).perform(click());
        onView(withText(R.string.goaltype)).perform(click());
        pressBack();

        onView(withId(R.id.player1)).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("Ernst"), pressImeActionButton());
        onView(withId(R.id.player1)).check(matches(withText("Ernst")));

        LinkedHashMap<Integer, Integer> map = new LinkedHashMap<>();
        map.put(R.id.p1, 24);
        map.put(R.id.p2, null);
        addRound(map);

        map = new LinkedHashMap<>();
        map.put(R.id.p2, 95);
        map.put(R.id.p1, null);
        addRound(map);

        map = new LinkedHashMap<>();
        map.put(R.id.p1, 60);
        map.put(R.id.p2, null);
        addRound(map);

        String text = String.format(getInstrumentation().getTargetContext().getString(R.string.winnerrounds), "Ernst");
        onView(withId(android.R.id.message)).check(matches(withText(text)));
        onView(withText(android.R.string.ok)).perform(click());
    }

    @Test
    public void testPointsPerRound() {
        onView(allOf(withId(R.id.startpunktetafel), withText(R.string.punktetafel))).perform(click());
        onView(allOf(withId(R.id.action_settings), withContentDescription(R.string.settings), isDisplayed())).perform(click());
        onData(withTitle(R.string.diff_players)).perform(click());
        onView(withText("3")).perform(click());
        onData(withTitle(R.string.pointsperround)).perform(click());
        onView(withId(android.R.id.edit)).perform(replaceText("3"), pressImeActionButton());
        onView(allOf(withId(android.R.id.button1), withText(android.R.string.ok))).perform(click());
        pressBack();

        onView(withId(R.id.player1)).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("Ernst"), pressImeActionButton());
        onView(withId(R.id.player1)).check(matches(withText("Ernst")));

        LinkedHashMap<Integer, Integer> map = new LinkedHashMap<>();
        map.put(R.id.p1, 2);
        map.put(R.id.p2, 0);
        map.put(R.id.p3, null);
        addRound(map);

        totals[0] += 2;
        totals[1] += 0;
        totals[2] += 1;
        checkTotal();

        map = new LinkedHashMap<>();
        map.put(R.id.p1, 1);
        map.put(R.id.p2, -1);
        map.put(R.id.p3, 2);
        addRound(map);

        totals[0] += 1;
        totals[1] += -1;
        totals[2] += 2;
        checkTotal();
    }

    @Test
    public void test8PlayersLandscape() {
        onView(allOf(withId(R.id.startpunktetafel), withText(R.string.punktetafel))).perform(click());
        onView(allOf(withId(R.id.action_settings), withContentDescription(R.string.settings), isDisplayed())).perform(click());
        onData(withTitle(R.string.diff_players)).perform(click());
        onView(withText("8")).perform(click());

        onData(withTitle(R.string.screenOrientation)).perform(click());
        onView(withText(R.string.landscape)).perform(click());
        pressBack();
        landscape = true;

        LinkedHashMap<Integer, Integer> map = new LinkedHashMap<>();
        map.put(R.id.p1, 2);
        map.put(R.id.p2, 10);
        map.put(R.id.p3, 42);
        map.put(R.id.p4, 1);
        map.put(R.id.p5, 0);
        map.put(R.id.p6, 22);
        map.put(R.id.p7, 0);
        map.put(R.id.p8, 80);
        addRound(map);
        totals[0] += 2;
        totals[1] += 10;
        totals[2] += 42;
        totals[3] += 1;
        totals[5] += 22;
        totals[7] += 80;
        checkTotal();

        onView(allOf(withId(R.id.action_settings), withContentDescription(R.string.settings), isDisplayed())).perform(click());
        onData(withTitle(R.string.screenOrientation)).perform(click());
        onView(withText(R.string.portrait)).perform(click());
        pressBack();
        landscape = false;

        checkTotal();
    }

    @Test
    public void testTwoWinnersPoints() {
        onView(allOf(withId(R.id.startpunktetafel), withText(R.string.punktetafel))).perform(click());
        onView(allOf(withId(R.id.action_settings), withContentDescription(R.string.settings), isDisplayed())).perform(click());
        onData(withTitle(R.string.goal)).perform(click());
        onView(withText(R.string.goalpoints)).perform(click());
        onData(withTitle(R.string.entergoalpoints)).perform(click());
        onView(withResourceName("alertTitle")).check(matches(withText(R.string.goalpoints)));
        onView(withId(android.R.id.edit)).perform(replaceText("100"), pressImeActionButton());
        onView(allOf(withId(android.R.id.button1), withText(android.R.string.ok))).perform(click());
        pressBack();

        LinkedHashMap<Integer, Integer> map = new LinkedHashMap<>();
        map.put(R.id.p2, 4);
        map.put(R.id.p1, 74);
        map.put(R.id.p4, 0);
        map.put(R.id.p3, null);
        addRound(map);

        map = new LinkedHashMap<>();
        map.put(R.id.p2, 5);
        map.put(R.id.p1, 34);
        map.put(R.id.p3, 29);
        map.put(R.id.p4, null);
        addRound(map);

        String winner1 = getInstrumentation().getTargetContext().getString(R.string.defpl1);
        String winner3 = getInstrumentation().getTargetContext().getString(R.string.defpl3);
        String text = String.format(getInstrumentation().getTargetContext().getString(R.string.winnersrounds), winner1, winner3);
        onView(withId(android.R.id.message)).check(matches(withText(text)));
        onView(withText(android.R.string.ok)).perform(click());
    }

    @Test
    public void testThreeWinnersRounds() {
        onView(allOf(withId(R.id.startpunktetafel), withText(R.string.punktetafel))).perform(click());
        onView(allOf(withId(R.id.action_settings), withContentDescription(R.string.settings), isDisplayed())).perform(click());
        onData(withTitle(R.string.goal)).perform(click());
        onView(withText(R.string.rounds)).perform(click());
        onData(withTitle(R.string.rounds)).perform(click());
        onView(withId(android.R.id.edit)).perform(replaceText("2"), pressImeActionButton());
        onView(allOf(withId(android.R.id.button1), withText(android.R.string.ok))).perform(click());
        pressBack();

        LinkedHashMap<Integer, Integer> map = new LinkedHashMap<>();
        map.put(R.id.p2, 24);
        map.put(R.id.p1, 74);
        map.put(R.id.p4, 0);
        map.put(R.id.p3, null);
        addRound(map);

        map = new LinkedHashMap<>();
        map.put(R.id.p2, 74);
        map.put(R.id.p1, 24);
        map.put(R.id.p4, 20);
        map.put(R.id.p3, null);
        addRound(map);

        String winners12 = getInstrumentation().getTargetContext().getString(R.string.defpl1) + ", " + getInstrumentation().getTargetContext().getString(R.string.defpl2);
        String winner3 = getInstrumentation().getTargetContext().getString(R.string.defpl3);
        String text = String.format(getInstrumentation().getTargetContext().getString(R.string.winnersrounds), winners12, winner3);
        onView(withId(android.R.id.message)).check(matches(withText(text)));
        onView(withText(android.R.string.ok)).perform(click());
    }

    @Test
    public void testNoFixPointsPerRound() {
        onView(allOf(withId(R.id.startpunktetafel), withText(R.string.punktetafel))).perform(click());
        onView(allOf(withId(R.id.addButton), withContentDescription(R.string.round),
                withParent(withId(R.id.FABcontainter)),
                isDisplayed())).perform(click());
        onView(allOf(withId(R.id.edit), withParent(withId(R.id.p2)), isDisplayed())).perform(click(), replaceText("15"), closeSoftKeyboard());
        onView(allOf(withId(R.id.button), withText(android.R.string.ok), isDisplayed())).check(matches(not(isEnabled())));
        onView(allOf(withId(R.id.edit), withParent(withId(R.id.p3)), isDisplayed())).perform(click(), replaceText("142"), closeSoftKeyboard());
        onView(allOf(withId(R.id.button), withText(android.R.string.ok), isDisplayed())).check(matches(isEnabled()));
        onView(allOf(withId(R.id.button), withText(android.R.string.ok), isDisplayed())).perform(click());

        onView(allOf(withId(R.id.action_settings), withContentDescription(R.string.settings), isDisplayed())).perform(click());
        onData(withTitle(R.string.enableppr)).check(matches(isEnabled()));
        onData(withTitle(R.string.enableppr)).perform(click());
        onView(withId(R.id.action_close)).perform(click());

        onView(allOf(withId(R.id.addButton), withContentDescription(R.string.round),
                withParent(withId(R.id.FABcontainter)),
                isDisplayed())).perform(click());
        onView(allOf(withId(R.id.edit), withParent(withId(R.id.p2)), isDisplayed())).perform(click(), replaceText("15"), closeSoftKeyboard());
        onView(allOf(withId(R.id.button), withText(android.R.string.ok), isDisplayed())).check(matches(isEnabled()));
        onView(allOf(withId(R.id.button), withText(android.R.string.ok), isDisplayed())).perform(click());
    }

    @Test
    public void testProfile_schlaeger() {
        onView(allOf(withId(R.id.startpunktetafel), withText(R.string.punktetafel))).perform(click());
        onView(withId(R.id.player2)).perform(click());
        onView(withId(R.id.inputField)).perform(pressImeActionButton());
        onView(withId(R.id.player2)).check(matches(withText(R.string.defpl2)));
        onView(allOf(withId(R.id.action_settings), withContentDescription(R.string.settings), isDisplayed())).perform(click());
        onData(withTitle(R.string.profiles)).perform(click());
        onView(withId(R.id.addButton)).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("Schlaeger"), pressImeActionButton());
        onView(withText("Schlaeger")).perform(click());
        pressBack();

        onData(withTitle(R.string.diff_players)).perform(click());
        onView(withText("3")).perform(click());
        onData(withTitle(R.string.goal)).perform(click());
        onView(withText(R.string.goalpoints)).perform(click());
        onData(withTitle(R.string.goalpoints)).perform(click());
        onView(withId(android.R.id.edit)).perform(replaceText("9"), pressImeActionButton());
        onView(allOf(withId(android.R.id.button1), withText(android.R.string.ok))).perform(click());
        onData(withTitle(R.string.pointsperround)).perform(click());
        onView(withId(android.R.id.edit)).perform(replaceText("3"), pressImeActionButton());
        onView(allOf(withId(android.R.id.button1), withText(android.R.string.ok))).perform(click());
        onView(settingsSummary(R.string.pointsperround)).check(matches(withText("3")));
        pressBack();

        LinkedHashMap<Integer, Integer> map = new LinkedHashMap<>();
        map.put(R.id.p2, 1);
        map.put(R.id.p1, 2);
        map.put(R.id.p3, -1);
        addRound(map);

        map = new LinkedHashMap<>();
        map.put(R.id.p2, -1);
        map.put(R.id.p3, 1);
        map.put(R.id.p1, null);
        addRound(map);

        onView(withId(R.id.player2)).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("Härdöpfu"), pressImeActionButton());
        onView(withId(R.id.player2)).check(matches(withText("Härdöpfu")));

        onView(allOf(withId(R.id.action_settings), withContentDescription(R.string.settings), isDisplayed())).perform(click());
        onData(withTitle(R.string.profiles)).perform(click());
        onView(withText("Standard")).perform(click());
        pressBack();

        onView(settingsSummary(R.string.pointsperround)).check(matches(withText("157")));
        onData(withTitle(R.string.diff_players)).perform(click());
        onView(withText("6")).perform(click());
        pressBack();
        onView(withId(R.id.player2)).check(matches(withText(R.string.defpl2)));
        checkTotal(); // zero

        onView(allOf(withId(R.id.action_settings), withContentDescription(R.string.settings), isDisplayed())).perform(click());
        onData(withTitle(R.string.profiles)).perform(click());
        onView(withText("Schlaeger")).perform(click());
        pressBack();
        onView(settingsSummary(R.string.pointsperround)).check(matches(withText("3")));
        pressBack();
        onView(withId(R.id.player2)).check(matches(withText("Härdöpfu")));

        totals[0] = 4;
        totals[1] = 0;
        totals[2] = 0;
        checkTotal();

        // add same profile again -> empty
        onView(allOf(withId(R.id.action_settings), withContentDescription(R.string.settings), isDisplayed())).perform(click());
        onData(withTitle(R.string.profiles)).perform(click());
        onView(withText("Standard")).perform(click());
        onView(withText("Schlaeger")).perform(longClick());
        onView(withId(R.id.action_delete)).perform(click());
        try {
            onView(withText("Schlaeger"));
        } catch (NoMatchingViewException e) {
            // was deleted
        }
        onView(withId(R.id.addButton)).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("Schlaeger"), pressImeActionButton());
        onView(withText("Schlaeger")).perform(click());
        pressBack();
        pressBack();
        totals[0] = 0;
        totals[1] = 0;
        checkTotal(); // zero

        onView(allOf(withId(R.id.action_settings), withContentDescription(R.string.settings), isDisplayed())).perform(click());
        onData(withTitle(R.string.profiles)).perform(click());
        onView(withText("Standard")).perform(click());
        onView(withText("Schlaeger")).perform(longClick());
        onView(withId(R.id.action_delete)).perform(click());
    }

    @Test
    public void testRotateDevice() {
        onView(allOf(withId(R.id.startpunktetafel), withText(R.string.punktetafel))).perform(click());

        onView(withId(R.id.player2)).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("Spieler xy"));
        onView(isRoot()).perform(OrientationChangeAction.orientationLandscape());
        onView(withId(R.id.inputField)).check(matches(withText("Spieler xy")));
        onView(withId(R.id.inputField)).perform(pressImeActionButton());
        onView(withId(R.id.player2)).check(matches(withText("Spieler xy")));
    }

    @Test
    public void testWhoBegins() {
        onView(allOf(withId(R.id.startpunktetafel), withText(R.string.punktetafel))).perform(click());

        onView(allOf(withId(R.id.action_settings), withContentDescription(R.string.settings), isDisplayed())).perform(click());
        onData(withTitle(R.string.diff_players)).perform(click());
        onView(withText("3")).perform(click());
        pressBack();

        onView(withId(R.id.footer)).perform(click());
        onView(allOf(withId(R.id.action_whobegins), withContentDescription(R.string.whoBegins), isDisplayed())).perform(click());
        onView(withId(R.id.r1c2)).perform(swipeTo(R.id.r1c1));
        onView(withId(R.id.r1c2)).perform(longClick());
        pressBack();
    }

    @Test
    public void startInLandscape() {
        onView(isRoot()).perform(OrientationChangeAction.orientationLandscape());
        onView(allOf(withId(R.id.startpunktetafel), withText(R.string.punktetafel))).perform(click());
    }
}
