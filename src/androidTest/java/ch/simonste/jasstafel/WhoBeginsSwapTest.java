package ch.simonste.jasstafel;

import android.widget.TextView;

import androidx.test.platform.app.InstrumentationRegistry;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class WhoBeginsSwapTest {

    private final TextView[] tw = new TextView[8];
    private Whobegins whobegins;

    private void check(int spot, int player) {
        assertEquals(player, (int) whobegins.swappedMap.get(spot));
    }

    private TextView getTW(int pl) {
        for (int i = 0; i < 8; ++i) {
            if (whobegins.getTextViewId(i) == pl) {
                return tw[i];
            }
        }
        return null;
    }

    @Before
    public void setUp() {
        whobegins = new Whobegins();
        whobegins.players = 4;

        for (int i = 0; i < 8; i++) {
            tw[i] = new TextView(InstrumentationRegistry.getInstrumentation().getTargetContext());
            tw[i].setId(Whobegins.RES[i]);
        }
    }

    @Test
    public final void test_firstSwap() {
        whobegins.swapPlayers(getTW(1), getTW(2));
        assertEquals(2, whobegins.swappedMap.size());
        check(2, 1);
        check(1, 2);
    }

    @Test
    public final void test_twoSwaps() {
        whobegins.swapPlayers(getTW(1), getTW(2));
        whobegins.swapPlayers(getTW(2), getTW(0));
        assertEquals(3, whobegins.swappedMap.size());
        check(0, 2);
        check(1, 0);
        check(2, 1);
    }

    @Test
    public final void test_revertSwaps() {
        whobegins.swapPlayers(getTW(1), getTW(2));
        whobegins.swapPlayers(getTW(2), getTW(1));
        assertEquals(0, whobegins.swappedMap.size());
    }

    @Test
    public final void test_multiSwaps() {
        whobegins.swapPlayers(getTW(0), getTW(1));
        whobegins.swapPlayers(getTW(2), getTW(1));
        whobegins.swapPlayers(getTW(3), getTW(1));
        assertEquals(4, whobegins.swappedMap.size());
        check(0, 2);
        check(1, 0);
        check(2, 3);
        check(3, 1);
    }

    @Test
    public final void test_multiSwaps2() {
        whobegins.swapPlayers(getTW(0), getTW(1));
        whobegins.swapPlayers(getTW(2), getTW(1));
        whobegins.swapPlayers(getTW(3), getTW(0));
        assertEquals(4, whobegins.swappedMap.size());
        check(0, 2);
        check(1, 3);
        check(2, 1);
        check(3, 0);
    }

    @Test
    public final void test_offsettingSwaps() {
        whobegins.swapPlayers(getTW(0), getTW(1));
        whobegins.swapPlayers(getTW(0), getTW(2));
        whobegins.swapPlayers(getTW(0), getTW(3));
        whobegins.swapPlayers(getTW(0), getTW(1));
        whobegins.swapPlayers(getTW(1), getTW(2));
        whobegins.swapPlayers(getTW(3), getTW(2));
        assertEquals(0, whobegins.swappedMap.size());
    }

    @Test
    public final void test_manySwaps() {
        whobegins.players = 8;

        whobegins.swapPlayers(getTW(0), getTW(1));
        whobegins.swapPlayers(getTW(3), getTW(2));
        whobegins.swapPlayers(getTW(1), getTW(3));
        whobegins.swapPlayers(getTW(7), getTW(1));
        whobegins.swapPlayers(getTW(4), getTW(2));
        whobegins.swapPlayers(getTW(6), getTW(5));
        whobegins.swapPlayers(getTW(6), getTW(3));
        whobegins.swapPlayers(getTW(1), getTW(4));
        whobegins.swapPlayers(getTW(3), getTW(5));
        assertEquals(7, whobegins.swappedMap.size());
        check(0, 6);
        check(1, 0);
        check(2, 7);
        check(3, 1);
        check(4, 2);
        check(6, 3);
        check(7, 4);

    }
}