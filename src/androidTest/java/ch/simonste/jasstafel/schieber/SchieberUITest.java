package ch.simonste.jasstafel.schieber;


import android.os.Build;
import android.os.SystemClock;
import android.view.View;

import androidx.test.espresso.NoMatchingViewException;
import androidx.test.espresso.matcher.BoundedMatcher;
import androidx.test.espresso.matcher.ViewMatchers;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;

import ch.simonste.jasstafel.OrientationChangeAction;
import ch.simonste.jasstafel.R;
import ch.simonste.jasstafel.UITest;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.pressImeActionButton;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.swipeLeft;
import static androidx.test.espresso.action.ViewActions.swipeRight;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.PreferenceMatchers.withTitle;
import static androidx.test.espresso.matcher.RootMatchers.isPlatformPopup;
import static androidx.test.espresso.matcher.ViewMatchers.hasChildCount;
import static androidx.test.espresso.matcher.ViewMatchers.isChecked;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.isEnabled;
import static androidx.test.espresso.matcher.ViewMatchers.isNotChecked;
import static androidx.test.espresso.matcher.ViewMatchers.isRoot;
import static androidx.test.espresso.matcher.ViewMatchers.withChild;
import static androidx.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static junit.framework.TestCase.fail;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.not;

public class SchieberUITest extends UITest {

    private final int[] total = new int[2];
    private boolean upDnClickAction = false;

    private static Matcher<View> strokes(final int strokes) {
        return new BoundedMatcher<View, View>(View.class) {
            private int noOfStrokes;

            @Override
            public boolean matchesSafely(View view) {
                StrokeRow strokeRow = (StrokeRow) view;
                noOfStrokes = strokeRow.getStrokes();
                return (noOfStrokes == strokes);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("strokes: " + strokes + " has: " + noOfStrokes);
            }
        };
    }

    private void checkPoints() {
        onView(withId(R.id.teamPoints1)).check(matches(withText(Integer.toString(total[0]))));
        onView(withId(R.id.teamPoints2)).check(matches(withText(Integer.toString(total[1]))));
    }

    private void deletePoints() {
        onView(withId(R.id.action_settings)).perform(click());
        onData(withTitle(R.string.reset)).perform(click());
        try {
            onView(withText(R.string.resetall)).perform(click());
        } catch (NoMatchingViewException e) {
            onView(withText(android.R.string.ok)).perform(click());
        }
        checkPoints();
    }

    public void touch(final int id, final int pts) {
        final boolean isUpperTeam = (R.id.upperTeam == id);
        total[(isUpperTeam ? 0 : 1)] += pts;
        float[] xy;
        final float begin = isUpperTeam ? 0.9f : 0.1f;
        final float end = isUpperTeam ? 0.1f : 0.9f;
        final float center = 0.5f;
        final float belowcenter = isUpperTeam ? 0.4f : 0.6f;
        switch (pts) {
            case -1:
                xy = new float[]{end, belowcenter};
                break;
            case 1:
                xy = new float[]{end, center};
                break;
            case 20:
                xy = new float[]{begin, end};
                break;
            case 50:
                xy = new float[]{begin, center};
                break;
            case 100:
                xy = new float[]{begin, begin};
                break;
            default:
                throw new IllegalArgumentException();
        }

        onView(withId(id)).perform(touch(xy));
    }

    private void addRound(int id, int[] resIds) {
        if (id != 0) {
            float[] xy = (R.id.upperTeam == id) ? new float[]{0.1f, 0.1f} : new float[]{0.9f, 0.9f};
            onView(withId(id)).perform(touch(xy));
            try {
                // 2nd try
                onView(allOf(withId(R.id.button0), isDisplayed())).check(matches(isDisplayed()));
            } catch (NoMatchingViewException e) {
                onView(withId(id)).perform(touch(xy));
            }
        }

        for (int resId : resIds) {
            if (upDnClickAction) {
                onView(allOf(withId(resId), isDisplayed())).perform(clickUpDn());
            } else {
                onView(allOf(withId(resId), isDisplayed())).perform(click());
            }
        }
    }

    @Before
    public void initPreferences() {
        super.initPreferences("Schieber");
    }

    @Test
    public void testStandard() {
        onView(allOf(withId(R.id.startschieber), withText(R.string.schieber))).perform(click());
        onView(allOf(withId(R.id.teamName1), isDisplayed())).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText(getInstrumentation().getTargetContext().getString(R.string.defteam1)), pressImeActionButton());

        onView(withId(R.id.goal)).perform(click());
        onView(withId(R.id.inputField))
                .perform(replaceText("2000"), pressImeActionButton());
        onView(withId(R.id.goal)).check(matches(withText("2000")));

        touch(R.id.upperTeam, 1);
        touch(R.id.upperTeam, 20);
        touch(R.id.upperTeam, 100);
        touch(R.id.upperTeam, 50);
        touch(R.id.upperTeam, 1);
        touch(R.id.upperTeam, 20);
        touch(R.id.upperTeam, -1);
        touch(R.id.lowerTeam, 100);
        touch(R.id.lowerTeam, 100);
        touch(R.id.lowerTeam, 20);
        touch(R.id.lowerTeam, -1);
        checkPoints();

        SystemClock.sleep(SchieberHistoryHelper.Group_Duration);
        addRound(R.id.lowerTeam, new int[]{R.id.button2, R.id.button2, R.id.count2, R.id.buttonEnter});
        total[0] += 2 * (157 - 22);
        total[1] += 2 * 22;
        checkPoints();

        upDnClickAction = true;
        addRound(R.id.upperTeam, new int[]{R.id.button5, R.id.button7, R.id.buttonBackspace, R.id.count3, R.id.button3, R.id.buttonEnter});
        total[0] += 3 * 53;
        total[1] += 3 * (157 - 53);
        checkPoints();

        addRound(R.id.upperTeam, new int[]{R.id.action_rotate});
        upDnClickAction = false;
        try {
            addRound(0, new int[]{R.id.buttonMatch, R.id.count4, R.id.buttonEnter});
        } catch (NoMatchingViewException e) {
            // on small devices countX is displayed instead of count4
            addRound(0, new int[]{R.id.countX, R.id.buttonEnter});
        }
        total[0] += 4 * 257;
        checkPoints();

        try {
            addRound(R.id.upperTeam, new int[]{R.id.button6, R.id.button4, R.id.count6, R.id.buttonEnter});
        } catch (NoMatchingViewException e) {
            // on small devices countX is displayed instead of count6
            addRound(0, new int[]{R.id.spinnerFactor});
            onView(withText("x6")).inRoot(isPlatformPopup()).perform(click());
            addRound(0, new int[]{R.id.buttonEnter});
        }

        String text = String.format(getInstrumentation().getTargetContext().getString(R.string.winner),
                getInstrumentation().getTargetContext().getString(R.string.defteam1));
        onView(withId(android.R.id.message)).check(matches(withText(text)));
        onView(withText(android.R.string.ok)).perform(click());
    }

    @Test
    public void testTwoGoals() {
        onView(allOf(withId(R.id.startschieber), withText(R.string.schieber))).perform(click());
        onView(allOf(withId(R.id.teamName1), isDisplayed())).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("Simon"), pressImeActionButton());
        onView(allOf(withId(R.id.teamName2), isDisplayed())).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("Sandro"), pressImeActionButton());
        onView(withId(R.id.action_settings)).perform(click());
        onData(withTitle(R.string.differentGoals)).perform(click());
        onData(withTitle(R.string.undoButton)).perform(click());
        pressBack();

        onView(withId(R.id.goal1)).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("1000"), pressImeActionButton());
        onView(withId(R.id.goal)).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("1300"), pressImeActionButton());
        onView(withId(R.id.goal1)).check(matches(withText("1000")));
        onView(withId(R.id.goal)).perform(click());
        onView(withId(R.id.inputField)).check(matches(withText("1300")));
        onView(withId(R.id.inputField)).perform(replaceText("1200"), pressImeActionButton());
        onView(withId(R.id.goal1)).perform(click());
        onView(withId(R.id.inputField)).check(matches(withText("1000")));
        onView(withId(R.id.inputField)).perform(replaceText("900"), pressImeActionButton());

        touch(R.id.upperTeam, 100);
        touch(R.id.upperTeam, 100);
        touch(R.id.upperTeam, 50);
        touch(R.id.lowerTeam, 20);
        touch(R.id.lowerTeam, 20);
        touch(R.id.lowerTeam, 20);
        checkPoints();

        try {
            addRound(R.id.lowerTeam, new int[]{R.id.button1, R.id.button4, R.id.count4, R.id.buttonEnter});
        } catch (NoMatchingViewException e) {
            // on small devices countX is displayed instead of count4
            addRound(0, new int[]{R.id.countX, R.id.buttonEnter});
        }
        total[0] += 4 * (157 - 14);
        total[1] += 4 * 14;
        checkPoints();

        touch(R.id.upperTeam, 100);
        String text = String.format(getInstrumentation().getTargetContext().getString(R.string.winner), "Simon");
        onView(withId(android.R.id.message)).check(matches(withText(text)));
        onView(withText(android.R.string.ok)).perform(click());
        checkPoints();
        onView(withId(R.id.info_undo)).perform(click());
        total[0] -= 100;
        checkPoints();

        touch(R.id.lowerTeam, 100);
        touch(R.id.lowerTeam, 100);
        addRound(R.id.lowerTeam, new int[]{R.id.buttonMatch, R.id.count3, R.id.buttonEnter});
        total[1] += 3 * 257;
        checkPoints();

        addRound(R.id.lowerTeam, new int[]{R.id.count2, R.id.button1, R.id.button2, R.id.button1, R.id.buttonEnter});
        total[0] += 2 * (157 - 121);
        total[1] += 2 * 121;
        text = String.format(getInstrumentation().getTargetContext().getString(R.string.winner), "Sandro");
        onView(withId(android.R.id.message)).check(matches(withText(text)));
        onView(withText(android.R.string.ok)).perform(click());
        checkPoints();
    }


    @Test
    public void testHistory() {
        onView(allOf(withId(R.id.startschieber), withText(R.string.schieber))).perform(click());
        onView(allOf(withId(R.id.teamName1), isDisplayed())).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("T1"), pressImeActionButton());
        addRound(R.id.lowerTeam, new int[]{R.id.button5, R.id.button2, R.id.count2, R.id.buttonEnter});
        total[0] += 2 * (157 - 52);
        total[1] += 2 * 52;
        checkPoints();

        touch(R.id.upperTeam, 20);
        touch(R.id.upperTeam, 100);
        touch(R.id.upperTeam, 50);
        checkPoints();
        SystemClock.sleep(SchieberHistoryHelper.Group_Duration);
        touch(R.id.upperTeam, 20);
        touch(R.id.lowerTeam, 100);
        touch(R.id.lowerTeam, -1);
        checkPoints();

        onView(withId(R.id.info_undo)).perform(click());
        onView(allOf(withId(R.id.undoAction), isDisplayed())).perform(click());
        pressBack();
        total[1] += 1;
        checkPoints();
        onView(withId(R.id.info_undo)).perform(click());
        onView(allOf(withId(R.id.undoRound), isDisplayed())).perform(click());
        pressBack();
        total[0] -= 20;
        total[1] -= 100;
        checkPoints();

        onView(allOf(withId(R.id.teamName1), isDisplayed())).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("A & B"), pressImeActionButton());
        String text = getInstrumentation().getTargetContext().getString(R.string.teamnamechanged);
        text += "\n";
        text += getInstrumentation().getTargetContext().getString(R.string.deletewhat);
        onView(withId(android.R.id.message)).check(matches(withText(text)));
        onView(withText(android.R.string.cancel)).perform(click());
        onView(allOf(withId(R.id.teamName2), isDisplayed())).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("C & D"), pressImeActionButton());
        addRound(R.id.lowerTeam, new int[]{R.id.button1, R.id.count2, R.id.button0, R.id.button0, R.id.buttonWeis});
        total[1] += 200;
        onView(withId(R.id.goal)).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("1600"), pressImeActionButton());
        SystemClock.sleep(500);

        try {
            addRound(R.id.lowerTeam, new int[]{R.id.button7, R.id.button3, R.id.count7, R.id.buttonEnter});
        } catch (NoMatchingViewException e) {
            // on small devices countX is displayed instead of count7
            addRound(0, new int[]{R.id.spinnerFactor});
            onView(withText("x7")).inRoot(isPlatformPopup()).perform(click());
            addRound(0, new int[]{R.id.buttonEnter});
        }

        onView(withId(android.R.id.message)).check(matches(withText(R.string.bergpreisBoth)));
        onView(withText("A & B")).perform(click());

        touch(R.id.upperTeam, 20);
        touch(R.id.upperTeam, 20);
        touch(R.id.lowerTeam, -1);
        touch(R.id.upperTeam, 20);
        touch(R.id.lowerTeam, 100);
        touch(R.id.lowerTeam, -1);
        touch(R.id.lowerTeam, -1);

        onView(withId(R.id.info_undo)).perform(click());
        onView(allOf(withParent(withChild(withText("97"))), withId(R.id.actions))).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
        onView(allOf(withParent(withChild(withText("97"))), withId(R.id.roundNo))).check(matches(withText("3")));
        onView(allOf(withParent(withChild(withText("511"))), withId(R.id.actions))).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.INVISIBLE)));
        onView(allOf(withParent(withChild(withText("511"))), withId(R.id.roundNo))).check(matches(withText("2")));
        onView(allOf(withParent(withChild(withText("170"))), withId(R.id.actions))).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
        onView(allOf(withParent(withChild(withText("170"))), withId(R.id.roundNo))).check(matches(withText("")));
        onView(allOf(withParent(withChild(withText("170"))), withId(R.id.actions))).check(matches(hasChildCount(3)));

        onView(allOf(withId(R.id.undoAction), isDisplayed())).perform(click());
        onView(allOf(withParent(withChild(withText("98"))), withId(R.id.roundNo))).check(matches(withText("")));
        onView(allOf(withId(R.id.undoRound), isDisplayed())).perform(click());

        onView(withId(R.id.action_statistics)).perform(click());
        onView(withId(R.id.bergpreis1)).check(matches(withText("✔")));
        onView(withId(R.id.bergpreis2)).check(matches(withText("")));
        onView(withId(R.id.ptsWeis1)).check(matches(withText("170")));
        onView(withId(R.id.ptsWeis2)).check(matches(withText("200")));
        onView(withId(R.id.ptsStich1)).check(matches(withText("798")));
        onView(withId(R.id.ptsStich2)).check(matches(withText("615")));
        onView(withId(R.id.ptsTotal1)).check(matches(withText("968")));
        onView(withId(R.id.ptsTotal2)).check(matches(withText("815")));
        pressBack();

        onView(withId(R.id.goal)).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("1000"), pressImeActionButton());

        try {
            addRound(R.id.lowerTeam, new int[]{R.id.button6, R.id.button4, R.id.count4, R.id.buttonEnter});
        } catch (NoMatchingViewException e) {
            // on small devices countX is displayed instead of count4
            addRound(0, new int[]{R.id.countX, R.id.buttonEnter});
        }
        onView(withId(android.R.id.message)).check(matches(withText(R.string.winnerboth)));
        onView(withText("C & D")).perform(click());
        onView(withId(R.id.info_undo)).perform(click());
        onView(withId(R.id.action_reset)).perform(click());
        onView(withText(R.string.currentround)).perform(click());

        touch(R.id.upperTeam, 100);
        touch(R.id.upperTeam, 100);

        onView(withId(R.id.info_undo)).perform(click());
        onView(withId(R.id.action_statistics)).perform(click());
        onView(withId(R.id.bergpreis1)).check(matches(withText("")));
        onView(withId(R.id.bergpreis2)).check(matches(withText("")));
        onView(withId(R.id.ptsWeis1)).check(matches(withText("200")));
        onView(withId(R.id.ptsWeis2)).check(matches(withText("0")));
        onView(withId(R.id.ptsStich1)).check(matches(withText("0")));
        onView(withId(R.id.ptsStich2)).check(matches(withText("0")));
        onView(withId(R.id.ptsTotal1)).check(matches(withText("200")));
        onView(withId(R.id.ptsTotal2)).check(matches(withText("0")));

        onView(withId(R.id.bergpreis1All)).check(matches(withText("1")));
        onView(withId(R.id.bergpreis2All)).check(matches(withText("0")));
        onView(withId(R.id.ptsWeis1All)).check(matches(withText("370")));
        onView(withId(R.id.ptsWeis2All)).check(matches(withText("200")));
        onView(withId(R.id.ptsStich1All)).check(matches(withText("1170")));
        onView(withId(R.id.ptsStich2All)).check(matches(withText("871")));
        onView(withId(R.id.ptsTotal1All)).check(matches(withText("1540")));
        onView(withId(R.id.ptsTotal2All)).check(matches(withText("1071")));
        onView(withId(R.id.action_reset)).perform(click());
        onView(withText(R.string.resetall)).perform(click());

        onView(withId(R.id.info_undo)).perform(click());
        onView(withId(R.id.action_statistics)).perform(click());
        onView(withId(R.id.ptsTotal1All)).check(matches(withText("0")));
        onView(withId(R.id.ptsTotal2All)).check(matches(withText("0")));
    }

    @Test
    public void testTwoDecks() {
        onView(allOf(withId(R.id.startschieber), withText(R.string.schieber))).perform(click());
        onView(withId(R.id.action_settings)).perform(click());
        onData(withTitle(R.string.pointsperround)).perform(click());

        onView(withId(android.R.id.edit)).perform(replaceText("314"), pressImeActionButton());
        onView(allOf(withId(android.R.id.button1), withText(android.R.string.ok))).perform(click());
        pressBack();


        addRound(R.id.lowerTeam, new int[]{R.id.buttonMatch, R.id.count1, R.id.buttonEnter});
        total[1] = 514;
        checkPoints();
        addRound(R.id.lowerTeam, new int[]{R.id.button7, R.id.button4, R.id.count2, R.id.buttonEnter});
        total[0] += 2 * (314 - 74);
        total[1] += 2 * 74;
        checkPoints();
    }

    @Test
    public void testBackside() {
        onView(allOf(withId(R.id.startschieber), withText(R.string.schieber))).perform(click());
        onView(withId(R.id.action_settings)).perform(click());
        onData(withTitle(R.string.backside)).perform(click());
        pressBack();

        onView(withId(R.id.ViewPager)).perform(swipeLeft());
        onView(allOf(withId(R.id.deleteButton), isDisplayed())).perform(click());
        try {
            onView(withText(android.R.string.ok)).perform(click());
            fail();
        } catch (NoMatchingViewException e) {
            // already deleted
        }

        onView(withId(R.id.strokes1)).perform(click());
        onView(withId(R.id.strokes1)).perform(click());
        onView(allOf(withId(R.id.teamName2), isDisplayed())).perform(click());
        onView(withId(R.id.strokes1)).perform(click());
        onView(withId(R.id.strokes2)).perform(click());
        onView(withId(R.id.strokes1)).perform(click());

        onView(withId(R.id.strokes1)).check(matches(strokes(4)));
        onView(withId(R.id.strokes2)).check(matches(strokes(1)));

        onView(withId(R.id.ViewPager)).perform(swipeRight());
        waitView(withId(R.id.teamPoints1));
        touch(R.id.upperTeam, 100);
        touch(R.id.lowerTeam, 20);

        onView(allOf(withId(R.id.action_settings), isDisplayed())).perform(click());
        onData(withTitle(R.string.columns2ndBoard)).perform(click());
        onView(withText("4")).perform(click());
        pressBack();

        total[0] = 0;
        total[1] = 0;
        deletePoints(); // does not reset strokes

        onView(withId(R.id.ViewPager)).perform(swipeLeft());
        clickUntilGone(withId(R.id.deleteButton));
        onView(withText(android.R.string.cancel)).perform(click());
        onView(withId(R.id.strokes1)).perform(click());
        onView(withId(R.id.strokes1)).perform(click());
        onView(withId(R.id.strokes4)).perform(click());
        onView(withId(R.id.strokes2)).perform(click());
        onView(withId(R.id.strokes1)).perform(click());

        onView(allOf(withId(R.id.teamName2), isDisplayed())).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("Hans"), pressImeActionButton());
        onView(allOf(withId(R.id.teamName2), isDisplayed())).check(matches(withText("Hans")));

        onView(allOf(withId(R.id.teamName4), isDisplayed())).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("Ernst"), pressImeActionButton());
        onView(allOf(withId(R.id.teamName4), isDisplayed())).check(matches(withText("Ernst")));

        onView(withId(R.id.strokes1)).check(matches(strokes(7)));
        onView(withId(R.id.strokes2)).check(matches(strokes(2)));
        onView(withId(R.id.strokes3)).check(matches(strokes(0)));
        onView(withId(R.id.strokes4)).check(matches(strokes(1)));

        onView(withId(R.id.strokes1)).perform(touch(new float[]{0.1f, 0.5f}, new float[]{0.1f, 0.1f}));
        onView(withId(R.id.strokes1)).check(matches(strokes(6)));

        // TODO: on landscape tablet this performs a swipe of the whole view
        onView(withId(R.id.strokes1)).perform(touch(new float[]{0.1f, 0.5f}, new float[]{0.8f, 0.52f}));
        onView(withId(R.id.strokes1)).perform(touch(new float[]{0.8f, 0.5f}, new float[]{0.1f, 0.6f}));
        onView(withId(R.id.strokes1)).check(matches(strokes(6))); // no change (horizontal move)

        onView(withId(R.id.strokes1)).perform(touch(new float[]{0.1f, 0.1f}, new float[]{0.5f, 0.5f}));
        onView(withId(R.id.strokes1)).check(matches(strokes(5)));

        onView(allOf(withId(R.id.deleteButton), isDisplayed())).perform(click());
        onView(withText(android.R.string.ok)).perform(click());

        onView(withId(R.id.strokes1)).check(matches(strokes(0)));
        onView(withId(R.id.strokes2)).check(matches(strokes(0)));
        onView(withId(R.id.strokes3)).check(matches(strokes(0)));
        onView(withId(R.id.strokes4)).check(matches(strokes(0)));
    }

    @Test
    public void testDeleteTeamName() {
        onView(allOf(withId(R.id.startschieber), withText(R.string.schieber))).perform(click());
        onView(allOf(withId(R.id.teamName1), isDisplayed())).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("A & B"), pressImeActionButton());
        onView(allOf(withId(R.id.teamName2), isDisplayed())).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("C & D"), pressImeActionButton());
        onView(allOf(withId(R.id.teamName1), isDisplayed())).check(matches(withText("A & B")));
        onView(allOf(withId(R.id.teamName2), isDisplayed())).check(matches(withText("C & D")));

        onView(allOf(withId(R.id.teamName2), isDisplayed())).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText(""), pressImeActionButton());
        onView(allOf(withId(R.id.teamName1), isDisplayed())).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText(""), pressImeActionButton());
        onView(allOf(withId(R.id.teamName2), isDisplayed())).check(matches(withText(R.string.defteam2)));
        onView(allOf(withId(R.id.teamName1), isDisplayed())).check(matches(withText(R.string.defteam1)));
    }

    @Test
    public void testDialogButtons() {
        onView(allOf(withId(R.id.startschieber), withText(R.string.schieber))).perform(click());
        String tot = getInstrumentation().getTargetContext().getString(R.string.total);
        String opponent = getInstrumentation().getTargetContext().getString(R.string.opponent);

        addRound(R.id.lowerTeam, new int[]{R.id.button7, R.id.count2});
        onView(withId(R.id.points)).check(matches(withText("7")));
        String summary = tot + ": 14 (" + opponent + ": 300)";
        onView(withId(R.id.summary)).check(matches(withText(summary)));

        addRound(0, new int[]{R.id.buttonMatch});
        onView(withId(R.id.points)).check(matches(withText("257")));
        summary = tot + ": 514 (" + opponent + ": 0)";
        onView(withId(R.id.summary)).check(matches(withText(summary)));

        addRound(0, new int[]{R.id.buttonBackspace});
        onView(withId(R.id.points)).check(matches(withText("25")));
        summary = tot + ": 50 (" + opponent + ": 264)";
        onView(withId(R.id.summary)).check(matches(withText(summary)));

        addRound(0, new int[]{R.id.buttonNone, R.id.count1});
        onView(withId(R.id.points)).check(matches(withText("0")));
        summary = tot + ": 0 (" + opponent + ": 257)";
        onView(withId(R.id.summary)).check(matches(withText(summary)));

        addRound(0, new int[]{R.id.button0});
        onView(withId(R.id.points)).check(matches(withText("0")));
        summary = tot + ": 0 (" + opponent + ": 157)";
        onView(withId(R.id.summary)).check(matches(withText(summary)));

        addRound(0, new int[]{R.id.buttonEnter});
        total[0] = 157;
        checkPoints();

        addRound(R.id.lowerTeam, new int[]{R.id.count3, R.id.buttonNone, R.id.buttonEnter});
        total[0] += 3 * 257;
        checkPoints();
    }

    @Test
    public void testBigScore() {
        onView(allOf(withId(R.id.startschieber), withText(R.string.schieber))).perform(click());
        onView(withId(R.id.action_settings)).perform(click());
        onData(withTitle(R.string.bigScore)).perform(click());
        pressBack();

        addRound(R.id.lowerTeam, new int[]{R.id.button7, R.id.count2, R.id.buttonEnter});
        // onView(withId(R.id.strokes1)).perform(click());

        // test of bitmap not possible
    }

    @Test
    public void testMultiWindow() {
        onView(allOf(withId(R.id.startschieber), withText(R.string.schieber))).perform(click());
        touch(R.id.upperTeam, 100);
        touch(R.id.upperTeam, 100);
        touch(R.id.upperTeam, 50);
        touch(R.id.lowerTeam, 20);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            // multi window mode changed in Oreo, do not test it anymore
            enterMultiWindowMode();
        }
        touch(R.id.lowerTeam, 50);
        touch(R.id.lowerTeam, -1);

        onView(withId(R.id.teamPoints1)).check(matches(withText("250")));
        onView(withId(R.id.teamPoints2)).check(matches(withText("69")));

        onView(withId(R.id.info_undo)).perform(click());
        onView(withId(R.id.action_reset)).perform(click());
        onView(withText(R.string.currentround)).perform(click());

        onView(withId(R.id.teamPoints1)).check(matches(withText("0")));
        onView(withId(R.id.teamPoints2)).check(matches(withText("0")));
    }

    @Test
    public void testSchieberProfiles() {
        onView(allOf(withId(R.id.startschieber), withText(R.string.schieber))).perform(click());
        onView(withId(R.id.action_settings)).perform(click());
        onData(withTitle(R.string.profiles)).perform(click());
        onView(withText("Standard")).perform(click());
        pressBack();
        pressBack();
        onView(allOf(withId(R.id.teamName1), isDisplayed())).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("ABBA"), pressImeActionButton());
        onView(allOf(withId(R.id.teamName1), isDisplayed())).check(matches(withText("ABBA")));
        onView(allOf(withId(R.id.teamName2), isDisplayed())).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("ACDC"), pressImeActionButton());

        touch(R.id.upperTeam, 100);
        touch(R.id.upperTeam, 1);
        touch(R.id.upperTeam, 1);
        touch(R.id.upperTeam, 50);

        onView(withId(R.id.goal)).perform(click());
        onView(withId(R.id.inputField))
                .perform(replaceText("1800"), pressImeActionButton());
        onView(withId(R.id.goal)).check(matches(withText("1800")));

        onView(withId(R.id.action_settings)).perform(click());
        onData(withTitle(R.string.profiles)).perform(click());
        onView(withText("Standard")).check(matches(isDisplayed()));
        onView(withId(R.id.addButton)).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("Bieter / 3 Spieler"), pressImeActionButton());
        onView(withText("Bieter / 3 Spieler")).perform(click());
        pressBack();

        onData(withTitle(R.string.backside)).perform(click());
        onData(withTitle(R.string.columns2ndBoard)).perform(click());
        onView(withText("3")).perform(click());
        onData(withTitle(R.string.differentGoals)).perform(click());
        onData(withTitle(R.string.reset)).perform(click());
        onView(withText(R.string.resetall)).perform(click());

        onView(withId(R.id.goal1)).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("1000"), pressImeActionButton());
        onView(allOf(withId(R.id.teamName1), isDisplayed())).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("Gustav"), pressImeActionButton());
        onView(allOf(withId(R.id.teamName1), isDisplayed())).check(matches(withText("Gustav")));

        addRound(R.id.lowerTeam, new int[]{R.id.button7, R.id.button2, R.id.count2, R.id.buttonEnter});
        addRound(R.id.lowerTeam, new int[]{R.id.button2, R.id.button2, R.id.countX, R.id.buttonEnter});

        onView(withId(R.id.ViewPager)).perform(swipeLeft());
        onView(allOf(withId(R.id.deleteButton), isDisplayed())).perform(click());
        try {
            onView(withText(android.R.string.ok)).perform(click());
            fail();
        } catch (NoMatchingViewException e) {
            // already deleted
        }
        onView(withId(R.id.strokes3)).perform(click());
        onView(withId(R.id.strokes3)).perform(click());
        onView(withId(R.id.strokes3)).perform(click());
        onView(allOf(withId(R.id.teamName3), isDisplayed())).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("3. Spieler"), pressImeActionButton());
        onView(withId(R.id.strokes1)).perform(click());
        onView(withId(R.id.strokes2)).perform(click());

        onView(withId(R.id.strokes1)).check(matches(strokes(1)));
        onView(withId(R.id.strokes2)).check(matches(strokes(1)));
        onView(withId(R.id.strokes3)).check(matches(strokes(3)));
        onView(withId(R.id.ViewPager)).perform(swipeRight());

        clickUntilGone(withId(R.id.action_settings));
        onData(withTitle(R.string.profiles)).perform(click());
        onView(withText("Standard")).perform(click());
        pressBack();
        onView(settingsCheckbox(R.string.backside)).check(matches(isNotChecked()));
        onView(settingsCheckbox(R.string.differentGoals)).check(matches(isNotChecked()));
        onData(withTitle(R.string.columns2ndBoard)).check(matches(not(isEnabled())));
        pressBack();

        onView(withId(R.id.teamPoints1)).check(matches(withText("152")));
        onView(withId(R.id.teamPoints2)).check(matches(withText("0")));
        onView(withId(R.id.ViewPager)).perform(swipeLeft());
        onView(allOf(withId(R.id.teamName1), isDisplayed())).check(matches(withText("ABBA")));
        onView(allOf(withId(R.id.teamName2), isDisplayed())).check(matches(withText("ACDC")));

        onView(allOf(withId(R.id.action_settings), isDisplayed())).perform(click());
        onData(withTitle(R.string.profiles)).perform(click());
        onView(withText("Bieter / 3 Spieler")).perform(click());
        pressBack();
        onView(settingsCheckbox(R.string.backside)).check(matches(isChecked()));
        onView(settingsCheckbox(R.string.differentGoals)).check(matches(isChecked()));
        onData(withTitle(R.string.columns2ndBoard)).check(matches(isEnabled()));
        pressBack();
        onView(withId(R.id.teamPoints1)).check(matches(withText("710")));
        onView(withId(R.id.teamPoints2)).check(matches(withText("232")));
        onView(allOf(withId(R.id.teamName1), isDisplayed())).check(matches(withText("Gustav")));
        onView(withId(R.id.ViewPager)).perform(swipeLeft());
        onView(withId(R.id.strokes1)).check(matches(strokes(1)));
        onView(withId(R.id.strokes2)).check(matches(strokes(1)));
        onView(withId(R.id.strokes3)).check(matches(strokes(3)));
        onView(allOf(withId(R.id.teamName3), isDisplayed())).check(matches(withText("3. Spieler")));
    }

    @Test
    public void testStatsDialogs() {
        onView(allOf(withId(R.id.startschieber), withText(R.string.schieber))).perform(click());
        touch(R.id.lowerTeam, 50);
        touch(R.id.upperTeam, 20);
        touch(R.id.upperTeam, 20);
        touch(R.id.upperTeam, -1);
        touch(R.id.lowerTeam, 100);
        touch(R.id.lowerTeam, 100);
        touch(R.id.lowerTeam, 20);

        onView(withId(R.id.info_undo)).perform(click());
        onView(withId(R.id.action_statistics)).perform(click());
        onView(withId(R.id.action_info)).perform(click());
        pressBack();

        onView(withId(R.id.action_settings)).perform(click());
        onData(withTitle(R.string.showstats)).perform(click());
        onView(withId(R.id.action_info)).perform(click());
        onView(withId(R.id.action_statistics)).perform(click());
    }

    @Test
    public void testNegativePoints() {
        onView(allOf(withId(R.id.startschieber), withText(R.string.schieber))).perform(click());
        addRound(R.id.lowerTeam, new int[]{R.id.button9, R.id.button2, R.id.count2, R.id.buttonEnter});
        total[0] += 2 * (157 - 92);
        total[1] += 2 * 92;
        checkPoints();

        addRound(R.id.lowerTeam, new int[]{R.id.button1, R.id.buttonPlusMinus, R.id.button0, R.id.count3, R.id.buttonEnter});
        total[1] -= 3 * 10;
        checkPoints();

        addRound(R.id.lowerTeam, new int[]{R.id.buttonPlusMinus, R.id.button1, R.id.button0, R.id.button0, R.id.count2, R.id.buttonEnter});
        total[1] -= 2 * 100;
        checkPoints();
    }

    @Test
    public void testRotateDevice() {
        onView(allOf(withId(R.id.startschieber), withText(R.string.schieber))).perform(click());
        onView(withId(R.id.action_settings)).perform(click());

        onView(isRoot()).perform(OrientationChangeAction.orientationLandscape());
        onData(withTitle(R.string.showstats)).perform(click());
    }

    @Test
    public void testRoundGoal() {
        onView(allOf(withId(R.id.startschieber), withText(R.string.schieber))).perform(click());
        onView(withId(R.id.action_settings)).perform(click());
        onData(withTitle(R.string.goal)).perform(click());
        onView(withText(R.string.rounds)).perform(click());
        pressBack();

        onView(withId(R.id.goal)).perform(click());
        onView(withId(R.id.inputField))
                .perform(replaceText("4"), pressImeActionButton());

        onView(withId(R.id.goal)).check(matches(withText(getInstrumentation().getTargetContext().getResources()
                .getQuantityString(R.plurals.goalrounds, 4, 4, 0))));

        addRound(R.id.lowerTeam, new int[]{R.id.button6, R.id.button4, R.id.count2, R.id.buttonEnter});

        touch(R.id.upperTeam, 20);
        touch(R.id.upperTeam, 100);

        upDnClickAction = true;
        addRound(R.id.upperTeam, new int[]{R.id.buttonMatch, R.id.count3, R.id.buttonEnter});
        upDnClickAction = false;
        onView(withId(R.id.goal)).check(matches(withText(getInstrumentation().getTargetContext().getResources()
                .getQuantityString(R.plurals.goalrounds, 4, 4, 2))));

        touch(R.id.upperTeam, 100);
        touch(R.id.upperTeam, 100);
        touch(R.id.upperTeam, 1);
        touch(R.id.lowerTeam, 100);
        touch(R.id.lowerTeam, 100);
        touch(R.id.lowerTeam, 50);
        touch(R.id.lowerTeam, 20);

        touch(R.id.lowerTeam, 50);
        onView(withId(R.id.goal)).check(matches(withText(getInstrumentation().getTargetContext().getResources()
                .getQuantityString(R.plurals.goalrounds, 4, 4, 3))));

        addRound(R.id.lowerTeam, new int[]{R.id.button3, R.id.button1, R.id.count1, R.id.buttonEnter});

        onView(withId(android.R.id.message)).check(matches(withText(String.format(getInstrumentation().getTargetContext()
                .getString(R.string.winnerrounds), getInstrumentation().getTargetContext().getString(R.string.defteam1)))));
        onView(withText(android.R.string.ok)).perform(click());
    }
}
