package ch.simonste.jasstafel;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class GuessPlayerNamesTest {

    @Test
    public final void test_2Teams_Spaces() {
        String[] teams = {"Max Moritz", "Peter Paul"};
        String[] players = Whobegins.guessPlayerNames(teams);

        assertEquals(4, players.length);
        assertEquals("Max", players[0]);
        assertEquals("Peter", players[1]);
        assertEquals("Moritz", players[2]);
        assertEquals("Paul", players[3]);
    }

    @Test
    public final void test_3Teams_Spaces() {
        String[] teams = {"Max Moritz", "Peter Paul", "Bonnie Clyde"};
        String[] players = Whobegins.guessPlayerNames(teams);

        assertEquals(6, players.length);
        assertEquals("Max", players[0]);
        assertEquals("Peter", players[1]);
        assertEquals("Bonnie", players[2]);
        assertEquals("Moritz", players[3]);
        assertEquals("Paul", players[4]);
        assertEquals("Clyde", players[5]);
    }

    @Test
    public final void test_2Teams_Ampersand() {
        String[] teams = {"Max & Moritz", "Peter&Paul"};
        String[] players = Whobegins.guessPlayerNames(teams);

        assertEquals(4, players.length);
        assertEquals("Max", players[0]);
        assertEquals("Peter", players[1]);
        assertEquals("Moritz", players[2]);
        assertEquals("Paul", players[3]);
    }

    @Test
    public final void test_2Teams_Minus() {
        String[] teams = {"Max-Moritz", "Peter - Paul"};
        String[] players = Whobegins.guessPlayerNames(teams);

        assertEquals(4, players.length);
        assertEquals("Max", players[0]);
        assertEquals("Peter", players[1]);
        assertEquals("Moritz", players[2]);
        assertEquals("Paul", players[3]);
    }


    @Test
    public final void test_2Teams_Slash() {
        String[] teams = {"Max/Moritz", "Peter / Paul"};
        String[] players = Whobegins.guessPlayerNames(teams);

        assertEquals(4, players.length);
        assertEquals("Max", players[0]);
        assertEquals("Peter", players[1]);
        assertEquals("Moritz", players[2]);
        assertEquals("Paul", players[3]);
    }

    @Test
    public final void test_2Teams_uni() {
        String[] teams = {"Müller", "Meier"};
        String[] players = Whobegins.guessPlayerNames(teams);

        assertEquals(4, players.length);
        assertEquals("Müller 1", players[0]);
        assertEquals("Meier 1", players[1]);
        assertEquals("Müller 2", players[2]);
        assertEquals("Meier 2", players[3]);
    }

    @Test
    public final void test_2Teams_teamNum() {
        String[] teams = {"Team 89", "Team 88"};
        String[] players = Whobegins.guessPlayerNames(teams);

        assertEquals(4, players.length);
        assertEquals("Team 89a", players[0]);
        assertEquals("Team 88a", players[1]);
        assertEquals("Team 89b", players[2]);
        assertEquals("Team 88b", players[3]);
    }

    @Test
    public final void test_2Teams_team() {
        String[] teams = {"Team Blau", "Team Rot"};
        String[] players = Whobegins.guessPlayerNames(teams);

        assertEquals(4, players.length);
        assertEquals("Team Blau 1", players[0]);
        assertEquals("Team Rot 1", players[1]);
        assertEquals("Team Blau 2", players[2]);
        assertEquals("Team Rot 2", players[3]);
    }

    @Test
    public final void test_2Teams_SpacesAround() {
        String[] teams = {" Max Moritz", "Peter Paul "};
        String[] players = Whobegins.guessPlayerNames(teams);

        assertEquals(4, players.length);
        assertEquals("Max", players[0]);
        assertEquals("Peter", players[1]);
        assertEquals("Moritz", players[2]);
        assertEquals("Paul", players[3]);
    }

    @Test
    public final void test_2Teams_DoubleNames() {
        String[] teams = {"Max A - Moritz B", "Peter S & Paul Q"};
        String[] players = Whobegins.guessPlayerNames(teams);

        assertEquals(4, players.length);
        assertEquals("Max A", players[0]);
        assertEquals("Peter S", players[1]);
        assertEquals("Moritz B", players[2]);
        assertEquals("Paul Q", players[3]);
    }

    @Test
    public final void test_2Teams_With3Players() {
        String[] teams = {"Max Moritz Marc", "Peter Paul Pius"};
        String[] players = Whobegins.guessPlayerNames(teams);

        assertEquals(6, players.length);
        assertEquals("Max", players[0]);
        assertEquals("Peter", players[1]);
        assertEquals("Moritz", players[2]);
        assertEquals("Paul", players[3]);
        assertEquals("Marc", players[4]);
        assertEquals("Pius", players[5]);
    }

    @Test
    public final void test_2Teams_WithDiffNPlayers() {
        String[] teams = {"Max S Moritz", "Peter Paul"};
        String[] players = Whobegins.guessPlayerNames(teams);

        assertEquals(4, players.length);
        assertEquals("Max", players[0]);
        assertEquals("Peter", players[1]);
        assertEquals("S Moritz", players[2]);
        assertEquals("Paul", players[3]);
    }

    @Test
    public final void test_badNames() {
        String[] teams = {"---", "&&&"};
        String[] players = Whobegins.guessPlayerNames(teams);

        assertEquals(4, players.length);
        assertEquals("--- 1", players[0]);
        assertEquals("&&& 1", players[1]);
        assertEquals("--- 2", players[2]);
        assertEquals("&&& 2", players[3]);
    }

    @Test
    public final void test_NoNames() {
        String[] teams = {"", ""};
        String[] players = Whobegins.guessPlayerNames(teams);

        assertEquals(4, players.length);
        assertEquals("P1", players[0]);
        assertEquals("P1", players[1]);
        assertEquals("P2", players[2]);
        assertEquals("P2", players[3]);
    }

    @Test
    public final void test_different_separation() {
        String[] teams = {"Al i-Bro", "X Y Z"};
        String[] players = Whobegins.guessPlayerNames(teams);

        assertEquals(4, players.length);
        assertEquals("Al i", players[0]);
        assertEquals("X", players[1]);
        assertEquals("Bro", players[2]);
        assertEquals("Y Z", players[3]);
    }

    @Test
    public final void test_3Names() {
        String[] teams = {"Al Bro C", "X Y Z"};
        String[] players = Whobegins.guessPlayerNames(teams);

        assertEquals(6, players.length);
        assertEquals("Al", players[0]);
        assertEquals("X", players[1]);
        assertEquals("Bro", players[2]);
        assertEquals("Y", players[3]);
        assertEquals("C", players[4]);
        assertEquals("Z", players[5]);
    }

    @Test
    public final void test_3Names_spaces() {
        String[] teams = {"Al & Bro + C", "X - Y - Z"};
        String[] players = Whobegins.guessPlayerNames(teams);

        assertEquals(6, players.length);
        assertEquals("Al", players[0]);
        assertEquals("X", players[1]);
        assertEquals("Bro", players[2]);
        assertEquals("Y", players[3]);
        assertEquals("C", players[4]);
        assertEquals("Z", players[5]);
    }

    @Test
    public final void test_2Names_spaces() {
        String[] teams = {"Al B - Bro", "Al X - Z"};
        String[] players = Whobegins.guessPlayerNames(teams);

        assertEquals(4, players.length);
        assertEquals("Al B", players[0]);
        assertEquals("Al X", players[1]);
        assertEquals("Bro", players[2]);
        assertEquals("Z", players[3]);
    }
}