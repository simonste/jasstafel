package ch.simonste.jasstafel.guggitaler;

import org.junit.Before;
import org.junit.Test;

import ch.simonste.jasstafel.ListFragmentUITest;
import ch.simonste.jasstafel.R;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.longClick;
import static androidx.test.espresso.action.ViewActions.swipeUp;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

public class GuggitalerUITest extends ListFragmentUITest {

    private void addPoints(int plId, int stiche, int schellen, int ober, int rosenkoenig, int letzter) {
        onView(allOf(withId(R.id.addButton), withParent(withId(R.id.FABcontainter)), isDisplayed())).perform(click());
        onView(allOf(childAtPosition(withId(R.id.toolbar), 0))).check(matches(withText(R.string.guggitaler)));

        editPoints(plId, stiche, schellen, ober, rosenkoenig, letzter);
    }

    private void editPoints(int plId, int stiche, int schellen, int ober, int rosenkoenig, int letzter) {
        onView(allOf(withId(plId), isDisplayed())).perform(swipeUp()); // TODO: why does click not work

        boolean negative = false;
        if (Math.min(stiche, Math.min(schellen, Math.min(ober, Math.min(rosenkoenig, letzter)))) < 0) {
            negative = true;
            onView(withId(R.id.action_negative)).perform(click());
        }
        onView(withId(R.id.stiche)).perform(setNumber(Math.abs(stiche)));
        onView(withId(R.id.schellen)).perform(setNumber(Math.abs(schellen)));
        onView(withId(R.id.ober)).perform(setNumber(Math.abs(ober)));
        onView(withId(R.id.rosenkoenig)).perform(setNumber(Math.abs(rosenkoenig)));
        onView(withId(R.id.laststich)).perform(setNumber(Math.abs(letzter)));

        GuggitalerPoints pts = new GuggitalerPoints();
        pts.negative = negative;
        pts.stiche = stiche;
        pts.schellen = schellen;
        pts.ober = ober;
        pts.rosenkoenig = rosenkoenig;
        pts.laststich = letzter;
        final int playerNo = GuggitalerDialog.getPlayerNoFromResource(plId);
        totals[playerNo] += (negative ? -1 : 1) * pts.sum();

        onView(withId(R.id.ok)).perform(click());
    }

    @Before
    public void initPreferences() {
        super.initPreferences("Guggitaler");
    }

    @Test
    public void testGuggitalerGame() {
        onView(allOf(withId(R.id.startguggitaler), withText(R.string.guggitaler))).perform(click());

        addPoints(R.id.player2, 1, 0, 0, 0, 0);

        onView(withId(R.id.total1)).perform(click());
        pressBack();

        addPoints(R.id.player1, 3, 0, 0, 0, 0);
        onView(allOf(withParent(withParent(childAtPosition(withId(R.id.list), 0))), withId(R.id.roundNo))).check(matches(withText("")));
        addPoints(R.id.player3, 5, 0, 0, 0, 0);
        onView(allOf(withParent(withParent(childAtPosition(withId(R.id.list), 0))), withId(R.id.roundNo))).check(matches(withText("1")));

        addPoints(R.id.player3, 0, 2, 0, 0, 0);
        checkTotal();
        addPoints(R.id.player4, 0, 5, 0, 0, 0);
        addPoints(R.id.player3, 0, 2, 0, 0, 0);

        checkTotal();
        addPoints(R.id.player2, 0, 0, 2, 0, 0);
        addPoints(R.id.player2, 0, 0, 2, 0, 0);
        addPoints(R.id.player1, 0, 0, 0, 1, 0);
        onView(allOf(withParent(withParent(childAtPosition(withId(R.id.list), 5))), withId(R.id.roundNo))).check(matches(withText("4")));
        addPoints(R.id.player4, 0, 0, 0, 0, 1);
        checkTotal();

        addPoints(R.id.player1, 1, 1, 0, 1, 0);
        addPoints(R.id.player2, 1, 0, 0, 0, 0);
        addPoints(R.id.player1, 1, 2, 0, 0, 0);
        addPoints(R.id.player4, 1, 0, 2, 0, 0);
        addPoints(R.id.player3, 1, 0, 0, 0, 0);
        addPoints(R.id.player3, 1, 3, 2, 0, 0);
        addPoints(R.id.player3, 1, 0, 0, 0, 0);
        addPoints(R.id.player1, 1, 2, 0, 0, 0);
        addPoints(R.id.player4, 1, 1, 0, 0, 1);
        onView(allOf(withParent(withParent(childAtPosition(withId(R.id.list), 10))), withId(R.id.roundNo))).check(matches(withText("6")));
        checkTotal();

        pressBack();
        onView(allOf(withId(R.id.startguggitaler), withText(R.string.guggitaler))).perform(click());
        onView(allOf(withParent(withParent(childAtPosition(withId(R.id.list), 9))), withId(R.id.roundNo))).check(matches(withText("")));
        onView(allOf(withParent(withParent(childAtPosition(withId(R.id.list), 10))), withId(R.id.roundNo))).check(matches(withText("6")));
        checkTotal();

        onView(withId(R.id.footer)).perform(click());
        onView(allOf(withParent(withParent(withId(R.id.row1))), withId(R.id.valA))).check(matches(withText("6")));
        onView(allOf(withParent(withParent(withId(R.id.row2))), withId(R.id.valB))).check(matches(withText("0")));
        onView(allOf(withParent(withParent(withId(R.id.row3))), withId(R.id.valB))).check(matches(withText("7")));
        onView(allOf(withParent(withParent(withId(R.id.row4))), withId(R.id.valE))).check(matches(withText("2")));

        pressBack();
        //deletePoints();
        checkTotal();
    }

    @Test
    public void testGuggitalerNegativeGame() {
        onView(allOf(withId(R.id.startguggitaler), withText(R.string.guggitaler))).perform(click());

        addPoints(R.id.player2, -1, 0, 0, 0, 0);
        addPoints(R.id.player1, -3, 0, 0, 0, 0);
        onView(allOf(withParent(withParent(childAtPosition(withId(R.id.list), 0))), withId(R.id.roundNo))).check(matches(withText("")));
        addPoints(R.id.player3, -5, 0, 0, 0, 0);
        onView(allOf(withParent(withParent(childAtPosition(withId(R.id.list), 0))), withId(R.id.roundNo))).check(matches(withText("1")));

        addPoints(R.id.player3, 0, -7, 0, 0, 0);
        checkTotal();
        addPoints(R.id.player4, 0, -2, 0, 0, 0);
    }

    @Test(timeout = 60000)
    public void testGuggitalerEdit() {
        onView(allOf(withId(R.id.startguggitaler), withText(R.string.guggitaler))).perform(click());

        addPoints(R.id.player2, 1, 0, 0, 0, 0);
        addPoints(R.id.player1, 3, 0, 0, 0, 0);
        onView(allOf(withParent(withParent(childAtPosition(withId(R.id.list), 0))), withId(R.id.roundNo))).check(matches(withText("")));
        addPoints(R.id.player3, 5, 0, 0, 0, 0);
        onView(allOf(withParent(withParent(childAtPosition(withId(R.id.list), 0))), withId(R.id.roundNo))).check(matches(withText("1")));

        addPoints(R.id.player3, 1, 0, 2, 0, 0);
        // TODO: fails on landscape
        onView(childAtPosition(withId(R.id.list), 1)).perform(click()); // nothing happens
        onView(childAtPosition(withId(R.id.list), 1)).perform(longClick());
        totals[2] -= 45;
        editPoints(R.id.player3, 1, 1, 2, 0, 0);
        checkTotal();
        addPoints(R.id.player3, 1, 0, 0, 0, 0);
        addPoints(R.id.player4, 1, 0, 0, 1, 1);
        onView(allOf(withParent(withParent(childAtPosition(withId(R.id.list), 2))), withId(R.id.roundNo))).check(matches(withText("2")));
        checkTotal();

        // undo
        addPoints(R.id.player1, 0, 0, 1, 0, 0);
        onView(withId(R.id.undoAction)).perform(click());
        totals[0] -= 20;
        checkTotal();

        // switch to negative
        onView(childAtPosition(withId(R.id.list), 0)).perform(longClick());
        totals[1] -= 5;
        editPoints(R.id.player2, -1, 0, 0, 0, 0);
        onView(allOf(withParent(withParent(childAtPosition(withId(R.id.list), 0))), withId(R.id.roundNo))).check(matches(withText("")));
        onView(childAtPosition(withId(R.id.list), 0)).perform(longClick());
        totals[0] -= 15;
        editPoints(R.id.player1, -3, 0, 0, 0, 0);
        onView(childAtPosition(withId(R.id.list), 0)).perform(longClick());
        totals[2] -= 25;
        editPoints(R.id.player3, -5, 0, 0, 0, 0);
        onView(allOf(withParent(withParent(childAtPosition(withId(R.id.list), 0))), withId(R.id.roundNo))).check(matches(withText("1")));
        checkTotal();

        deletePoints();
        checkTotal();
    }
}
