package ch.simonste.jasstafel.differenzler;


import androidx.test.espresso.NoMatchingViewException;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import java.util.LinkedHashMap;

import ch.simonste.jasstafel.ListFragmentUITest;
import ch.simonste.jasstafel.OrientationChangeAction;
import ch.simonste.jasstafel.R;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.longClick;
import static androidx.test.espresso.action.ViewActions.pressImeActionButton;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.PreferenceMatchers.withTitle;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.isRoot;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withResourceName;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.not;

public class DifferenzlerUITest extends ListFragmentUITest {

    private void addGuess(int plId, int pts) {
        onView(allOf(withParent(allOf(withId(plId), withParent(withId(R.id.guess_actions))))))
                .perform(click());
        onView(withId(R.id.inputField)).perform(replaceText(Integer.toString(pts)), pressImeActionButton());
    }

    @Before
    public void initPreferences() {
        super.initPreferences("Differenzler");
    }

    @Test
    public void testDifferenzler() {
        onView(allOf(withId(R.id.startdifferenzler), withText(R.string.differenzler))).perform(click());
        onView(withId(R.id.footer)).perform(click());
        String text = String.format(getInstrumentation().getTargetContext().getString(R.string.avgRoundGuess), "0.0");
        onView(allOf(withId(R.id.text), isDisplayed())).check(matches(withText(text)));
        pressBack();

        onView(allOf(withParent(withParent(withParent(withId(R.id.p2)))), withId(R.id.guess))).check(matches(withText("")));
        addGuess(R.id.guess_action2, 55);
        onView(allOf(withParent(withParent(withParent(withId(R.id.p2)))), withId(R.id.guess))).check(matches(withText("*")));
        onView(allOf(withParent(withParent(withParent(withId(R.id.p1)))), withId(R.id.guess))).check(matches(withText("")));
        addGuess(R.id.guess_action1, 12);
        onView(allOf(withParent(withParent(withParent(withId(R.id.p1)))), withId(R.id.guess))).check(matches(withText("*")));
        addGuess(R.id.guess_action3, 0);
        addGuess(R.id.guess_action4, 62);

        LinkedHashMap<Integer, Integer> map = new LinkedHashMap<>();
        map.put(R.id.p2, 34);
        map.put(R.id.p1, 44);
        map.put(R.id.p4, 0);
        map.put(R.id.p3, null);
        addRound(map);
        totals[0] += 44 - 12;
        totals[1] += 55 - 34;
        totals[2] += 79;
        totals[3] += 62;

        checkTotal();

        setWhoIsNext(1, R.id.r2c1, 0);

        addGuess(R.id.guess_action4, 0);
        addGuess(R.id.guess_action1, 57);
        addGuess(R.id.guess_action3, 66);

        // TODO: fails on landscape
        onView(childAtPosition(withId(R.id.list), 0)).perform(longClick());
        onView(allOf(withId(R.id.edit), withText("79"), withParent(withId(R.id.p3))))
                .perform(replaceText("0"));
        onView(allOf(withId(R.id.edit), withText("0"), withParent(withId(R.id.p4))))
                .perform(replaceText("79"));
        onView(allOf(withId(R.id.button), withText(android.R.string.ok), isDisplayed())).perform(closeSoftKeyboard(), click());
        totals[2] -= 79;
        totals[3] -= 62;
        totals[3] += 79 - 62;
        checkTotal();

        pressBack();
        onView(allOf(withId(R.id.startdifferenzler), withText(R.string.differenzler))).perform(click());
        checkTotal();

        onView(childAtPosition(withId(R.id.list), 0)).perform(click());
        text = String.format(getInstrumentation().getTargetContext().getString(R.string.roundsummary), 1);
        onView(withResourceName("alertTitle")).check(matches(withText(text)));
        text = String.format(getInstrumentation().getTargetContext().getString(R.string.totalguessed), 129);
        text += "\n\n";
        text += String.format(getInstrumentation().getTargetContext().getString(R.string.totalpoints), 157);
        onView(withId(android.R.id.message)).check(matches(withText(text)));
    }

    @Test
    public void testDifferenzlerShowGuess() {
        onView(allOf(withId(R.id.startdifferenzler), withText(R.string.differenzler))).perform(click());
        onView(allOf(withId(R.id.action_settings), withContentDescription(R.string.settings), isDisplayed())).perform(click());
        onData(withTitle(R.string.hideGuess)).perform(click());
        pressBack();

        addGuess(R.id.guess_action2, 55);
        onView(allOf(withParent(withParent(withParent(withId(R.id.p2)))), withId(R.id.guess))).check(matches(withText("55")));
        onView(allOf(withParent(withParent(withParent(withId(R.id.p1)))), withId(R.id.guess))).check(matches(withText("")));
        addGuess(R.id.guess_action1, 12);
        onView(allOf(withParent(withParent(withParent(withId(R.id.p1)))), withId(R.id.guess))).check(matches(withText("12")));
    }

    @Test
    public void testDifferenzlerEditGuess() {
        onView(allOf(withId(R.id.startdifferenzler), withText(R.string.differenzler))).perform(click());
        onView(allOf(withId(R.id.action_settings), withContentDescription(R.string.settings), isDisplayed())).perform(click());
        onData(withTitle(R.string.hideGuess)).perform(click());

        onData(withTitle(R.string.diff_players)).perform(click());
        onView(withText("2")).perform(click());
        pressBack();

        addGuess(R.id.guess_action2, 55);
        onView(allOf(withParent(withParent(withParent(withId(R.id.p2)))), withId(R.id.guess))).check(matches(withText("55")));
        onView(allOf(withParent(withParent(withParent(withId(R.id.p1)))), withId(R.id.guess))).check(matches(withText("")));
        addGuess(R.id.guess_action1, 72);
        onView(allOf(withParent(withParent(withParent(withId(R.id.p1)))), withId(R.id.guess))).check(matches(withText("72")));
        onView(allOf(withParent(withParent(childAtPosition(withId(R.id.list), 0))), withId(R.id.roundNo))).check(matches(withText("1")));

        LinkedHashMap<Integer, Integer> map = new LinkedHashMap<>();
        map.put(R.id.p2, 88);
        map.put(R.id.p1, null);
        addRound(map);
        onView(withId(R.id.addButton)).check(matches(not(isDisplayed())));
        onView(allOf(withParent(withParent(childAtPosition(withId(R.id.list), 1))), withId(R.id.roundNo))).check(matches(withText("2")));

        // TODO: fails on landscape
        onView(childAtPosition(withId(R.id.list), 0)).perform(longClick());
        onView(withId(android.R.id.title)).check(matches(withText("Runde 1 bearbeiten")));
        pressBack();
        pressBack();

        onView(allOf(withId(R.id.action_settings), withContentDescription(R.string.settings), isDisplayed())).perform(click());
        onData(withTitle(R.string.hideGuess)).perform(click());
        pressBack();

        addGuess(R.id.guess_action2, 10);
        onView(childAtPosition(withId(R.id.list), 1)).perform(longClick());
        try {
            onView(withId(android.R.id.title)).check(matches(isDisplayed()));
            Assert.fail("View should not be displayed");
        } catch (NoMatchingViewException e) {
            // fine
        }

        onView(allOf(withId(R.id.action_settings), withContentDescription(R.string.settings), isDisplayed())).perform(click());
        onData(withTitle(R.string.hideGuess)).perform(click());
        pressBack();

        onView(allOf(withParent(withParent(withParent(withId(R.id.p2)))), withId(R.id.guess), withText("10"))).check(matches(isDisplayed()));
        onView(withId(R.id.guess_action1)).check(matches(isDisplayed()));
        onView(childAtPosition(withId(R.id.list), 1)).perform(longClick());
        onView(withId(android.R.id.title)).check(matches(withText(R.string.guessedit)));
        onView(allOf(withId(R.id.edit), withText("10"), withParent(withId(R.id.p2)))).perform(replaceText("16"));
        onView(allOf(withId(R.id.edit), withText(""), withParent(withId(R.id.p1)))).perform(replaceText("99"));
        onView(allOf(withId(R.id.button), withText(android.R.string.ok), isDisplayed())).perform(closeSoftKeyboard(), click());
        onView(allOf(withParent(withParent(withParent(withId(R.id.p1)))), withId(R.id.guess), withText("99"))).check(matches(isDisplayed()));
        onView(allOf(withParent(withParent(withParent(withId(R.id.p2)))), withId(R.id.guess), withText("16"))).check(matches(isDisplayed()));
        onView(withId(R.id.guess_action1)).check(matches(not(isDisplayed())));
        onView(withId(R.id.addButton)).check(matches(isDisplayed()));
        pressBack();
        onView(allOf(withId(R.id.startdifferenzler), withText(R.string.differenzler))).perform(click());
        onView(withId(R.id.guess_action1)).check(matches(not(isDisplayed())));
        onView(withId(R.id.addButton)).check(matches(isDisplayed()));
        onView(childAtPosition(withId(R.id.list), 1)).perform(longClick());
        onView(allOf(withId(R.id.edit), withText("16"), withParent(withId(R.id.p2)))).perform(replaceText(""));
        onView(allOf(withId(R.id.button), withText(android.R.string.ok), isDisplayed())).perform(closeSoftKeyboard(), click());
        onView(allOf(withParent(withParent(withParent(withId(R.id.p2)))), withId(R.id.guess), withText(""))).check(matches(isDisplayed()));
        onView(withId(R.id.guess_action2)).check(matches(isDisplayed()));
        onView(withId(R.id.addButton)).check(matches(not(isDisplayed())));
        pressBack();
        onView(allOf(withId(R.id.startdifferenzler), withText(R.string.differenzler))).perform(click());
        onView(allOf(withParent(withParent(withParent(withId(R.id.p2)))), withId(R.id.guess), withText(""))).check(matches(isDisplayed()));
    }

    @Test
    public void testDifferenzlerProfiles() {
        onView(allOf(withId(R.id.startdifferenzler), withText(R.string.differenzler))).perform(click());
        onView(allOf(withId(R.id.action_settings), withContentDescription(R.string.settings), isDisplayed())).perform(click());
        onData(withTitle(R.string.profiles)).perform(click());
        pressBack();
        pressBack();
    }

    @Test
    public void testRotateDevice() {
        onView(allOf(withId(R.id.startdifferenzler), withText(R.string.differenzler))).perform(click());

        onView(withId(R.id.player2)).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("Spieler xy"));
        onView(isRoot()).perform(OrientationChangeAction.orientationLandscape());
        onView(withId(R.id.inputField)).check(matches(withText("Spieler xy")));
        onView(withId(R.id.inputField)).perform(pressImeActionButton());
        onView(withId(R.id.player2)).check(matches(withText("Spieler xy")));
    }
}