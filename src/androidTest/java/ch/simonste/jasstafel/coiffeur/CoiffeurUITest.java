package ch.simonste.jasstafel.coiffeur;


import android.graphics.Color;
import android.os.SystemClock;
import android.view.View;
import android.widget.NumberPicker;

import androidx.test.espresso.NoMatchingViewException;
import androidx.test.espresso.UiController;
import androidx.test.espresso.ViewAction;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.filters.FlakyTest;

import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;

import java.util.Locale;

import ch.simonste.jasstafel.OrientationChangeAction;
import ch.simonste.jasstafel.R;
import ch.simonste.jasstafel.RoundDuration;
import ch.simonste.jasstafel.UITest;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.longClick;
import static androidx.test.espresso.action.ViewActions.pressImeActionButton;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.scrollTo;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.PreferenceMatchers.withSummaryText;
import static androidx.test.espresso.matcher.PreferenceMatchers.withTitle;
import static androidx.test.espresso.matcher.ViewMatchers.isChecked;
import static androidx.test.espresso.matcher.ViewMatchers.isClickable;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.isRoot;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withResourceName;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.hamcrest.Matchers.allOf;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class CoiffeurUITest extends UITest {
    private final static int header = 0;
    private final static int footer = 13;
    private final int[] total = new int[3];


    public static ViewAction setFactor(final int num) {
        return new ViewAction() {
            @Override
            public void perform(UiController uiController, View view) {
                ((NumberPicker) view).setValue(num);
            }

            @Override
            public String getDescription() {
                return "Set the passed number into the NumberPicker";
            }

            @Override
            public Matcher<View> getConstraints() {
                return ViewMatchers.isAssignableFrom(NumberPicker.class);
            }
        };
    }

    @Before
    public void initPreferences() {
        super.initPreferences("Coiffeur");
    }

    private Matcher<View> viewInRow(int teamId, int rowNo) {
        return allOf(withId(teamId), withParent(withParent(childAtPosition(withResourceName("fragment"), rowNo))));
    }

    private Matcher<View> subViewInRow(int teamId, int rowNo) {
        return allOf(withId(teamId), withParent(withParent(withParent(childAtPosition(withResourceName("fragment"), rowNo)))));
    }

    private void checkPoints() {
        onView(viewInRow(R.id.team1, footer)).check(matches(withText(startsWith(Integer.toString(total[0])))));
        onView(viewInRow(R.id.team2, footer)).check(matches(withText(startsWith(Integer.toString(total[1])))));
        onView(viewInRow(R.id.team3, footer)).check(matches(withText(startsWith(Integer.toString(total[2])))));
    }

    private int getTimeSince(long startTime) {
        return (int) (System.currentTimeMillis() / 1000 - startTime);
    }

    private void checkRoundCounter(int roundNo, long duration) {
        String text = getInstrumentation().getTargetContext().getResources().getQuantityString(R.plurals.rounds, roundNo, roundNo);
        final String twText = getText(withId(R.id.roundNumber));

        boolean matched = false;
        if (duration >= 0) {
            for (int offset = -1; offset <= 1; offset++) {
                String t = text + " - " + (duration + offset) + "\"";
                matched = matched || twText.equals(t);
            }
        }
        if (!matched) {
            text += " - " + duration + "\"";
            onView(withId(R.id.roundNumber)).check(matches(withText(text)));
        }
        assertTrue(matched);
    }

    private void addPoints(int teamId, int rowNo, Integer pts, boolean otherTeam) {
        onView(viewInRow(teamId, rowNo)).perform(click());
        if (pts != null) {
            onView(allOf(withId(R.id.inputField), isDisplayed())).perform(replaceText(Integer.toString(pts)), closeSoftKeyboard());
        }
        if (otherTeam) onView(withId(R.id.action_157_x)).perform(click());
        onView(allOf(withId(R.id.inputField), isDisplayed())).perform(pressImeActionButton());
    }

    @Override
    protected void setWhoIsNext(int roundNo, int plId, int prevPlId) {
        onView(withId(R.id.roundNumber)).perform(click());
        super.setWhoIsNext(roundNo, plId, prevPlId);
    }

    private void deletePoints() {
        onView(withId(R.id.action_settings)).perform(click());
        onView(withText(R.string.reset)).perform(click());
        onView(withText(android.R.string.ok)).perform(click());
    }

    @Test
    public void testCoiffeur() {
        onView(allOf(withId(R.id.startcoiffeur), withText(R.string.coiffeur))).perform(click());
        onView(withId(R.id.action_reset)).perform(click());
        onView(withText(android.R.string.ok)).perform(click());

        onView(viewInRow(R.id.team1, header)).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("TKKG"), pressImeActionButton());
        onView(viewInRow(R.id.team2, header)).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("???"), pressImeActionButton());

        onView(viewInRow(R.id.jasstype, 2)).perform(longClick());
        onView(allOf(withText(R.string.rosen), isDisplayed())).perform(click());
        onView(viewInRow(R.id.jasstype, 4)).perform(longClick());
        onView(allOf(withText(R.string.schellen), isDisplayed())).perform(click());

        setWhoIsNext(0, R.id.r2c1, 0);
        addPoints(R.id.team2, 2, 25, true);
        setWhoIsNext(1, R.id.r1c1, R.id.r2c2);
        addPoints(R.id.team2, 8, 88, false);
        addPoints(R.id.team1, 6, 34, true);
        addPoints(R.id.team1, 8, 14, true);
        total[0] = 6 * (157 - 34) + 8 * (157 - 14);
        total[1] = 2 * (157 - 25) + 8 * 88;
        total[2] = 8 * (157 - 14 - 88);
        checkPoints();
        checkRoundCounter(4, 0);

        addPoints(R.id.team2, 8, 92, false);
        total[1] += 8 * 4;
        total[2] -= 8 * 4;
        checkPoints();
        setWhoIsNext(4, R.id.r1c2, R.id.r1c2);

        onView(viewInRow(R.id.team1, 3)).perform(click());
        onView(withId(R.id.action_157_x)).check(matches(withBackground(R.drawable.ic_action_match)));
        onView(allOf(withId(R.id.inputField), isDisplayed())).perform(replaceText(Integer.toString(2)), closeSoftKeyboard());
        onView(withId(R.id.action_157_x)).check(matches(withBackground(R.drawable.ic_action_157_x)));
        onView(allOf(withId(R.id.inputField), isDisplayed())).perform(pressImeActionButton());
    }

    @Test
    public void testThreeTeams() {
        onView(allOf(withId(R.id.startcoiffeur), withText(R.string.coiffeur))).perform(click());
        onView(viewInRow(R.id.team3, header)).check(matches(not(isDisplayed())));
        onView(withId(R.id.action_settings)).perform(click());
        onData(withTitle(R.string.coif_3teams)).perform(click());
        pressBack();
        onView(viewInRow(R.id.team3, 6)).check(matches(isClickable()));
        onView(viewInRow(R.id.action_reset, header)).check(matches(not(isDisplayed())));
        onView(viewInRow(R.id.team3, header)).check(matches(isDisplayed()));
        onView(viewInRow(R.id.team1, header)).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("TKKG"), pressImeActionButton());
        onView(viewInRow(R.id.team2, header)).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("???"), pressImeActionButton());
        onView(viewInRow(R.id.team3, header)).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("Tim & Struppi"), pressImeActionButton());

        addPoints(R.id.team3, 10, 24, true);
        total[2] += 10 * (157 - 24);
        addPoints(R.id.team2, 5, 120, false);
        total[1] += 5 * 120;
        addPoints(R.id.team2, 9, 34, true);
        total[1] += 9 * (157 - 34);
        addPoints(R.id.team3, 2, 44, true);
        total[2] += 2 * (157 - 44);
        addPoints(R.id.team1, 10, 55, true);
        total[0] += 10 * (157 - 55);
        addPoints(R.id.team1, 11, null, true);
        total[0] += 11 * 257;
        addPoints(R.id.team1, 8, 12, true);
        total[0] += 8 * (157 - 12);
        addPoints(R.id.team2, 7, 120, false);
        total[1] += 7 * 120;
        checkPoints();
        addPoints(R.id.team1, 9, null, true);
        onView(viewInRow(R.id.team1, 9)).check(matches(withText("257")));
        total[0] += 9 * 257;
        addPoints(R.id.team2, 11, 12, false);
        total[1] += 11 * 12;
        addPoints(R.id.team3, 11, 24, false);
        total[2] += 11 * 24;
        addPoints(R.id.team2, 4, 82, false);
        total[1] += 4 * 82;
        addPoints(R.id.team3, 7, 65, true);
        total[2] += 7 * (157 - 65);
        addPoints(R.id.team1, 4, 15, true);
        total[0] += 4 * (157 - 15);
        checkPoints();
        addPoints(R.id.team1, 5, 45, true);
        total[0] += 5 * (157 - 45);
        addPoints(R.id.team2, 6, 15, true);
        total[1] += 6 * (157 - 15);
        addPoints(R.id.team2, 10, 45, false);
        total[1] += 10 * 45;
        addPoints(R.id.team1, 7, 120, false);
        total[0] += 7 * 120;
        checkPoints();
        onView(viewInRow(R.id.team2, footer)).check(matches(withText(containsString("max"))));
        onView(viewInRow(R.id.team3, footer)).check(matches(not(withText(containsString("max")))));
        onView(viewInRow(R.id.team1, footer)).check(matches(withTextColor(Color.WHITE)));
        onView(viewInRow(R.id.team2, footer)).check(matches(withTextColor(Color.WHITE)));
        onView(viewInRow(R.id.team3, footer)).check(matches(withTextColor(Color.WHITE)));

        addPoints(R.id.team3, 8, 120, true);
        total[2] += 8 * (157 - 120);
        addPoints(R.id.team3, 6, 78, true);
        total[2] += 6 * (157 - 78);
        addPoints(R.id.team1, 6, 130, false);
        total[0] += 6 * 130;
        checkPoints();
        onView(viewInRow(R.id.team1, footer)).check(matches(withTextColor(Color.BLACK)));
        onView(viewInRow(R.id.team2, footer)).check(matches(withText(containsString("max"))));
        onView(viewInRow(R.id.team3, footer)).check(matches(withText(containsString("max"))));
    }

    @Test
    public void testTwoColumns() {
        onView(allOf(withId(R.id.startcoiffeur), withText(R.string.coiffeur))).perform(click());
        onView(withId(R.id.action_settings)).perform(click());
        onData(withTitle(R.string.coif_3rdcolumn)).perform(click());
        onView(withId(R.id.action_close)).perform(click());
        onView(viewInRow(R.id.team3, footer)).check(matches(not(isDisplayed())));

        addPoints(R.id.team2, 10, 24, true);
        addPoints(R.id.team2, 5, 120, false);
        addPoints(R.id.team1, 9, 34, true);
        addPoints(R.id.team1, 5, 44, true);
        total[0] = 9 * (157 - 34) + 5 * (157 - 44);
        total[1] = 10 * (157 - 24) + 5 * 120;
        total[2] = 5 * (157 - 44 - 120);
        checkPoints();
        onView(viewInRow(R.id.team3, footer)).check(matches(not(isDisplayed())));
    }

    @Test
    public void testCustomMultiplicators() {
        onView(allOf(withId(R.id.startcoiffeur), withText(R.string.coiffeur))).perform(click());

        onView(withId(R.id.action_settings)).perform(click());
        onData(withTitle(R.string.setFactorManually)).perform(click());
        onView(withId(R.id.action_close)).perform(click());

        addPoints(R.id.team2, 5, 88, false);
        total[1] = 5 * 88;
        checkPoints();

        onView(viewInRow(R.id.jasstype, 5)).perform(click()); // nothing happens
        onView(viewInRow(R.id.jasstype, 5)).perform(longClick());

        onView(withId(R.id.numberPicker)).perform(setFactor(9));
        onView(allOf(withId(R.id.selection), isDisplayed())).perform(replaceText("Obenaben"), closeSoftKeyboard());
        onView(allOf(withId(R.id.buttonEmpty), withText(android.R.string.ok), isDisplayed())).perform(click());

        onView(subViewInRow(R.id.factor, 5)).check(matches(withText("9")));
        onView(subViewInRow(R.id.description, 5)).check(matches(withText("Obenaben")));

        addPoints(R.id.team1, 5, 43, true);
        total[0] = 9 * (157 - 43);
        total[1] = 9 * 88;
        total[2] = 9 * (114 - 88);
        checkPoints();

        onView(viewInRow(R.id.jasstype, 5)).perform(longClick());
        onView(withId(R.id.numberPicker)).perform(setFactor(7));
        onView(allOf(withText(R.string.obenabe), isDisplayed())).perform(click());

        onView(subViewInRow(R.id.factor, 5)).check(matches(withText("7")));
        onView(subViewInRow(R.id.description, 5)).check(matches(withText(R.string.obenabe)));
        total[0] = 7 * 114;
        total[1] = 7 * 88;
        total[2] = total[0] - total[1];
        checkPoints();

        pressBack();
        onView(allOf(withId(R.id.startcoiffeur), withText(R.string.coiffeur))).perform(click());
        checkPoints();
    }

    @Test
    public void testRoundedValues() {
        onView(allOf(withId(R.id.startcoiffeur), withText(R.string.coiffeur))).perform(click());
        onView(withId(R.id.action_settings)).perform(click());
        onView(withText(R.string.denominator10)).perform(click());
        onData(withTitle(R.string.coif_diffrows)).perform(click());
        onView(withText("6")).perform(click());
        pressBack();

        addPoints(R.id.team1, 4, 144, false);
        total[0] += 4 * 14;
        addPoints(R.id.team1, 6, null, true);
        onView(viewInRow(R.id.team1, 6)).check(matches(withText("26")));
        total[0] += 6 * 26;
        addPoints(R.id.team2, 6, 51, false);
        total[1] += 6 * 5;
        total[2] += 6 * (26 - 5);
        addPoints(R.id.team2, 5, 16, false);
        total[1] += 5 * 2;
        addPoints(R.id.team2, 2, 22, false);
        total[1] += 2 * 2;
        addPoints(R.id.team1, 3, 145, false);
        total[0] += 3 * 15;
        addPoints(R.id.team1, 5, 146, false);
        total[0] += 5 * 15;
        total[2] += 5 * (15 - 2);
        checkPoints();

        onView(viewInRow(R.id.team1, footer)).check(matches(withTextColor(Color.BLACK)));
        onView(viewInRow(R.id.team2, footer)).check(matches(withTextColor(Color.WHITE)));
        onView(viewInRow(R.id.team2, footer)).check(matches(withText(containsString("max"))));

        addPoints(R.id.team1, 4, 15, false);
        onView(viewInRow(R.id.team1, footer)).check(matches(withTextColor(Color.BLACK)));

        onView(withId(R.id.action_settings)).perform(click());
        onView(withText(R.string.denominator10)).perform(click());
        pressBack();
        onView(viewInRow(R.id.team1, footer)).check(matches(withTextColor(Color.WHITE)));
        onView(viewInRow(R.id.team2, footer)).check(matches(withTextColor(Color.WHITE)));
    }

    @Test
    public void testMatchBonus() {
        onView(allOf(withId(R.id.startcoiffeur), withText(R.string.coiffeur))).perform(click());

        int bonus = 50;
        int matchPoints = 157;
        onView(withId(R.id.action_settings)).perform(click());
        onData(withTitle(R.string.matchpoints)).perform(click());
        onView(withId(android.R.id.edit)).perform(replaceText(Integer.toString(matchPoints)), pressImeActionButton());
        onView(allOf(withId(android.R.id.button1), withText(android.R.string.ok))).perform(click());
        onData(withTitle(R.string.matchBonus)).perform(click());
        onData(withTitle(R.string.matchBonusVal)).perform(click());
        onView(withId(android.R.id.edit)).perform(replaceText(Integer.toString(bonus)), pressImeActionButton());
        onView(allOf(withId(android.R.id.button1), withText(android.R.string.ok))).perform(click());
        onData(withTitle(R.string.coif_diffrows)).perform(click());
        onView(withText("7")).perform(click());
        pressBack();


        addPoints(R.id.team2, 5, 88, false);
        addPoints(R.id.team1, 3, null, true);
        onView(viewInRow(R.id.team1, 3)).check(matches(withText("MATCH")));
        addPoints(R.id.team1, 4, 14, true);
        addPoints(R.id.team2, 3, 55, true);

        total[0] = 3 * matchPoints + 4 * (157 - 14) + bonus;
        total[1] = 3 * (157 - 55) + 5 * 88;
        total[2] = 3 * 55 + bonus;
        checkPoints();

        bonus = 100;
        onView(withId(R.id.action_settings)).perform(click());
        onData(withTitle(R.string.matchBonusVal)).perform(click());
        onView(withId(android.R.id.edit)).perform(replaceText(Integer.toString(bonus)), pressImeActionButton());
        onView(allOf(withId(android.R.id.button1), withText(android.R.string.ok))).perform(click());
        pressBack();
        total[0] += 50;
        total[2] += 50;
        checkPoints();

        onView(viewInRow(R.id.team1, 3)).perform(click());
        onView(allOf(withId(R.id.inputField), isDisplayed())).check(matches(withText("257")));
        pressBack();
        pressBack();

        matchPoints = 257;
        onView(withId(R.id.action_settings)).perform(click());
        onData(withTitle(R.string.matchpoints)).perform(click());
        onView(withId(android.R.id.edit)).perform(replaceText(Integer.toString(matchPoints)), pressImeActionButton());
        onView(allOf(withId(android.R.id.button1), withText(android.R.string.ok))).perform(click());
        onView(withText(R.string.resetMatchBonus)).check(matches(isDisplayed()));
        onView(allOf(withId(android.R.id.button1), withText(android.R.string.ok))).perform(click());
        pressBack();
        onView(viewInRow(R.id.team1, 3)).check(matches(withText(String.valueOf(matchPoints))));
        total[0] += 3 * 100 - bonus;
        total[2] += 3 * 100 - bonus;
        checkPoints();
    }

    @Test
    public void testMatchBonusRounded() {
        onView(allOf(withId(R.id.startcoiffeur), withText(R.string.coiffeur))).perform(click());

        int bonus = 20;
        int malus = -10;
        onView(withId(R.id.action_settings)).perform(click());
        onData(withTitle(R.string.denominator10)).perform(click());
        onData(withTitle(R.string.matchpoints)).perform(click());
        onView(withId(android.R.id.edit)).perform(replaceText(Integer.toString(157)), pressImeActionButton());
        onView(allOf(withId(android.R.id.button1), withText(android.R.string.ok))).perform(click());
        onData(allOf(withTitle(R.string.matchpoints), withSummaryText("157"))).check(matches(isDisplayed()));

        onData(withTitle(R.string.matchpoints)).perform(click());
        onView(withId(android.R.id.edit)).perform(replaceText(Integer.toString(0)), pressImeActionButton());
        onView(allOf(withId(android.R.id.button1), withText(android.R.string.ok))).perform(click());
        onData(allOf(withTitle(R.string.matchpoints), withSummaryText("157"))).check(matches(isDisplayed())); // still 157 (not changed)

        onData(withTitle(R.string.matchBonus)).perform(click());

        // check settings
        String text = String.format(getInstrumentation().getTargetContext().getString(R.string.matchBonusRounded), 500, 50);
        onData(allOf(withTitle(R.string.matchBonusVal), withSummaryText(text))).check(matches(isDisplayed()));
        text = String.format(getInstrumentation().getTargetContext().getString(R.string.matchBonusRounded), -500, -50);
        onData(allOf(withTitle(R.string.matchMalusVal), withSummaryText(text))).check(matches(isDisplayed()));

        // change bonus value
        onData(withTitle(R.string.matchBonusVal)).perform(click());
        onView(withId(android.R.id.edit)).perform(replaceText(Integer.toString(bonus * 10)), pressImeActionButton());
        onView(allOf(withId(android.R.id.button1), withText(android.R.string.ok))).perform(click());
        // change malus value
        onData(withTitle(R.string.matchMalusVal)).perform(click());
        onView(withId(android.R.id.edit)).perform(replaceText("100"), pressImeActionButton());
        onView(allOf(withId(android.R.id.button1), withText(android.R.string.ok))).perform(click());
        // not changed since positive values are not accepted
        onData(allOf(withTitle(R.string.matchMalusVal), withSummaryText(text))).check(matches(isDisplayed()));
        onData(withTitle(R.string.matchMalusVal)).perform(click());
        onView(withId(android.R.id.edit)).perform(replaceText(Integer.toString(malus * 10)), pressImeActionButton());
        onView(allOf(withId(android.R.id.button1), withText(android.R.string.ok))).perform(click());
        // now changed to 0
        text = String.format(getInstrumentation().getTargetContext().getString(R.string.matchBonusRounded), bonus * 10, bonus);
        onData(allOf(withTitle(R.string.matchBonusVal), withSummaryText(text))).check(matches(isDisplayed()));
        text = String.format(getInstrumentation().getTargetContext().getString(R.string.matchBonusRounded), malus * 10, malus);
        onData(allOf(withTitle(R.string.matchMalusVal), withSummaryText(text))).check(matches(isDisplayed()));

        onData(withTitle(R.string.coif_diffrows)).perform(click());
        onView(withText("7")).perform(click());
        pressBack();

        addPoints(R.id.team2, 5, 144, false);
        addPoints(R.id.team1, 3, 160, false);
        onView(viewInRow(R.id.team1, 3)).check(matches(withText("MATCH")));
        addPoints(R.id.team1, 4, 98, false);
        addPoints(R.id.team2, 3, 94, false);

        total[0] = 3 * 16 + bonus + 4 * 10;
        total[1] = 3 * 9 + 5 * 14;
        total[2] = 3 * 7 + bonus;
        checkPoints();

        onView(viewInRow(R.id.team1, 3)).perform(click());
        onView(allOf(withId(R.id.inputField), isDisplayed())).check(matches(withText("257")));
        pressBack();
        pressBack();

        addPoints(R.id.team1, 5, 138, false);
        addPoints(R.id.team1, 6, 257, false);
        addPoints(R.id.team1, 7, 110, false);
        addPoints(R.id.team2, 7, 64, false);
        addPoints(R.id.team2, 6, 74, false);
        onView(viewInRow(R.id.team1, footer)).check(matches(withTextColor(Color.WHITE)));
        onView(viewInRow(R.id.team2, footer)).check(matches(withTextColor(Color.WHITE)));
        total[0] = 371;
        total[1] = 181;
        total[2] = 150;
        checkPoints();

        bonus = 12;
        onView(withId(R.id.action_settings)).perform(click());
        onData(withTitle(R.string.matchBonusVal)).perform(click());
        onView(withId(android.R.id.edit)).perform(replaceText(Integer.toString(bonus * 10)), pressImeActionButton());
        onView(allOf(withId(android.R.id.button1), withText(android.R.string.ok))).perform(click());
        pressBack();
        onView(viewInRow(R.id.team1, footer)).check(matches(withTextColor(Color.BLACK)));
        onView(viewInRow(R.id.team2, footer)).check(matches(withTextColor(Color.WHITE)));
        onView(viewInRow(R.id.team2, footer)).check(matches(withText(containsString("max"))));
        total[0] -= 2 * 8;
        total[2] -= 2 * 8;
        checkPoints();

        onView(withId(R.id.action_settings)).perform(click());
        onData(withTitle(R.string.matchBonus)).perform(click());
        pressBack();
        pressBack();
        onView(viewInRow(R.id.team1, 3)).check(matches(withText("16")));
        total[0] += -2 * bonus;
        total[2] += -2 * bonus;
        checkPoints();

        pressBack();
        onView(allOf(withId(R.id.startcoiffeur), withText(R.string.coiffeur))).perform(click());
        onView(viewInRow(R.id.team1, 3)).check(matches(withText("16")));
        checkPoints();

        onView(withId(R.id.action_settings)).perform(click());
        onData(withTitle(R.string.matchpoints)).perform(click());
        onView(withId(android.R.id.edit)).perform(replaceText(Integer.toString(257)), pressImeActionButton());
        onView(allOf(withId(android.R.id.button1), withText(android.R.string.ok))).perform(click());
        pressBack();
        onView(viewInRow(R.id.team1, 3)).check(matches(withText("26")));
    }

    @Test
    public void testMatchBonusHintToChangePoints() {
        onView(allOf(withId(R.id.startcoiffeur), withText(R.string.coiffeur))).perform(click());

        int bonus = 200;
        onView(withId(R.id.action_settings)).perform(click());
        onData(withTitle(R.string.matchBonusVal)).perform(click()); // nothing happens
        onData(withTitle(R.string.matchBonus)).perform(click());
        onView(withText(android.R.string.ok)).perform(click());
        onData(withTitle(R.string.matchpoints)).perform(click());
        onView(withId(android.R.id.edit)).check(matches(withText("157")));
        onView(withText(android.R.string.cancel)).perform(click());
        onData(withTitle(R.string.matchBonusVal)).perform(click());
        onView(withId(android.R.id.edit)).perform(replaceText(Integer.toString(bonus)), pressImeActionButton());
        onView(allOf(withId(android.R.id.button1), withText(android.R.string.ok))).perform(click());
        pressBack();

        addPoints(R.id.team2, 5, 144, false);
        addPoints(R.id.team1, 3, 160, false);
        onView(viewInRow(R.id.team1, 3)).check(matches(withText("MATCH")));
        addPoints(R.id.team1, 4, 51, false);
        addPoints(R.id.team2, 3, 94, false);

        total[0] = 3 * 157 + bonus + 4 * 51;
        total[1] = 3 * 94 + 5 * 144;
        total[2] = 3 * (157 - 94) + bonus;
        checkPoints();

        onView(withId(R.id.action_settings)).perform(click());
        onData(withTitle(R.string.matchBonus)).perform(click());
        onView(withText(android.R.string.ok)).perform(click());
        onView(withId(R.id.action_close)).perform(click());
        onView(viewInRow(R.id.team1, 3)).check(matches(withText("257")));
        total[0] += 3 * 100 - bonus;
        total[2] += 3 * 100 - bonus;
        checkPoints();

        onView(withId(R.id.action_settings)).perform(click());
        onData(withTitle(R.string.matchBonus)).perform(click());
        onView(withText(android.R.string.ok)).perform(click());
        onData(withTitle(R.string.matchBonus)).perform(click());
        onView(withText(android.R.string.cancel)).perform(click());
        onView(withId(R.id.action_close)).perform(click());
        onView(viewInRow(R.id.team1, 3)).check(matches(withText("157")));
        total[0] -= 3 * 100;
        total[2] -= 3 * 100;
        checkPoints();
    }

    @Test
    public void testScratchedRound() {
        onView(allOf(withId(R.id.startcoiffeur), withText(R.string.coiffeur))).perform(click());

        onView(withId(R.id.action_settings)).perform(click());
        onData(withTitle(R.string.coif_diffrows)).perform(click());
        onView(withText("6")).perform(click());
        pressBack();

        addPoints(R.id.team2, 2, 141, false);

        onView(viewInRow(R.id.team1, 1)).perform(click());
        onView(withId(R.id.action_scratch)).perform(click());
        onView(viewInRow(R.id.team1, 1)).check(matches(withBackground(R.drawable.scratch)));

        onView(viewInRow(R.id.description, footer)).perform(click());
        onView(withId(R.id.sum0)).check(matches(withText(Integer.toString(0))));
        onView(withId(R.id.sum1)).check(matches(withText(Integer.toString(2 * 141))));
        onView(withId(R.id.max0)).check(matches(withText(Integer.toString((2 + 3 + 4 + 5 + 6) * 257))));
        onView(withId(R.id.max1)).check(matches(withText(Integer.toString((3 + 4 + 5 + 6) * 257 + 2 * 141))));
        pressBack();

        pressBack();
        onView(allOf(withId(R.id.startcoiffeur), withText(R.string.coiffeur))).perform(click());
        onView(viewInRow(R.id.team1, 1)).check(matches(withBackground(R.drawable.scratch)));

        onView(viewInRow(R.id.team1, 1)).perform(click());
        onView(allOf(withId(R.id.inputField), isDisplayed())).check(matches(withText("")));
        onView(allOf(withId(R.id.inputField), isDisplayed())).perform(pressImeActionButton());
        onView(viewInRow(R.id.team1, 1)).check(matches(withText("")));

        onView(viewInRow(R.id.team1, footer)).perform(click());
        onView(withId(R.id.max0)).check(matches(withText(Integer.toString((1 + 2 + 3 + 4 + 5 + 6) * 257))));
        pressBack();

        addPoints(R.id.team1, 1, 0, false);
        onView(viewInRow(R.id.team1, 1)).check(matches(withText("0")));

        onView(viewInRow(R.id.team2, 1)).perform(click());
        onView(withId(R.id.action_scratch)).perform(click());
        onView(withId(R.id.action_reset)).perform(click());
        onView(withText(android.R.string.ok)).perform(click());
        // onView(viewInRow(R.id.team1, 1)).check(matches(withBackground(R.drawable.scratch))));
    }

    @Test
    public void testScratchedRound_Bonus() {
        onView(allOf(withId(R.id.startcoiffeur), withText(R.string.coiffeur))).perform(click());

        onView(withId(R.id.action_settings)).perform(click());
        onData(withTitle(R.string.coif_diffrows)).perform(click());
        onView(withText("6")).perform(click());
        onData(withTitle(R.string.matchBonus)).perform(click());
        onView(allOf(withId(android.R.id.button1), withText(android.R.string.ok))).perform(click());
        pressBack();

        addPoints(R.id.team2, 2, 141, false);

        onView(viewInRow(R.id.team1, 1)).perform(click());
        onView(withId(R.id.action_scratch)).perform(click());
        onView(viewInRow(R.id.team1, 1)).check(matches(withBackground(R.drawable.scratch)));

        onView(viewInRow(R.id.description, footer)).perform(click());
        onView(withId(R.id.sum0)).check(matches(withText(Integer.toString(0))));
        onView(withId(R.id.sum1)).check(matches(withText(Integer.toString(2 * 141))));
        onView(withId(R.id.max0)).check(matches(withText(Integer.toString((2 + 3 + 4 + 5 + 6) * 157 + 5 * 500))));
        onView(withId(R.id.max1)).check(matches(withText(Integer.toString((3 + 4 + 5 + 6) * 157 + 2 * 141 + 4 * 500))));
        pressBack();

        pressBack();
        onView(allOf(withId(R.id.startcoiffeur), withText(R.string.coiffeur))).perform(click());
        onView(viewInRow(R.id.team1, 1)).check(matches(withBackground(R.drawable.scratch)));

        onView(viewInRow(R.id.team1, 1)).perform(click());
        onView(allOf(withId(R.id.inputField), isDisplayed())).check(matches(withText("")));
        onView(allOf(withId(R.id.inputField), isDisplayed())).perform(pressImeActionButton());
        onView(viewInRow(R.id.team1, 1)).check(matches(withText("")));

        onView(viewInRow(R.id.team1, footer)).perform(click());
        onView(withId(R.id.max0)).check(matches(withText(Integer.toString((1 + 2 + 3 + 4 + 5 + 6) * 157 + 6 * 500))));
        pressBack();

        addPoints(R.id.team1, 1, 0, false);
        onView(viewInRow(R.id.team1, 1)).check(matches(withText("0")));

        onView(viewInRow(R.id.team2, 1)).perform(click());
        onView(withId(R.id.action_scratch)).perform(click());
        onView(withId(R.id.action_reset)).perform(click());
        onView(withText(android.R.string.ok)).perform(click());
    }

    @Test
    public void testRoundDuration() {
        // speed up
        CoiffeurHeader.UPDATEINTERVAL = 1;
        RoundDuration.SEC2MIN = 1;

        onView(allOf(withId(R.id.startcoiffeur), withText(R.string.coiffeur))).perform(click());
        long t0 = System.currentTimeMillis() / 1000;
        checkRoundCounter(0, getTimeSince(t0));

        onView(withId(R.id.action_settings)).perform(click());
        onData(withTitle(R.string.coif_diffrows)).perform(click());
        onView(withText("6")).perform(click());
        pressBack();

        checkRoundCounter(0, getTimeSince(t0));

        pressBack();
        onView(allOf(withId(R.id.startcoiffeur), withText(R.string.coiffeur))).perform(click());
        checkRoundCounter(0, getTimeSince(t0));

        SystemClock.sleep(10 * 1000);
        checkRoundCounter(0, getTimeSince(t0));

        pressBack();
        onView(allOf(withId(R.id.startcoiffeur), withText(R.string.coiffeur))).perform(click());
        t0 = System.currentTimeMillis() / 1000;
        checkRoundCounter(0, getTimeSince(t0));

        addPoints(R.id.team1, 1, 55, false);
        addPoints(R.id.team1, 2, 55, false);
        addPoints(R.id.team1, 3, 55, false);
        checkRoundCounter(3, getTimeSince(t0));
        addPoints(R.id.team1, 4, 55, false);
        addPoints(R.id.team1, 5, 55, false);
        addPoints(R.id.team1, 6, 55, false);
        pressBack();
        onView(allOf(withId(R.id.startcoiffeur), withText(R.string.coiffeur))).perform(click());
        checkRoundCounter(6, getTimeSince(t0));
        addPoints(R.id.team2, 6, 55, false);
        addPoints(R.id.team2, 5, 55, false);
        addPoints(R.id.team2, 4, 55, false);
        addPoints(R.id.team2, 3, 55, false);
        addPoints(R.id.team2, 2, 55, false);
        SystemClock.sleep(2 * 1000);
        addPoints(R.id.team2, 1, 55, false);

        pressBack();
        onView(allOf(withId(R.id.startcoiffeur), withText(R.string.coiffeur))).perform(click());
        long duration = getTimeSince(t0);
        checkRoundCounter(12, duration);

        SystemClock.sleep(2 * 1000);
        checkRoundCounter(12, duration);

        onView(withId(R.id.action_settings)).perform(click());
        onData(withTitle(R.string.coif_3teams)).perform(click());
        pressBack();
        checkRoundCounter(12, getTimeSince(t0));

        deletePoints();
        checkRoundCounter(0, 0);

        CoiffeurHeader.UPDATEINTERVAL = 10;
        RoundDuration.SEC2MIN = 60;
    }

    @Test
    public void testWhoBeginsSwap() {
        onView(allOf(withId(R.id.startcoiffeur), withText(R.string.coiffeur))).perform(click());

        onView(viewInRow(R.id.team1, header)).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("A & B"), pressImeActionButton());
        onView(viewInRow(R.id.team2, header)).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("C & D"), pressImeActionButton());

        setWhoIsNext(0, R.id.r2c1, 0);

        onView(withId(R.id.roundNumber)).perform(click());
        onView(withId(R.id.r1c1)).check(matches(withText("A")));
        onView(withId(R.id.r1c2)).check(matches(withText("D")));
        onView(withId(R.id.r2c1)).check(matches(withText("C")));
        onView(withId(R.id.r2c2)).check(matches(withText("B")));
        onView(withId(R.id.r1c2)).perform(swipeTo(R.id.r1c1));
        onView(withId(R.id.r1c1)).check(matches(withText("D")));
        onView(withId(R.id.r1c2)).check(matches(withText("A")));
        pressBack();

        onView(withId(R.id.action_settings)).perform(click());
        onData(withTitle(R.string.coif_3teams)).perform(click());
        pressBack();

        onView(withId(R.id.roundNumber)).perform(click());
        onView(withId(R.id.r1c1)).check(matches(withText("B")));
        onView(withId(R.id.r1c2)).check(matches(withText("Team 3b")));
        onView(withId(R.id.r3c2)).perform(swipeTo(R.id.r1c1));
        pressBack();

        addPoints(R.id.team2, 2, 25, true);
        setWhoIsNext(1, R.id.r1c1, R.id.r3c1);

        onView(withId(R.id.action_settings)).perform(click());
        onData(withTitle(R.string.coif_3teams)).perform(click());
        pressBack();

        onView(withId(R.id.roundNumber)).perform(click());
        onView(withId(R.id.r1c1)).check(matches(withText("A")));
        onView(withId(R.id.r1c2)).check(matches(withText("D")));
        pressBack();

        setWhoIsNext(1, R.id.r2c2, R.id.r2c2);
        addPoints(R.id.team2, 8, 88, false);
        checkRoundCounter(2, 0);
        setWhoIsNext(2, R.id.r2c2, R.id.r1c2);
    }

    @Test
    public void testCoiffeurTypes() {
        onView(allOf(withId(R.id.startcoiffeur), withText(R.string.coiffeur))).perform(click());

        onView(viewInRow(R.id.jasstype, 1)).perform(longClick());
        onView(allOf(withText(R.string.schellen), isDisplayed())).perform(click());
        onView(subViewInRow(R.id.description, 1)).check(matches(withText(R.string.schellen)));

        onView(viewInRow(R.id.jasstype, 2)).perform(longClick());
        onView(withId(R.id.selection)).perform(replaceText("Rose"));
        onView(withId(R.id.buttonEmpty)).perform(click());
        onView(subViewInRow(R.id.description, 2)).check(matches(withText("Rose")));
        onView(subViewInRow(R.id.icon, 2)).check(matches(withDrawable(R.drawable.ic_rosen)));

        onView(viewInRow(R.id.jasstype, 3)).perform(longClick());
        onView(withId(R.id.selection)).perform(replaceText("Obenabe/Undenufe"));
        onView(withId(R.id.buttonEmpty)).perform(click());
        onView(subViewInRow(R.id.description, 3)).check(matches(withText("Obenabe/Undenufe")));
        onView(subViewInRow(R.id.icon, 3)).check(matches(withDrawable(R.drawable.ic_obeunde_dark)));

        onView(viewInRow(R.id.jasstype, 4)).perform(longClick());
        onView(withId(R.id.selection)).perform(replaceText("Trumpf"));
        onView(withId(R.id.buttonEmpty)).perform(click());
        onView(subViewInRow(R.id.description, 4)).check(matches(withText(R.string.trumpf)));
        onView(subViewInRow(R.id.icon, 4)).check(matches(withDrawable(R.drawable.ic_trumpf)));

        onView(viewInRow(R.id.jasstype, 5)).perform(longClick());
        onView(withId(R.id.selection)).perform(replaceText("Slalom - Gusti"));
        onView(withId(R.id.buttonEmpty)).perform(click());
        onView(subViewInRow(R.id.description, 5)).check(matches(withText("Slalom - Gusti")));
        onView(subViewInRow(R.id.icon, 5)).check(matches(withDrawable(R.drawable.ic_slalomgusti_dark)));

        onView(viewInRow(R.id.jasstype, 6)).perform(longClick());
        onView(withId(R.id.selection)).perform(replaceText("Ondeufe"));
        onView(withId(R.id.buttonEmpty)).perform(click());
        onView(subViewInRow(R.id.description, 6)).check(matches(withText("Ondeufe")));
        onView(subViewInRow(R.id.icon, 6)).check(matches(withDrawable(R.drawable.ic_undenufe_dark)));

        onView(viewInRow(R.id.jasstype, 7)).perform(longClick());
        onView(withId(R.id.selection)).perform(replaceText("Schälle"));
        onView(withId(R.id.buttonEmpty)).perform(click());
        onView(subViewInRow(R.id.description, 7)).check(matches(withText("Schälle")));
        onView(subViewInRow(R.id.icon, 7)).check(matches(withDrawable(R.drawable.ic_schellen)));

        onView(viewInRow(R.id.jasstype, 8)).perform(longClick());
        onView(withId(R.id.selection)).perform(replaceText("Super Eicheln"));
        onView(withId(R.id.buttonEmpty)).perform(click());
        onView(subViewInRow(R.id.description, 8)).check(matches(withText("Super Eicheln")));
        onView(subViewInRow(R.id.icon, 8)).check(matches(withDrawable(R.drawable.ic_supereichel)));

        onView(viewInRow(R.id.jasstype, 9)).perform(longClick());
        onView(withId(R.id.selection)).perform(replaceText("Frei"));
        onView(withId(R.id.buttonEmpty)).perform(click());
        onView(subViewInRow(R.id.description, 9)).check(matches(withText("Frei")));
        onView(subViewInRow(R.id.icon, 9)).check(matches(withDrawable(R.drawable.ic_questionmark_dark)));

        onView(viewInRow(R.id.jasstype, 10)).perform(longClick());
        onView(withId(R.id.selection)).perform(replaceText("Gusti / Mery"));
        onView(withId(R.id.buttonEmpty)).perform(click());
        onView(subViewInRow(R.id.description, 10)).check(matches(withText("Gusti / Mery")));
        onView(subViewInRow(R.id.icon, 10)).check(matches(withDrawable(R.drawable.ic_gustimery_dark)));

        onView(viewInRow(R.id.jasstype, 11)).perform(longClick());
        onView(withId(R.id.selection)).perform(replaceText("Tutti"));
        onView(withId(R.id.buttonEmpty)).perform(click());
        onView(subViewInRow(R.id.description, 11)).check(matches(withText("Tutti")));
        onView(subViewInRow(R.id.icon, 11)).check(matches(withDrawable(R.drawable.ic_tutti)));

        onView(viewInRow(R.id.jasstype, 11)).perform(longClick());
        onView(withId(R.id.selection)).perform(replaceText("Ergendöppis"));
        onView(withId(R.id.buttonEmpty)).perform(click());
        onView(subViewInRow(R.id.description, 11)).check(matches(withText("Ergendöppis")));
        onView(subViewInRow(R.id.icon, 11)).check(matches(withDrawable(R.drawable.ic_custom)));
    }

    @Test
    public void testCoiffeurProfiles() {
        onView(allOf(withId(R.id.startcoiffeur), withText(R.string.coiffeur))).perform(click());
        onView(withId(R.id.action_reset)).perform(click());
        onView(withText(android.R.string.ok)).perform(click());

        onView(viewInRow(R.id.team1, header)).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("A & B"), pressImeActionButton());
        onView(viewInRow(R.id.team2, header)).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("C & D"), pressImeActionButton());

        setWhoIsNext(0, R.id.r2c1, 0);
        addPoints(R.id.team2, 5, 144, false);
        addPoints(R.id.team1, 3, 32, true);
        onView(viewInRow(R.id.team2, 5)).check(matches(withText("144")));

        onView(withId(R.id.action_settings)).perform(click());
        onData(withTitle(R.string.profiles)).perform(click());
        onView(withText("Standard")).check(matches(isChecked()));
        onView(withId(R.id.addButton)).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("Kurz"), pressImeActionButton());
        onView(withText("Standard")).check(matches(not(isChecked())));
        onView(withText("Kurz")).check(matches(isChecked())); // new profile is automatically selected
        onView(withId(R.id.action_close)).perform(click());
        onData(withTitle(R.string.profiles)).perform(click());
        onView(withText("Kurz")).check(matches(isChecked())); // new profile is still selected
        pressBack();
        onData(withTitle(R.string.coif_diffrows)).perform(click());
        onView(withText("6")).perform(click());
        pressBack();
        onView(viewInRow(R.id.team2, 5)).check(matches(withText("")));

        onView(withId(R.id.action_reset)).perform(click());
        onView(withText(android.R.string.ok)).perform(click());
        onView(viewInRow(R.id.team1, header)).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("Team1"), pressImeActionButton());
        onView(viewInRow(R.id.team2, header)).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("Team2"), pressImeActionButton());
        setWhoIsNext(0, R.id.r1c1, 0);
        addPoints(R.id.team2, 2, 12, true);
        addPoints(R.id.team2, 3, 32, true);
        setWhoIsNext(2, R.id.r2c2, R.id.r2c2);

        onView(withId(R.id.action_settings)).perform(click());
        onData(withTitle(R.string.profiles)).perform(click());
        onView(withText("Standard")).perform(click());
        pressBack();
        onData(withTitle(R.string.coif_diffrows)).perform(scrollTo());
        onData(allOf(withTitle(R.string.coif_diffrows), withSummaryText("11"))).check(matches(isDisplayed()));
        pressBack();
        onView(viewInRow(R.id.team1, header)).check(matches(withText("A & B")));
        onView(viewInRow(R.id.team2, header)).check(matches(withText("C & D")));
        onView(viewInRow(R.id.team2, 5)).check(matches(withText("144")));
        setWhoIsNext(2, R.id.r1c2, R.id.r1c2);

        onView(withId(R.id.action_settings)).perform(click());
        onData(withTitle(R.string.profiles)).perform(click());

        // do not add same profile again
        onView(withText("Standard")).check(matches(isDisplayed()));
        onView(withText("Kurz")).check(matches(isDisplayed()));
        onView(withId(R.id.addButton)).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("Standard"), pressImeActionButton());
        // do not rename to other profiles name
        onView(withText("Kurz")).perform(longClick());
        onView(withId(R.id.inputField)).perform(replaceText("Standard"), pressImeActionButton());
        onView(withText("Kurz")).check(matches(isDisplayed()));
        // check rename
        onView(withText("Kurz")).perform(longClick());
        onView(withId(R.id.inputField)).perform(replaceText("6 Runden"), pressImeActionButton());
        try {
            waitView(withText("6 Runden")); // wait renamed
            onView(withText("Kurz")).check(matches(isDisplayed()));
            fail();
        } catch (NoMatchingViewException e) {
            onView(withText("6 Runden")).check(matches(isDisplayed()));
        }
        // do not delete active profile
        onView(withText("Standard")).perform(longClick());
        onView(withId(R.id.action_delete)).perform(click());
        onView(withText("Standard")).check(matches(isDisplayed()));
        // check renaming active profile
        onView(withText("Standard")).perform(longClick());
        onView(withId(R.id.inputField)).perform(replaceText("New Standard"), pressImeActionButton());
        try {
            waitView(withText("New Standard")); // wait renamed
            onView(withText("Standard")).check(matches(isDisplayed()));
            fail();
        } catch (NoMatchingViewException e) {
            onView(withText("New Standard")).check(matches(isDisplayed()));
        }
        pressBack();
        onData(withTitle(R.string.profiles)).perform(click());
        try {
            onView(withText("Standard")).check(matches(isDisplayed()));
            fail();
        } catch (NoMatchingViewException e) {
            onView(withText("New Standard")).check(matches(isDisplayed()));
            onView(withText("6 Runden")).check(matches(isDisplayed()));
        }

        onView(withText("6 Runden")).perform(click());
        pressBack();
        onData(withTitle(R.string.coif_diffrows)).perform(scrollTo());
        onData(allOf(withTitle(R.string.coif_diffrows), withSummaryText("6"))).check(matches(isDisplayed()));

        onData(withTitle(R.string.profiles)).perform(click());
        onView(withText("New Standard")).perform(click());
        onView(withText("6 Runden")).check(matches(not(isChecked())));
        onView(withText("6 Runden")).perform(longClick());
        onView(withId(R.id.action_delete)).perform(click());
        try {
            onView(withText("6 Runden")).check(matches(isDisplayed()));
            fail();
        } catch (NoMatchingViewException e) {
            // was deleted
        }
        onView(withText("New Standard")).check(matches(isChecked()));
        onView(withText("New Standard")).perform(longClick());
        onView(withId(R.id.inputField)).perform(replaceText("Standard"), pressImeActionButton());
        pressBack();
    }

    @Test
    @FlakyTest
    public void testRotateDevice() {
        onView(allOf(withId(R.id.startcoiffeur), withText(R.string.coiffeur))).perform(click());
        onView(withId(R.id.action_settings)).perform(click());
        onData(withTitle(R.string.screenOrientation)).perform(click());
        onView(withText(R.string.sensor)).perform(click());
        pressBack();

        onView(viewInRow(R.id.team2, 4)).perform(click());
        onView(allOf(withId(R.id.inputField), isDisplayed())).perform(replaceText(Integer.toString(89)), closeSoftKeyboard());
        onView(isRoot()).perform(OrientationChangeAction.orientationLandscape());

        onView(allOf(withId(R.id.inputField), isDisplayed())).check(matches(withText("89")));
        onView(allOf(withId(R.id.inputField), isDisplayed())).perform(pressImeActionButton());
        onView(isRoot()).perform(OrientationChangeAction.orientationLandscape());

        onView(viewInRow(R.id.team2, 4)).check(matches(withText("89")));

        onView(viewInRow(R.id.team1, header)).perform(click());
        onView(withId(R.id.inputField)).perform(replaceText("AAA"));
        onView(isRoot()).perform(OrientationChangeAction.orientationPortrait());
        onView(withId(R.id.inputField)).perform(pressImeActionButton());
        onView(viewInRow(R.id.team1, header)).check(matches(withText("AAA")));
        onView(viewInRow(R.id.team2, 4)).check(matches(withText("89")));
    }

    @Test
    public void testLanguageSelection() {
        final String originalLanguage = Locale.getDefault().getLanguage();
        class Jass {
            private final int name;
            private final int settings;

            private Jass(int n, int s) {
                name = n;
                settings = s;
            }
        }
        Jass[] boards = new Jass[]{new Jass(R.string.schieber, R.string.schiebersettings),
                new Jass(R.string.coiffeur, R.string.coiffeursettings),
                new Jass(R.string.differenzler, R.string.differenzlersettings),
                new Jass(R.string.molotow, R.string.molotowersettings),
                new Jass(R.string.punktetafel, R.string.punktetafelsettings)};

        for (String lang : new String[]{"de", "fr", "en"}) {
            onView(withText(R.string.coiffeur)).perform(click());
            onView(withId(R.id.action_settings)).perform(click());

            selectLanguage(lang);
            checkAppTitle(R.string.app_name);
            for (Jass jass : boards) {
                onView(withText(jass.name)).perform(click());
                onView(withId(R.id.action_settings)).perform(click());
                checkAppTitle(jass.settings);
                pressBack();
                pressBack();
            }
            onView(withText(R.string.punktetafel)).perform(click());
            onView(withId(R.id.action_settings)).perform(click());
            onData(withTitle(R.string.profiles)).perform(click());
            checkAppTitle(R.string.profiles);
            pressBack();
            pressBack();
            pressBack();
        }
        onView(withText(R.string.coiffeur)).perform(click());
        onView(withId(R.id.action_settings)).perform(click());

        selectLanguage(originalLanguage);
    }
}
