package ch.simonste.jasstafel.coiffeur;

import android.os.Bundle;

import androidx.test.platform.app.InstrumentationRegistry;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotSame;
import static junit.framework.Assert.assertSame;

public class CoiffeurSummaryTest {
    private final int[] pts = new int[3];
    private CoiffeurSummary Summary;

    @Before
    public void setUp() {
        InstrumentationRegistry.registerInstance(InstrumentationRegistry.getInstrumentation(), new Bundle());
        Summary = new CoiffeurSummary();
    }

    private Bundle setupBundle(int match, int bonus, float displayFactor, ArrayList<Integer> factors1, ArrayList<Integer> factors2) {
        Bundle bundle = new Bundle();
        bundle.putInt("match", match);
        String[] names = new String[]{Team.T1.toString(), Team.T2.toString()};
        bundle.putStringArray("names", names);
        bundle.putIntArray("pts", pts);
        bundle.putInt("bonus", bonus);
        bundle.putInt("malus", -bonus);
        bundle.putFloat("displayFactor", displayFactor);
        bundle.putIntegerArrayList("factors1", factors1);
        bundle.putIntegerArrayList("factors2", factors2);
        return bundle;
    }

    private Bundle setupBundle(int match, int bonus, float displayFactor, ArrayList<Integer> factors1, ArrayList<Integer> factors2, ArrayList<Integer> factors3) {
        Bundle bundle = setupBundle(match, bonus, displayFactor, factors1, factors2);
        String[] names = new String[]{Team.T1.toString(), Team.T2.toString(), Team.T3.toString()};
        bundle.putStringArray("names", names);
        bundle.putIntegerArrayList("factors3", factors3);
        return bundle;
    }

    private void check(CoiffeurSummary.Hint hint, CoiffeurSummary.HintType type, Team team, int par2) {
        assertNotSame(type, CoiffeurSummary.HintType.winPointsSingle);
        assertEquals(type, hint.type);
        assertEquals(team.toString(), hint.name);
        assertEquals(par2, hint.par2);
    }

    private void check(CoiffeurSummary.Hint hint, CoiffeurSummary.HintType type, Team team, int par2, int par3) {
        assertSame(type, CoiffeurSummary.HintType.winPointsSingle);
        assertEquals(type, hint.type);
        assertEquals(team.toString(), hint.name);
        assertEquals(par2, hint.par2);
        assertEquals(par3, hint.par3);
    }

    @Test
    public final void test_2Teams_GameOver() {
        pts[0] = 200;
        pts[1] = 600;
        ArrayList<Integer> factors1 = new ArrayList<>();
        ArrayList<Integer> factors2 = new ArrayList<>();
        Bundle bundle = setupBundle(257, 0, 1, factors1, factors2);
        ArrayList<CoiffeurSummary.Hint> hints = Summary.calculate(bundle);

        assertEquals(0, hints.size());
    }

    @Test
    public final void test_2Teams_1GameOpen() {
        pts[0] = 500;
        pts[1] = 200;
        ArrayList<Integer> factors1 = new ArrayList<>();
        ArrayList<Integer> factors2 = new ArrayList<>(Collections.singletonList(2));
        Bundle bundle = setupBundle(257, 0, 1, factors1, factors2);
        ArrayList<CoiffeurSummary.Hint> hints = Summary.calculate(bundle);

        assertEquals(1, hints.size());
        check(hints.get(0), CoiffeurSummary.HintType.winPointsSingle, Team.T2, 2, 150);
    }

    @Test
    public final void test_2Teams_2GamesOpen() {
        pts[0] = 500;
        pts[1] = 200;
        ArrayList<Integer> factors1 = new ArrayList<>();
        ArrayList<Integer> factors2 = new ArrayList<>(Arrays.asList(1, 2));
        Bundle bundle = setupBundle(257, 0, 1, factors1, factors2);
        ArrayList<CoiffeurSummary.Hint> hints = Summary.calculate(bundle);

        assertEquals(2, hints.size());
        check(hints.get(0), CoiffeurSummary.HintType.winPoints, Team.T2, 100);
    }

    @Test
    public final void test_2Teams_BothWinWithMatches() {
        pts[0] = 200;
        pts[1] = 200;
        ArrayList<Integer> factors1 = new ArrayList<>(Arrays.asList(1, 2, 3));
        ArrayList<Integer> factors2 = new ArrayList<>(Arrays.asList(1, 2, 3));
        Bundle bundle = setupBundle(257, 0, 1, factors1, factors2);
        ArrayList<CoiffeurSummary.Hint> hints = Summary.calculate(bundle);

        assertEquals(2, hints.size());
        check(hints.get(0), CoiffeurSummary.HintType.winPoints, Team.T1, 257);
        check(hints.get(1), CoiffeurSummary.HintType.winPoints, Team.T2, 257);
    }

    @Test
    public final void test_2Teams_winWithAvgOrMatch() {
        pts[0] = 450;
        pts[1] = 200;
        ArrayList<Integer> factors1 = new ArrayList<>(Collections.singletonList(3));
        ArrayList<Integer> factors2 = new ArrayList<>(Arrays.asList(1, 4));
        Bundle bundle = setupBundle(257, 0, 1, factors1, factors2);
        ArrayList<CoiffeurSummary.Hint> hints = Summary.calculate(bundle);

        assertEquals(2, hints.size());
        check(hints.get(0), CoiffeurSummary.HintType.winPoints, Team.T2, 205);
        check(hints.get(1), CoiffeurSummary.HintType.winWithMatch, Team.T2, 4);
    }

    @Test
    public final void test_2Teams_pointsNotLosing() {
        pts[0] = 450;
        pts[1] = 200;
        ArrayList<Integer> factors1 = new ArrayList<>(Arrays.asList(3, 4));
        ArrayList<Integer> factors2 = new ArrayList<>(Arrays.asList(1, 4));
        Bundle bundle = setupBundle(257, 0, 1, factors1, factors2);
        ArrayList<CoiffeurSummary.Hint> hints = Summary.calculate(bundle);

        assertEquals(2, hints.size());
        check(hints.get(0), CoiffeurSummary.HintType.winPoints, Team.T1, 148);
        check(hints.get(1), CoiffeurSummary.HintType.pointsNotLose, Team.T2, 50);
    }

    @Test
    public final void test_2Teams_matchNotLosing() {
        pts[0] = 1250;
        pts[1] = 200;
        ArrayList<Integer> factors1 = new ArrayList<>(Collections.singletonList(2));
        ArrayList<Integer> factors2 = new ArrayList<>(Arrays.asList(1, 4));
        Bundle bundle = setupBundle(257, 0, 1, factors1, factors2);
        ArrayList<CoiffeurSummary.Hint> hints = Summary.calculate(bundle);

        assertEquals(3, hints.size());
        check(hints.get(0), CoiffeurSummary.HintType.winPointsSingle, Team.T1, 2, 118);
        check(hints.get(1), CoiffeurSummary.HintType.matchNotLose, Team.T2, 4);
        check(hints.get(2), CoiffeurSummary.HintType.pointsNotLoseOther, Team.T2, 22);
    }

    @Test
    public final void test_2Teams_lost() {
        pts[0] = 2250;
        pts[1] = 500;
        ArrayList<Integer> factors1 = new ArrayList<>(Collections.singletonList(2));
        ArrayList<Integer> factors2 = new ArrayList<>(Arrays.asList(1, 4));
        Bundle bundle = setupBundle(257, 0, 1, factors1, factors2);
        ArrayList<CoiffeurSummary.Hint> hints = Summary.calculate(bundle);

        assertEquals(1, hints.size());
        check(hints.get(0), CoiffeurSummary.HintType.lost, Team.T2, (pts[0] - pts[1] - 200) / 5);
    }

    @Test
    public final void test_2Teams_157ppm() {
        pts[0] = 800;
        pts[1] = 500;
        ArrayList<Integer> factors1 = new ArrayList<>(Collections.singletonList(2));
        ArrayList<Integer> factors2 = new ArrayList<>(Arrays.asList(1, 4));
        Bundle bundle = setupBundle(157, 0, 1, factors1, factors2);
        ArrayList<CoiffeurSummary.Hint> hints = Summary.calculate(bundle);

        assertEquals(2, hints.size());
        check(hints.get(0), CoiffeurSummary.HintType.winPoints, Team.T2, 123);
        check(hints.get(1), CoiffeurSummary.HintType.winPointsSingle, Team.T2, 4, 154);
    }

    @Test
    public final void test_2Teams_200ppm() {
        pts[0] = 800;
        pts[1] = 500;
        ArrayList<Integer> factors1 = new ArrayList<>(Collections.singletonList(2));
        ArrayList<Integer> factors2 = new ArrayList<>(Arrays.asList(1, 4));
        Bundle bundle = setupBundle(200, 0, 1, factors1, factors2);
        ArrayList<CoiffeurSummary.Hint> hints = Summary.calculate(bundle);

        assertEquals(2, hints.size());
        check(hints.get(0), CoiffeurSummary.HintType.winPoints, Team.T2, 140);
        check(hints.get(1), CoiffeurSummary.HintType.winWithMatch, Team.T2, 4);
    }

    @Test
    public final void test_2Teams_bonus50() {
        pts[0] = 800;
        pts[1] = 500;
        ArrayList<Integer> factors1 = new ArrayList<>(Collections.singletonList(2));
        ArrayList<Integer> factors2 = new ArrayList<>(Arrays.asList(1, 4));
        Bundle bundle = setupBundle(157, 50, 1, factors1, factors2);
        ArrayList<CoiffeurSummary.Hint> hints = Summary.calculate(bundle);

        assertEquals(2, hints.size());
        check(hints.get(0), CoiffeurSummary.HintType.winPoints, Team.T2, 133);
        check(hints.get(1), CoiffeurSummary.HintType.winWithMatch, Team.T2, 4);
    }

    @Test
    public final void test_2Teams_bonus_open() {
        pts[0] = 800;
        pts[1] = 500;
        ArrayList<Integer> factors1 = new ArrayList<>(Arrays.asList(1, 2));
        ArrayList<Integer> factors2 = new ArrayList<>(Arrays.asList(1, 4));
        Bundle bundle = setupBundle(157, 50, 1, factors1, factors2);
        ArrayList<CoiffeurSummary.Hint> hints = Summary.calculate(bundle);

        assertEquals(1, hints.size());
        check(hints.get(0), CoiffeurSummary.HintType.pointsNotLose, Team.T2, 60);
    }

    @Test
    public final void test_2Teams_bonus() {
        pts[0] = 800;
        pts[1] = 500;
        ArrayList<Integer> factors1 = new ArrayList<>(Arrays.asList(1, 2));
        ArrayList<Integer> factors2 = new ArrayList<>(Collections.singletonList(4));
        Bundle bundle = setupBundle(157, 50, 1, factors1, factors2);
        ArrayList<CoiffeurSummary.Hint> hints = Summary.calculate(bundle);

        assertEquals(2, hints.size());
        check(hints.get(0), CoiffeurSummary.HintType.winPoints, Team.T1, 126);
        check(hints.get(1), CoiffeurSummary.HintType.pointsNotLose, Team.T2, 75);
    }

    @Test
    public final void test_3Teams_countermatch_needed() {
        pts[0] = 250;
        pts[1] = 800;
        pts[2] = 1000;
        ArrayList<Integer> factors1 = new ArrayList<>(Collections.singletonList(2));
        ArrayList<Integer> factors2 = new ArrayList<>(Arrays.asList(1, 2));
        ArrayList<Integer> factors3 = new ArrayList<>(Arrays.asList(1, 4));
        Bundle bundle = setupBundle(257, 0, 1, factors1, factors2, factors3);
        ArrayList<CoiffeurSummary.Hint> hints = Summary.calculate(bundle);

        assertEquals(5, hints.size());
        check(hints.get(0), CoiffeurSummary.HintType.counterMatch, Team.T1, 0);
        check(hints.get(1), CoiffeurSummary.HintType.matchNotLose, Team.T1, 2);
        check(hints.get(2), CoiffeurSummary.HintType.pointsNotLose, Team.T2, 67);
        check(hints.get(3), CoiffeurSummary.HintType.winPoints, Team.T3, 115);
        check(hints.get(4), CoiffeurSummary.HintType.winWithMatch, Team.T3, 4);
    }

    @Test
    public final void test_3Teams_lost() {
        pts[0] = 250;
        pts[1] = 780;
        pts[2] = 1000;
        ArrayList<Integer> factors1 = new ArrayList<>(Collections.singletonList(2));
        ArrayList<Integer> factors2 = new ArrayList<>(Collections.singletonList(2));
        ArrayList<Integer> factors3 = new ArrayList<>(Collections.singletonList(2));
        Bundle bundle = setupBundle(257, 0, 1, factors1, factors2, factors3);
        ArrayList<CoiffeurSummary.Hint> hints = Summary.calculate(bundle);

        assertEquals(3, hints.size());
        check(hints.get(0), CoiffeurSummary.HintType.lost, Team.T1, 550 / 2);
        check(hints.get(1), CoiffeurSummary.HintType.pointsNotLose, Team.T2, 110);
        check(hints.get(2), CoiffeurSummary.HintType.winPointsSingle, Team.T3, 2, 147);
    }

    @Test
    public final void test_2Teams_rounded_2open_won() {
        pts[0] = 200;
        pts[1] = 1000;
        ArrayList<Integer> factors1 = new ArrayList<>(Arrays.asList(1, 2));
        ArrayList<Integer> factors2 = new ArrayList<>();
        Bundle bundle = setupBundle(257, 0, 0.1f, factors1, factors2);
        ArrayList<CoiffeurSummary.Hint> hints = Summary.calculate(bundle);

        assertEquals(1, hints.size());
        check(hints.get(0), CoiffeurSummary.HintType.lost, Team.T1, 266);
    }

    @Test
    public final void test_2Teams_rounded_2open() {
        pts[0] = 200;
        pts[1] = 800;
        ArrayList<Integer> factors1 = new ArrayList<>(Arrays.asList(1, 2));
        ArrayList<Integer> factors2 = new ArrayList<>();
        Bundle bundle = setupBundle(257, 0, 0.1f, factors1, factors2);
        ArrayList<CoiffeurSummary.Hint> hints = Summary.calculate(bundle);

        assertEquals(1, hints.size());
        check(hints.get(0), CoiffeurSummary.HintType.lost, Team.T1, 200);
    }

    @Test
    public final void test_2Teams_rounded_1open_winWithCounterMatch() {
        pts[0] = 700;
        pts[1] = 710;
        ArrayList<Integer> factors1 = new ArrayList<>();
        ArrayList<Integer> factors2 = new ArrayList<>(Collections.singletonList(2));
        Bundle bundle = setupBundle(257, 0, 0.1f, factors1, factors2);
        ArrayList<CoiffeurSummary.Hint> hints = Summary.calculate(bundle);

        assertEquals(2, hints.size());
        check(hints.get(0), CoiffeurSummary.HintType.counterMatch, Team.T1, 0);
        check(hints.get(1), CoiffeurSummary.HintType.winPointsSingle, Team.T2, 2, 0);
    }

    @Test
    public final void test_2Teams_1open_bonus_winWithCounterMatch() {
        pts[0] = 700;
        pts[1] = 790;
        ArrayList<Integer> factors1 = new ArrayList<>();
        ArrayList<Integer> factors2 = new ArrayList<>(Collections.singletonList(4));
        Bundle bundle = setupBundle(257, 100, 1f, factors1, factors2);
        ArrayList<CoiffeurSummary.Hint> hints = Summary.calculate(bundle);

        assertEquals(2, hints.size());
        check(hints.get(0), CoiffeurSummary.HintType.counterMatch, Team.T1, 0);
        check(hints.get(1), CoiffeurSummary.HintType.winPointsSingle, Team.T2, 4, 0);
    }

    @Test
    public final void test_2Teams_1open_bonus_winWithMatch() {
        pts[0] = 437;
        pts[1] = 646;
        ArrayList<Integer> factors1 = new ArrayList<>(Collections.singletonList(10));
        ArrayList<Integer> factors2 = new ArrayList<>();
        Bundle bundle = setupBundle(157, 500, 0.1f, factors1, factors2);
        ArrayList<CoiffeurSummary.Hint> hints = Summary.calculate(bundle);

        assertEquals(1, hints.size());
        check(hints.get(0), CoiffeurSummary.HintType.winWithMatch, Team.T1, 10);
    }


    @Test
    public final void test_2Teams_2open_bonus_winWithMatch() {
        pts[0] = 400;
        pts[1] = 646;
        ArrayList<Integer> factors1 = new ArrayList<>(Arrays.asList(4, 10));
        ArrayList<Integer> factors2 = new ArrayList<>();
        Bundle bundle = setupBundle(157, 500, 0.1f, factors1, factors2);
        ArrayList<CoiffeurSummary.Hint> hints = Summary.calculate(bundle);

        assertEquals(2, hints.size());
        check(hints.get(0), CoiffeurSummary.HintType.matchNotLose, Team.T1, 4);
        check(hints.get(1), CoiffeurSummary.HintType.pointsNotLoseOther, Team.T1, 14);
    }

    @Test
    public final void test_2Teams_3open_bonus_winWith2Match() {
        pts[0] = 735;
        pts[1] = 286;
        ArrayList<Integer> factors1 = new ArrayList<>();
        ArrayList<Integer> factors2 = new ArrayList<>(Arrays.asList(6, 7, 10));
        Bundle bundle = setupBundle(157, 500, 0.1f, factors1, factors2);
        ArrayList<CoiffeurSummary.Hint> hints = Summary.calculate(bundle);

        assertEquals(3, hints.size());
        check(hints.get(0), CoiffeurSummary.HintType.matchNotLose, Team.T2, 10);
        check(hints.get(1), CoiffeurSummary.HintType.matchNotLose, Team.T2, 6);
        check(hints.get(2), CoiffeurSummary.HintType.pointsNotLoseOther, Team.T2, 14);
    }

    public enum Team {
        T1, T2, T3
    }
}
