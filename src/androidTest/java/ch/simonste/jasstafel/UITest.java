package ch.simonste.jasstafel;


import android.accessibilityservice.AccessibilityService;
import android.app.UiAutomation;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import android.view.InputDevice;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.preference.PreferenceManager;
import androidx.test.espresso.NoMatchingViewException;
import androidx.test.espresso.PerformException;
import androidx.test.espresso.UiController;
import androidx.test.espresso.ViewAction;
import androidx.test.espresso.action.CoordinatesProvider;
import androidx.test.espresso.action.GeneralClickAction;
import androidx.test.espresso.action.GeneralSwipeAction;
import androidx.test.espresso.action.Press;
import androidx.test.espresso.action.Swipe;
import androidx.test.espresso.action.Tap;
import androidx.test.espresso.matcher.BoundedMatcher;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.internal.util.Checks;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;

import java.util.Locale;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.longClick;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.PreferenceMatchers.withTitle;
import static androidx.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withChild;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withResourceName;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static junit.framework.TestCase.fail;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.not;

public abstract class UITest {

    @Rule
    public final ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule<>(MainActivity.class);

    protected static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }

    protected static Matcher<View> settingsSummary(final int setting) {
        return allOf(withId(android.R.id.summary), withParent(withParent(withChild(withChild(withText(setting))))));
    }

    protected static Matcher<View> settingsCheckbox(final int setting) {
        return allOf(withResourceName("checkbox"), withParent(withParent(withChild(withChild(withText(setting))))));
    }

    protected static Matcher<View> withTextColor(final int color) {
        Checks.checkNotNull(color);
        return new BoundedMatcher<View, TextView>(TextView.class) {
            @Override
            public boolean matchesSafely(TextView textView) {
                return color == textView.getCurrentTextColor();
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("with text color: ");
            }
        };
    }

    protected static Matcher<View> gravity(final int gravity) {
        return new BoundedMatcher<View, View>(View.class) {
            private int isGravity;

            @Override
            public boolean matchesSafely(View view) {
                FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) view.getLayoutParams();
                isGravity = layoutParams.gravity;
                return ((layoutParams.gravity & gravity) == gravity);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("gravity: " + gravity + " is: " + isGravity);
            }
        };
    }

    protected static Matcher<View> withDrawable(final int drawableId) {
        return new BoundedMatcher<View, ImageView>(ImageView.class) {

            @Override
            public boolean matchesSafely(ImageView view) {
                Drawable drawable = ContextCompat.getDrawable(getInstrumentation().getTargetContext(), drawableId);
                Drawable ivDrawable = view.getDrawable();
                if (ivDrawable == null || drawable == null) {
                    return (ivDrawable == drawable);
                }
                if (drawable instanceof BitmapDrawable) {
                    Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                    Bitmap otherBitmap = ((BitmapDrawable) ivDrawable).getBitmap();
                    return bitmap.sameAs(otherBitmap);
                }
                return false;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("with background drawable: " + drawableId);
            }
        };
    }

    protected static Matcher<View> withBackground(final int drawableId) {
        return new BoundedMatcher<View, TextView>(TextView.class) {

            @Override
            public boolean matchesSafely(TextView textView) {
                Drawable drawable = ContextCompat.getDrawable(getInstrumentation().getTargetContext(), drawableId);
                Drawable twDrawable = textView.getBackground();
                if (twDrawable == null || drawable == null) {
                    return (twDrawable == drawable);
                }
                if (drawable.getConstantState() == twDrawable.getConstantState()) {
                    return true;
                }
                return true; // TODO: check if it is the same drawable
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("with background drawable: " + drawableId);
            }
        };
    }

    protected String getText(final Matcher<View> matcher) {
        final String[] twString = new String[1];
        onView(matcher).perform(new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return isAssignableFrom(TextView.class);
            }

            @Override
            public String getDescription() {
                return "getting text from a TextView";
            }

            @Override
            public void perform(UiController uiController, View view) {
                TextView tv = (TextView) view; //Save, because of check in getConstraints()
                twString[0] = tv.getText().toString();
            }
        });
        return twString[0];
    }

    protected void setWhoIsNext(int roundNo, int plId, int prevPlId) {
        String text = getInstrumentation().getTargetContext().getResources().getQuantityString(R.plurals.rounds, roundNo, roundNo);
        final int drawableId = R.drawable.border;
        onView(allOf(withId(R.id.subtitle), isDisplayed())).check(matches(withText(text)));
        if (prevPlId != 0)
            onView(withId(prevPlId)).check(matches(withBackground(drawableId)));
        onView(withId(plId)).perform(longClick());
        onView(allOf(withId(plId), isDisplayed())).check(matches(withBackground(drawableId)));
        if (prevPlId != 0 && prevPlId != plId) {
            onView(allOf(withId(prevPlId), isDisplayed())).check(matches(not(withBackground(drawableId))));
        }
        pressBack();
    }

    public void initPreferences(String preferenceName) {
        PreferenceManager.getDefaultSharedPreferences(getInstrumentation().getTargetContext()).edit().clear().apply();
        InstrumentationRegistry.getInstrumentation().getTargetContext().getSharedPreferences(preferenceName, Context.MODE_PRIVATE).edit().clear().commit();
        InstrumentationRegistry.getInstrumentation().getTargetContext().getSharedPreferences(preferenceName + "Profiles", Context.MODE_PRIVATE).edit().clear().commit();
    }

    protected void deleteProfile(String profileName) {
        onView(allOf(withId(R.id.action_settings), withContentDescription(R.string.settings), isDisplayed())).perform(click());
        onData(withTitle(R.string.profiles)).perform(click());

        try {
            onView(withText(profileName));
            onView(withText("Standard")).perform(click());
            onView(withText(profileName)).perform(longClick());
            onView(withId(R.id.action_delete)).perform(click());
        } catch (NoMatchingViewException e) {
            // profile does not exist
        }
        pressBack();
        pressBack();
    }

    protected ViewAction clickUpDn() {
        return new GeneralClickAction(
                Tap.SINGLE,
                new UpsideDownCoordinatesProvider(),
                Press.FINGER);
    }

    protected ViewAction swipeTo(final int viewId2) {
        return new GeneralSwipeAction(
                Swipe.FAST,
                new CustomCoordinatesProvider(new float[]{0f, 0f}),
                new ViewCoordinatesProvider(viewId2),
                Press.FINGER);
    }

    protected ViewAction touch(final float[] xy) {
        return new GeneralSwipeAction(
                Swipe.FAST,
                new CustomCoordinatesProvider(xy),
                new CustomCoordinatesProvider(xy),
                Press.FINGER);
    }

    protected ViewAction touch(final float[] xy1, final float[] xy2) {
        return new GeneralSwipeAction(
                Swipe.FAST,
                new CustomCoordinatesProvider(xy1),
                new CustomCoordinatesProvider(xy2),
                Press.FINGER);
    }

    protected ViewAction setNumber(final int num) {
        return new ViewAction() {
            @Override
            public void perform(UiController uiController, View view) {
                NumberPicker np = (NumberPicker) view;
                np.setValue(num);

            }

            @Override
            public String getDescription() {
                return "Set the passed number into the NumberPicker";
            }

            @Override
            public Matcher<View> getConstraints() {
                return ViewMatchers.isAssignableFrom(NumberPicker.class);
            }
        };
    }

    protected void enterMultiWindowMode() {
        UiAutomation uiAutomation = InstrumentationRegistry.getInstrumentation().getUiAutomation();
        //enter multi-window mode
        uiAutomation.performGlobalAction(AccessibilityService.GLOBAL_ACTION_TOGGLE_SPLIT_SCREEN);
        try {
            // wait for completion, unfortunately waitForIdle doesn't applicable here
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            // nothing
        }
        //simulate selection of our activity
        MotionEvent motionDown = MotionEvent.obtain(
                SystemClock.uptimeMillis(),
                SystemClock.uptimeMillis(),
                KeyEvent.ACTION_DOWN,
                150, 200, 0);
        motionDown.setSource(InputDevice.SOURCE_TOUCHSCREEN);
        uiAutomation.injectInputEvent(motionDown, true);
        motionDown.recycle();
    }

    protected void selectLanguage(String lang) {
        onData(withTitle(R.string.language)).perform(click());
        switch (lang) {
            case "de":
                onView(withText(R.string.german)).perform(click());
                break;
            case "fr":
                onView(withText(R.string.french)).perform(click());
                break;
            case "en":
                onView(withText(R.string.english)).perform(click());
                break;
            default:
                fail();
        }
        Locale locale = new Locale(lang, lang);
        Locale.setDefault(locale);
        Resources res = getInstrumentation().getContext().getResources();
        Configuration config = res.getConfiguration();
        config.locale = locale;
        res.updateConfiguration(config, res.getDisplayMetrics());
    }

    protected void waitView(Matcher<View> matcher) {
        int i = 0;
        while (true) {
            try {
                onView(allOf(matcher, isDisplayed())).check(matches(isDisplayed()));
                return;
            } catch (NoMatchingViewException | PerformException e) {
                ++i;
                if (i > 20) throw e;
            }
        }
    }

    protected void clickUntilGone(Matcher<View> matcher) {
        while (true) {
            try {
                onView(allOf(matcher, isDisplayed())).perform(click());
            } catch (NoMatchingViewException | PerformException e) {
                return;
            }
        }
    }

    protected void checkAppTitle(int title) {
        onView(childAtPosition(
                allOf(withId(R.id.action_bar), childAtPosition(withId(R.id.action_bar_container), 0))
                , 0))
                .check(matches(withText(title)));

    }

    private class CustomCoordinatesProvider implements CoordinatesProvider {
        private final float[] pos;

        private CustomCoordinatesProvider(final float[] xy) {
            pos = xy;
        }

        @Override
        public float[] calculateCoordinates(View view) {
            final int[] xy = new int[2];
            view.getLocationOnScreen(xy);
            final float x = xy[0] + pos[0] * view.getWidth();
            final float y = xy[1] + pos[1] * view.getHeight();
            return new float[]{x, y};
        }
    }

    private class UpsideDownCoordinatesProvider implements CoordinatesProvider {
        @Override
        public float[] calculateCoordinates(View view) {
            final int[] xy = new int[2];
            view.getLocationOnScreen(xy);
            Rect visibleParts = new Rect();
            view.getGlobalVisibleRect(visibleParts);
            final float x = xy[0] - (visibleParts.width() - 1) / 2.0f;
            final float y = xy[1] - (visibleParts.height() - 1) / 2.0f;
            return new float[]{x, y};
        }
    }

    private class ViewCoordinatesProvider implements CoordinatesProvider {
        private final int resId;

        private ViewCoordinatesProvider(final int rId) {
            resId = rId;
        }

        @Override
        public float[] calculateCoordinates(View view) {
            final int[] xy = new int[2];
            view.getRootView().findViewById(resId).getLocationOnScreen(xy);
            return new float[]{xy[0], xy[1]};
        }
    }
}