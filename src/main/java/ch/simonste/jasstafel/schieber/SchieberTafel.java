package ch.simonste.jasstafel.schieber;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.PointF;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.preference.PreferenceManager;

import org.jetbrains.annotations.NotNull;

import java.util.Locale;

import ch.simonste.helpers.NumberTextView;
import ch.simonste.helpers.ScreenPct;
import ch.simonste.helpers.SimpleInputDialog;
import ch.simonste.jasstafel.PreferenceConverterV2;
import ch.simonste.jasstafel.R;
import ch.simonste.jasstafel.RoundDuration;
import ch.simonste.jasstafel.common.VibraHelper;

public class SchieberTafel extends Fragment
        implements View.OnTouchListener, SimpleInputDialog.SimpleDialogListener {
    private static final int TEAM_1 = SchieberActivity.TEAM_1;
    private static final int DIALOG_MODIFY_TEAM1 = TEAM_1;
    private static final int TEAM_2 = SchieberActivity.TEAM_2;
    private static final int DIALOG_MODIFY_TEAM2 = TEAM_2;
    private static final int DIALOG_MODIFY_GOAL = 2;
    private static final int DIALOG_MODIFY_GOAL1 = 3;
    private static final int DIALOG_MODIFY_GOAL_ROUNDS = 4;
    private static final SchiebertafelTeam[] teamView = new SchiebertafelTeam[2];
    private static final SchiebertafelTeamData[] team = new SchiebertafelTeamData[2];
    private final String[] teamName = new String[2];
    private SharedPreferences defaultPrefs;
    private SharedPreferences schieberPrefs;
    private LinearLayout mainView;
    private boolean goalTypeRounds;
    private boolean differentGoals;
    private int goalRounds;
    private int pointsPerRound;
    private boolean winner = false;
    private int winnerteam = 0;
    private boolean allowTouch = true;
    private long lastTimeNameChanged;
    private boolean undoButton = false;
    private RoundDuration roundDuration;
    private boolean bigScore = false;
    private String activeProfile;

    public SchieberTafel() {
        team[TEAM_1] = new SchiebertafelTeamData();
        team[TEAM_2] = new SchiebertafelTeamData();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LinearLayout mainLayout = (LinearLayout) inflater.inflate(R.layout.schiebertafel, container, false);

        teamView[TEAM_1] = mainLayout.findViewById(R.id.upperTeam);
        teamView[TEAM_2] = mainLayout.findViewById(R.id.lowerTeam);
        teamView[TEAM_1].setData(team[TEAM_1], this);
        teamView[TEAM_2].setData(team[TEAM_2], this);
        mainView = mainLayout.findViewById(R.id.main);

        roundDuration = new RoundDuration(schieberPrefs);

        teamView[TEAM_1].setOnTouchListener(this);
        teamView[TEAM_2].setOnTouchListener(this);
        mainView.findViewById(R.id.info_undo).setOnTouchListener(this);
        mainView.findViewById(R.id.action_settings).setOnTouchListener(this);
        mainView.findViewById(R.id.goal).setOnTouchListener(this);
        mainView.findViewById(R.id.goal1).setOnTouchListener(this);
        TextView teamName1 = mainView.findViewById(R.id.teamName1);
        teamName1.setOnTouchListener(this);
        TextView teamName2 = mainView.findViewById(R.id.teamName2);
        teamName2.setOnTouchListener(this);

        ScreenPct screenPct = new ScreenPct(((Activity) getContext()).getWindowManager().getDefaultDisplay());
        float fontsize = 2.5f * screenPct.getPct();
        teamName1.setTextSize(TypedValue.COMPLEX_UNIT_PX, fontsize);
        teamName2.setTextSize(TypedValue.COMPLEX_UNIT_PX, fontsize);
        ((TextView) mainView.findViewById(R.id.teamPoints1)).setTextSize(TypedValue.COMPLEX_UNIT_PX, 1.1f * fontsize);
        ((TextView) mainView.findViewById(R.id.teamPoints2)).setTextSize(TypedValue.COMPLEX_UNIT_PX, 1.1f * fontsize);
        ((TextView) mainView.findViewById(R.id.goal1)).setTextSize(TypedValue.COMPLEX_UNIT_PX, 1.4f * fontsize);
        ((TextView) mainView.findViewById(R.id.goal)).setTextSize(TypedValue.COMPLEX_UNIT_PX, 1.4f * fontsize);

        return mainLayout;
    }

    @TargetApi(23)
    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        if (context instanceof Activity) {
            loadPreferences();
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(@NotNull Activity activity) {
        super.onAttach(activity);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            loadPreferences();
        }

    }

    private void loadPreferences() {
        defaultPrefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        schieberPrefs = getContext().getSharedPreferences("Schieber", Context.MODE_PRIVATE);
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferences();
        SchieberHistory history = ((SchieberActivity) getContext()).getHistory();
        checkforwinner(history);
        team[TEAM_1].restore();
        team[TEAM_2].restore();
    }

    private void getPreferences() {
        ((SchieberActivity) getContext()).allowBackside(defaultPrefs.getBoolean("backside", false));

        String profile = ((Activity) getContext()).getSharedPreferences("SchieberProfiles", Context.MODE_PRIVATE).getString("ActiveProfile", "Standard");
        if (activeProfile != null && !profile.equals(activeProfile)) {
            lastTimeNameChanged = getTimestamp();
        }
        activeProfile = profile;

        setTeamName(schieberPrefs.getString("TeamName1", getString(R.string.defteam1)), TEAM_1);
        setTeamName(schieberPrefs.getString("TeamName2", getString(R.string.defteam2)), TEAM_2);

        allowTouch = defaultPrefs.getBoolean("allowTouch", true);
        bigScore = defaultPrefs.getBoolean("bigScore", false);
        undoButton = defaultPrefs.getBoolean("undoButton", false);
        if (undoButton) {
            ((ImageView) mainView.findViewById(R.id.info_undo)).setImageResource(R.drawable.ic_action_undo);
        } else {
            ((ImageView) mainView.findViewById(R.id.info_undo)).setImageResource(R.drawable.ic_info_dark);
        }
        pointsPerRound = Integer.valueOf(defaultPrefs.getString("PointsPerRound", "157"));
        goalTypeRounds = (defaultPrefs.getString("goal", getString(R.string.goalpoints)).equals(getString(R.string.rounds)));
        differentGoals = defaultPrefs.getBoolean("differentGoals", false);
        if (differentGoals && !goalTypeRounds) {
            mainView.findViewById(R.id.goal1).setVisibility(View.VISIBLE);
            setGoalPoints(Integer.parseInt(schieberPrefs.getString("GoalPoints", "2500")),
                    Integer.parseInt(schieberPrefs.getString("GoalPoints1", "2500")));
        } else {
            mainView.findViewById(R.id.goal1).setVisibility(View.GONE);
            if (goalTypeRounds) {
                setGoalRounds(Integer.parseInt(schieberPrefs.getString("GoalRounds", "8")));
            } else {
                setGoalPoints(Integer.parseInt(schieberPrefs.getString("GoalPoints", "2500")), 0);
            }
        }

        if (defaultPrefs.getBoolean("keepScreenOn", false)) {
            ((Activity) getContext()).getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        } else {
            ((Activity) getContext()).getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }

        invalidate();
    }

    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            switch (v.getId()) {
                case R.id.teamName1:
                    showDialogSimple(DIALOG_MODIFY_TEAM1, teamName[TEAM_1]);
                    break;
                case R.id.teamName2:
                    showDialogSimple(DIALOG_MODIFY_TEAM2, teamName[TEAM_2]);
                    break;
                case R.id.goal:
                    if (goalTypeRounds) {
                        showDialogSimple(DIALOG_MODIFY_GOAL_ROUNDS, goalRounds);
                    } else {
                        showDialogSimple(DIALOG_MODIFY_GOAL, ((NumberTextView) v).getInt());
                    }
                    break;
                case R.id.goal1:
                    showDialogSimple(DIALOG_MODIFY_GOAL1, Integer.parseInt((String) ((TextView) v).getText()));
                    break;
                case R.id.info_undo:
                    if (undoButton)
                        quickundo();
                    else
                        showHistory();
                    break;
                case R.id.action_settings:
                    getActivity().startActivityForResult(new Intent(getActivity(), SchieberPreferences.class), 2);
                    break;
            }
        }
        if (event.getAction() == MotionEvent.ACTION_UP) {
            SchiebertafelTeam team;
            try {
                team = (SchiebertafelTeam) v;
            } catch (ClassCastException e) {
                return false;
            }

            PointF Ratio = new PointF(event.getX() / v.getWidth(), event.getY() / v.getHeight());

            if (team.isUpperTeam) {
                Ratio.x = 1 - Ratio.x;
                Ratio.y = 1 - Ratio.y;
            }
            team.performClick();

            if (Ratio.x > 0.7 && Ratio.y > 0.7) {
                if (team.isUpperTeam)
                    showDialogPoints(DIALOG_MODIFY_TEAM1);
                else
                    showDialogPoints(DIALOG_MODIFY_TEAM2);
            } else if (allowTouch) {
                if (team.isUpperTeam)
                    addPoints(team.onTouch(Ratio), 0);
                else
                    addPoints(0, team.onTouch(Ratio));
                SchieberHistory history = ((SchieberActivity) getActivity()).getHistory();
                checkforwinner(history);
            }
        }
        return true;
    }

    private long getTimestamp() {
        return System.currentTimeMillis() / 1000;
    }

    private void showDialogSimple(int dialogId, int value) {
        FragmentManager fm = ((Activity) getContext()).getFragmentManager();
        SimpleInputDialog dialog = new SimpleInputDialog();
        dialog.setArguments(dialogId, SimpleInputDialog.DIALOG_INPUT_NUMBER, value);
        dialog.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        dialog.setTarget(this);
        int title = (dialogId == DIALOG_MODIFY_GOAL_ROUNDS) ? R.string.coif_diffrows : R.string.entergoalpoints;
        dialog.show(fm, getResources().getString(title));
    }

    private void showDialogSimple(int dialogId, String value) {
        FragmentManager fm = ((Activity) getContext()).getFragmentManager();
        SimpleInputDialog dialog = new SimpleInputDialog();
        dialog.setArguments(dialogId, SimpleInputDialog.DIALOG_INPUT_STRING, value);
        dialog.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        dialog.setTarget(this);
        dialog.show(fm, getResources().getString(R.string.enterteamname));
    }

    private void showDialogPoints(int dialogId) {
        FragmentManager fm = ((Activity) getContext()).getFragmentManager();
        SchieberDialog dialog = new SchieberDialog();
        Bundle bundle = new Bundle();
        bundle.putInt("dialogID", dialogId);
        dialog.setArguments(bundle);
        dialog.setTarget(this);
        dialog.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        dialog.show(fm, "SchieberDialog");
    }

    @Override
    public void onFinishDialog(int dialogId, String userInput) {
        switch (dialogId) {
            case DIALOG_MODIFY_TEAM1:
                schieberPrefs.edit().putString("TeamName1", userInput).apply();
                setTeamName(userInput, TEAM_1);
                break;
            case DIALOG_MODIFY_TEAM2:
                schieberPrefs.edit().putString("TeamName2", userInput).apply();
                setTeamName(userInput, TEAM_2);
                break;
        }
    }

    @Override
    public void onFinishDialog(int dialogId, Integer userInput) {
        if (userInput == null || userInput == 0) {
            switch (dialogId) {
                case DIALOG_MODIFY_GOAL:
                    showDialogSimple(DIALOG_MODIFY_GOAL, getGoalPoints(TEAM_2));
                    break;
                case DIALOG_MODIFY_GOAL1:
                    showDialogSimple(DIALOG_MODIFY_GOAL1, getGoalPoints(TEAM_1));
                    break;
                case DIALOG_MODIFY_GOAL_ROUNDS:
                    showDialogSimple(DIALOG_MODIFY_GOAL_ROUNDS, goalRounds);
                    break;
            }
            return;
        }
        switch (dialogId) {
            case DIALOG_MODIFY_GOAL:
                schieberPrefs.edit().putString("GoalPoints", userInput.toString()).apply();
                if (differentGoals) setGoalPoints(userInput, getGoalPoints(TEAM_1));
                else setGoalPoints(userInput, 0);
                break;
            case DIALOG_MODIFY_GOAL1:
                schieberPrefs.edit().putString("GoalPoints1", userInput.toString()).apply();
                setGoalPoints(getGoalPoints(TEAM_2), userInput);
                break;
            case DIALOG_MODIFY_GOAL_ROUNDS:
                schieberPrefs.edit().putString("GoalRounds", userInput.toString()).apply();
                setGoalRounds(userInput);
                break;
        }
    }

    private void setTeamName(String name, int team) {
        if (name.equals("")) {
            if (team == TEAM_1) name = getString(R.string.defteam1);
            else name = getString(R.string.defteam2);
        }
        if (team == TEAM_1)
            ((TextView) mainView.findViewById(R.id.teamName1)).setText(name);
        else
            ((TextView) mainView.findViewById(R.id.teamName2)).setText(name);

        if (!name.equals(teamName[team]) && teamName[team] != null) {
            // Team name has changed
            ((SchieberActivity) getContext()).getSchieberStrichtafel().setTeamName(team, name);

            long now = getTimestamp();
            final int totalPoints = getTotalPoints(TEAM_1) + getTotalPoints(TEAM_2) + schieberPrefs.getInt("Pts1", 0);
            if (now - lastTimeNameChanged > 3 * 60 && totalPoints > 0) {
                ((SchieberActivity) getContext()).resetDialog(SchieberActivity.ResetReason.ChangedName);
                lastTimeNameChanged = now;
            }
        }
        teamName[team] = name;
    }

    private void setGoalPoints(final int goalPoints, final int goalPoints1) {
        NumberTextView goal = mainView.findViewById(R.id.goal);
        goal.setInt(goalPoints);
        if (goalPoints1 > 0) {
            TextView goal1 = mainView.findViewById(R.id.goal1);
            goal1.setText(String.format(Locale.getDefault(), "%d", goalPoints1));
            ((ProgressBar) mainView.findViewById(R.id.progressTeam1A)).setMax(goalPoints1);
            ((ProgressBar) mainView.findViewById(R.id.progressTeam1B)).setMax(goalPoints1);
        } else {
            ((ProgressBar) mainView.findViewById(R.id.progressTeam1A)).setMax(goalPoints);
            ((ProgressBar) mainView.findViewById(R.id.progressTeam1B)).setMax(goalPoints);
        }
        ((ProgressBar) mainView.findViewById(R.id.progressTeam2A)).setMax(goalPoints);
        ((ProgressBar) mainView.findViewById(R.id.progressTeam2B)).setMax(goalPoints);
    }

    private void setGoalRounds(int GoalRounds) {
        goalRounds = GoalRounds;
        NumberTextView goal = mainView.findViewById(R.id.goal);
        SchieberHistory history = ((SchieberActivity) getContext()).getHistory();
        final int roundsPlayed = new SchieberHistoryHelper(pointsPerRound).rounds(history);
        goal.setText(getResources().getQuantityString(R.plurals.goalrounds, goalRounds, goalRounds, roundsPlayed));
        ((ProgressBar) mainView.findViewById(R.id.progressTeam1A)).setMax(0);
        ((ProgressBar) mainView.findViewById(R.id.progressTeam1B)).setMax(0);
        ((ProgressBar) mainView.findViewById(R.id.progressTeam2A)).setMax(0);
        ((ProgressBar) mainView.findViewById(R.id.progressTeam2B)).setMax(0);
    }

    private int getGoalPoints(int team) {
        if (differentGoals && team == TEAM_1) {
            TextView goal1 = mainView.findViewById(R.id.goal1);
            return Integer.parseInt((String) goal1.getText());
        }
        return ((NumberTextView) mainView.findViewById(R.id.goal)).getInt();
    }

    private int getTotalPoints(int team) {
        return SchieberTafel.team[team].getTotalPoints();
    }

    public void addPoints(int pts1, int pts2) {
        if (pts1 + pts2 == 0) return;

        SchieberHistory history = ((SchieberActivity) getContext()).getHistory();
        if (getTotalPoints(TEAM_1) + getTotalPoints(TEAM_2) == 0) {
            roundDuration.addFirstPoints();
        }

        team[TEAM_1].addPoints(pts1);
        team[TEAM_2].addPoints(pts2);

        history.addAction(pts1, pts2);
        checkforwinner(history);
    }

    public String getTeamName(int id) {
        return teamName[id];
    }


    private void invalidate() {
        teamView[TEAM_1].invalidate();
        teamView[TEAM_2].invalidate();
    }

    public void updatePoints() {
        int p1 = getTotalPoints(TEAM_1);
        int p2 = getTotalPoints(TEAM_2);
        ((ProgressBar) mainView.findViewById(R.id.progressTeam1A)).setProgress(p1);
        ((ProgressBar) mainView.findViewById(R.id.progressTeam1B)).setProgress(p1);
        ((ProgressBar) mainView.findViewById(R.id.progressTeam2A)).setProgress(p2);
        ((ProgressBar) mainView.findViewById(R.id.progressTeam2B)).setProgress(p2);
        if (bigScore) {
            ((TextView) mainView.findViewById(R.id.teamPoints1)).setText("");
            ((TextView) mainView.findViewById(R.id.teamPoints2)).setText("");
        } else {
            ((TextView) mainView.findViewById(R.id.teamPoints1)).setText(String.format(Locale.getDefault(), "%d", p1));
            ((NumberTextView) mainView.findViewById(R.id.teamPoints2)).setInt(p2);
        }
    }

    private void checkforwinner(SchieberHistory history) {

        // update all settings
        PreferenceConverterV2.convert(getContext());

        if (mainView != null) {
            int winner = 0;

            if (goalTypeRounds) {
                winner = checkForWinnerByRounds(history);
            } else {
                winner = checkForWinnerByPoints(history);
            }

            if (!this.winner && winner > 0) {
                this.winner = true;

                if (history.getWinner() > 0) {
                    winnerteam = history.getWinner();
                    return;
                }

                VibraHelper.vibrate((getContext()), 1000);

                history.setWinner(winner);
                roundDuration.roundFinished(true);
                winnerteam = winner;
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
                alertDialog.setTitle(R.string.gameover);
                switch (winner) {
                    case 3:
                        if (goalTypeRounds) {
                            alertDialog.setNeutralButton(android.R.string.ok, null);
                            alertDialog.setMessage(String.format(getString(R.string.winnersrounds), teamName[0], teamName[1]));
                        } else {
                            alertDialog.setNegativeButton(teamName[0],
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            winnerteam = 1;
                                        }
                                    }
                            );
                            alertDialog.setPositiveButton(teamName[1],
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            winnerteam = 2;
                                        }
                                    }
                            );
                            alertDialog.setMessage(R.string.winnerboth);
                        }
                        break;
                    case 2:
                    case 1:
                        alertDialog.setNeutralButton(android.R.string.ok, null);
                        final int textId = (goalTypeRounds ? R.string.winnerrounds : R.string.winner);
                        alertDialog.setMessage(String.format(getString(textId), teamName[winner - 1]));
                        break;
                }
                alertDialog.show();
            }
        } else {
            Log.e("Schieber", "main View is null");
        }
    }

    private int checkForWinnerByPoints(SchieberHistory history) {
        int p1 = getTotalPoints(TEAM_1);
        int p2 = getTotalPoints(TEAM_2);

        if (history.getBergpreis() == 0) {
            int bp = 0;
            if (p1 >= getGoalPoints(TEAM_1) / 2) bp += 1;
            if (p2 >= getGoalPoints(TEAM_2) / 2) bp += 2;

            history.setBergpreis(bp, teamName);
        }

        int winner = 0;

        if (p1 >= getGoalPoints(TEAM_1)) winner += 1;
        if (p2 >= getGoalPoints(TEAM_2)) winner += 2;

        return winner;
    }

    private int checkForWinnerByRounds(SchieberHistory history) {
        final int p1 = getTotalPoints(TEAM_1);
        final int p2 = getTotalPoints(TEAM_2);

        final int roundsPlayed = new SchieberHistoryHelper(pointsPerRound).rounds(history);

        NumberTextView goal = mainView.findViewById(R.id.goal);
        goal.setText(getResources().getQuantityString(R.plurals.goalrounds, goalRounds, goalRounds, roundsPlayed));

        int winner = 0;
        if (roundsPlayed >= goalRounds) {
            if (p1 > p2) winner = 1;
            else if (p2 > p1) winner = 2;
            else winner = 3;
        }
        return winner;
    }

    public void doResetCurrentRound(SchieberHistory history) {
        if (winner) storeCurrentRound();
        team[TEAM_1].reset();
        team[TEAM_2].reset();
        winner = false;
        history.clear();
        checkforwinner(history);
        if (roundDuration != null) {
            roundDuration.reset();
        }
    }

    public void showHistory() {
        FragmentManager fm = ((Activity) getContext()).getFragmentManager();
        SchieberHistoryDialog dialog = new SchieberHistoryDialog();
        dialog.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        if (fm != null)
            dialog.show(fm, "Undo");
    }

    public void showStats() {
        FragmentManager fm = ((Activity) getContext()).getFragmentManager();
        SchieberStats dialog = new SchieberStats();
        Bundle bundle = new Bundle();
        if (roundDuration != null) {
            if (getTotalPoints(TEAM_1) + getTotalPoints(TEAM_2) == 0) {
                // reset duration when no points are written so far
                roundDuration.addFirstPoints();
            }
            bundle.putInt("Duration", roundDuration.getRoundDuration());
        }
        dialog.setArguments(bundle);
        dialog.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        if (fm != null)
            dialog.show(fm, "Stats");
    }

    public int getWinnerTeam() {
        if (winner) return winnerteam;
        return 0;
    }

    private void quickundo() {
        SchieberHistory history = ((SchieberActivity) getContext()).getHistory();
        Point pt = history.undoLast();
        undo(history, pt);
        String text = getString(R.string.undo) + ": ";
        if (pt.x != 0) {
            text += teamName[TEAM_1] + ": " + pt.x + " " + getString(R.string.points);
            if (pt.y != 0) text += ", ";
        }
        if (pt.y != 0) text += teamName[TEAM_2] + ": " + pt.y + " " + getString(R.string.points);

        if ((pt.x + pt.y) != 0) {
            Toast toast = Toast.makeText(getContext(), text, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public void undo(SchieberHistory history, Point pt) {
        team[TEAM_1].addPoints(-pt.x);
        team[TEAM_2].addPoints(-pt.y);

        if (winner) {
            if (getTotalPoints(TEAM_1) < getGoalPoints(TEAM_1)
                    && getTotalPoints(TEAM_2) < getGoalPoints(TEAM_2)) {
                history.setWinner(0);
                roundDuration.roundFinished(false);
                winner = false;
            }
        }
        checkforwinner(history);
    }

    private void storeCurrentRound() {
        SchieberStats stats = new SchieberStats();
        stats.calculateStats((SchieberActivity) getContext());

        SharedPreferences.Editor edit = schieberPrefs.edit();
        edit.putInt("Pts1", schieberPrefs.getInt("Pts1", 0) + getTotalPoints(TEAM_1));
        edit.putInt("Pts2", schieberPrefs.getInt("Pts2", 0) + getTotalPoints(TEAM_2));
        Point Weise = stats.getCurrentRoundWeis();
        edit.putInt("Weis1", schieberPrefs.getInt("Weis1", 0) + Weise.x);
        edit.putInt("Weis2", schieberPrefs.getInt("Weis2", 0) + Weise.y);
        Point Bergpreis = stats.getCurrentRoundBergpreis();
        edit.putInt("Bergpreis1", schieberPrefs.getInt("Bergpreis1", 0) + Bergpreis.x);
        edit.putInt("Bergpreis2", schieberPrefs.getInt("Bergpreis2", 0) + Bergpreis.y);
        Point Matches = stats.getCurrentRoundMatches();
        edit.putInt("Matches1", schieberPrefs.getInt("Matches1", 0) + Matches.x);
        edit.putInt("Matches2", schieberPrefs.getInt("Matches2", 0) + Matches.y);
        edit.putInt("Duration", schieberPrefs.getInt("Duration", 0) + roundDuration.getRoundDuration());

        switch (getWinnerTeam()) {
            case 1:
                edit.putInt("Wins1", schieberPrefs.getInt("Wins1", 0) + 1);
                break;
            case 2:
                edit.putInt("Wins2", schieberPrefs.getInt("Wins2", 0) + 1);
                break;
        }
        edit.apply();
    }

}
