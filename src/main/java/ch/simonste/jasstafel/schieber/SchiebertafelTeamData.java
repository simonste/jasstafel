package ch.simonste.jasstafel.schieber;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;

class SchiebertafelTeamData {
    static final int KEY_1 = 0;
    static final int KEY_20 = 1;
    static final int KEY_50 = 2;
    static final int KEY_100 = 3;
    static final int KEY_500 = 4;
    private static final int[] VALUES = {1, 20, 50, 100, 500};
    private final int[] status = new int[5];
    private SharedPreferences settings;
    private int touchPoints;
    private View view;
    private String prefix;

    public void setView(View view, boolean isUpperTeam) {
        this.view = view;
        prefix = isUpperTeam ? "Points1_" : "Points2_";
        settings = view.getContext().getSharedPreferences("Schieber", Context.MODE_PRIVATE);
        restore();
        redraw();
    }

    public void restore() {
        status[KEY_1] = settings.getInt(prefix + "1", 0);
        status[KEY_20] = settings.getInt(prefix + "20", 0);
        status[KEY_50] = settings.getInt(prefix + "50", 0);
        status[KEY_100] = settings.getInt(prefix + "100", 0);
        status[KEY_500] = settings.getInt(prefix + "500", 0);
    }

    private void backup() {
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(prefix + "1", status[KEY_1]);
        editor.putInt(prefix + "20", status[KEY_20]);
        editor.putInt(prefix + "50", status[KEY_50]);
        editor.putInt(prefix + "100", status[KEY_100]);
        editor.putInt(prefix + "500", status[KEY_500]);
        editor.apply();
    }

    // this amount of points will be added soon, do not rearrange other strokes when adding it
    void setTouchPoints(int touchPoints) {
        this.touchPoints = touchPoints;
    }

    public void addPoints(int points) {
        boolean onTouch = (touchPoints == points);
        touchPoints = 0;

        if (points == 0) return;

        if (onTouch) {
            // add touch points immediately to prevent rearranged strokes
            for (int i = KEY_20; i <= KEY_100; i++) {
                if (points == VALUES[i]) {
                    status[i]++;
                    redraw();
                    return;
                }
            }
        }

        int remainingPoints = points + status[KEY_1];
        int sign = 1;
        if (remainingPoints < 0) sign = -1;

        for (int i = KEY_500; i > 0; i--) {
            int tmp = (sign * remainingPoints / VALUES[i]);
            if (sign == -1) {
                tmp = Math.min(tmp, status[i]);
            }
            status[i] += sign * tmp;
            remainingPoints -= sign * tmp * VALUES[i];
        }

        status[KEY_1] = remainingPoints;
        redraw();
    }

    private void redraw() {
        checkoverflow();
        backup();
        view.invalidate();
    }

    public void reset() {
        for (int i = 0; i < status.length; i++) {
            status[i] = 0;
        }
        redraw();
    }

    public int getPoints(int key) {
        return status[key];
    }

    int getTotalPoints() {
        int sum = 0;
        for (int i = 0; i < VALUES.length; i++) {
            sum += VALUES[i] * status[i];
        }
        return sum;
    }

    private void checkoverflow() {
        if (status[KEY_20] > 35) {
            status[KEY_20] -= 10;
            status[KEY_100] += 2;
        }
        if (status[KEY_50] > 18) {
            status[KEY_50] -= 4;
            status[KEY_100] += 2;
        }
        if (status[KEY_100] > 20) {
            status[KEY_100] -= 5;
            status[KEY_500] += 1;
        }

        while (status[KEY_20] < 0 || status[KEY_50] < 0 || status[KEY_100] < 0 || status[KEY_500] < 0) {
            if (status[KEY_500] < 0) {
                status[KEY_500]++;
                status[KEY_100] -= 5;
            }
            if (status[KEY_100] < 0) {
                if (status[KEY_500] > 0) {
                    status[KEY_100] += 5;
                    status[KEY_500]--;
                } else {
                    status[KEY_100]++;
                    status[KEY_50] -= 2;
                }
            }
            if (status[KEY_50] < 0) {
                status[KEY_50] += 2;
                if (status[KEY_100] > 0) {
                    status[KEY_100]--;
                } else {
                    status[KEY_20] -= 5;
                }
            }
            if (status[KEY_20] < 0) {
                if (status[KEY_100] > 0) {
                    status[KEY_20] += 5;
                    status[KEY_100]--;
                } else {
                    status[KEY_50]--;
                    if (status[KEY_1] < 0) {
                        status[KEY_20] += 2;
                        status[KEY_1] += 10;
                    } else {
                        status[KEY_20] += 3;
                        status[KEY_1] -= 10;
                    }
                }
            }

            if (getTotalPoints() < 0) {
                for (int i = 0; i < VALUES.length; i++) {
                    status[i] = 0;
                }
                break;
            }
        }
    }
}
