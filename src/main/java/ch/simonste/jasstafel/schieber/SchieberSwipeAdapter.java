package ch.simonste.jasstafel.schieber;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import org.jetbrains.annotations.NotNull;

class SchieberSwipeAdapter extends FragmentPagerAdapter {
    private final SchieberTafel schieberTafel;
    private final SchieberStrichtafel strichTafel;

    private boolean backside = true;

    public SchieberSwipeAdapter(FragmentManager fm) {
        super(fm);

        schieberTafel = new SchieberTafel();
        strichTafel = new SchieberStrichtafel();
    }

    @NotNull
    @Override
    public Fragment getItem(int i) {
        if (i == 0) {
            return schieberTafel;
        } else {
            return strichTafel;
        }
    }

    public void allowBackside(boolean allow) {
        backside = allow;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if (backside) return 2;
        return 1;
    }
}
