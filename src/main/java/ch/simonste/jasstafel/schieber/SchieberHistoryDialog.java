package ch.simonste.jasstafel.schieber;

import android.app.FragmentManager;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.preference.PreferenceManager;

import java.util.ArrayList;

import ch.simonste.helpers.NumberTextView;
import ch.simonste.helpers.SingleDialog;
import ch.simonste.jasstafel.R;
import ch.simonste.jasstafel.Whobegins;

public class SchieberHistoryDialog extends SingleDialog implements
        android.view.View.OnClickListener {
    private SchieberTafel schieberTafel;
    private View view;
    private LayoutInflater inflater;
    private SchieberHistory history;
    private int roundNo = 0;
    private SchieberHistoryHelper helper;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.inflater = inflater;
        view = inflater.inflate(R.layout.schieberhistory, container);
        getDialog().getWindow().setWindowAnimations(R.style.DialogAnimationFlip);

        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.currentround);
        toolbar.inflateMenu(R.menu.schieberinfomenu);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                SingleDialog.switchDialogs();
                switch (item.getItemId()) {
                    case R.id.action_statistics:
                        schieberTafel.showStats();
                        break;
                    case R.id.action_reset:
                        ((SchieberActivity) getActivity()).resetDialog(SchieberActivity.ResetReason.Round);
                        break;
                    case R.id.action_whobegins:
                        whoBegins();
                        break;
                }
                dismiss();
                return true;
            }
        });

        schieberTafel = ((SchieberActivity) getActivity()).getSchiebertafel();
        ((TextView) view.findViewById(R.id.teamName1)).setText(schieberTafel.getTeamName(SchieberActivity.TEAM_1));
        ((TextView) view.findViewById(R.id.teamName2)).setText(schieberTafel.getTeamName(SchieberActivity.TEAM_2));

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getActivity());
        helper = new SchieberHistoryHelper(Integer.valueOf(settings.getString("PointsPerRound", "157")));

        roundNo = 0;
        history = ((SchieberActivity) getActivity()).getHistory();
        final int size = history.getSize();

        Point roundTotal = new Point(0, 0);
        long prevTS = 0;

        for (int i = 0; i < size; i++) {
            SchieberAction act = history.getAction(i);

            if (helper.isRound(roundTotal.x + roundTotal.y) || helper.isRound(act.pt.x + act.pt.y) || Math.abs(prevTS - act.ts) > helper.Group_Duration || (act.ts == 0/*bugfix wrong preference convertion*/)) {
                roundTotal = new Point(0, 0);
                addRow();
            }
            addAction(act);

            roundTotal.x += act.pt.x;
            roundTotal.y += act.pt.y;
            ((NumberTextView) getLatestRound().findViewById(R.id.roundPts1)).setInt(roundTotal.x);
            ((NumberTextView) getLatestRound().findViewById(R.id.roundPts2)).setInt(roundTotal.y);

            if (helper.isRound(roundTotal.x + roundTotal.y)) {
                roundNo++;
                ((NumberTextView) getLatestRound().findViewById(R.id.roundNo)).setInt(roundNo);
            }

            prevTS = act.ts;
        }
        hideActionsInRoundsWithOnlyOneAction();
        showActionButtonsInLatestRound();

        return view;
    }

    private void hideActionsInRoundsWithOnlyOneAction() {
        LinearLayout historyLayout = view.findViewById(R.id.historylayout);
        for (int i = 0; i < historyLayout.getChildCount(); ++i) {
            LinearLayout round = (LinearLayout) historyLayout.getChildAt(i);
            LinearLayout actions = (LinearLayout) round.findViewById(R.id.actions);
            if (actions.getChildCount() == 1) {
                actions.setVisibility(View.INVISIBLE);
            }
        }
    }

    private LinearLayout getLatestRound() {
        LinearLayout latestRound = null;
        LinearLayout historyLayout = view.findViewById(R.id.historylayout);
        for (int i = 0; i < historyLayout.getChildCount(); ++i) {
            latestRound = (LinearLayout) historyLayout.getChildAt(i);
            int a = latestRound.getVisibility();
            int b = latestRound.getChildAt(0).getVisibility();
            if (latestRound.getChildAt(0).getVisibility() == View.VISIBLE) {
                break;
            }
        }
        return latestRound;
    }

    private ArrayList<LinearLayout> getLatestActions() {
        ArrayList<LinearLayout> latestActions = new ArrayList<>();
        LinearLayout latestRound = getLatestRound();
        if (latestRound != null) {
            LinearLayout actions = (LinearLayout) latestRound.findViewById(R.id.actions);
            for (int i = 0; i < actions.getChildCount(); ++i) {
                LinearLayout action = (LinearLayout) actions.getChildAt(i);
                if (action.getChildAt(0).getVisibility() == View.VISIBLE) {
                    latestActions.add(action);
                }
            }
        }
        return latestActions;
    }

    private void showActionButtonsInLatestRound() {
        LinearLayout latestRound = getLatestRound();
        if (latestRound != null) {
            latestRound.findViewById(R.id.undoRound).setVisibility(View.VISIBLE);
        }

        ArrayList<LinearLayout> actions = getLatestActions();
        if (actions.size() > 1) {
            // remove action only when more than one
            actions.get(0).findViewById(R.id.undoAction).setVisibility(View.VISIBLE);
        }
    }

    private void whoBegins() {
        FragmentManager fm = getFragmentManager();
        Whobegins dialog = new Whobegins();
        String[] teamNames = {schieberTafel.getTeamName(SchieberActivity.TEAM_1), schieberTafel.getTeamName(SchieberActivity.TEAM_2)};
        String[] playerNames = Whobegins.guessPlayerNames(teamNames);

        Bundle bundle = new Bundle();
        bundle.putStringArray(Whobegins.Names_Key, playerNames);
        bundle.putInt(Whobegins.RoundNo_Key, roundNo);
        bundle.putString(Whobegins.Preferences_Key, "Schieber");
        dialog.setArguments(bundle);
        dialog.show(fm, "whoBegins");
    }

    public void onClick(View v) {
        View clicked = (View) v.getParent();

        if (clicked.getId() == R.id.action) {
            if (getLatestActions().size() > 1)
                removelatest(clicked);
            else
                removeRound(getRound(clicked));
        } else {
            removeRound((View) clicked.getParent());
        }
    }

    private void removeRound(View round) {
        ArrayList<LinearLayout> latestActions = getLatestActions();
        while (!latestActions.isEmpty()) {
            removelatest(latestActions.get(0));
            latestActions.remove(0);
        }
        round.setVisibility(View.GONE);
        showActionButtonsInLatestRound();
    }

    private View getRound(View action) {
        return (View) action.getParent().getParent().getParent().getParent();
    }

    private void removelatest(View action) {
        Point pt = history.undoLast();
        schieberTafel.undo(history, pt);

        LinearLayout round = (LinearLayout) getRound(action);
        updateRound(round, pt);

        action.setVisibility(View.GONE);
        showActionButtonsInLatestRound();
    }

    private void updateRound(LinearLayout round, Point pt) {
        final NumberTextView pts1 = round.findViewById(R.id.roundPts1);
        final NumberTextView pts2 = round.findViewById(R.id.roundPts2);
        final int ptsOld1 = pts1.getInt();
        final int ptsOld2 = pts2.getInt();
        pts1.setInt(ptsOld1 - pt.x);
        pts2.setInt(ptsOld2 - pt.y);

        boolean wasRound = helper.isRound(ptsOld1 + ptsOld2);
        if (wasRound) {
            roundNo--;
            ((NumberTextView) round.findViewById(R.id.roundNo)).setText("");
        }
    }

    private void addRow() {
        LinearLayout round = new LinearLayout(getActivity());
        inflater.inflate(R.layout.schieberhistory_round, round);
        LinearLayout container = view.findViewById(R.id.historylayout);
        container.addView(round, 0);
        ImageView undoButton = round.findViewById(R.id.undoRound);
        undoButton.setOnClickListener(this);
    }

    private void addAction(SchieberAction act) {
        LinearLayout action = new LinearLayout(getActivity());
        inflater.inflate(R.layout.schieberhistory_action, action);

        LinearLayout round = getLatestRound();
        LinearLayout container = round.findViewById(R.id.actions);
        container.addView(action, 0);
        ImageView undoButton = action.findViewById(R.id.undoAction);
        undoButton.setOnClickListener(this);

        ((NumberTextView) action.findViewById(R.id.pts1)).setInt(act.pt.x);
        ((NumberTextView) action.findViewById(R.id.pts2)).setInt(act.pt.y);
    }
}
