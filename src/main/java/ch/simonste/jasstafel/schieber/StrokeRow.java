package ch.simonste.jasstafel.schieber;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;


public class StrokeRow extends View {
    private Bitmap bitmap;
    private Canvas canvas;
    private int strokes;
    private int width;
    private int height;
    private Paint strokePaint;

    private boolean isInitialized;

    public StrokeRow(Context context) {
        super(context);
    }

    public StrokeRow(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    public StrokeRow(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void init(int strokes) {
        this.strokes = strokes;
        invalidate();
    }

    public void invalidate() {
        isInitialized = false;
        super.invalidate();
    }

    private void initialize() {
        width = getWidth();
        height = getHeight();

        bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        canvas = new Canvas();
        canvas.setBitmap(bitmap);

        strokePaint = new Paint();
        strokePaint.setColor(Color.WHITE);
        strokePaint.setStrokeWidth((float) width / 50);

        isInitialized = true;
    }

    @Override
    public void onDraw(Canvas canvas) {
        if (!isInitialized) initialize();

        int strokeLength = width / 2;
        if (Math.ceil(strokes / 10.0) * 1.3 * strokeLength > height) {
            strokeLength = (int) (height / (Math.ceil(strokes / 10.0) * 1.3));
        }

        int hoffset = strokeLength / 6;

        for (int i = 0; i < strokes; i++) {
            int x = (i % 10 + 2) * hoffset;
            int y = height / 50 + (int) (strokeLength * 1.3 * (i / 10));

            if ((i + 1) % 5 == 0) {
                this.canvas.drawLine(x, y, x - 5 * hoffset, y + strokeLength, strokePaint);
            } else {
                this.canvas.drawLine(x, y, x, y + strokeLength, strokePaint);
            }
        }
        canvas.drawBitmap(bitmap, 0, 0, strokePaint);
    }

    public int getStrokes() {
        return strokes;
    }

    public void addStroke() {
        strokes++;
        invalidate();
    }

    public void removeStroke() {
        if (strokes > 0) {
            strokes--;
            invalidate();
        }
    }
}

