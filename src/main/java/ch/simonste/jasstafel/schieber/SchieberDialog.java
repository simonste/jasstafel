package ch.simonste.jasstafel.schieber;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.preference.PreferenceManager;

import java.util.Locale;

import ch.simonste.helpers.SingleDialog;
import ch.simonste.jasstafel.R;

public class SchieberDialog extends SingleDialog implements
        android.view.View.OnClickListener,
        AdapterView.OnItemSelectedListener {
    private int points = 0;
    private int pointsOther = 0;
    private int factor = 1;
    private int spinnerCounter = 0;
    private SchieberTafel schieberTafel;
    private int teamID;
    private Resources res;
    private View view;
    private SharedPreferences settings;
    private int pointsperround = 157;
    private boolean match = false;
    private int sign = 1;

    public SchieberDialog() {
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Bundle bundle = this.getArguments();
        teamID = bundle.getInt("dialogID");

        view = inflater.inflate(R.layout.schieberdialog, container);

        res = getResources();

        settings = PreferenceManager.getDefaultSharedPreferences(getActivity());
        pointsperround = Integer.valueOf(settings.getString("PointsPerRound", "157"));

        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setTitle(String.format(res.getString(R.string.pointsof), schieberTafel.getTeamName(teamID)));
        toolbar.inflateMenu(R.menu.schieberdialogmenu);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                flipview(true);
                return true;
            }
        });

        try {
            if (teamID == SchieberActivity.TEAM_1 && settings.getBoolean("flipUpper", true)) {
                flipview(false);
            }
            if (teamID == SchieberActivity.TEAM_2 && settings.getBoolean("flipLower", false)) {
                flipview(false);
            }
        } catch (ClassCastException e) {
            // do not flip
        }

        view.findViewById(R.id.buttonPlusMinus).setOnClickListener(this);
        view.findViewById(R.id.buttonNone).setOnClickListener(this);
        view.findViewById(R.id.buttonMatch).setOnClickListener(this);
        view.findViewById(R.id.button0).setOnClickListener(this);
        view.findViewById(R.id.button1).setOnClickListener(this);
        view.findViewById(R.id.button2).setOnClickListener(this);
        view.findViewById(R.id.button3).setOnClickListener(this);
        view.findViewById(R.id.button4).setOnClickListener(this);
        view.findViewById(R.id.button5).setOnClickListener(this);
        view.findViewById(R.id.button6).setOnClickListener(this);
        view.findViewById(R.id.button7).setOnClickListener(this);
        view.findViewById(R.id.button8).setOnClickListener(this);
        view.findViewById(R.id.button9).setOnClickListener(this);
        view.findViewById(R.id.buttonBackspace).setOnClickListener(this);
        view.findViewById(R.id.buttonEnter).setOnClickListener(this);
        view.findViewById(R.id.buttonWeis).setOnClickListener(this);

        RadioGroup radioGroup = view.findViewById(R.id.radioFactor);
        for (int i = 0; i < radioGroup.getChildCount(); ++i) {
            View view = radioGroup.getChildAt(i);
            if (view.getId() != R.id.spinnerFactor) {
                radioGroup.getChildAt(i).setOnClickListener(this);
            }
        }
        Spinner spinner = view.findViewById(R.id.spinnerFactor);
        if (spinner != null) {
            spinner.setOnItemSelectedListener(this);
        }

        return view;
    }

    public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
        if (spinnerCounter > 0) {
            RadioButton countX = view.findViewById(R.id.countX);
            countX.setChecked(true);
            updatepoints();
        } else {
            spinnerCounter = 1;
        }
    }

    public void setTarget(SchieberTafel target) {
        schieberTafel = target;
    }

    public void onNothingSelected(AdapterView<?> arg0) {
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonWeis:
                pointsOther = 0;
            case R.id.buttonEnter:
                if (teamID == SchieberActivity.TEAM_1) {
                    schieberTafel.addPoints(sign * points * factor, pointsOther * factor);
                } else {
                    schieberTafel.addPoints(pointsOther * factor, sign * points * factor);
                }
                dismiss();
                break;
            case R.id.buttonPlusMinus:
                sign = sign * -1;
                ((TextView) v).setText(sign == 1 ? "+" : "-");
                break;
            case R.id.buttonBackspace:
                points = (points / 10);
                match = false;
                break;
            case R.id.button0:
                keyinput(0);
                break;
            case R.id.button1:
                keyinput(1);
                break;
            case R.id.button2:
                keyinput(2);
                break;
            case R.id.button3:
                keyinput(3);
                break;
            case R.id.button4:
                keyinput(4);
                break;
            case R.id.button5:
                keyinput(5);
                break;
            case R.id.button6:
                keyinput(6);
                break;
            case R.id.button7:
                keyinput(7);
                break;
            case R.id.button8:
                keyinput(8);
                break;
            case R.id.button9:
                keyinput(9);
                break;
            case R.id.buttonMatch:
                points = getMatchPoints(pointsperround);
                match = true;
                break;
            case R.id.buttonNone:
                points = 0;
                match = true;
                break;
            default:
        }
        updatepoints();
    }

    private void flipview(boolean animate) {
        if (animate) {
            // TODO: animate whole window
            Animation animation = AnimationUtils.loadAnimation(getDialog().getContext(), R.anim.rotate_180);
            view.startAnimation(animation);
        }
        view.setRotation(Math.abs(view.getRotation() - 180));
        SharedPreferences.Editor editor = settings.edit();
        if (teamID == SchieberActivity.TEAM_1) {
            editor.putBoolean("flipUpper", view.getRotation() == 180);
        } else {
            editor.putBoolean("flipLower", view.getRotation() == 180);
        }
        editor.apply();
    }

    private int getMatchPoints(int roundpoints) {
        if (roundpoints == 157) return 257;
        if (roundpoints == 314) return 514;
        return roundpoints;
    }

    private void keyinput(int key) {
        match = false;
        if (points > 99)
            return;
        points = points * 10 + key;
    }

    private void updatepoints() {
        if (!match && sign == 1) {
            pointsOther = Math.max(0, pointsperround - points);
        } else if (points == 0) {
            pointsOther = getMatchPoints(pointsperround);
        } else {
            pointsOther = 0;
        }

        TextView PointsField = view.findViewById(R.id.points);
        PointsField.setText(String.format(Locale.GERMAN, "%1$d", points));

        RadioGroup RG = view.findViewById(R.id.radioFactor);
        switch (RG.getCheckedRadioButtonId()) {
            case R.id.count1:
                factor = 1;
                break;
            case R.id.count2:
                factor = 2;
                break;
            case R.id.count3:
                factor = 3;
                break;
            case R.id.count4:
                factor = 4;
                break;
            case R.id.count5:
                factor = 5;
                break;
            case R.id.count6:
                factor = 6;
                break;
            case R.id.count7:
                factor = 7;
                break;
            case R.id.countX:
                // FIXME: selection on fliped dialog
                Spinner spinFactor = view.findViewById(R.id.spinnerFactor);
                factor = 4 + spinFactor.getSelectedItemPosition();
                break;
        }

        TextView SummaryField = view.findViewById(R.id.summary);
        String summary = res.getString(R.string.total) + ": " + sign * points * factor;

        if (pointsOther >= 0) {
            summary = summary.concat(" (" + res.getString(R.string.opponent) + ": " + pointsOther * factor + ")");
        }
        SummaryField.setText(summary);

    }
}
