package ch.simonste.jasstafel.schieber;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import ch.simonste.helpers.CustomViewPager;
import ch.simonste.jasstafel.R;
import ch.simonste.jasstafel.common.NFCUtils;

public class SchieberActivity extends AppCompatActivity {
    public static final int TEAM_1 = 0;
    public static final int TEAM_2 = 1;

    private static final int MENU_RESET = 1;
    private static final int MENU_SETTINGS = 2;
    private static final int MENU_HISTORY = 3;
    private static final int MENU_STATISTICS = 4;

    private SchieberTafel schieberTafel;

    private SchieberSwipeAdapter adapter;
    private CustomViewPager viewPager;
    private SchieberHistory history;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) actionBar.hide();
        setContentView(R.layout.schieber);

        adapter = new SchieberSwipeAdapter(getSupportFragmentManager());
        viewPager = findViewById(R.id.ViewPager);
        viewPager.setAdapter(adapter);

        schieberTafel = (SchieberTafel) adapter.getItem(0);
        history = new SchieberHistory(this);

        NFCUtils.createSender(this, "Schieber");
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        NFCUtils.handleIntent(this, intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        onNewIntent(getIntent());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (data.getExtras().getInt("Action")) {
                case R.id.action_reset:
                    resetDialog(ResetReason.Round);
                    break;
                case R.id.action_statistics:
                    schieberTafel.showStats();
                    break;
                case R.id.action_info:
                    schieberTafel.showHistory();
                    break;
            }
        }
    }

    public SchieberTafel getSchiebertafel() {
        return schieberTafel;
    }

    public SchieberStrichtafel getSchieberStrichtafel() {
        return (SchieberStrichtafel) adapter.getItem(1);
    }

    @Override
    public void onBackPressed() {
        if (viewPager.getCurrentItem() == 0) {
            super.onBackPressed();
        } else {
            viewPager.setCurrentItem(0);
        }
    }

    public void allowBackside(boolean allow) {
        adapter.allowBackside(allow);
        viewPager.setPagingEnabled(allow);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, MENU_RESET, 0, R.string.reset);
        menu.add(0, MENU_STATISTICS, 0, R.string.showstats);
        menu.add(0, MENU_HISTORY, 0, R.string.showcurrentround);
        menu.add(0, MENU_SETTINGS, 0, R.string.settings);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case MENU_RESET:
                resetDialog(ResetReason.Round);
                break;
            case MENU_SETTINGS:
                startActivityForResult(new Intent(this, SchieberPreferences.class), MENU_SETTINGS);
                break;
            case MENU_STATISTICS:
                schieberTafel.showStats();
                break;
            case MENU_HISTORY:
                schieberTafel.showHistory();
                break;
        }
        return true;
    }

    public void resetDialog(ResetReason reason) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(R.string.reset);
        if (!hasStatistics() && history.getSize() == 0) {
            dialog.setMessage(getString(R.string.emptystatistics));
            dialog.setPositiveButton(android.R.string.ok, null);
        } else {
            String message = getString(R.string.deletewhat);
            if (reason == ResetReason.ChangedName) {
                message = getString(R.string.teamnamechanged) + "\n" + message;
            }
            dialog.setMessage(message);
            dialog.setPositiveButton(R.string.resetall,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            schieberTafel.doResetCurrentRound(getHistory());
                            doResetStatistics();
                        }
                    }
            );
            if (reason == ResetReason.Statistics) {
                dialog.setNegativeButton(R.string.stats,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                doResetStatistics();
                            }
                        }
                );
            } else {
                dialog.setNegativeButton(R.string.currentround,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                schieberTafel.doResetCurrentRound(getHistory());
                            }
                        }
                );
            }
            dialog.setNeutralButton(android.R.string.no, null);
        }
        dialog.show();
    }

    private void doResetStatistics() {
        SharedPreferences.Editor statistics = getSharedPreferences("Schieber", Context.MODE_PRIVATE).edit();
        statistics.remove("Bergpreis1").remove("Bergpreis2").remove("Duration").remove("Matches1").remove("Matches2")
                .remove("Pts1").remove("Pts2").remove("Weis1").remove("Weis2").remove("Wins1").remove("Wins2").apply();
    }

    private boolean hasStatistics() {
        SharedPreferences statistics = getSharedPreferences("Schieber", Context.MODE_PRIVATE);
        return statistics.getInt("Pts1", 0) > 0 || statistics.getInt("Pts2", 0) > 0;
    }

    public SchieberHistory getHistory() {
        return history;
    }

    enum ResetReason {
        ChangedName,
        Round,
        Statistics
    }
}