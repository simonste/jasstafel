package ch.simonste.jasstafel.schieber;

import android.content.Intent;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceFragment;

import ch.simonste.jasstafel.PreferencesActivity;
import ch.simonste.jasstafel.R;
import ch.simonste.jasstafel.common.NFCUtils;
import ch.simonste.jasstafel.common.Profiles;

public class SchieberPreferences extends PreferencesActivity {

    protected PreferenceFragment createFragment() {
        return new SchieberPreferenceFragment();
    }

    public static class SchieberPreferenceFragment extends PreferenceFragment {

        public SchieberPreferenceFragment() {
        }

        private final OnPreferenceClickListener customListener = new OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                switch (preference.getKey()) {
                    case "reset":
                        ((PreferencesActivity) getActivity()).resultAction(R.id.action_reset);
                        break;
                    case "launchhistory":
                        ((PreferencesActivity) getActivity()).resultAction(R.id.action_info);
                        break;
                    case "launchstats":
                        ((PreferencesActivity) getActivity()).resultAction(R.id.action_statistics);
                        break;
                    case "profile":
                        Intent intent = new Intent(getActivity(), Profiles.class);
                        intent.putExtra("Name", "Schieber");
                        startActivity(intent);
                        break;
                }
                return true;
            }
        };

        private final Preference.OnPreferenceChangeListener enableGoalsListener = new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                Preference differentGoals = findPreference("differentGoals");
                differentGoals.setEnabled(newValue.equals(getString(R.string.goalpoints)));
                return true;
            }
        };

        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            getActivity().setTitle(R.string.schiebersettings);
            addPreferencesFromResource(R.xml.schieberpreferences);

            findPreference("launchhistory").setOnPreferenceClickListener(customListener);
            findPreference("launchstats").setOnPreferenceClickListener(customListener);
            findPreference("reset").setOnPreferenceClickListener(customListener);
            findPreference("profile").setOnPreferenceClickListener(customListener);

            findPreference("PointsPerRound").setOnPreferenceChangeListener(validWatcher);
            findPreference("language").setOnPreferenceChangeListener(languageWatcher);

            ListPreference goal = (ListPreference) findPreference("goal");
            goal.setOnPreferenceChangeListener(enableGoalsListener);
            enableGoalsListener.onPreferenceChange(goal, goal.getValue()); // initial call to disable dependency preference

            NFCUtils.updatePreference(findPreference("nfc"));
        }
    }
}

