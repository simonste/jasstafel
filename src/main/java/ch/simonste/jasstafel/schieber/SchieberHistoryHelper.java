package ch.simonste.jasstafel.schieber;

import android.graphics.Point;

public class SchieberHistoryHelper {
    public static final int Group_Duration = 3000; // ms
    private static int pointsPerRound;

    public SchieberHistoryHelper(int ppr) {
        pointsPerRound = ppr;
    }

    public static boolean isRound(int sum) {
        if (sum == 0) return false;
        int matchpoints = pointsPerRound;
        if (pointsPerRound == 157) matchpoints = 257;
        if (pointsPerRound == 314) matchpoints = 514;

        return (sum % pointsPerRound == 0 || sum % matchpoints == 0);
    }

    public static int rounds(SchieberHistory history) {
        int rounds = 0;
        final int size = history.getSize();

        Point roundTotal = new Point(0, 0);
        long prevTS = 0;

        for (int i = 0; i < size; i++) {
            SchieberAction act = history.getAction(i);
            if (isRound(roundTotal.x + roundTotal.y) || isRound(act.pt.x + act.pt.y) || Math.abs(prevTS - act.ts) > Group_Duration || (act.ts == 0/*bugfix wrong preference convertion*/)) {
                roundTotal = new Point(0, 0);
            }
            roundTotal.x += act.pt.x;
            roundTotal.y += act.pt.y;

            if (isRound(roundTotal.x + roundTotal.y)) {
                rounds++;
            }
            prevTS = act.ts;
        }
        return rounds;
    }
}
