package ch.simonste.jasstafel.schieber;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import ch.simonste.helpers.NumberTextView;
import ch.simonste.helpers.SingleDialog;
import ch.simonste.jasstafel.R;

// Klasse für langfristige Statistiken (mehrere Runden)
// aktuelle Runde wird von SchieberHistory geholt
public class SchieberStats extends SingleDialog {
    private final Point ptsWeis = new Point(0, 0);
    private final Point ptsTot = new Point(0, 0);
    private final Point bergpreis = new Point(0, 0);
    private final Point matches = new Point(0, 0);
    private final Point ptsWeisAll = new Point(0, 0);
    private final Point ptsTotAll = new Point(0, 0);
    private final Point bergpreisAll = new Point(0, 0);
    private final Point matchesAll = new Point(0, 0);
    private final Point wins = new Point(0, 0);
    private View view;
    private int duration = 0;
    private int durationAll = 0;
    private SchieberHistory history;

    public void calculateStats(SchieberActivity activity) {
        history = activity.getHistory();

        calculateCurrentRound();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.schieberstats, container);
        getDialog().getWindow().setWindowAnimations(R.style.DialogAnimationFlip);

        final SchieberActivity activity = (SchieberActivity) getActivity();
        final SchieberTafel schieberTafel = activity.getSchiebertafel();
        calculateStats(activity);

        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.stats);
        toolbar.inflateMenu(R.menu.schieberstatsmenu);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                SingleDialog.switchDialogs();
                switch (item.getItemId()) {
                    case R.id.action_info:
                        schieberTafel.showHistory();
                        break;
                    case R.id.action_reset:
                        activity.resetDialog(SchieberActivity.ResetReason.Statistics);
                        break;
                }
                dismiss();
                return true;
            }
        });

        ((TextView) view.findViewById(R.id.teamName1)).setText(schieberTafel.getTeamName(SchieberActivity.TEAM_1));
        ((TextView) view.findViewById(R.id.teamName2)).setText(schieberTafel.getTeamName(SchieberActivity.TEAM_2));

        SharedPreferences statistics = activity.getSharedPreferences("Schieber", Context.MODE_PRIVATE);
        ptsTotAll.x = ptsTot.x + statistics.getInt("Pts1", 0);
        ptsTotAll.y = ptsTot.y + statistics.getInt("Pts2", 0);
        ptsWeisAll.x = ptsWeis.x + statistics.getInt("Weis1", 0);
        ptsWeisAll.y = ptsWeis.y + statistics.getInt("Weis2", 0);
        bergpreisAll.x = bergpreis.x + statistics.getInt("Bergpreis1", 0);
        bergpreisAll.y = bergpreis.y + statistics.getInt("Bergpreis2", 0);
        matchesAll.x = matches.x + statistics.getInt("Matches1", 0);
        matchesAll.y = matches.y + statistics.getInt("Matches2", 0);
        Bundle bundle = getArguments();
        duration = bundle.getInt("Duration");
        durationAll = duration + statistics.getInt("Duration", 0);

        wins.x = statistics.getInt("Wins1", 0);
        wins.y = statistics.getInt("Wins2", 0);
        switch (activity.getSchiebertafel().getWinnerTeam()) {
            case 1:
                wins.x++;
                break;
            case 2:
                wins.y++;
                break;
        }
        fillDialog();

        return view;
    }

    public Point getCurrentRoundWeis() {
        return ptsWeis;
    }

    public Point getCurrentRoundBergpreis() {
        return bergpreis;
    }

    public Point getCurrentRoundMatches() {
        return matches;
    }

    private void calculateCurrentRound() {
        Point currentRow = new Point(0, 0);
        long prevTS = 0;

        for (int i = history.getSize() - 1; i >= 0; i--) {
            SchieberAction act = history.getAction(i);

            if (Math.abs(prevTS - act.ts) > 2000 && prevTS != 0 || i == 0 /*last */) {
                classifyAndAddPoints(currentRow.x, currentRow.y);

                currentRow.set(0, 0);
            }
            currentRow.x += act.pt.x;
            currentRow.y += act.pt.y;

            prevTS = act.ts;
        }
        classifyAndAddPoints(currentRow.x, currentRow.y);

        int bp = history.getBergpreis();
        if (bp == 1)
            bergpreis.x = 1;
        else if (bp == 2)
            bergpreis.y = 1;
    }

    private void classifyAndAddPoints(int x, int y) {
        int rowpts = x + y;
        if (x == 0 || y == 0) {
            if (rowpts % 10 == 0) {
                ptsWeis.x += x;
                ptsWeis.y += y;
            }
            if (x > 0 && x % 257 == 0) matches.x++;
            if (y > 0 && y % 257 == 0) matches.y++;
        }
        ptsTot.x += x;
        ptsTot.y += y;
    }


    private void fillDialog() {
        ((TextView) view.findViewById(R.id.currentround)).setText(String.format(getString(R.string.currentroundmins), duration));
        ((NumberTextView) view.findViewById(R.id.ptsWeis1)).setInt(ptsWeis.x);
        ((NumberTextView) view.findViewById(R.id.ptsWeis2)).setInt(ptsWeis.y);
        ((NumberTextView) view.findViewById(R.id.matches1)).setInt(matches.x);
        ((NumberTextView) view.findViewById(R.id.matches2)).setInt(matches.y);
        ((NumberTextView) view.findViewById(R.id.ptsStich1)).setInt(ptsTot.x - ptsWeis.x);
        ((NumberTextView) view.findViewById(R.id.ptsStich2)).setInt(ptsTot.y - ptsWeis.y);
        ((NumberTextView) view.findViewById(R.id.ptsTotal1)).setInt(ptsTot.x);
        ((NumberTextView) view.findViewById(R.id.ptsTotal2)).setInt(ptsTot.y);
        if (bergpreis.x == 1) ((TextView) view.findViewById(R.id.bergpreis1)).setText("✔");
        if (bergpreis.y == 1) ((TextView) view.findViewById(R.id.bergpreis2)).setText("✔");

        ((TextView) view.findViewById(R.id.allrounds)).setText(String.format(getString(R.string.allrounds), durationAll));
        ((NumberTextView) view.findViewById(R.id.ptsWeis1All)).setInt(ptsWeisAll.x);
        ((NumberTextView) view.findViewById(R.id.ptsWeis2All)).setInt(ptsWeisAll.y);
        ((NumberTextView) view.findViewById(R.id.matches1All)).setInt(matchesAll.x);
        ((NumberTextView) view.findViewById(R.id.matches2All)).setInt(matchesAll.y);
        ((NumberTextView) view.findViewById(R.id.ptsStich1All)).setInt(ptsTotAll.x - ptsWeisAll.x);
        ((NumberTextView) view.findViewById(R.id.ptsStich2All)).setInt(ptsTotAll.y - ptsWeisAll.y);
        ((NumberTextView) view.findViewById(R.id.ptsTotal1All)).setInt(ptsTotAll.x);
        ((NumberTextView) view.findViewById(R.id.ptsTotal2All)).setInt(ptsTotAll.y);
        ((NumberTextView) view.findViewById(R.id.bergpreis1All)).setInt(bergpreisAll.x);
        ((NumberTextView) view.findViewById(R.id.bergpreis2All)).setInt(bergpreisAll.y);
        ((NumberTextView) view.findViewById(R.id.wins1)).setInt(wins.x);
        ((NumberTextView) view.findViewById(R.id.wins2)).setInt(wins.y);
    }
}
