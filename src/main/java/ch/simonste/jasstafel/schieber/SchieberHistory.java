package ch.simonste.jasstafel.schieber;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Point;

import ch.simonste.jasstafel.R;

class SchieberAction {
    public final Point pt;
    public final long ts;

    SchieberAction(int pts1, int pts2, long timestamp) {
        pt = new Point(pts1, pts2);
        ts = timestamp;
    }
}


public class SchieberHistory {

    private final Context context;
    private final SharedPreferences preferences;
    private final SharedPreferences.Editor editor;
    private int size;

    public SchieberHistory(Context context) {
        this.context = context;
        preferences = this.context.getSharedPreferences("Schieber", Context.MODE_PRIVATE);
        editor = preferences.edit();

        size = preferences.getInt("HistorySize", 0);

        // set to 0 if not available
        for (int i = 0; i < size; i++) {
            long TS = preferences.getLong("History_TS" + i, 0);
            int pts1 = preferences.getInt("HistoryPts1_" + i, 0);
            int pts2 = preferences.getInt("HistoryPts2_" + i, 0);
            editor.putLong("History_TS" + i, TS);
            editor.putInt("HistoryPts1_" + i, pts1);
            editor.putInt("HistoryPts2_" + i, pts2);
        }
        editor.apply();
    }

    public void clear() {
        for (int i = 0; i < size; i++) {
            editor.remove("History_TS" + i).remove("HistoryPts1_" + i).remove("HistoryPts2_" + i);
        }
        editor.remove("Bergpreis").remove("Winner");
        editor.putInt("HistorySize", 0);
        editor.commit();
        size = 0;
    }

    public void addAction(int pts1, int pts2) {
        editor.putLong("History_TS" + size, System.currentTimeMillis());
        editor.putInt("HistoryPts1_" + size, pts1);
        editor.putInt("HistoryPts2_" + size, pts2);
        editor.putInt("HistorySize", ++size);
        editor.commit();
    }

    public Point undoLast() {
        int id = size - 1;
        Point pt = getAction(id).pt;
        editor.remove("HistoryPts1_" + id);
        editor.remove("HistoryPts2_" + id);
        editor.remove("History_TS" + id);
        editor.putInt("HistorySize", --size);
        editor.commit();
        return pt;
    }

    public SchieberAction getAction(int id) {
        int pts1 = preferences.getInt("HistoryPts1_" + id, 0);
        int pts2 = preferences.getInt("HistoryPts2_" + id, 0);
        long ts = preferences.getLong("History_TS" + id, 0);

        return new SchieberAction(pts1, pts2, ts);
    }

    public int getSize() {
        return size;
    }

    public void setBergpreis(int team, final String[] teamname) {
        if (team == 3) {
            AlertDialog.Builder confirmBP = new AlertDialog.Builder(context);
            confirmBP.setTitle(R.string.bergpreis);
            confirmBP.setMessage(context.getString(R.string.bergpreisBoth));
            confirmBP.setNegativeButton(teamname[0],
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            setBergpreis(1, teamname);
                        }
                    }
            );
            confirmBP.setPositiveButton(teamname[1],
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            setBergpreis(2, teamname);
                        }
                    }
            );
            confirmBP.show();
        } else {
            editor.putInt("Bergpreis", team);
            editor.commit();
        }
    }

    public int getBergpreis() {
        return preferences.getInt("Bergpreis", 0);
    }

    public int getWinner() {
        return preferences.getInt("Winner", 0);
    }

    public void setWinner(int team) {
        editor.putInt("Winner", team);
        editor.commit();
    }
}
