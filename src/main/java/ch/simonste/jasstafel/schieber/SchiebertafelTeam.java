package ch.simonste.jasstafel.schieber;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Point;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.View;

import androidx.preference.PreferenceManager;

import ch.simonste.jasstafel.common.VibraHelper;


public class SchiebertafelTeam extends View {
    public boolean isUpperTeam;
    private SchiebertafelTeamData data;
    private Canvas bitmapCanvas;
    private Bitmap BM;
    private int strokeLength;
    private int margin;
    private int width;
    private int height;
    private Paint strokePaint;
    private Paint circlePaint;
    private boolean isInitialized;
    private SchieberTafel tafel;

    public SchiebertafelTeam(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (isInEditMode()) return;

        String packageName = "http://schemas.android.com/apk/res-auto";
        isUpperTeam = attrs.getAttributeBooleanValue(packageName, "upperTeam", false);
        isInitialized = false;

        strokePaint = new Paint();
        strokePaint.setTextAlign(Align.RIGHT);
        circlePaint = new Paint(strokePaint);
        circlePaint.setStyle(Style.STROKE);
        circlePaint.setStrokeWidth(5);
    }

    public void setData(SchiebertafelTeamData data, SchieberTafel tafel) {
        this.data = data;
        this.tafel = tafel;
        data.setView(this, isUpperTeam);
    }

    private void initialize() {
        width = getWidth();
        height = getHeight();
        strokeLength = height / 6;
        margin = height / 20;
        strokePaint.setTextSize(height / 15);
        strokePaint.setStrokeWidth(height / 120);

        BM = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);

        bitmapCanvas = new Canvas();
        bitmapCanvas.setBitmap(BM);

        if (isUpperTeam) bitmapCanvas.rotate(180, width / 2, height / 2);

        isInitialized = true;
    }

    @Override
    public void onDraw(Canvas canvas) {
        if (isInEditMode()) return;

        if (!isInitialized) initialize();

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        final boolean bigScore = sharedPrefs.getBoolean("bigScore", false);
        if (bigScore) {
            strokePaint.setColor(Color.DKGRAY);
            circlePaint.setColor(Color.WHITE);
        } else {
            strokePaint.setColor(Color.WHITE);
            circlePaint.setColor(Color.DKGRAY);
        }

        BM.eraseColor(Color.BLACK);

        drawZ(bigScore);
        drawAddButton();

        drawStrokes();
        if (bigScore) {
            drawBigScore();
        }

        canvas.drawBitmap(BM, 0, 0, strokePaint);

        tafel.updatePoints();
    }

    private void drawZ(boolean bigScore) {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        boolean drawZ = sharedPrefs.getBoolean("drawZ", true);
        if (drawZ) {
            Paint Red = new Paint();
            Red.setColor(Color.RED);
            if (bigScore) {
                Red.setAlpha(120);
            }
            int vdiff = margin + strokeLength / 2;
            bitmapCanvas.drawLine(margin, vdiff, width - margin, vdiff, Red);
            bitmapCanvas.drawLine(width - margin, vdiff, margin, height - vdiff, Red);
            bitmapCanvas.drawLine(margin, height - vdiff, width - margin, height - vdiff, Red);
        }
    }

    private void verticalStroke(int x, int y) {
        bitmapCanvas.drawLine(x, y - strokeLength / 2, x, y + strokeLength / 2, strokePaint);
    }

    private void drawStrokes() {
        draw1(width - margin, height / 2);
        draw20or100(height - (margin + strokeLength / 2), SchiebertafelTeamData.KEY_20);
        draw50(height / 2);
        draw20or100((margin + strokeLength / 2), SchiebertafelTeamData.KEY_100);
        draw500(margin + strokeLength / 2);
    }

    private void draw1(int x, int y) {
        bitmapCanvas.drawText(Integer.toString(data.getPoints(SchiebertafelTeamData.KEY_1)), x, y, strokePaint);
    }

    private void draw20or100(int y, int key) {
        for (int i = 0; i < data.getPoints(key); i++) {
            int d = strokeLength / 8;
            int x = margin + i * d + (i / 5) * d;
            if ((i + 1) % 5 == 0) {
                bitmapCanvas.drawLine(x, y - strokeLength / 2, x - 5 * d, y + strokeLength / 2, strokePaint);
            } else {
                verticalStroke(x, y);
            }
        }
    }

    private void draw50(int y) {
        int x1 = 0;
        int x2 = 0;
        for (int i = 0; i < data.getPoints(SchiebertafelTeamData.KEY_50); i++) {
            if (i % 2 == 0) {
                x1 = margin + i * strokeLength / 4;
                x2 = x1 + strokeLength / 3;
            } else {
                int swap = x2;
                x2 = x1;
                x1 = swap;
            }
            bitmapCanvas.drawLine(x1, y - strokeLength / 2, x2, y + strokeLength / 2, strokePaint);
        }
    }

    private void draw500(int y) {
        int x;
        for (int i = 0; i < data.getPoints(SchiebertafelTeamData.KEY_500); i++) {
            int d = strokeLength / 3;
            x = width / 2 + i * 5 * d / 4;
            bitmapCanvas.drawLine(x, y - strokeLength / 2, x + d / 2, y + strokeLength / 2, strokePaint);
            bitmapCanvas.drawLine(x + d / 2, y + strokeLength / 2, x + d, y - strokeLength / 2, strokePaint);
        }
    }

    private void drawAddButton() {
        Point center = new Point(width * 7 / 8, height * 3 / 4);
        int r = width / 20;

        bitmapCanvas.drawCircle(center.x, center.y, r, circlePaint);
        bitmapCanvas.drawLine(center.x, center.y - r * 2 / 4, center.x, center.y + r * 2 / 4, circlePaint);
        bitmapCanvas.drawLine(center.x - r * 2 / 4, center.y, center.x + r * 2 / 4, center.y, circlePaint);
    }

    private void drawBigScore() {
        Point center = new Point(width / 2, height / 2);
        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setTextAlign(Align.CENTER);
        paint.setAntiAlias(true);
        paint.setTextSize(height / 4);
        bitmapCanvas.drawText(String.valueOf(data.getTotalPoints()), center.x, center.y, paint);
    }

    public int onTouch(PointF Ratio) {
        int sector = (int) Math.ceil(Ratio.y * 3);
        int pts = 0;
        if (Ratio.x < 0.6) {
            int[] sectorpts = {0, 100, 50, 20};
            if (sector >= 0 && sector < 4)
                pts = sectorpts[sector];
        }
        if (Ratio.x > 0.7 && sector == 2) {
            if (Ratio.y > 0.55)
                pts = -1;
            else
                pts = 1;
        }
        final boolean vibrate = PreferenceManager.getDefaultSharedPreferences(getContext()).getBoolean("vibrate", true);
        if (pts != 0 && vibrate) {
            VibraHelper.vibrate(getContext(), 50);
        }

        data.setTouchPoints(pts);
        return pts;
    }

    @Override
    public boolean performClick() {
        super.performClick();
        return true;
    }
}

