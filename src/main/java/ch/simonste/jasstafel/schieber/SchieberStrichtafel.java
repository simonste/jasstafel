package ch.simonste.jasstafel.schieber;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.PointF;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.preference.PreferenceManager;

import java.util.ArrayList;

import ch.simonste.helpers.SimpleInputDialog;
import ch.simonste.jasstafel.R;

public class SchieberStrichtafel extends Fragment
        implements View.OnTouchListener, View.OnClickListener, SimpleInputDialog.SimpleDialogListener {
    private static final int MAX_COLUMNS = 6;
    private final ArrayList<TextView> teamNames = new ArrayList<>();
    private final ArrayList<StrokeRow> strokes = new ArrayList<>();
    private final ArrayList<View> separators = new ArrayList<>();
    private final ArrayList<View> cols = new ArrayList<>();
    private final PointF position = new PointF();
    private int columns = 0;
    private boolean active = false;
    private SchieberActivity activity;
    private SharedPreferences defaultPreferences;
    private SharedPreferences schieberPreferences;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.schieberstrichtafel, container, false);

        strokes.clear();
        teamNames.clear();
        separators.clear();
        cols.clear();

        activity = (SchieberActivity) getActivity();
        defaultPreferences = PreferenceManager.getDefaultSharedPreferences(activity);
        schieberPreferences = activity.getSharedPreferences("Schieber", Context.MODE_PRIVATE);

        setupStrokes((StrokeRow) rootView.findViewById(R.id.strokes1));
        setupStrokes((StrokeRow) rootView.findViewById(R.id.strokes2));
        setupStrokes((StrokeRow) rootView.findViewById(R.id.strokes3));
        setupStrokes((StrokeRow) rootView.findViewById(R.id.strokes4));
        setupStrokes((StrokeRow) rootView.findViewById(R.id.strokes5));
        setupStrokes((StrokeRow) rootView.findViewById(R.id.strokes6));

        teamNames.add((TextView) rootView.findViewById(R.id.teamName1));
        teamNames.add((TextView) rootView.findViewById(R.id.teamName2));
        teamNames.add((TextView) rootView.findViewById(R.id.teamName3));
        teamNames.add((TextView) rootView.findViewById(R.id.teamName4));
        teamNames.add((TextView) rootView.findViewById(R.id.teamName5));
        teamNames.add((TextView) rootView.findViewById(R.id.teamName6));

        teamNames.get(2).setOnClickListener(this);
        teamNames.get(3).setOnClickListener(this);
        teamNames.get(4).setOnClickListener(this);
        teamNames.get(5).setOnClickListener(this);

        separators.add(rootView.findViewById(R.id.line3));
        separators.add(rootView.findViewById(R.id.line4));
        separators.add(rootView.findViewById(R.id.line5));
        separators.add(rootView.findViewById(R.id.line6));

        cols.add(rootView.findViewById(R.id.col3));
        cols.add(rootView.findViewById(R.id.col4));
        cols.add(rootView.findViewById(R.id.col5));
        cols.add(rootView.findViewById(R.id.col6));

        rootView.findViewById(R.id.deleteButton).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean empty = true;
                        for (StrokeRow row : strokes) {
                            if (row.getStrokes() > 0) {
                                empty = false;
                                break;
                            }
                        }
                        if (empty) return;

                        AlertDialog.Builder confirmreset = new AlertDialog.Builder(activity);
                        confirmreset.setTitle(R.string.resetstats);
                        confirmreset.setMessage(getString(R.string.resetConfirmStrokes));
                        confirmreset.setPositiveButton(android.R.string.yes,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        reset();
                                    }
                                }
                        );
                        confirmreset.setNegativeButton(android.R.string.no, null);
                        confirmreset.show();
                    }
                }
        );

        return rootView;
    }

    private void setupStrokes(StrokeRow view) {
        strokes.add(view);
        view.setOnTouchListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferences();
    }

    public void setTeamName(int team, String name) {
        if (columns != 2) return;
        teamNames.get(team).setText(name);
    }


    private void getPreferences() {
        boolean active = defaultPreferences.getBoolean("backside", false);
        int cols = Integer.parseInt(defaultPreferences.getString("backsideColumns", "2"));

        boolean changed = ((columns != cols && columns != 0) || active != this.active);
        this.active = active;
        if (changed && this.active) {
            columns = cols;

            for (int i = 2; i < MAX_COLUMNS; i++) {
                int visibility = View.VISIBLE;
                if (i >= columns) visibility = View.GONE;

                this.cols.get(i - 2).setVisibility(visibility);
                separators.get(i - 2).setVisibility(visibility);
            }
        }

        if (columns == 2) {
            teamNames.get(0).setText(schieberPreferences.getString("TeamName1", getString(R.string.defteam1)));
            teamNames.get(1).setText(schieberPreferences.getString("TeamName2", getString(R.string.defteam2)));
            teamNames.get(0).setOnClickListener(null);
            teamNames.get(1).setOnClickListener(null);
        } else {
            switch (columns) {
                case 6:
                    teamNames.get(5).setText(schieberPreferences.getString("ColName6", getString(R.string.defteam6)));
                case 5:
                    teamNames.get(4).setText(schieberPreferences.getString("ColName5", getString(R.string.defteam5)));
                case 4:
                    teamNames.get(3).setText(schieberPreferences.getString("ColName4", getString(R.string.defteam4)));
                case 3:
                    teamNames.get(2).setText(schieberPreferences.getString("ColName3", getString(R.string.defteam3)));
                default:
                    teamNames.get(1).setText(schieberPreferences.getString("ColName2", getString(R.string.defteam2)));
                    teamNames.get(0).setText(schieberPreferences.getString("ColName1", getString(R.string.defteam1)));
                    teamNames.get(1).setOnClickListener(this);
                    teamNames.get(0).setOnClickListener(this);
            }
        }

        strokes.get(0).init(schieberPreferences.getInt("SStrokes1", 0));
        strokes.get(1).init(schieberPreferences.getInt("SStrokes2", 0));
        strokes.get(2).init(schieberPreferences.getInt("SStrokes3", 0));
        strokes.get(3).init(schieberPreferences.getInt("SStrokes4", 0));
        strokes.get(4).init(schieberPreferences.getInt("SStrokes5", 0));
        strokes.get(5).init(schieberPreferences.getInt("SStrokes6", 0));
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getPointerId(0) != 0) return false;
        if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
            position.set(motionEvent.getX(), motionEvent.getY());
        } else {
            StrokeRow sr = (StrokeRow) view;
            int strokes = sr.getStrokes();
            if (motionEvent.getAction() == MotionEvent.ACTION_MOVE && !position.equals(0, 0)) {
                PointF move = new PointF();
                move.set(position.x - motionEvent.getX(), position.y - motionEvent.getY());
                Log.d("Move", "length:" + move.length() + " viewWidth:" + view.getWidth());
                if (move.length() > view.getWidth() / 4.) {
                    Log.d("Move", "(x" + Math.round(move.x) + ", y" + Math.round(move.y) + ")");
                    if (Math.abs(move.y) > Math.sqrt(3) * Math.abs(move.x)) {
                        sr.removeStroke();
                    }
                    position.set(0, 0);
                }
            }
            if (motionEvent.getAction() == MotionEvent.ACTION_UP && !position.equals(0, 0)) {
                sr.addStroke();
            }

            if (sr.getStrokes() != strokes) {
                SharedPreferences.Editor editor = schieberPreferences.edit();
                strokes = sr.getStrokes();
                switch (sr.getId()) {
                    case R.id.strokes1:
                        editor.putInt("SStrokes1", strokes);
                        break;
                    case R.id.strokes2:
                        editor.putInt("SStrokes2", strokes);
                        break;
                    case R.id.strokes3:
                        editor.putInt("SStrokes3", strokes);
                        break;
                    case R.id.strokes4:
                        editor.putInt("SStrokes4", strokes);
                        break;
                    case R.id.strokes5:
                        editor.putInt("SStrokes5", strokes);
                        break;
                    case R.id.strokes6:
                        editor.putInt("SStrokes6", strokes);
                        break;
                }
                editor.apply();
            }
        }
        return true;
    }

    private void reset() {
        SharedPreferences.Editor editor = schieberPreferences.edit();
        editor.putInt("SStrokes1", 0);
        editor.putInt("SStrokes2", 0);
        editor.putInt("SStrokes3", 0);
        editor.putInt("SStrokes4", 0);
        editor.putInt("SStrokes5", 0);
        editor.putInt("SStrokes6", 0);
        editor.apply();
        strokes.get(0).init(0);
        strokes.get(1).init(0);
        strokes.get(2).init(0);
        strokes.get(3).init(0);
        strokes.get(4).init(0);
        strokes.get(5).init(0);
    }

    @Override
    public void onClick(View view) {
        FragmentManager fm = activity.getFragmentManager();
        SimpleInputDialog dialog = new SimpleInputDialog();
        dialog.setArguments(view.getId(), SimpleInputDialog.DIALOG_INPUT_STRING, ((TextView) view).getText().toString());
        dialog.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        dialog.setTarget(this);
        dialog.show(fm, getResources().getString(R.string.entercolname));
    }

    @Override
    public void onFinishDialog(int dialogId, String userInput) {
        if (userInput.equals("")) return;
        SharedPreferences.Editor editor = schieberPreferences.edit();

        for (int i = 0; i < MAX_COLUMNS; i++) {
            if (teamNames.get(i).getId() == dialogId) {
                editor.putString("ColName" + (i + 1), userInput);
                teamNames.get(i).setText(userInput);
            }
        }
        editor.apply();
    }

    @Override
    public void onFinishDialog(int dialogId, Integer userInput) {
    }
}
