package ch.simonste.jasstafel;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.preference.PreferenceManager;

class PreferenceConverter {
    static private SharedPreferences target;
    static private SharedPreferences defaultPreferences;

    public static void convert(Context context) {
        defaultPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        final int oldVersion = defaultPreferences.getInt("Version", 1);

        if (oldVersion < 2) {
            schieber_v2(context);
            coiffeur_v2(context);
            context.getSharedPreferences("WhoBegins", Context.MODE_PRIVATE).edit().clear().apply();
            PreferenceManager.getDefaultSharedPreferences(context).edit().remove("reset").remove("history").remove("stats").apply();
        }

        defaultPreferences.edit().putInt("Version", 2).apply();
    }

    private static void update(SharedPreferences from, String keyOld) {
        update(from, keyOld, keyOld);
    }

    private static void update(SharedPreferences from, String keyOld, String keyNew) {
        if (from.contains(keyOld)) {
            Class c = from.getAll().get(keyOld).getClass();
            if (c.equals(String.class)) {
                target.edit().putString(keyNew, from.getString(keyOld, "")).apply();
            } else if (c.equals(Integer.class)) {
                target.edit().putInt(keyNew, from.getInt(keyOld, 0)).apply();
            } else if (c.equals(Long.class)) {
                target.edit().putLong(keyNew, from.getLong(keyOld, 0)).apply();
            } else if (c.equals(Boolean.class)) {
                target.edit().putBoolean(keyNew, from.getBoolean(keyOld, false)).apply();
            }
            from.edit().remove(keyOld).apply();
        }
    }


    private static void schieber_v2(Context context) {
        target = context.getSharedPreferences("Schieber", Context.MODE_PRIVATE);
        SharedPreferences schieberTeam1Preferences = context.getSharedPreferences("SchieberTeam1", Context.MODE_PRIVATE);
        SharedPreferences schieberTeam2Preferences = context.getSharedPreferences("SchieberTeam2", Context.MODE_PRIVATE);
        SharedPreferences schieberHistPreferences = context.getSharedPreferences("SchieberHistory", Context.MODE_PRIVATE);
        SharedPreferences schieberStatPreferences = context.getSharedPreferences("Statistics", Context.MODE_PRIVATE);

        update(defaultPreferences, "GoalPoints");
        update(defaultPreferences, "GoalPoints1");
        update(defaultPreferences, "ColName1");
        update(defaultPreferences, "ColName2");
        update(defaultPreferences, "ColName3");
        update(defaultPreferences, "ColName4");
        update(defaultPreferences, "ColName5");
        update(defaultPreferences, "ColName6");
        update(defaultPreferences, "SStrokes1");
        update(defaultPreferences, "SStrokes2");
        update(defaultPreferences, "SStrokes3");
        update(defaultPreferences, "SStrokes4");
        update(defaultPreferences, "SStrokes5");
        update(defaultPreferences, "SStrokes6");
        update(defaultPreferences, "SStrokes6");
        update(defaultPreferences, "SStrokes6");
        update(defaultPreferences, "TeamName1");
        update(defaultPreferences, "TeamName2");
        update(schieberHistPreferences, "StartTime");
        update(schieberHistPreferences, "EndTime");
        update(schieberHistPreferences, "Bergpreis");
        for (int i = 0; i < schieberHistPreferences.getInt("HistorySize", 0); ++i) {
            update(schieberHistPreferences, "Pts1" + i, "HistoryPts1_" + i);
            update(schieberHistPreferences, "Pts2" + i, "HistoryPts2_" + i);
            update(schieberHistPreferences, "TS" + i, "History_TS" + i);
        }
        update(schieberHistPreferences, "HistorySize");
        update(schieberTeam1Preferences, "Points1", "Points1_1");
        update(schieberTeam1Preferences, "Points20", "Points1_20");
        update(schieberTeam1Preferences, "Points50", "Points1_50");
        update(schieberTeam1Preferences, "Points100", "Points1_100");
        update(schieberTeam1Preferences, "Points500", "Points1_500");
        update(schieberTeam2Preferences, "Points1", "Points2_1");
        update(schieberTeam2Preferences, "Points20", "Points2_20");
        update(schieberTeam2Preferences, "Points50", "Points2_50");
        update(schieberTeam2Preferences, "Points100", "Points2_100");
        update(schieberTeam2Preferences, "Points500", "Points2_500");
        update(schieberStatPreferences, "Duration");
        update(schieberStatPreferences, "Bergpreis1");
        update(schieberStatPreferences, "Bergpreis2");
        update(schieberStatPreferences, "Matches1");
        update(schieberStatPreferences, "Matches2");
        update(schieberStatPreferences, "Weis1");
        update(schieberStatPreferences, "Weis2");
        update(schieberStatPreferences, "Pts1");
        update(schieberStatPreferences, "Pts1");
        update(schieberStatPreferences, "Wins1");
        update(schieberStatPreferences, "Wins1");
    }

    private static void coiffeur_v2(Context context) {
        target = context.getSharedPreferences("Coiffeur", Context.MODE_PRIVATE);
        SharedPreferences coiffeurDurationPreferences = context.getSharedPreferences("CoiffeurDuration", Context.MODE_PRIVATE);
        update(defaultPreferences, "CTeamName1", "TeamName1");
        update(defaultPreferences, "CTeamName2", "TeamName2");
        update(defaultPreferences, "CTeamName3", "TeamName3");
        for (int t = 1; t <= 3; ++t) {
            for (int r = 1; r <= 12; ++r) {
                update(target, "Points" + t + r, "Points" + t + "_" + r);
            }
        }
        update(coiffeurDurationPreferences, "StartTime");
        update(coiffeurDurationPreferences, "EndTime");
    }
}
