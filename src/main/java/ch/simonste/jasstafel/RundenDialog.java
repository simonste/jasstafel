package ch.simonste.jasstafel;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Locale;

import ch.simonste.helpers.NumberTextView;
import ch.simonste.helpers.SimpleTextWatcher;
import ch.simonste.helpers.SingleDialog;

public class RundenDialog extends SingleDialog
        implements View.OnClickListener, View.OnFocusChangeListener, SimpleTextWatcher.Listener {
    private static final int MAX_PLAYERS = 8;
    private final LinearLayout[] rows = new LinearLayout[MAX_PLAYERS];
    private final Integer[] points = new Integer[MAX_PLAYERS];
    private Button button;
    private TextView sum;
    private boolean isEdit = false;
    private int id;
    private int roundTotal;
    private double factor;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.rundendialog, container);

        getDialog().setTitle(getTag());
        Bundle bundle = getArguments();
        String[] names = bundle.getStringArray("Names");
        int players = bundle.getInt("Players");
        id = bundle.getInt("Item");
        roundTotal = bundle.getInt("RoundTotal", 157);
        factor = bundle.getDouble("Factor", 1.0);
        boolean enableOK = bundle.getBoolean("enableOK", false);

        rows[0] = view.findViewById(R.id.p1);
        rows[1] = view.findViewById(R.id.p2);
        rows[2] = view.findViewById(R.id.p3);
        rows[3] = view.findViewById(R.id.p4);
        rows[4] = view.findViewById(R.id.p5);
        rows[5] = view.findViewById(R.id.p6);
        rows[6] = view.findViewById(R.id.p7);
        rows[7] = view.findViewById(R.id.p8);

        button = view.findViewById(R.id.button);
        button.setOnClickListener(this);
        sum = view.findViewById(R.id.sum);

        String oldString = bundle.getString("Points");
        String[] oldData = oldString.split(";");

        if (oldData.length == 6 || oldData.length == MAX_PLAYERS) {
            isEdit = true;
            for (int i = 0; i < oldData.length; i++) {
                if ("null".equals(oldData[i])) oldData[i] = "";
            }
        } else {
            button.setEnabled(false);
        }

        String[] tmpData = null;
        if (savedInstanceState != null) {
            tmpData = savedInstanceState.getString("Points").split(";");
            for (int i = 0; i < MAX_PLAYERS; i++) {
                if ("null".equals(tmpData[i])) tmpData[i] = "";
            }
        }

        if (roundTotal == 0 || enableOK) button.setEnabled(true);

        for (int i = 0; i < MAX_PLAYERS; i++) {
            ((TextView) rows[i].findViewById(R.id.name)).setText(names[i]);
            EditText et = rows[i].findViewById(R.id.edit);
            et.setOnFocusChangeListener(this);
            et.addTextChangedListener(new SimpleTextWatcher((NumberTextView) rows[i].findViewById(R.id.pts), factor, this, i));
            et.setTag(i);
            if (tmpData != null) {
                et.setText(tmpData[i]);
            } else if (isEdit) {
                et.setText(oldData[i]);
            }
            try {
                points[i] = Integer.valueOf(et.getText().toString());
            } catch (NumberFormatException e) {
                points[i] = null;
            }

            if (i >= players) {
                rows[i].setVisibility(View.GONE);
                points[i] = 0;
                ((TextView) rows[i].findViewById(R.id.pts)).setText("0");
            }
            if (factor == 1) {
                rows[i].findViewById(R.id.pts).setVisibility(View.GONE);
            }
        }

        InputMethodManager mgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < MAX_PLAYERS; i++) {
            String twStr = ((TextView) rows[i].findViewById(R.id.edit)).getText().toString();
            try {
                int pts = (int) Float.parseFloat(twStr);
                str.append(pts);
                str.append(";");
            } catch (NumberFormatException e) {
                str.append("null;");
            }
        }
        bundle.putString("Points", str.toString());
    }

    @Override
    public void onClick(View view) {
        int[] pts = new int[MAX_PLAYERS];
        Integer[] pts2 = new Integer[MAX_PLAYERS];
        for (int i = 0; i < MAX_PLAYERS; i++) {
            String str = ((TextView) rows[i].findViewById(R.id.edit)).getText().toString();
            try {
                pts[i] = (int) Float.parseFloat(str);
                pts2[i] = pts[i];
            } catch (NumberFormatException e) {
                pts2[i] = null;
            }
        }

        RundenDialogInterface target = (RundenDialogInterface) getTargetFragment();

        if (isEdit) {
            target.editPoints(id, pts2);
        } else {
            target.addPoints(pts);
        }

        InputMethodManager mgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(rows[0].findViewById(R.id.edit).getWindowToken(), 0);
        dismiss();
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (!hasFocus && roundTotal != 0) {
            EditText et = (EditText) v;
            int pl = (Integer) v.getTag();
            try {
                points[pl] = Integer.valueOf(et.getText().toString());
            } catch (NumberFormatException e) {
                points[pl] = null;
            }

            int rest = roundTotal;
            int open = 0;

            for (Integer p : points) {
                if (p != null) rest -= Math.max(0, p);
                else open++;
            }

            if ((open == 1 || rest == 0) && !isEdit) {
                for (int i = 0; i < MAX_PLAYERS; i++) {
                    if (points[i] == null && pl != i) {
                        points[i] = rest;
                        ((EditText) rows[i].findViewById(R.id.edit)).setText(String.format(Locale.GERMAN, "%1$d", rest));
                        if (factor != 1.0)
                            ((NumberTextView) rows[i].findViewById(R.id.pts)).setInt((int) Math.round(factor * rest));
                        rest = 0;
                    }
                }
                button.setEnabled(true);
            }
            sum.setText(String.format(getString(R.string.remainingPoints), (roundTotal - rest), rest));
        }
    }

    @Override
    public void textChanged(int id, int pts) {
        if (roundTotal == 0) return;
        int rest = roundTotal;
        points[id] = pts;

        for (Integer p : points) {
            if (p != null) rest -= p;
        }
        sum.setText(String.format(getString(R.string.remainingPoints), (roundTotal - rest), rest));

        if (rest == 0 || (pts == -roundTotal)) {
            for (int i = 0; i < MAX_PLAYERS; i++) {
                EditText edit = rows[i].findViewById(R.id.edit);
                if (edit.getText().length() == 0) {
                    edit.setText("0");
                    if (factor != 1.0) ((TextView) rows[i].findViewById(R.id.pts)).setText("0");
                }
            }
            button.setEnabled(true);
        }
    }
}
