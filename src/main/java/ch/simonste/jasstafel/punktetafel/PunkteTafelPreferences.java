package ch.simonste.jasstafel.punktetafel;

import android.content.Intent;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;

import ch.simonste.jasstafel.PreferencesActivity;
import ch.simonste.jasstafel.R;
import ch.simonste.jasstafel.common.NFCUtils;
import ch.simonste.jasstafel.common.Profiles;

public class PunkteTafelPreferences extends PreferencesActivity {

    protected PreferenceFragment createFragment() {
        return new PunkteTafelPreferenceFragment();
    }

    public static class PunkteTafelPreferenceFragment extends PreferenceFragment {

        public PunkteTafelPreferenceFragment() {
        }

        private final Preference.OnPreferenceClickListener customListener = new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                if (preference.getKey().equals("profile")) {
                    Intent intent = new Intent(getActivity(), Profiles.class);
                    intent.putExtra("Name", "PunkteTafel");
                    startActivity(intent);
                }
                return true;
            }
        };

        private final Preference.OnPreferenceChangeListener enableGoalsListener = new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                Preference goalvalue = findPreference("goalvalue");
                Preference higherIsBetter = findPreference("higherIsBetter");

                if (newValue.equals(getString(R.string.nogoal))) {
                    goalvalue.setEnabled(false);
                    higherIsBetter.setEnabled(false);
                } else if (newValue.equals(getString(R.string.goalpoints))) {
                    goalvalue.setEnabled(true);
                    goalvalue.setTitle(R.string.goalpoints);
                    higherIsBetter.setEnabled(true);
                } else {
                    goalvalue.setEnabled(true);
                    goalvalue.setTitle(R.string.rounds);
                    higherIsBetter.setEnabled(true);
                }
                return true;
            }
        };

        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            getActivity().setTitle(R.string.punktetafelsettings);
            addPreferencesFromResource(R.xml.punktetafelpreferences);

            ListPreference goal = (ListPreference) findPreference("goal");
            goal.setOnPreferenceChangeListener(enableGoalsListener);
            enableGoalsListener.onPreferenceChange(goal, goal.getValue());
            findPreference("goalvalue").setOnPreferenceChangeListener(validWatcher);
            findPreference("ppr").setOnPreferenceChangeListener(validWatcher);
            findPreference("profile").setOnPreferenceClickListener(customListener);
            findPreference("language").setOnPreferenceChangeListener(languageWatcher);

            NFCUtils.updatePreference(findPreference("nfc"));
        }
    }
}

