package ch.simonste.jasstafel.punktetafel;

import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.preference.PreferenceManager;

import java.util.List;

import ch.simonste.helpers.SimpleInputDialog;
import ch.simonste.jasstafel.R;
import ch.simonste.jasstafel.StatsDialog;
import ch.simonste.jasstafel.Whobegins;
import ch.simonste.jasstafel.common.Footer;
import ch.simonste.jasstafel.common.ListFragment;
import ch.simonste.jasstafel.common.VibraHelper;

public class PunkteTafelFragment extends ListFragment<PunkteTafelRow> implements SimpleInputDialog.SimpleDialogListener {
    private int winner = 0;

    @Override
    protected PunkteTafelRow createRow() {
        return new PunkteTafelRow(getActivity());
    }


    @Override
    public void showStats() {
        float[] points = new float[header.getPlayers()];
        int[] zeros = new int[header.getPlayers()];

        int rows = adapter.getCount();
        for (int i = 0; i < rows; i++) {
            PunkteTafelRow row = adapter.getItem(i);
            for (int j = 0; j < header.getPlayers(); j++) {
                points[j] += (float) row.getPoints(j) / rows;
                if (row.getPoints(j) == 0) zeros[j]++;
            }
        }

        FragmentManager fm = getFragmentManager();
        StatsDialog dialog = new StatsDialog();

        Bundle bundle = new Bundle();
        String[] head = new String[StatsDialog.MAX_COLUMNS];
        head[0] = "\'0\'";
        head[1] = "Ø";
        bundle.putStringArray(StatsDialog.Head_Key, head);
        bundle.putStringArray(StatsDialog.Names_Key, header.getNames());
        bundle.putIntArray(StatsDialog.Col1_Key, zeros);
        bundle.putFloatArray(StatsDialog.Col2_Key, points);
        bundle.putInt(StatsDialog.RoundNo_Key, rows);
        bundle.putString(Whobegins.Preferences_Key, NAME);
        String text = "";
        if (roundDuration.isReasonable()) {
            text = String.format(getString(R.string.duration), roundDuration.getRoundDuration());
        }
        bundle.putString(StatsDialog.Text_Key, text);
        dialog.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        dialog.setArguments(bundle);
        dialog.show(fm, getString(R.string.stats));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        NAME = "PunkteTafel";
        View mainView = super.onCreateView(inflater, container, R.layout.punktetafelfragment);

        mainView.findViewById(R.id.addButton).setOnClickListener(this);

        return mainView;
    }

    @Override
    protected void getSpecificPreferences() {
        final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getActivity());
        if (!settings.getBoolean("enableppr", true)) {
            pointsperround = 0;
        } else {
            pointsperround = Integer.parseInt(settings.getString("ppr", "157"));
        }
    }

    @Override
    public void onClick(View view) {
        enterRound(-1, "");
    }

    @Override
    public void addPoints(int[] pts) {
        PunkteTafelRow newRow = createRow();
        newRow.setPoints(pts);

        addRow(newRow);
        checkForWinner();
    }

    @Override
    protected void checkForWinner() {
        final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String goal = settings.getString("goal", getString(R.string.nogoal));
        if (goal.equals(getString(R.string.nogoal))) return;

        int goalvalue = Integer.parseInt(settings.getString("goalvalue", "0"));
        if (goalvalue == 0) return;
        boolean higherIsBetter = settings.getBoolean("higherIsBetter", true);

        List<Footer.Player> ranking = footer.getRanking(header.getNames(), higherIsBetter);
        if (goal.equals(getString(R.string.rounds))) {
            if (adapter.getCount() == goalvalue) {
                showWinnerDialog(ranking);
            } else {
                winner = 0;
            }
        } else {
            if (ranking.get(0).points >= goalvalue || ranking.get(ranking.size() - 1).points >= goalvalue) {
                showWinnerDialog(ranking);
            } else {
                winner = 0;
            }
        }
    }

    private void showWinnerDialog(List<Footer.Player> ranking) {
        if (winner != 0 && winner == ranking.get(0).no) {
            return;
        }
        winner = ranking.get(0).no;
        VibraHelper.vibrate(getActivity(), 1000);
        if (ranking.get(0).points != ranking.get(1).points) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle(R.string.gameover);
            alertDialog.setNeutralButton(android.R.string.ok, null);
            alertDialog.setMessage(String.format(getString(R.string.winnerrounds), ranking.get(0).name));
            alertDialog.show();
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle(R.string.gameover);
            alertDialog.setNeutralButton(android.R.string.ok, null);
            StringBuilder str = new StringBuilder();

            for (Footer.Player i : ranking) {
                if (i.points == ranking.get(0).points) {
                    if (i != ranking.get(0)) {
                        str.append(", ");
                    }
                    str.append(i.name);
                }
            }

            String string = str.toString();
            int i = string.lastIndexOf(',');
            String[] winnerstrings = {string.substring(0, i), string.substring(i + 2)};
            alertDialog.setMessage(String.format(getString(R.string.winnersrounds), winnerstrings[0], winnerstrings[1]));
            alertDialog.show();
        }
    }

    @Override
    public void onFinishDialog(int dialogId, String userInput) {
        header.onFinishDialog(dialogId, userInput);
    }

    @Override
    public void onFinishDialog(int dialogId, Integer userInput) {

    }
}