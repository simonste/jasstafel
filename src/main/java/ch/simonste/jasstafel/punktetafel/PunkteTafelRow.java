package ch.simonste.jasstafel.punktetafel;

import android.app.Activity;
import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import ch.simonste.helpers.NumberTextView;
import ch.simonste.helpers.ScreenPct;
import ch.simonste.jasstafel.R;
import ch.simonste.jasstafel.common.ListRow;
import ch.simonste.jasstafel.common.RowInterface;

public class PunkteTafelRow extends ListRow implements RowInterface {
    private final NumberTextView[] pointsTW = new NumberTextView[MaxPlayers];

    public PunkteTafelRow(Context context) {
        super(context);

        LayoutInflater.from(context).inflate(R.layout.listrow, this);

        ScreenPct screenPct = new ScreenPct(((Activity) context).getWindowManager().getDefaultDisplay());
        LinearLayout linearLayout = (LinearLayout) getChildAt(0);
        for (int i = 0; i < pointsTW.length; i++) {
            pointsTW[i] = (NumberTextView) linearLayout.getChildAt(i + 2);
            pointsTW[i].setTextSize(TypedValue.COMPLEX_UNIT_PX, (float) 3.0 * screenPct.getPct());
        }
    }

    @Override
    public void restore(String string) {
        if (string.length() == 0) return;
        String[] elemstrs = string.split(";");
        if (elemstrs.length != 6 && elemstrs.length != PunkteTafelFragment.MAX_PLAYERS) return;

        for (int i = 0; i < elemstrs.length; i++) {
            if ("null".equals(elemstrs[i])) {
                pointsTW[i] = null;
            } else {
                pointsTW[i].setText(elemstrs[i]);
            }
        }
    }

    @Override
    protected void updateColumns() {
        for (int i = 0; i < MaxPlayers; i++) {
            if (i < columns) {
                pointsTW[i].setVisibility(View.VISIBLE);
            } else {
                pointsTW[i].setVisibility(View.GONE);
            }
        }
        roundNo = no + 1;
        ((NumberTextView) findViewById(R.id.roundNo)).setInt(roundNo);
    }

    public void setPoints(int[] pts) {
        for (int i = 0; i < MaxPlayers; i++) {
            pointsTW[i].setInt(pts[i]);
        }
    }

    @Override
    public String getString() {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < MaxPlayers; i++) {
            str.append(pointsTW[i].getInt());
            str.append(";");
        }
        return str.toString();
    }

    @Override
    public int[] updatePoints(Integer[] newPoints) {
        int[] diff = new int[PunkteTafelFragment.MAX_PLAYERS];
        for (int i = 0; i < PunkteTafelFragment.MAX_PLAYERS; i++) {
            int oldPts = pointsTW[i].getInt();

            if (newPoints[i] == null) {
                diff[i] = oldPts;
            } else {
                int newPts = newPoints[i];
                diff[i] = newPts - oldPts;
                pointsTW[i].setInt(newPts);
            }
        }
        return diff;
    }

    @Override
    public int getPoints(int playerNo) {
        return pointsTW[playerNo].getInt();
    }

    @Override
    protected boolean isEmpty(int i) {
        return pointsTW[i].getInteger() == null;
    }

}
