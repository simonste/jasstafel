package ch.simonste.jasstafel;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.preference.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.zip.GZIPOutputStream;

// Convert preferences to a format which is readable by Jasstafel V2
public class PreferenceConverterV2 {
    static private SharedPreferences defaultPreferences;
    static private SharedPreferences flutterPreferences;

    public static void convert(Context context) {
        defaultPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        flutterPreferences = context.getSharedPreferences("FlutterSharedPreferences", Context.MODE_PRIVATE);

        schieber(context);
        coiffeur(context);
        punktetafel(context);
        molotow(context);
        differenzler(context);
        guggitaler(context);

        Log.d("ConvertedPreferences", flutterPreferences.getAll().toString());
    }

    private static void putBoolean(String key, boolean value) {
        flutterPreferences.edit().putBoolean(String.format("flutter.%s", key), value).apply();
    }

    private static void putInt(String key, int value) {
        flutterPreferences.edit().putInt(String.format("flutter.%s", key), value).apply();
    }

    private static void putString(String key, String value) {
        flutterPreferences.edit().putString(String.format("flutter.%s", key), value).apply();
    }

    private static void putStringSet(String key, Set<String> value) {
        flutterPreferences.edit().putStringSet(String.format("flutter.%s", key), value).apply();
    }


    private static String toIso(long ts_milli) {
        if (ts_milli == 0) {
            return null;
        }
        Instant instant = Instant.ofEpochMilli(ts_milli);
        ZoneId zoneId = ZoneId.of("Europe/Paris");
        return instant.atZone(zoneId).format(DateTimeFormatter.ISO_INSTANT);
    }

    private static Map<String, String> splitToMap(String input) {
        Map<String, String> output = new HashMap<>();

        String[] list = input.split(";");
        for (String l : list) {
            String[] pair = l.split(":");
            output.put(pair[0], pair[1]);
        }
        return output;
    }

    private static String gzipCompressToBase64(final String content) throws IOException {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            GZIPOutputStream gzipOutputStream = new GZIPOutputStream(out);

            gzipOutputStream.write(content.getBytes("UTF-8"));
            gzipOutputStream.close();
            return android.util.Base64.encodeToString(out.toByteArray(), android.util.Base64.NO_WRAP);
        } catch (IOException e) {
            return "";
        }
    }

    private static JSONArray getPlayers(SharedPreferences sPref, Context context) {
        JSONArray players = new JSONArray();
        players.put(sPref.getString("Player1", context.getString(R.string.defpl1)));
        players.put(sPref.getString("Player2", context.getString(R.string.defpl2)));
        players.put(sPref.getString("Player3", context.getString(R.string.defpl3)));
        players.put(sPref.getString("Player4", context.getString(R.string.defpl4)));
        players.put(sPref.getString("Player5", context.getString(R.string.defpl5)));
        players.put(sPref.getString("Player6", context.getString(R.string.defpl6)));
        players.put(sPref.getString("Player7", context.getString(R.string.defpl7)));
        players.put(sPref.getString("Player8", context.getString(R.string.defpl8)));
        return players;
    }

    private static JSONObject getCommonPrefs(SharedPreferences sPref) throws JSONException {
        JSONObject common = new JSONObject();
        JSONObject timestamps = new JSONObject();
        timestamps.put("startTime", toIso(sPref.getLong("StartTime", 0) * 1000));
        timestamps.put("endTime", toIso(sPref.getLong("EndTime", 0) * 1000));
        common.put("timestamps", timestamps);
        JSONObject whoIsNext = new JSONObject();
        whoIsNext.put("swapPlayers", "");
        whoIsNext.put("whoBeginsOffset", null);
        common.put("whoIsNext", whoIsNext);
        return common;
    }

    @SuppressLint("DefaultLocale")
    private static JSONObject convert_schieber_round(Context context, String sPrefName) throws JSONException {
        SharedPreferences sPref = context.getSharedPreferences(sPrefName, Context.MODE_PRIVATE);

        JSONObject schieber = new JSONObject();
        {
            JSONArray teams = new JSONArray();
            for (int t = 1; t <= 2; t++) {
                JSONObject team = new JSONObject();
                team.put("name", sPref.getString(String.format("TeamName%d", t), String.format("Team %d", t)));
                if (t == 2 && defaultPreferences.getBoolean("differentGoals", false)) {
                    team.put("goalPoints", Integer.parseInt(sPref.getString("GoalPoints1", "2500")));
                } else {
                    team.put("goalPoints", Integer.parseInt(sPref.getString("GoalPoints", "2500")));
                }
                if (sPref.getInt("Bergpreis", 0) == t) {
                    team.put("hill", true);
                }
                team.put("win", null);
                team.put("flip", false);
                JSONArray strokes = new JSONArray();
                strokes.put(sPref.getInt(String.format("Points%d_1", t), 0));
                strokes.put(sPref.getInt(String.format("Points%d_20", t), 0));
                strokes.put(sPref.getInt(String.format("Points%d_50", t), 0));
                strokes.put(sPref.getInt(String.format("Points%d_100", t), 0));
                strokes.put(sPref.getInt(String.format("Points%d_500", t), 0));

                team.put("strokes", strokes);
                teams.put(team);
            }
            schieber.put("team", teams);
        }
        schieber.put("goalRounds", Integer.parseInt(sPref.getString("GoalRounds", "8")));

        {
            JSONArray rounds = new JSONArray();
            for (int i = 0; i < sPref.getInt("HistorySize", 0); i++) {
                int p1 = sPref.getInt(String.format("HistoryPts1_%d", i), 0);
                int p2 = sPref.getInt(String.format("HistoryPts2_%d", i), 0);

                if (p1 != 0 || p2 != 0) { // fix empty rows
                    JSONObject history_row = new JSONObject();
                    history_row.put("time", toIso(sPref.getLong(String.format("History_TS%d", i), System.currentTimeMillis()- (2*60+i)*60*1000)));
                    JSONArray pts = new JSONArray();
                    pts.put(p1);
                    pts.put(p2);
                    history_row.put("pts", pts);
                    rounds.put(history_row);
                }
            }

            schieber.put("rounds", rounds);
        }
        {
            JSONObject statistics = new JSONObject();
            JSONArray teams = new JSONArray();
            for (int t = 1; t <= 2; t++) {
                JSONObject team = new JSONObject();
                team.put("wins", sPref.getInt(String.format("Wins%d", t), 0));
                team.put("hills", sPref.getInt(String.format("Bergpreis%d", t), 0));
                team.put("weis", sPref.getInt(String.format("Weis%d", t), 0));
                team.put("matches", sPref.getInt(String.format("Matches%d", t), 0));
                team.put("pts", sPref.getInt(String.format("Pts%d", t), 0));

                teams.put(team);
            }
            statistics.put("team", teams);
            statistics.put("duration", sPref.getInt("Duration", 0));
            schieber.put("statistics", statistics);
        }

        {
            JSONArray backside = new JSONArray();
            for (int t = 1; t <= 6; t++) {
                JSONObject bs_row = new JSONObject();
                bs_row.put("name", sPref.getString(String.format("ColName%d", t), String.format("Team %d", t)));
                bs_row.put("strokes", sPref.getInt(String.format("SStrokes%d", t), 0));
                backside.put(bs_row);
            }
            schieber.put("backside", backside);
        }

        schieber.put("common", getCommonPrefs(sPref));

        return schieber;
    }

    private static int schieber_goal(String goal) {
        if (goal.equals("Runden") || goal.equals("Rounds") || goal.equals("Tours")) {
            return 2;
        }
        return 1;
    }

    private static void schieber(Context context) {
        putBoolean("schieber_touch_screen", defaultPreferences.getBoolean("allowTouch", true));
        putBoolean("schieber_backside", defaultPreferences.getBoolean("backside", false));
        putBoolean("schieber_big_score", defaultPreferences.getBoolean("bigScore", false));
        putBoolean("schieber_different_goals", defaultPreferences.getBoolean("differentGoals", false));
        putBoolean("schieber_draw_Z", defaultPreferences.getBoolean("drawZ", true));
        putInt("schieber_backside_columns", Integer.parseInt(defaultPreferences.getString("backsideColumns", "2")));
        putInt("schieber_match", Integer.parseInt(defaultPreferences.getString("matchPoints", "257")));
        putInt("schieber_goal_type", schieber_goal(defaultPreferences.getString("goal", context.getString(R.string.points))));
        putBoolean("schieber_vibrate", defaultPreferences.getBoolean("vibrate", true));

        try {
            putString("schieber", convert_schieber_round(context, "Schieber").toString());
        } catch (JSONException e) {
            Log.e("JSON", e.toString());
        }

        schieber_profiles(context);
    }

    private static void schieber_profiles(Context context) {
        SharedPreferences sPref = context.getSharedPreferences("SchieberProfiles", Context.MODE_PRIVATE);

        String activeProfile = sPref.getString("ActiveProfile", "Standard");
        putString("schieber_profile", activeProfile);

        Map<String, ?> profileMap = sPref.getAll();
        profileMap.remove("ActiveProfile");

        Set<String> profiles = new HashSet<>();
        try {
            for (String entry : profileMap.keySet()) {
                JSONObject schieberProfile = new JSONObject();

                String prof_string = profileMap.get(entry).toString();
                if (!prof_string.isEmpty()) {
                    Map<String, String> map = splitToMap(prof_string);
                    schieberProfile.put("schieber_goal_type", schieber_goal(map.get("goal")));
                    schieberProfile.put("schieber_different_goals", Boolean.parseBoolean(map.get("differentGoals")));
                    schieberProfile.put("schieber_match", Integer.parseInt(map.get("PointsPerRound")) + 100);
                    schieberProfile.put("schieber_backside", Boolean.parseBoolean(map.get("backside")));
                    schieberProfile.put("schieber_backside_columns", Integer.parseInt(map.get("backsideColumns")));
                    schieberProfile.put("schieber_touch_screen", Boolean.parseBoolean(map.get("allowTouch")));
                    schieberProfile.put("schieber_vibrate", true);
                    schieberProfile.put("schieber_big_score", Boolean.parseBoolean(map.get("bigScore")));
                    schieberProfile.put("schieber_draw_Z", Boolean.parseBoolean(map.get("drawZ")));
                }
                String profile_pref_name = (entry.equals(activeProfile)) ? "Schieber" : "Schieber_" + entry;
                JSONObject schieber = convert_schieber_round(context, profile_pref_name);

                schieberProfile.put("schieber", schieber.toString());
                String zipped = gzipCompressToBase64(schieberProfile.toString());
                profiles.add(entry + ":" + zipped);

            }
        } catch (JSONException e) {
            Log.e("JSONException", e.toString());
        } catch (IOException e) {
            Log.e("IOException", e.toString());

        }
        putStringSet("schieber_profiles", profiles);
    }

    @SuppressLint("DefaultLocale")
    private static JSONObject convert_coiffeur_round(Context context, String sPrefName) throws JSONException {
        SharedPreferences sPref = context.getSharedPreferences(sPrefName, Context.MODE_PRIVATE);

        JSONObject coiffeur = new JSONObject();

        int matchPoints = Integer.parseInt(defaultPreferences.getString("matchPoints", "257"));

        {
            JSONArray teams = new JSONArray();
            teams.put(sPref.getString("TeamName1", "Team 1"));
            teams.put(sPref.getString("TeamName2", "Team 2"));
            teams.put(sPref.getString("TeamName3", "Team 3"));
            coiffeur.put("teamName", teams);
        }
        {
            String[] trump_strings = context.getResources().getStringArray(R.array.trumpf_arr);
            int[] defaultTrump = {0, 1, 2, 3, 4, 9, 10, 11, 12, 13, 14, 15, 16};

            JSONArray rows = new JSONArray();
            for (int i = 1; i <= 12; i++) {
                JSONObject row = new JSONObject();
                row.put("factor", sPref.getInt(String.format("Factor%d", i), i));
                row.put("type", sPref.getString(String.format("Description%d", i), trump_strings[defaultTrump[i]]));

                JSONArray pts = new JSONArray();
                for (int t = 1; t <= 3; t++) {
                    JSONObject pt = new JSONObject();
                    String p = sPref.getString(String.format("Points%d_%d", t, i), "null");

                    Integer points = null;
                    boolean match = false;
                    try {
                        points = Integer.parseInt(p);
                        if (points == -3513) {
                            points = null;
                        }
                        if (points != null && points >= matchPoints) {
                            match = true;
                            points = matchPoints;
                        }
                    } catch (NumberFormatException e) {
                    }
                    pt.put("match", match);
                    pt.put("scratched", p.equals("-3513"));
                    pt.put("pts", points);
                    pts.put(pt);
                }
                row.put("pts", pts);
                rows.put(row);
            }
            coiffeur.put("rows", rows);
        }
        coiffeur.put("common", getCommonPrefs(sPref));
        return coiffeur;
    }

    private static void coiffeur(Context context) {
        putInt("coiffeur_rows", Integer.parseInt(defaultPreferences.getString("rows", "11")));
        putBoolean("coiffeur_3teams", defaultPreferences.getBoolean("3teams", false));
        putInt("coiffeur_match", Integer.parseInt(defaultPreferences.getString("matchPoints", "257")));
        putBoolean("coiffeur_rounded", defaultPreferences.getBoolean("den10", false));
        putBoolean("coiffeur_third_column", defaultPreferences.getBoolean("3rd_column", false));
        putBoolean("coiffeur_custom_factor", defaultPreferences.getBoolean("setFactorManually", true));
        putBoolean("coiffeur_bonus", defaultPreferences.getBoolean("matchBonus", false));
        putInt("coiffeur_bonus_value", Integer.parseInt(defaultPreferences.getString("matchBonusVal", "500")));
        putInt("coiffeur_counter", -Integer.parseInt(defaultPreferences.getString("matchMalusVal", "-500")));

        try {
            putString("coiffeur", convert_coiffeur_round(context, "Coiffeur").toString());
        } catch (JSONException e) {
            Log.e("JSON", e.toString());
        }

        coiffeur_profiles(context);
    }

    private static void coiffeur_profiles(Context context) {
        SharedPreferences sPref = context.getSharedPreferences("CoiffeurProfiles", Context.MODE_PRIVATE);

        String activeProfile = sPref.getString("ActiveProfile", "Standard");
        putString("coiffeur_profile", activeProfile);

        Map<String, ?> profileMap = sPref.getAll();
        profileMap.remove("ActiveProfile");

        Set<String> profiles = new HashSet<>();
        try {
            for (String entry : profileMap.keySet()) {
                JSONObject coiffeurProfile = new JSONObject();

                String prof_string = profileMap.get(entry).toString();
                if (!prof_string.isEmpty()) {
                    Map<String, String> map = splitToMap(prof_string);
                    coiffeurProfile.put("coiffeur_rows", Integer.parseInt(map.get("rows")));
                    coiffeurProfile.put("coiffeur_3teams", Boolean.parseBoolean(map.get("3teams")));
                    coiffeurProfile.put("coiffeur_match", Integer.parseInt(map.get("matchPoints")));
                    coiffeurProfile.put("coiffeur_rounded", Boolean.parseBoolean(map.get("den10")));
                    coiffeurProfile.put("coiffeur_third_column", Boolean.parseBoolean(map.get("3rd_column")));
                    coiffeurProfile.put("coiffeur_custom_factor", Boolean.parseBoolean(map.get("setFactorManually")));
                    coiffeurProfile.put("coiffeur_bonus", Boolean.parseBoolean(map.get("matchBonus")));
                    coiffeurProfile.put("coiffeur_bonus_value", Integer.parseInt(map.get("matchBonusVal")));
                    coiffeurProfile.put("coiffeur_counter", Integer.parseInt(map.get("matchMalusVal")));
                }

                String profile_pref_name = (entry.equals(activeProfile)) ? "Coiffeur" : "Coiffeur_" + entry;
                JSONObject coiffeur = convert_coiffeur_round(context, profile_pref_name);

                coiffeurProfile.put("coiffeur", coiffeur.toString());
                String zipped = gzipCompressToBase64(coiffeurProfile.toString());
                profiles.add(entry + ":" + zipped);
            }
        } catch (JSONException e) {
            Log.e("JSONException", e.toString());
        } catch (IOException e) {
            Log.e("IOException", e.toString());

        }
        putStringSet("coiffeur_profiles", profiles);
    }


    @SuppressLint("DefaultLocale")
    private static JSONObject convert_punktetafel_round(Context context, String sPrefName) throws JSONException {
        SharedPreferences sPref = context.getSharedPreferences(sPrefName, Context.MODE_PRIVATE);

        JSONObject punktetafel = new JSONObject();
        {
            JSONArray players = getPlayers(sPref, context);
            punktetafel.put("playerName", players);
        }
        {
            JSONArray rows = new JSONArray();
            for (int r = 0; r < sPref.getInt("Rows", 0); r++) {
                JSONObject row = new JSONObject();
                JSONArray pts = new JSONArray();

                String[] pt = sPref.getString(String.format("row%d", r), ";;;;;;;;").split(";");
                for (String p : pt) {
                    Integer v = null;
                    try {
                        v = Integer.parseInt(p);
                    } catch (NumberFormatException e) {

                    }
                    pts.put(v);
                }
                row.put("pts", pts);
                rows.put(row);
            }
            punktetafel.put("rows", rows);
        }
        punktetafel.put("common", getCommonPrefs(sPref));
        return punktetafel;
    }

    private static int punktetafel_goal(String goal) {
        if (goal.equals("kein Ziel") || goal.equals("no target") || goal.equals("Aucune cible")) {
            return 0;
        } else if (goal.equals("Runden") || goal.equals("Rounds") || goal.equals("Tours")) {
            return 2;
        }
        return 1;
    }

    private static void punktetafel(Context context) {
        putInt("point_board_players", Integer.parseInt(defaultPreferences.getString("diffPlayers", "4")));
        putBoolean("point_board_enable_points_per_round", defaultPreferences.getBoolean("enableppr", true));
        putInt("point_board_points_per_round", Integer.parseInt(defaultPreferences.getString("ppr", "157")));
        putBoolean("point_board_rounded", defaultPreferences.getBoolean("den10", false));
        putInt("point_board_goal_type", punktetafel_goal(defaultPreferences.getString("goal", context.getString(R.string.nogoal))));
        putInt("point_board_goal_points", Integer.parseInt(defaultPreferences.getString("goalvalue", "100")));
        putInt("point_board_goal_rounds", Integer.parseInt(defaultPreferences.getString("goalvalue", "10")));
        putBoolean("point_board_goal_maximum", defaultPreferences.getBoolean("higherIsBetter", true));

        try {
            putString("point_board", convert_punktetafel_round(context, "PunkteTafel").toString());
        } catch (JSONException e) {
            Log.e("JSON", e.toString());
        }

        punktetafel_profiles(context);
    }

    private static void punktetafel_profiles(Context context) {
        SharedPreferences sPref = context.getSharedPreferences("PunkteTafelProfiles", Context.MODE_PRIVATE);

        String activeProfile = sPref.getString("ActiveProfile", "Standard");
        putString("point_board_profile", activeProfile);

        Map<String, ?> profileMap = sPref.getAll();
        profileMap.remove("ActiveProfile");

        Set<String> profiles = new HashSet<>();
        try {
            for (String entry : profileMap.keySet()) {
                JSONObject punktetafelProfile = new JSONObject();

                String prof_string = profileMap.get(entry).toString();
                if (!prof_string.isEmpty()) {
                    Map<String, String> map = splitToMap(prof_string);
                    punktetafelProfile.put("point_board_players", Integer.parseInt(map.get("diffPlayers")));
                    punktetafelProfile.put("point_board_points_per_round", Integer.parseInt(map.get("ppr")));
                    punktetafelProfile.put("point_board_rounded", Boolean.parseBoolean(map.get("den10")));
                    punktetafelProfile.put("point_board_goal_type", punktetafel_goal(map.get("goal")));
                    punktetafelProfile.put("point_board_goal_points", Integer.parseInt(map.get("goalvalue")));
                    punktetafelProfile.put("point_board_goal_rounds", Integer.parseInt(map.get("goalvalue")));
                    punktetafelProfile.put("point_board_goal_maximum", Boolean.parseBoolean(map.get("higherIsBetter")));
                }
                String profile_pref_name = (entry.equals(activeProfile)) ? "PunkteTafel" : "PunkteTafel_" + entry;
                JSONObject punktetafel = convert_punktetafel_round(context, profile_pref_name);

                punktetafelProfile.put("point_board", punktetafel.toString());
                String zipped = gzipCompressToBase64(punktetafelProfile.toString());
                profiles.add(entry + ":" + zipped);

            }
        } catch (JSONException e) {
            Log.e("JSONException", e.toString());
        } catch (IOException e) {
            Log.e("IOException", e.toString());

        }
        putStringSet("point_board_profiles", profiles);
    }


    @SuppressLint("DefaultLocale")
    private static JSONObject convert_molotow_round(Context context, String sPrefName) throws JSONException {
        SharedPreferences sPref = context.getSharedPreferences(sPrefName, Context.MODE_PRIVATE);

        JSONObject molotow = new JSONObject();
        {
            JSONArray players = getPlayers(sPref, context);
            molotow.put("playerName", players);
        }
        {
            JSONArray rows = new JSONArray();
            for (int r = 0; r < sPref.getInt("Rows", 0); r++) {
                JSONObject row = new JSONObject();
                JSONArray pts = new JSONArray();

                String[] pt = sPref.getString(String.format("row%d", r), ";;;;;;;;").split(";");
                boolean uneven = false;
                int total = 0;
                for (String p : pt) {
                    Integer v = null;

                    try {
                        v = Integer.parseInt(p);
                        total += v;
                        if (v % 10 != 0) {
                            uneven = true;
                        }
                    } catch (NumberFormatException e) {

                    }
                    pts.put(v);
                }
                row.put("isRound", uneven && total > 100);
                row.put("pts", pts);
                rows.put(row);
            }
            molotow.put("rows", rows);
        }
        molotow.put("common", getCommonPrefs(sPref));
        return molotow;
    }

    private static void molotow(Context context) {
        putInt("molotow_players", Integer.parseInt(defaultPreferences.getString("diffPlayers", "4")));
        putInt("molotow_points_per_round", Integer.parseInt(defaultPreferences.getString("ppr", "157")));
        putBoolean("molotow_rounded", defaultPreferences.getBoolean("den10", false));
        putInt("molotow_goal_type", 0);

        try {
            putString("molotow", convert_molotow_round(context, "Molotow").toString());
        } catch (JSONException e) {
            Log.e("JSON", e.toString());
        }

        molotow_profiles(context);
    }

    private static void molotow_profiles(Context context) {
        SharedPreferences sPref = context.getSharedPreferences("MolotowProfiles", Context.MODE_PRIVATE);

        String activeProfile = sPref.getString("ActiveProfile", "Standard");
        putString("molotow_profile", activeProfile);

        Map<String, ?> profileMap = sPref.getAll();
        profileMap.remove("ActiveProfile");

        Set<String> profiles = new HashSet<>();
        try {
            for (String entry : profileMap.keySet()) {
                JSONObject molotowProfile = new JSONObject();

                String prof_string = profileMap.get(entry).toString();
                if (!prof_string.isEmpty()) {
                    Map<String, String> map = splitToMap(prof_string);
                    molotowProfile.put("molotow_players", Integer.parseInt(map.get("diffPlayers")));
                    molotowProfile.put("molotow_points_per_round", Integer.parseInt(map.get("PointsPerRound")));
                    molotowProfile.put("molotow_rounded", Boolean.parseBoolean(map.get("den10")));
                    molotowProfile.put("molotow_goal_type", 0);
                }
                String profile_pref_name = (entry.equals(activeProfile)) ? "Molotow" : "Molotow_" + entry;
                JSONObject molotow = convert_molotow_round(context, profile_pref_name);

                molotowProfile.put("molotow", molotow.toString());
                String zipped = gzipCompressToBase64(molotowProfile.toString());
                profiles.add(entry + ":" + zipped);

            }
        } catch (JSONException e) {
            Log.e("JSONException", e.toString());
        } catch (IOException e) {
            Log.e("IOException", e.toString());

        }
        putStringSet("molotow_profiles", profiles);
    }

    @SuppressLint("DefaultLocale")
    private static JSONObject convert_differenzler_round(Context context, String sPrefName) throws JSONException {
        SharedPreferences sPref = context.getSharedPreferences(sPrefName, Context.MODE_PRIVATE);

        JSONObject differenzler = new JSONObject();
        {
            JSONArray players = getPlayers(sPref, context);
            differenzler.put("playerName", players);
        }
        {
            JSONArray rows = new JSONArray();
            for (int r = 0; r < sPref.getInt("Rows", 0); r++) {
                JSONObject row = new JSONObject();
                JSONArray guesses = new JSONArray();
                JSONArray pts = new JSONArray();

                String[] pt = sPref.getString(String.format("row%d", r), ";;;;;;;;").split(";");
                for (String p : pt) {
                    Integer g = null;
                    Integer v = null;

                    try {
                        String[] tmp = p.split(":");
                        g = Integer.parseInt(tmp[0]);
                        v = Integer.parseInt(tmp[1]);
                    } catch (NumberFormatException e) {

                    }
                    guesses.put(g);
                    pts.put(v);
                }
                row.put("guesses", guesses);
                row.put("pts", pts);
                rows.put(row);
            }
            differenzler.put("rows", rows);
        }
        differenzler.put("common", getCommonPrefs(sPref));
        return differenzler;
    }

    private static void differenzler(Context context) {
        putInt("differenzler_players", Integer.parseInt(defaultPreferences.getString("diffPlayers", "4")));
        putInt("differenzler_points_per_round", Integer.parseInt(defaultPreferences.getString("ppr", "157")));
        putBoolean("differenzler_hide_guess", defaultPreferences.getBoolean("hideGuess", true));

        try {
            putString("differenzler", convert_differenzler_round(context, "Differenzler").toString());
        } catch (JSONException e) {
            Log.e("JSON", e.toString());
        }

        differenzler_profiles(context);
    }

    private static void differenzler_profiles(Context context) {
        SharedPreferences sPref = context.getSharedPreferences("DifferenzlerProfiles", Context.MODE_PRIVATE);

        String activeProfile = sPref.getString("ActiveProfile", "Standard");
        putString("differenzler_profile", activeProfile);

        Map<String, ?> profileMap = sPref.getAll();
        profileMap.remove("ActiveProfile");

        Set<String> profiles = new HashSet<>();
        try {
            for (String entry : profileMap.keySet()) {
                JSONObject differenzlerProfile = new JSONObject();

                String prof_string = profileMap.get(entry).toString();
                if (!prof_string.isEmpty()) {
                    Map<String, String> map = splitToMap(prof_string);
                    differenzlerProfile.put("differenzler_players", Integer.parseInt(map.get("diffPlayers")));
                    differenzlerProfile.put("differenzler_points_per_round", Integer.parseInt(map.get("PointsPerRound")));
                    differenzlerProfile.put("differenzler_hide_guess", Boolean.parseBoolean(map.get("hideGuess")));
                }
                String profile_pref_name = (entry.equals(activeProfile)) ? "Differenzler" : "Differenzler_" + entry;
                JSONObject differenzler = convert_differenzler_round(context, profile_pref_name);

                differenzlerProfile.put("differenzler", differenzler.toString());
                String zipped = gzipCompressToBase64(differenzlerProfile.toString());
                profiles.add(entry + ":" + zipped);

            }
        } catch (JSONException e) {
            Log.e("JSONException", e.toString());
        } catch (IOException e) {
            Log.e("IOException", e.toString());

        }
        putStringSet("differenzler_profiles", profiles);
    }

    @SuppressLint("DefaultLocale")
    private static JSONObject convert_guggitaler_round(Context context, String sPrefName) throws JSONException {
        SharedPreferences sPref = context.getSharedPreferences(sPrefName, Context.MODE_PRIVATE);

        JSONObject guggitaler = new JSONObject();
        {
            JSONArray players = getPlayers(sPref, context);
            guggitaler.put("playerName", players);
        }
        {
            JSONArray rows = new JSONArray();
            for (int r = 0; r < sPref.getInt("Rows", 0); r++) {
                JSONObject row = new JSONObject();
                JSONArray pts = new JSONArray();

                String[] pt = sPref.getString(String.format("row%d", r), ";;;;;;;;").split(";");
                for (String p : pt) {
                    JSONArray ppts = new JSONArray();
                    Integer p5 = null;
                    Integer p10 = null;
                    Integer p20 = null;
                    Integer p40 = null;
                    Integer p50 = null;

                    String[] tmp = p.split(":");
                    try {
                        p5 = Integer.parseInt(tmp[0]);
                        p10 = Integer.parseInt(tmp[1]);
                        p20 = Integer.parseInt(tmp[2]);
                        p40 = Integer.parseInt(tmp[3]);
                        p50 = Integer.parseInt(tmp[4]);
                    } catch (NumberFormatException e) {

                    }
                    ppts.put(p5);
                    ppts.put(p10);
                    ppts.put(p20);
                    ppts.put(p40);
                    ppts.put(p50);

                    pts.put(ppts);
                }
                row.put("pts", pts);
                rows.put(row);
            }
            guggitaler.put("rows", rows);
        }
        guggitaler.put("common", getCommonPrefs(sPref));
        return guggitaler;
    }

    private static void guggitaler(Context context) {
        putInt("guggitaler_players", Integer.parseInt(defaultPreferences.getString("diffPlayers", "4")));

        try {
            putString("guggitaler", convert_guggitaler_round(context, "Guggitaler").toString());
        } catch (JSONException e) {
            Log.e("JSON", e.toString());
        }

        guggitaler_profiles(context);
    }

    private static void guggitaler_profiles(Context context) {
        SharedPreferences sPref = context.getSharedPreferences("GuggitalerProfiles", Context.MODE_PRIVATE);

        String activeProfile = sPref.getString("ActiveProfile", "Standard");
        putString("guggitaler_profile", activeProfile);

        Map<String, ?> profileMap = sPref.getAll();
        profileMap.remove("ActiveProfile");

        Set<String> profiles = new HashSet<>();
        try {
            for (String entry : profileMap.keySet()) {
                JSONObject guggitalerProfile = new JSONObject();

                String prof_string = profileMap.get(entry).toString();
                if (!prof_string.isEmpty()) {
                    Map<String, String> map = splitToMap(prof_string);
                    guggitalerProfile.put("guggitaler_players", Integer.parseInt(map.get("diffPlayers")));
                }
                String profile_pref_name = (entry.equals(activeProfile)) ? "Guggitaler" : "Guggitaler_" + entry;
                JSONObject guggitaler = convert_guggitaler_round(context, profile_pref_name);

                guggitalerProfile.put("guggitaler", guggitaler.toString());
                String zipped = gzipCompressToBase64(guggitalerProfile.toString());
                profiles.add(entry + ":" + zipped);

            }
        } catch (JSONException e) {
            Log.e("JSONException", e.toString());
        } catch (IOException e) {
            Log.e("IOException", e.toString());

        }
        putStringSet("guggitaler_profiles", profiles);
    }
}