package ch.simonste.jasstafel.differenzler;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.shamanland.fab.FloatingActionButton;

import ch.simonste.helpers.FabListView;
import ch.simonste.helpers.SimpleAlertDialog;
import ch.simonste.helpers.SimpleInputDialog;
import ch.simonste.jasstafel.R;
import ch.simonste.jasstafel.StatsDialog;
import ch.simonste.jasstafel.Whobegins;
import ch.simonste.jasstafel.common.ListFragment;

public class DifferenzlerFragment extends ListFragment<DifferenzlerRow> implements
        AdapterView.OnItemClickListener,
        SimpleInputDialog.SimpleDialogListener,
        FabListView.OnSizeChangedListener {

    private final LinearLayout[] guessButtons = new LinearLayout[MAX_PLAYERS];
    private LinearLayout guessView;
    private View FABcontainer;

    @Override
    protected DifferenzlerRow createRow() {
        return new DifferenzlerRow(getActivity());
    }

    @Override
    public void showStats() {
        float[] guesses = new float[header.getPlayers()];
        int[] zeroGuess = new int[header.getPlayers()];
        int[] zeroDiff = new int[header.getPlayers()];

        float avgGuessingTotal = 0;

        int rows = adapter.getCount() - 1;
        for (int i = 0; i < rows; i++) {
            DifferenzlerRow row = adapter.getItem(i);
            avgGuessingTotal += (float) row.getTotalGuessed() / rows;
            for (int j = 0; j < header.getPlayers(); j++) {
                guesses[j] += (float) row.getGuess(j) / rows;
                if (row.getGuess(j) == 0) zeroGuess[j]++;
                if (row.getGuess(j) == row.getPointsMade(j)) zeroDiff[j]++;
            }
        }

        FragmentManager fm = getFragmentManager();
        StatsDialog dialog = new StatsDialog();

        Bundle bundle = new Bundle();
        String[] head = new String[StatsDialog.MAX_COLUMNS];
        head[0] = getString(R.string.avgGuess);
        head[1] = getString(R.string.zeroGuess);
        head[2] = getString(R.string.zeroDiff);
        bundle.putStringArray(StatsDialog.Head_Key, head);
        bundle.putStringArray(StatsDialog.Names_Key, header.getNames());
        bundle.putFloatArray(StatsDialog.Col1_Key, guesses);
        bundle.putIntArray(StatsDialog.Col2_Key, zeroGuess);
        bundle.putIntArray(StatsDialog.Col3_Key, zeroDiff);
        bundle.putInt(StatsDialog.RoundNo_Key, rows);
        bundle.putString(Whobegins.Preferences_Key, NAME);
        String avgGuessingTotalStr = String.valueOf(Math.round(avgGuessingTotal * 10.0f) / 10.0f);
        String text = String.format(getString(R.string.avgRoundGuess), avgGuessingTotalStr);
        if (roundDuration.isReasonable()) {
            text += " - " + String.format(getString(R.string.duration), roundDuration.getRoundDuration());
        }
        bundle.putString(StatsDialog.Text_Key, text);
        dialog.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        dialog.setArguments(bundle);
        dialog.show(fm, getString(R.string.stats));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        NAME = "Differenzler";
        View mainView = super.onCreateView(inflater, container, R.layout.differenzlerfragment);

        mainView.findViewById(R.id.addButton).setOnClickListener(this);
        guessView = mainView.findViewById(R.id.guess_actions);
        FABcontainer = mainView.findViewById(R.id.FABcontainter);
        listView.setOnItemClickListener(this);

        for (int i = 0; i < MAX_PLAYERS; i++) {
            guessButtons[i] = (LinearLayout) guessView.getChildAt(i + 1);
            FloatingActionButton fab = (FloatingActionButton) guessButtons[i].getChildAt(0);
            fab.setOnClickListener(this);
            fab.setTag(i);
        }

        return mainView;
    }

    @Override
    public void notify(boolean scrollable) {
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) guessView.getLayoutParams();
        if (scrollable) {
            params.addRule(RelativeLayout.ALIGN_TOP, R.id.list);
            params.addRule(RelativeLayout.ALIGN_BOTTOM, 0);
            int pxMargin = (int) getResources().getDimension(R.dimen.guess_fab_offset);
            params.setMargins(0, 0, 0, pxMargin);
        } else {
            params.addRule(RelativeLayout.ALIGN_TOP, 0);
            params.addRule(RelativeLayout.ALIGN_BOTTOM, R.id.list);
            int pxMargin = (int) getResources().getDimension(R.dimen.guess_fab_margin_minus);
            params.setMargins(0, 0, 0, pxMargin);
        }
        guessView.setLayoutParams(params);
    }

    @Override
    protected void finishSpecificPreferences() {
        if (adapter.isEmpty()) {
            DifferenzlerRow newRow = createRow();
            addRow(newRow);
        }

        for (int i = 0; i < MAX_PLAYERS; i++) {
            if (i < header.getPlayers()) {
                guessButtons[i].setVisibility(View.VISIBLE);
            } else {
                guessButtons[i].setVisibility(View.GONE);
            }
        }

        switch (adapter.getLastItem().getState()) {
            case Open:
                FABcontainer.setVisibility(View.INVISIBLE);
                for (int j = 0; j < header.getPlayers(); j++) {
                    if (adapter.getLastItem().getState(j) == DifferenzlerRow.State.Guessed) {
                        guessButtons[j].setVisibility(View.INVISIBLE);
                    }
                }
                break;
            case Guessed:
                FABcontainer.setVisibility(View.VISIBLE);
                for (int j = 0; j < header.getPlayers(); j++) {
                    guessButtons[j].setVisibility(View.INVISIBLE);
                }
                break;
            case Finished:
                FABcontainer.setVisibility(View.INVISIBLE);
                break;
        }
    }

    @Override
    public void addPoints(int[] pts) {
        if (adapter.getLastItem().getState() == DifferenzlerRow.State.Guessed) {
            adapter.getLastItem().setPoints(pts);
            backup(adapter.getCount(), adapter.getCount() - 1, adapter.getLastItem().getString());
            footer.addRound(adapter.getLastItem());

            DifferenzlerRow newRow = createRow();
            addRow(newRow);

            for (int i = 0; i < header.getPlayers(); i++) {
                guessButtons[i].setVisibility(View.VISIBLE);
            }
            FABcontainer.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        try {
            int playerNo = (Integer) v.getTag();
            showDialogSimple(playerNo);
        } catch (NullPointerException e) {
            enterRound(-1, "");
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        DifferenzlerRow row = adapter.getItem(position);
        if (row.getState() == DifferenzlerRow.State.Finished) {
            int guessed = row.getTotalGuessed();
            int points = row.getTotalPoints();

            showRoundSummary(position + 1, guessed, points);
        }
    }

    private void showDialogSimple(int playerNo) {
        FragmentManager fm = getFragmentManager();
        SimpleInputDialog dialog = new SimpleInputDialog();
        dialog.setArguments(playerNo, SimpleInputDialog.DIALOG_INPUT_NUMBER, 0);
        dialog.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        dialog.setTargetFragment(this, 0);
        dialog.show(fm, String.format(getString(R.string.enterguess), header.getPlayerName(playerNo)));
    }

    private void showRoundSummary(int round, int guessed, int points) {
        FragmentManager fm = getFragmentManager();
        SimpleAlertDialog dialog = new SimpleAlertDialog();
        String str1 = String.format(getString(R.string.totalguessed), guessed);
        String str2 = String.format(getString(R.string.totalpoints), points);
        dialog.setMessage(str1 + "\n\n" + str2);
        dialog.show(fm, String.format(getString(R.string.roundsummary), round));
    }

    @Override
    public void onFinishDialog(int playerNo, String name) {
        header.onFinishDialog(playerNo, name);
    }

    @Override
    public void onFinishDialog(int player, Integer userInput) {
        if (userInput == null) return;
        if (adapter.getCount() == 1) {
            roundDuration.addFirstPoints();
        }

        if (adapter.getLastItem().getState() == DifferenzlerRow.State.Open) {
            adapter.getLastItem().setGuess(player, userInput);
            guessButtons[player].setVisibility(View.INVISIBLE);
        }
        backup(adapter.getCount(), adapter.getCount() - 1, adapter.getLastItem().getString());
        adapter.notifyDataSetChanged();

        if (adapter.getLastItem().getState() == DifferenzlerRow.State.Guessed) {
            FABcontainer.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
        if (adapter.getItem(i).getState() == DifferenzlerRow.State.Finished) {
            enterRound(i, adapter.getItem(i).getPointsString());
        } else {
            if (!adapter.getItem(i).isHideGuess()) {
                enterRound(i, adapter.getItem(i).getGuessString());
            }
        }
        return true;
    }

    @Override
    public void editPoints(int id, Integer[] pts) {
        DifferenzlerRow row = adapter.getItem(id);
        if (row.getState() == DifferenzlerRow.State.Finished) {
            super.editPoints(id, pts);
        } else {
            for (int i = 0; i < MAX_PLAYERS; i++) {
                if (pts[i] != null) {
                    row.setGuess(i, pts[i]);
                    guessButtons[i].setVisibility(View.INVISIBLE);
                } else {
                    if (i < header.getPlayers()) {
                        row.resetGuess(i);
                        guessButtons[i].setVisibility(View.VISIBLE);
                    }
                }
            }
            backup(adapter.getCount(), adapter.getCount() - 1, adapter.getLastItem().getString());
            if (adapter.getLastItem().getState() == DifferenzlerRow.State.Guessed) {
                FABcontainer.setVisibility(View.VISIBLE);
            } else {
                FABcontainer.setVisibility(View.INVISIBLE);
            }
        }
    }
}
