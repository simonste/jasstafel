package ch.simonste.jasstafel.differenzler;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import androidx.preference.PreferenceManager;

import ch.simonste.helpers.NumberTextView;
import ch.simonste.helpers.ScreenPct;
import ch.simonste.jasstafel.R;

public class DifferenzlerElement extends LinearLayout {

    private DifferenzlerRow.State state = DifferenzlerRow.State.Open;

    private Integer guessValue = null;
    private NumberTextView guess;
    private NumberTextView points;
    private NumberTextView difference;
    private boolean hideGuess;

    public DifferenzlerElement(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater.from(context).inflate(R.layout.differenzlerelement, this);
        if (isInEditMode()) return;

        guess = findViewById(R.id.guess);
        points = findViewById(R.id.points);
        difference = findViewById(R.id.difference);
        difference.setText("-");

        ScreenPct screenPct = new ScreenPct(((Activity) context).getWindowManager().getDefaultDisplay());
        guess.setTextSize(TypedValue.COMPLEX_UNIT_PX, (float) 2.0 * screenPct.getPct());
        points.setTextSize(TypedValue.COMPLEX_UNIT_PX, (float) 2.0 * screenPct.getPct());
        difference.setTextSize(TypedValue.COMPLEX_UNIT_PX, (float) 3.5 * screenPct.getPct());

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        hideGuess = settings.getBoolean("hideGuess", true);
    }

    public void restore(String str) {
        String[] strpts = str.split(":");
        try {
            int guess = Integer.parseInt(strpts[0]);
            if (guess >= 0) {
                setGuess(guess);
            } else {
                guessValue = null;
            }
            int pts = Integer.parseInt(strpts[1]);
            if (pts >= 0) setPoints(pts);
        } catch (NumberFormatException ignored) {
        }
    }

    public DifferenzlerRow.State getState() {
        return state;
    }

    public int getGuess() {
        return guess.getInt();
    }

    public void setGuess(int guess) {
        guessValue = guess;
        if (hideGuess) {
            this.guess.setText("*");
        } else {
            this.guess.setInt(guess);
        }
        state = DifferenzlerRow.State.Guessed;
    }

    public void resetGuess() {
        guessValue = null;
        this.guess.setText("");
        state = DifferenzlerRow.State.Open;
    }

    public String getString() {
        String str = guessValue + ":";
        if (state != DifferenzlerRow.State.Finished) {
            str += "null";
        } else {
            str += points.getInt();
        }
        return str + ";";
    }

    public int getPoints() {
        return points.getInt();
    }

    public void setPoints(int pts) {
        if (state != DifferenzlerRow.State.Open) {
            guess.setInt(guessValue);
            points.setInt(pts);
            difference.setInt(Math.abs(guess.getInt() - this.points.getInt()));
        }
        state = DifferenzlerRow.State.Finished;
    }

    public int getDifference() {
        return difference.getInt();
    }

    public boolean isHideGuess() {
        return hideGuess;
    }
}
