package ch.simonste.jasstafel.differenzler;


import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;

import ch.simonste.helpers.SimpleInputDialog;
import ch.simonste.helpers.Utils;
import ch.simonste.jasstafel.R;
import ch.simonste.jasstafel.common.NFCUtils;


public class DifferenzlerActivity extends AppCompatActivity implements SimpleInputDialog.SimpleDialogListener {
    private DifferenzlerFragment fragment;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.setCommonPreferences(this);
        setTitle(R.string.differenzlertitle);
        setContentView(R.layout.differenzler);

        fragment = (DifferenzlerFragment) getFragmentManager().findFragmentById(R.id.fragment);

        NFCUtils.createSender(this, "Differenzler");
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        NFCUtils.handleIntent(this, intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.setCommonPreferences(this);
        onNewIntent(getIntent());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.tafelmenu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_reset:
                fragment.reset();
                break;
            case R.id.action_settings:
                startActivity(new Intent(this, DifferenzlerPreferences.class));
        }
        return true;
    }

    @Override
    public void onFinishDialog(int dialogId, String userInput) {
        fragment.onFinishDialog(dialogId, userInput);
    }

    @Override
    public void onFinishDialog(int dialogId, Integer userInput) {
        fragment.onFinishDialog(dialogId, userInput);
    }
}
