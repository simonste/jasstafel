package ch.simonste.jasstafel.differenzler;

import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;

import ch.simonste.jasstafel.PreferencesActivity;
import ch.simonste.jasstafel.R;
import ch.simonste.jasstafel.common.NFCUtils;
import ch.simonste.jasstafel.common.Profiles;


public class DifferenzlerPreferences extends PreferencesActivity {

    protected PreferenceFragment createFragment() {
        return new DifferenzlerPreferenceFragment();
    }

    public static class DifferenzlerPreferenceFragment extends PreferenceFragment {

        public DifferenzlerPreferenceFragment() {
        }

        private final Preference.OnPreferenceClickListener customListener = new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                if (preference.getKey().equals("profile")) {
                    Intent intent = new Intent(getActivity(), Profiles.class);
                    intent.putExtra("Name", "Differenzler");
                    startActivity(intent);
                }
                return true;
            }
        };

        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            getActivity().setTitle(R.string.differenzlersettings);
            addPreferencesFromResource(R.xml.differenzlerpreferences);

            findPreference("profile").setOnPreferenceClickListener(customListener);
            findPreference("language").setOnPreferenceChangeListener(languageWatcher);

            NFCUtils.updatePreference(findPreference("nfc"));
        }

    }
}
