package ch.simonste.jasstafel.differenzler;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import ch.simonste.helpers.NumberTextView;
import ch.simonste.jasstafel.R;
import ch.simonste.jasstafel.common.ListRow;
import ch.simonste.jasstafel.common.RowInterface;

public class DifferenzlerRow extends ListRow implements RowInterface {

    private final DifferenzlerElement[] elements = new DifferenzlerElement[MaxPlayers];
    private State state = State.Open;

    public DifferenzlerRow(Context context) {
        super(context);

        LayoutInflater.from(context).inflate(R.layout.differenzlerrow, this);
        if (isInEditMode()) return;

        LinearLayout linearLayout = (LinearLayout) getChildAt(0);
        for (int i = 0; i < elements.length; i++) {
            elements[i] = (DifferenzlerElement) linearLayout.getChildAt(i + 2);
        }
    }

    @Override
    public void restore(String string) {
        Log.i("Restore differenzler: ", string);

        String[] elemstrs = string.split(";");
        boolean guessed = true;
        for (int i = 0; i < MaxPlayers; i++) {
            if (i < elemstrs.length) {
                elements[i].restore(elemstrs[i]);
            }
            if (elements[i].getState() == State.Open && i < columns) {
                guessed = false;
            }
        }

        if (guessed) {
            state = State.Guessed;
        }
        if (elements[0].getState() == State.Finished) {
            state = State.Finished;
        }
    }

    @Override
    public String getString() {
        StringBuilder str = new StringBuilder();
        for (DifferenzlerElement element : elements) {
            str.append(element.getString());
        }
        return str.toString();
    }

    @Override
    protected void updateColumns() {
        for (int i = 0; i < MaxPlayers; i++) {
            if (i < columns) {
                elements[i].setVisibility(View.VISIBLE);
            } else {
                elements[i].setVisibility(View.GONE);
            }
        }
        roundNo = no + 1;
        ((NumberTextView) findViewById(R.id.roundNo)).setInt(roundNo);
        if (state != State.Finished) {
            roundNo = -1;
        }
    }

    @Override
    public int getPoints(int player) {
        return elements[player].getDifference();
    }

    public int getPointsMade(int player) {
        return elements[player].getPoints();
    }

    public int getGuess(int player) {
        return elements[player].getGuess();
    }

    public State getState() {
        return state;
    }

    public State getState(int i) {
        return elements[i].getState();
    }

    public void setGuess(int player, int guess) {
        elements[player].setGuess(guess);

        for (int i = 0; i < columns; i++) {
            if (elements[i].getState() == State.Open) {
                state = State.Open;
                return;
            }
        }
        state = State.Guessed;
    }

    public void resetGuess(int player) {
        elements[player].resetGuess();
        state = State.Open;
    }

    public int getTotalGuessed() {
        int total = 0;
        for (DifferenzlerElement element : elements) {
            total += element.getGuess();
        }
        return total;
    }

    public int getTotalPoints() {
        int total = 0;
        for (DifferenzlerElement element : elements) {
            total += element.getPoints();
        }
        return total;
    }


    public void setPoints(int[] pts) {
        for (int i = 0; i < elements.length; i++) {
            elements[i].setPoints(pts[i]);
        }
        state = State.Finished;
        roundNo = no + 1;
    }

    public String getGuessString() {
        StringBuilder str = new StringBuilder();
        for (DifferenzlerElement element : elements) {
            if (element.getState() != State.Guessed) {
                str.append("null;");
            } else {
                str.append(element.getGuess());
                str.append(";");
            }
        }
        return str.toString();
    }

    public String getPointsString() {
        StringBuilder str = new StringBuilder();
        for (DifferenzlerElement element : elements) {
            str.append(element.getPoints());
            str.append(";");
        }
        return str.toString();
    }

    @Override
    public boolean isEmpty(int i) {
        return (getState(i) == State.Open);
    }

    public int[] updatePoints(Integer[] newPoints) {
        int[] diff = new int[MaxPlayers];
        for (int i = 0; i < MaxPlayers; i++) {
            int oldDiff = 0;
            if (elements[i].getPoints() != -1) {
                oldDiff = elements[i].getDifference();
            }

            if (newPoints[i] == null) {
                elements[i].setPoints(0);
                diff[i] = elements[i].getGuess() - oldDiff;
            } else {
                int newPts = newPoints[i];
                elements[i].setPoints(newPts);

                diff[i] = elements[i].getDifference() - oldDiff;
            }
        }
        return diff;
    }

    public boolean isHideGuess() {
        return elements[0].isHideGuess();
    }

    public enum State {Open, Guessed, Finished}
}
