package ch.simonste.jasstafel;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import org.jetbrains.annotations.NotNull;

import ch.simonste.helpers.Utils;

public abstract class PreferencesActivity extends AppCompatActivity {

    private static Activity activity;

    protected static final Preference.OnPreferenceChangeListener validWatcher = new Preference.OnPreferenceChangeListener() {
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            try {
                final boolean negative = preference.getKey().equals("matchMalusVal");
                int factor = negative ? -1 : 1;
                int min = negative ? 0 : 1;
                if (!newValue.equals("") && (Integer.valueOf((String) newValue) * factor >= min)) {
                    EditTextPreference editPref = (EditTextPreference) preference;
                    int value = Integer.valueOf((String) newValue);
                    editPref.setText(String.valueOf(value));
                    return true;
                }
            } catch (NumberFormatException e) {
                // do not change anything
            }
            Toast.makeText(PreferencesActivity.activity.getApplicationContext(), R.string.invalidInput, Toast.LENGTH_SHORT).show();
            return false;
        }
    };

    protected static final Preference.OnPreferenceChangeListener languageWatcher = new Preference.OnPreferenceChangeListener() {
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            activity.startActivity(new Intent(activity, MainActivity.class));
            return true;
        }
    };

    abstract protected PreferenceFragment createFragment();

    public void resultAction(int action) {
        Intent intent = new Intent();
        intent.putExtra("Action", action);
        setResult(RESULT_OK, intent);
        finish();
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) actionBar.setDisplayHomeAsUpEnabled(false);
        overridePendingTransition(R.anim.translate_left_in, R.anim.fade_out);
        activity = this;

        // Display the fragment as the main content.
        getFragmentManager().beginTransaction().replace(android.R.id.content, createFragment()).commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.setCommonPreferences(this);

        // reload settings after return from profiles activity
        getFragmentManager().beginTransaction().replace(android.R.id.content, createFragment()).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.preferencemenu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NotNull MenuItem item) {
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.translate_left_out);
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.fade_in, R.anim.translate_left_out);
    }
}