package ch.simonste.jasstafel.common;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;
import androidx.preference.PreferenceManager;

import ch.simonste.helpers.ScreenPct;
import ch.simonste.helpers.SimpleInputDialog;
import ch.simonste.jasstafel.R;

public class Header extends LinearLayout
        implements View.OnClickListener, SimpleInputDialog.SimpleDialogListener {

    // names must be static to get changes even in case of rotate during inputdialog
    private static final String[] names = new String[8];
    private static final TextView[] namesTW = new TextView[8];
    private final Context context;
    private int players;
    private SharedPreferences settings;
    private int MaxPlayers;


    public Header(Context contxt, AttributeSet attrs) {
        super(contxt, attrs);
        context = contxt;

        LayoutInflater.from(context).inflate(R.layout.header, this);
        if (isInEditMode()) return;

        MaxPlayers = context.getResources().getInteger(R.integer.maxPlayers);

        settings = PreferenceManager.getDefaultSharedPreferences(context);

        ScreenPct screenPct = new ScreenPct(((Activity) context).getWindowManager().getDefaultDisplay());
        float fontSize = screenPct.getPct() * 2.0f;
        ((TextView) findViewById(R.id.type)).setTextSize(TypedValue.COMPLEX_UNIT_PX, 1.2f * fontSize);

        LinearLayout linearLayout = (LinearLayout) this.getChildAt(0);
        for (int i = 0; i < MaxPlayers; i++) {
            namesTW[i] = (TextView) linearLayout.getChildAt(i + 2);
            namesTW[i].setTag(i);
            namesTW[i].setOnClickListener(this);
            namesTW[i].setTextSize(TypedValue.COMPLEX_UNIT_PX, fontSize);
        }
    }

    public void reload() {
        names[0] = settings.getString("PlayerName1", context.getString(R.string.defpl1));
        names[1] = settings.getString("PlayerName2", context.getString(R.string.defpl2));
        names[2] = settings.getString("PlayerName3", context.getString(R.string.defpl3));
        names[3] = settings.getString("PlayerName4", context.getString(R.string.defpl4));
        names[4] = settings.getString("PlayerName5", context.getString(R.string.defpl5));
        names[5] = settings.getString("PlayerName6", context.getString(R.string.defpl6));
        names[6] = settings.getString("PlayerName7", context.getString(R.string.defpl7));
        names[7] = settings.getString("PlayerName8", context.getString(R.string.defpl8));
        players = Integer.parseInt(settings.getString("diffPlayers", "4"));

        for (int i = 0; i < MaxPlayers; i++) {
            namesTW[i].setText(names[i]);
            if (i < players) {
                namesTW[i].setVisibility(View.VISIBLE);
            } else {
                namesTW[i].setVisibility(View.GONE);
            }
        }
    }

    public int getPlayers() {
        return players;
    }

    public String getPlayerName(int playerNo) {
        return names[playerNo];
    }

    public String[] getNames() {
        return names;
    }

    @Override
    public void onClick(View v) {
        int playerNo = (Integer) v.getTag();
        FragmentManager fm = ((Activity) context).getFragmentManager();
        SimpleInputDialog dialog = new SimpleInputDialog();
        dialog.setArguments(playerNo, SimpleInputDialog.DIALOG_INPUT_STRING, getPlayerName(playerNo));
        dialog.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        dialog.setTarget(this);
        dialog.show(fm, context.getString(R.string.enterplayername));
    }

    @Override
    public void onFinishDialog(int playerNo, String userInput) {
        if (userInput.equals("")) return;

        // only accept non empty names
        namesTW[playerNo].setText(userInput);
        names[playerNo] = userInput;
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(String.format("PlayerName%1$s", playerNo + 1), userInput);
        editor.apply();
    }

    @Override
    public void onFinishDialog(int dialogId, Integer userInput) {
    }
}
