package ch.simonste.jasstafel.common;

import android.content.Context;
import android.graphics.Color;
import android.widget.LinearLayout;

import ch.simonste.helpers.NumberTextView;
import ch.simonste.jasstafel.R;

abstract public class ListRow extends LinearLayout {
    static protected int MaxPlayers;
    protected int columns;
    protected int no;
    protected double factor;
    protected int roundNo = 0;

    public ListRow(Context context) {
        super(context, null);

        MaxPlayers = context.getResources().getInteger(R.integer.maxPlayers);
    }

    final public void setColumns(int columns) {
        this.columns = columns;
    }

    final public void setNo(int no) {
        this.no = no;

        if (isComplete()) {
            this.setBackgroundColor(Color.BLACK);
        }
    }

    protected boolean isComplete() {
        return true;
    }

    protected abstract void updateColumns();

    public abstract void restore(String string);

    public abstract String getString();

    public abstract int[] updatePoints(Integer[] newPoints);

    public void setFactor(double factor) {
        this.factor = factor;
    }

    final public boolean isEmpty() {
        for (int i = 0; i < MaxPlayers; i++) {
            if (!isEmpty(i)) {
                return false;
            }
        }
        return true;
    }

    protected abstract boolean isEmpty(int i);

    final public int getRoundNo() {
        return roundNo;
    }

    final public void resetRoundNo() {
        roundNo = 0;
        ((NumberTextView) findViewById(R.id.roundNo)).setText("");
        this.setBackgroundColor(Color.TRANSPARENT);
    }
}
