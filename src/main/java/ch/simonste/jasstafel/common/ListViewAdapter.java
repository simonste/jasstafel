package ch.simonste.jasstafel.common;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;

import java.util.ArrayList;

public class ListViewAdapter<T> extends ArrayAdapter<T> {
    private int columns = 6;

    public ListViewAdapter(Context context, int textViewResourceId, ArrayList<T> objects) {
        super(context, textViewResourceId, objects);
    }

    public void setPlayers(int players) {
        if (columns != players) {
            columns = players;
        }
    }

    public T getLastItem() {
        if (getCount() > 0)
            return getItem(getCount() - 1);
        return null;
    }

    @NonNull
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        ListRow item = (ListRow) getItem(position);
        item.setColumns(columns);
        item.setNo(position);
        item.updateColumns();
        return item;
    }
}

