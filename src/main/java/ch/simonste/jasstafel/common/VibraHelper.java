package ch.simonste.jasstafel.common;

import android.content.Context;
import android.os.Build;
import android.os.VibrationEffect;

public class VibraHelper {

    public static void vibrate(Context context, int vibrateMS) {
        android.os.Vibrator v = (android.os.Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        if (v != null && v.hasVibrator()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                v.vibrate(VibrationEffect.createOneShot(vibrateMS, VibrationEffect.DEFAULT_AMPLITUDE));
            } else {
                v.vibrate(vibrateMS);
            }
        }
    }
}
