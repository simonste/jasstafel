package ch.simonste.jasstafel.common;

public interface RowInterface {
    int getPoints(int playerNo);
}

