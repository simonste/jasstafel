package ch.simonste.jasstafel.common;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.os.Bundle;
import android.os.Parcelable;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import ch.simonste.jasstafel.R;


public class NFCUtils {
    private static String type;

    public static void updatePreference(Preference preference) {
        NfcAdapter adapter = NfcAdapter.getDefaultAdapter(preference.getContext());
        if (adapter == null) {
            preference.setSummary(preference.getContext().getString(R.string.nfc_notavailable));
            preference.setEnabled(false);
        } else if (!adapter.isEnabled()) {
            preference.setSummary(preference.getContext().getString(R.string.nfc_disabled));
            preference.setOnPreferenceClickListener(new OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {
                    preference.getContext().startActivity(new Intent(android.provider.Settings.ACTION_NFC_SETTINGS));
                    return true;
                }
            });
        } else {
            preference.setSummary(preference.getContext().getString(R.string.nfc_enabled));
        }
    }

    public static void handleIntent(final Activity activity, final Intent intent) {
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())) {
            Parcelable[] rawMessages = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            if (rawMessages != null) {
                final String message = new String(((NdefMessage) rawMessages[0]).getRecords()[0].getPayload());
                Log.d("NFCUtils", "handleIntent '" + message);

                final String[] content = message.split(ProfileUtils.separator);
                final String profileName = content[0];
                final String defaultSettings = content[1];
                final String specificSettings = content[2];

                SharedPreferences profilePreferences = activity.getSharedPreferences(type + "Profiles", Context.MODE_PRIVATE);
                final String currentActiveProfile = profilePreferences.getString("ActiveProfile", "Standard");
                AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
                if (profileName.equals(currentActiveProfile) || !profilePreferences.getString(profileName, "").isEmpty()) {
                    dialog.setTitle(activity.getString(R.string.replaceprofile, profileName));
                    dialog.setMessage(R.string.replaceprofilehint);
                } else {
                    dialog.setTitle(String.format(activity.getString(R.string.importprofile), profileName));
                    dialog.setMessage(String.format(activity.getString(R.string.importprofilehint), currentActiveProfile));
                }
                dialog.setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                restoreSettings(activity, profileName, defaultSettings, specificSettings);
                                activity.recreate();
                            }
                        }
                );
                dialog.setNeutralButton(android.R.string.no, null);
                dialog.show();
            }
            intent.replaceExtras(new Bundle());
        }
    }

    public static void createSender(final AppCompatActivity activity, final String type) {
        NFCUtils.type = type;
        NfcAdapter adapter = NfcAdapter.getDefaultAdapter(activity);
        if (adapter == null || !adapter.isEnabled()) {
            return;
        }
        Log.d("NFCUtils", "NFC ready");
        adapter.setNdefPushMessageCallback(new NfcAdapter.CreateNdefMessageCallback() {
            @Override
            public NdefMessage createNdefMessage(NfcEvent event) {
                String message = ProfileUtils.dumpSettings(activity, type);

                return new NdefMessage(NdefRecord.createExternal("ch.simonste.jasstafel", type.toLowerCase(),
                        message.getBytes()));
            }
        }, activity);
    }

    private static void restoreSettings(final Context context,
                                        final String profileName,
                                        final String defaultSettings,
                                        final String specificSettings) {
        SharedPreferences profilePreferences = context.getSharedPreferences(type + "Profiles", Context.MODE_PRIVATE);
        final String currentActiveProfile = profilePreferences.getString("ActiveProfile", "Standard");

        Log.d("NFCUtils", "ActiveProfile '" + currentActiveProfile + "' changed to: '" + profileName + "'");
        Log.d("NFCUtils", "DefaultSettings: " + defaultSettings);
        Log.d("NFCUtils", "SpecificSettings: " + specificSettings);

        // backup current profile
        ProfileUtils.moveSettings(context, type, type + "_" + currentActiveProfile);
        profilePreferences.edit()
                .putString(currentActiveProfile, ProfileUtils.collectDefaultPreferences(context))
                .putString(profileName, defaultSettings)
                .putString("ActiveProfile", profileName).apply();

        // restore
        ProfileUtils.restoreSettings(PreferenceManager.getDefaultSharedPreferences(context), defaultSettings, true);
        ProfileUtils.restoreSettings(context.getSharedPreferences(type, Context.MODE_PRIVATE), specificSettings, false);
    }
}
