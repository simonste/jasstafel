package ch.simonste.jasstafel.common;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Set;

import ch.simonste.helpers.SimpleInputDialog;
import ch.simonste.jasstafel.PreferencesActivity;
import ch.simonste.jasstafel.R;

public class Profiles extends PreferencesActivity implements SimpleInputDialog.SimpleDialogListener {
    private static final Comparator<String> alphabetically = new Comparator<String>() {
        @Override
        public int compare(String lhs, String rhs) {
            return lhs.toLowerCase().compareTo(rhs.toLowerCase());
        }
    };
    ProfilesFragment profilesFragment;

    protected PreferenceFragment createFragment() {
        profilesFragment = new ProfilesFragment();
        profilesFragment.setArguments(getIntent().getExtras());
        return profilesFragment;
    }

    @Override
    public void onFinishDialog(int dialogId, String userInput) {
        profilesFragment.onFinishDialog(dialogId, userInput);
    }

    @Override
    public void onFinishDialog(int dialogId, Integer userInput) {
        profilesFragment.onFinishDialog(dialogId, userInput);
    }

    public static class ProfilesFragment extends PreferenceFragment implements SimpleInputDialog.SimpleDialogListener {
        String name;
        private ArrayList<String> profiles;
        private ListView listView;
        private SharedPreferences profilePreferences;
        private Activity activity;


        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            getActivity().setTitle(R.string.profiles);
            name = getArguments().getString("Name");
        }

        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View mainView = inflater.inflate(R.layout.profiles, container, false);

            activity = getActivity();
            profilePreferences = activity.getSharedPreferences(name + "Profiles", 0);

            profiles = new ArrayList<>();
            final ArrayAdapter<String> adapter = new ArrayAdapter<>(activity, android.R.layout.simple_list_item_single_choice, profiles);

            listView = mainView.findViewById(R.id.list);
            listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            listView.setAdapter(adapter);

            final String activeProfile = profilePreferences.getString("ActiveProfile", "Standard");
            Set<String> keys = profilePreferences.getAll().keySet();
            if (keys.isEmpty()) {
                profilePreferences.edit().putString("Standard", "").apply();
                keys = profilePreferences.getAll().keySet();
            }
            for (String key : keys) {
                if (!key.equals("ActiveProfile")) {
                    profiles.add(key);
                }
            }
            adapter.sort(alphabetically);
            listView.setItemChecked(adapter.getPosition(activeProfile), true);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    selectProfile(adapter, i);
                }
            });

            listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                    showDialogSimple(i, ((TextView) view).getText().toString());
                    return true;
                }
            });

            mainView.findViewById(R.id.addButton).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showDialogSimple(-1, "");
                }
            });

            return mainView;
        }

        private void selectProfile(ArrayAdapter<String> adapter, int i) {
            String newDefaultPreferences = profilePreferences.getString(adapter.getItem(i), "");
            final String activeProfile = profilePreferences.getString("ActiveProfile", "Standard");
            // backup current settings
            ProfileUtils.moveSettings(activity, name, name + "_" + activeProfile);
            profilePreferences.edit().putString(activeProfile, ProfileUtils.collectDefaultPreferences(activity)).apply();
            // restore profile settings
            ProfileUtils.moveSettings(activity, name + "_" + adapter.getItem(i), name);
            SharedPreferences preferences = getPreferenceManager().getSharedPreferences();
            ProfileUtils.restoreSettings(preferences, newDefaultPreferences, true);
            profilePreferences.edit().putString("ActiveProfile", adapter.getItem(i)).apply();
        }

        private void showDialogSimple(int team, String value) {
            FragmentManager fm = getFragmentManager();
            ProfilenameDialog dialog = new ProfilenameDialog();
            dialog.setTarget(this);
            dialog.setArguments(team, SimpleInputDialog.DIALOG_INPUT_STRING, value);
            dialog.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
            dialog.show(fm, getResources().getString(R.string.profilename));
        }

        @Override
        public void onFinishDialog(int rowNo, String userInput) {
            final SharedPreferences.Editor profileEditor = profilePreferences.edit();
            ArrayAdapter<String> adapter = (ArrayAdapter<String>) listView.getAdapter();
            if (rowNo >= 0 && userInput.equals(profiles.get(rowNo)) || userInput.isEmpty()) {
                // nothing changed
                return;
            }
            if (profiles.contains(userInput)) {
                Toast.makeText(activity.getApplicationContext(), R.string.addprofilefail, Toast.LENGTH_LONG).show();
                return;
            }

            if (rowNo < 0) {
                // add new Profile
                profileEditor.putString(userInput, "");
                adapter.add(userInput);
                adapter.sort(alphabetically);
                final int i = adapter.getPosition(userInput);
                listView.setItemChecked(i, true);
                selectProfile(adapter, i);
            } else {
                String activeProfile = profilePreferences.getString("ActiveProfile", "Standard");
                if (userInput.equals(ProfilenameDialog.DELETE) && profiles.get(rowNo).equals(activeProfile)) {
                    Toast.makeText(activity.getApplicationContext(), R.string.deleteprofilefail, Toast.LENGTH_LONG).show();
                    return;
                }

                // rename / remove
                final String previousName = profiles.get(rowNo);
                String profile = profilePreferences.getString(previousName, "");
                profileEditor.remove(previousName);

                if (userInput.equals(ProfilenameDialog.DELETE)) {
                    profiles.remove(rowNo);
                    listView.setItemChecked(profiles.indexOf(activeProfile), true);
                    activity.getSharedPreferences(name + "_" + previousName, MODE_PRIVATE).edit().clear().apply();
                } else {
                    profileEditor.putString(userInput, profile);
                    if (previousName.equals(activeProfile)) {
                        profileEditor.putString("ActiveProfile", userInput);
                    }
                    profiles.set(rowNo, userInput);
                }
            }
            profileEditor.apply();
        }

        @Override
        public void onFinishDialog(int dialogId, Integer userInput) {
            throw new AssertionError("Profiles never have integer input");
        }
    }
}