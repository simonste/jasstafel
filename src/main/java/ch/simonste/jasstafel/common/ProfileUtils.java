package ch.simonste.jasstafel.common;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.preference.PreferenceManager;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

class ProfileUtils {
    static private final String[] defaultDelimiter = {":", ";"}; // used for profile / nfc
    static private final String[] specificDelimiter = {"#", "§"}; // used for nfc
    static final String separator = "###";

    private static String dumpPreferences(SharedPreferences preferences, boolean defaultPrefs) {
        String[] delimiter = defaultPrefs ? defaultDelimiter : specificDelimiter;
        StringBuilder result = new StringBuilder();
        Map<String, ?> map = preferences.getAll();
        for (String key : map.keySet()) {
            Object val = map.get(key);
            if (map.get(key) instanceof String) {
                val = ((String) val).replace(delimiter[1], delimiter[0]);
            }
            result.append(key).append(delimiter[0]).append(val).append(delimiter[1]);
        }
        return result.toString();
    }

    private static String collectPreferences(Context context, String type) {
        SharedPreferences preferences = context.getSharedPreferences(type, Context.MODE_PRIVATE);
        return dumpPreferences(preferences, false);
    }

    static String collectDefaultPreferences(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return dumpPreferences(preferences, true);
    }

    static String dumpSettings(Activity activity, String type) {
        final String activeProfile = activity.getSharedPreferences(type + "Profiles", Context.MODE_PRIVATE).
                getString("ActiveProfile", "Standard");
        return activeProfile +
                separator + ProfileUtils.collectDefaultPreferences(activity) +
                separator + ProfileUtils.collectPreferences(activity, type);
    }

    static void restoreSettings(SharedPreferences prefs, String settingsString, boolean defaultPrefs) {
        String[] delimiter = defaultPrefs ? defaultDelimiter : specificDelimiter;

        SharedPreferences.Editor preferences = prefs.edit();
        final boolean isSchieber = settingsString.contains("Bergpreis");
        for (String set : settingsString.split(delimiter[1])) {
            try {
                String[] kv = set.split(delimiter[0], 2);
                if (kv.length < 2) {
                    continue;
                }
                if (kv[0].equals("Version")) {
                    preferences.putInt(kv[0], Integer.parseInt(kv[1]));
                    continue;
                }

                final List<String> booleans = Arrays.asList("den10", "matchBonus", "keepScreenOn", "3rd_column", "3teams",
                        "setFactorManually", "hideGuess", "enableppr", "higherIsBetter", "allowTouch", "vibrate",
                        "backside", "bigScore", "differentGoals", "drawZ", "undoButton", "reset", "flipUpper", "flipLower");

                if (kv[0].startsWith("History_TS") || kv[0].endsWith("Time")) {
                    preferences.putLong(kv[0], Long.parseLong(kv[1]));
                } else if (booleans.indexOf(kv[0]) >= 0) {
                    preferences.putBoolean(kv[0], Boolean.parseBoolean(kv[1]));
                } else if (kv[0].equals("RoundOffset") ||
                        kv[0].equals("Rows") ||
                        kv[0].equals("Duration") ||
                        kv[0].startsWith("Factor") ||
                        kv[0].startsWith("Type") ||
                        kv[0].startsWith("History") ||
                        kv[0].startsWith("ColStrokes") ||
                        kv[0].startsWith("Bergpreis") ||
                        kv[0].startsWith("Matches") ||
                        kv[0].startsWith("Pts") ||
                        kv[0].startsWith("Weis") ||
                        kv[0].startsWith("Win") ||
                        (kv[0].startsWith("Points") && isSchieber)) {
                    preferences.putInt(kv[0], Integer.parseInt(kv[1]));
                } else {
                    preferences.putString(kv[0], kv[1]);
                }
            } catch (NumberFormatException e) {
                Log.e("Jasstafel", e.toString());
            }
        }
        preferences.apply();
    }

    static void moveSettings(Context context, String from, String to) {
        from = from.replace("/", "-");
        to = to.replace("/", "-");

        SharedPreferences current = context.getSharedPreferences(from, Context.MODE_PRIVATE);
        SharedPreferences.Editor backup = context.getSharedPreferences(to, Context.MODE_PRIVATE).edit();

        Map<String, ?> all = current.getAll();
        for (Map.Entry<String, ?> v : all.entrySet()) {
            if (v.getValue().getClass().equals(Boolean.class))
                backup.putBoolean(v.getKey(), (Boolean) v.getValue());
            else if (v.getValue().getClass().equals(Integer.class))
                backup.putInt(v.getKey(), (Integer) v.getValue());
            else if (v.getValue().getClass().equals(Long.class))
                backup.putLong(v.getKey(), (Long) v.getValue());
            else if (v.getValue().getClass().equals(String.class))
                backup.putString(v.getKey(), (String) v.getValue());
        }
        backup.apply();
        current.edit().clear().apply();
    }
}
