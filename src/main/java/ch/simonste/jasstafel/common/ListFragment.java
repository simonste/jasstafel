package ch.simonste.jasstafel.common;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import java.util.ArrayList;

import ch.simonste.helpers.FabListView;
import ch.simonste.helpers.UndoBar;
import ch.simonste.jasstafel.PreferenceConverterV2;
import ch.simonste.jasstafel.R;
import ch.simonste.jasstafel.RoundDuration;
import ch.simonste.jasstafel.RundenDialog;
import ch.simonste.jasstafel.RundenDialogInterface;

public abstract class ListFragment<T> extends Fragment
        implements View.OnClickListener, RundenDialogInterface,
        AdapterView.OnItemLongClickListener {

    public static final int MAX_PLAYERS = 8;
    private final View.OnClickListener showStatsListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            showStats();
        }
    };
    protected ListViewAdapter<T> adapter;
    protected Header header;
    protected Footer footer;
    protected FabListView listView;
    protected UndoBar undoBar;
    protected double factor = 1.0;
    protected int pointsperround = 157;
    protected RoundDuration roundDuration;
    protected String NAME;
    private SharedPreferences saveddata;

    abstract protected void showStats();

    protected void getSpecificPreferences() {
    }

    protected void finishSpecificPreferences() {
    }

    abstract protected T createRow();

    public View onCreateView(LayoutInflater inflater, ViewGroup container, int layoutId) {
        View mainView = inflater.inflate(layoutId, container, false);

        header = mainView.findViewById(R.id.header);
        footer = mainView.findViewById(R.id.footer);
        footer.setOnClickListener(showStatsListener);

        listView = mainView.findViewById(R.id.list);
        adapter = new ListViewAdapter<>(getActivity(), R.layout.listrow, new ArrayList<T>());
        listView.setAdapter(adapter);
        listView.setOnItemLongClickListener(this);
        listView.addContainer(mainView.findViewById(R.id.FABcontainter));

        saveddata = getActivity().getSharedPreferences(NAME, Context.MODE_PRIVATE);
        roundDuration = new RoundDuration(saveddata);
        return mainView;
    }

    final public void onResume() {
        super.onResume();
        getPreferences();
    }

    private void getPreferences() {
        header.reload();
        footer.refresh(header.getPlayers());
        adapter.setPlayers(header.getPlayers());

        adapter.clear();
        footer.resetPoints();

        getSpecificPreferences();

        int rows = saveddata.getInt("Rows", 0);
        for (int i = 0; i < rows; i++) {
            T newRow = createRow();
            ListRow listRow = (ListRow) newRow;
            listRow.setColumns(header.getPlayers());
            listRow.setFactor(factor);
            listRow.restore(saveddata.getString("row" + i, ""));
            adapter.add(newRow);
            footer.addRound((RowInterface) newRow);
        }

        finishSpecificPreferences();
    }

    protected void enterRound(int id, String points) {
        RundenDialog dialog = new RundenDialog();
        dialog.setTargetFragment(this, 0);
        Bundle bundle = new Bundle();
        bundle.putStringArray("Names", header.getNames());
        bundle.putInt("Players", header.getPlayers());
        bundle.putInt("Item", id);
        bundle.putString("Points", points);
        bundle.putDouble("Factor", factor);
        bundle.putInt("RoundTotal", pointsperround);
        bundle.putBoolean("enableOK", (pointsperround == 0));
        String title = getString(R.string.round);
        if (id >= 0) {
            int roundNo = ((ListRow) adapter.getItem(id)).getRoundNo();
            if (roundNo > 0) {
                title = String.format(getString(R.string.roundeditX), roundNo);
            } else if (roundNo == 0) {
                title = getString(R.string.roundedit);
            } else {
                title = getString(R.string.guessedit);
                bundle.putInt("RoundTotal", 0);
            }
        }
        dialog.setArguments(bundle);
        dialog.show(getFragmentManager(), title);
    }

    protected void addRow(T row) {
        adapter.add(row);
        backup(adapter.getCount(), adapter.getCount() - 1, ((ListRow) row).getString());
        footer.addRound((RowInterface) row);
        listView.smoothScrollToPosition(adapter.getCount());
    }

    @Override
    public void editPoints(int id, Integer[] pts) {
        ListRow row = (ListRow) adapter.getItem(id);

        int[] diff = row.updatePoints(pts);
        for (int i = 0; i < MAX_PLAYERS; i++) {
            footer.addPoints(i, (int) Math.round(factor * diff[i]));
        }
        int r = adapter.getCount();

        if (row.isEmpty()) {
            r--;
            adapter.remove((T) row);

            for (int i = id; i < adapter.getCount(); i++) {
                backup(r, i, ((ListRow) adapter.getItem(i)).getString());
            }
        } else {
            backup(r, id, row.getString());
        }
        adapter.notifyDataSetChanged();
        checkForWinner();
    }

    protected void checkForWinner() {
    }

    protected void backup(int rows, int row, String data) {
        SharedPreferences.Editor editor = saveddata.edit();
        editor.putString("row" + row, data);
        editor.putInt("Rows", rows);
        editor.apply();

        // update all settings
        PreferenceConverterV2.convert(getContext());
    }

    public void reset() {
        AlertDialog.Builder confirmreset = new AlertDialog.Builder(getActivity());
        confirmreset.setTitle(R.string.reset);
        confirmreset.setMessage(getString(R.string.resetConfirm));
        confirmreset.setPositiveButton(getString(android.R.string.yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        doReset();
                    }
                }
        );
        confirmreset.setNegativeButton(getString(android.R.string.no), null);
        confirmreset.show();
    }

    private void doReset() {
        SharedPreferences.Editor editor = saveddata.edit();
        editor.clear();
        editor.apply();
        adapter.clear();
        if (undoBar != null) undoBar.hide();
        footer.resetPoints();
        roundDuration.reset();
        getSpecificPreferences();
        finishSpecificPreferences();
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
        enterRound(i, ((ListRow) adapter.getItem(i)).getString());
        return true;
    }
}