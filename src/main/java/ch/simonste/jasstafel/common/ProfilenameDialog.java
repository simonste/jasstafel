package ch.simonste.jasstafel.common;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.Toolbar;

import ch.simonste.helpers.SimpleInputDialog;
import ch.simonste.jasstafel.R;

public class ProfilenameDialog extends SimpleInputDialog {
    static final public String DELETE = "-3517";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        if (tag >= 0) {
            toolbar.inflateMenu(R.menu.profilename);
            toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.action_delete:
                            target.onFinishDialog(tag, DELETE);
                            dismiss();
                            break;
                    }
                    return true;
                }
            });
        }
        return view;
    }
}
