package ch.simonste.jasstafel.common;


import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ch.simonste.helpers.NumberTextView;
import ch.simonste.helpers.ScreenPct;
import ch.simonste.jasstafel.R;

public class Footer extends LinearLayout {
    private final NumberTextView[] totals = new NumberTextView[8];

    private int MaxPlayers = 8;

    public Footer(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater.from(context).inflate(R.layout.footer, this);
        if (isInEditMode()) return;

        MaxPlayers = context.getResources().getInteger(R.integer.maxPlayers);

        ScreenPct screenPct = new ScreenPct(((Activity) context).getWindowManager().getDefaultDisplay());
        float fontSize = screenPct.getPct();
        ((TextView) findViewById(R.id.type)).setTextSize(TypedValue.COMPLEX_UNIT_PX, 2.5f * fontSize);

        LinearLayout linearLayout = (LinearLayout) this.getChildAt(0);
        for (int i = 0; i < MaxPlayers; i++) {
            totals[i] = (NumberTextView) linearLayout.getChildAt(i + 2);
            totals[i].setTextSize(TypedValue.COMPLEX_UNIT_PX, 3.3f * fontSize);
            totals[i].setInt(0);
        }
    }

    public void refresh(int players) {
        for (int i = 0; i < MaxPlayers; i++) {
            if (i < players) {
                totals[i].setVisibility(View.VISIBLE);
            } else {
                totals[i].setVisibility(View.GONE);
            }
        }
    }

    public void resetPoints() {
        for (NumberTextView tw : totals) {
            tw.setInt(0);
        }
    }

    public void addPoints(int playerNo, int points) {
        totals[playerNo].add(points);
    }

    public void addRound(RowInterface row) {
        for (int i = 0; i < MaxPlayers; i++) {
            addPoints(i, row.getPoints(i));
        }
    }

    private int getPoints(int playerNo) {
        return totals[playerNo].getInt();
    }

    public List<Player> getRanking(String[] names, boolean higherIsBetter) {
        List<Player> players = new ArrayList<>();
        for (int i = 0; i < MaxPlayers; i++) {
            if (totals[i].getVisibility() == VISIBLE) {
                Player p = new Player();
                p.no = i;
                p.name = names[i];
                p.points = getPoints(i);
                players.add(p);
            }
        }
        Collections.sort(players, new PlayerComparator(higherIsBetter));
        return players;
    }

    public class Player {
        public int no;
        public String name;
        public int points;
    }

    private class PlayerComparator implements Comparator<Player> {
        private final int factor;

        PlayerComparator(boolean higherIsBetter) {
            if (higherIsBetter)
                factor = 1;
            else
                factor = -1;
        }

        @Override
        public int compare(Player p1, Player p2) {
            return factor * (p2.points - p1.points);
        }
    }
}