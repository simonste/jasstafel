package ch.simonste.jasstafel.molotow;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import ch.simonste.helpers.NumberTextView;
import ch.simonste.helpers.ScreenPct;
import ch.simonste.jasstafel.R;
import ch.simonste.jasstafel.common.ListRow;
import ch.simonste.jasstafel.common.RowInterface;

public class MolotowRow extends ListRow implements RowInterface {

    public static int roundCounter;
    private final NumberTextView[] pointsTW = new NumberTextView[MaxPlayers];
    private final int[] points = new int[MaxPlayers];

    public MolotowRow(Context context) {
        super(context);

        LayoutInflater.from(context).inflate(R.layout.listrow, this);

        ScreenPct screenPct = new ScreenPct(((Activity) context).getWindowManager().getDefaultDisplay());
        LinearLayout linearLayout = (LinearLayout) getChildAt(0);
        for (int i = 0; i < pointsTW.length; i++) {
            pointsTW[i] = (NumberTextView) linearLayout.getChildAt(i + 2);
            pointsTW[i].setTextSize(TypedValue.COMPLEX_UNIT_PX, (float) 3.0 * screenPct.getPct());
        }
    }

    @Override
    public void restore(String string) {
        Log.i("Restore", string);

        if (string.length() == 0) return;
        String[] elemstrs = string.split(";");
        if (elemstrs.length != 6 && elemstrs.length != MolotowFragment.MAX_PLAYERS) return;

        for (int i = 0; i < elemstrs.length; i++) {
            if ("null".equals(elemstrs[i])) {
                points[i] = 0;
                pointsTW[i].setText("");
            } else {
                points[i] = Integer.parseInt(elemstrs[i]);
                pointsTW[i].setInt(getPoints(i));
            }
        }
    }

    @Override
    public String getString() {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < MaxPlayers; i++) {
            if (pointsTW[i].getInteger() == null) {
                str.append("null;");
            } else {
                str.append(points[i]);
                str.append(";");
            }
        }
        return str.toString();
    }

    @Override
    protected void updateColumns() {
        for (int i = 0; i < MaxPlayers; i++) {
            if (i < columns) {
                pointsTW[i].setVisibility(View.VISIBLE);
            } else {
                pointsTW[i].setVisibility(View.GONE);
            }
        }
        if (roundNo > 0) {
            ((NumberTextView) findViewById(R.id.roundNo)).setInt(roundNo);
        }
    }

    @Override
    public boolean isComplete() {
        return isRound();
    }

    public void setPoints(int playerNo, int points) {
        this.points[playerNo] = points;
        pointsTW[playerNo].setInt(getPoints(playerNo));
    }

    public void resetPoints(int playerNo) {
        points[playerNo] = 0;
        pointsTW[playerNo].setText("");
    }

    public void setPoints(int[] pts) {
        for (int i = 0; i < MaxPlayers; i++) {
            if (i < columns) {
                setPoints(i, pts[i]);
            } else {
                pointsTW[i].setText("");
            }
        }
    }

    public int[] updatePoints(Integer[] newPoints) {
        int[] diff = new int[MaxPlayers];
        for (int i = 0; i < columns; i++) {
            int oldPts = points[i];

            if (newPoints[i] == null) {
                diff[i] = -oldPts;
                pointsTW[i].setText("");
            } else {
                int newPts = newPoints[i];
                diff[i] = newPts - oldPts;
                setPoints(i, newPoints[i]);
            }
        }
        return diff;
    }

    @Override
    protected boolean isEmpty(int playerNo) {
        return pointsTW[playerNo].getInteger() == null;
    }

    public boolean isRound() {
        int total = 0;
        int prevEmpty = MaxPlayers;
        boolean notOnlyWeis = false;
        // check if a cell is empty which is not at the end of the row
        for (int i = 0; i < MaxPlayers; i++) {
            if (isEmpty(i)) {
                if (i - prevEmpty > 1) {
                    return false;
                }
                prevEmpty = i;
            }
            total += points[i];
            if (points[i] % 10 != 0) {
                notOnlyWeis = true;
            }
        }
        // a row may not contain 157 points if a card is missing (5*7, ...)
        boolean isRound = (Math.abs(total) > 100) && notOnlyWeis;
        if (roundNo == 0) {
            if (isRound) {
                roundCounter++;
                roundNo = roundCounter;
            } else {
                roundNo = -1;
            }
        }
        return isRound;
    }

    @Override
    public int getPoints(int playerNo) {
        return (int) Math.round(points[playerNo] * factor);
    }
}
