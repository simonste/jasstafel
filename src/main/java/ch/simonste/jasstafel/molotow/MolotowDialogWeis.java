package ch.simonste.jasstafel.molotow;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.appcompat.widget.Toolbar;

import ch.simonste.helpers.SingleDialog;
import ch.simonste.jasstafel.R;

public class MolotowDialogWeis extends SingleDialog
        implements View.OnClickListener {
    private final int[] resouceId = new int[]{R.id.player1, R.id.player2, R.id.player3, R.id.player4, R.id.player5, R.id.player6, R.id.player7, R.id.player8};
    private final int[] weisId = new int[]{R.id.w20, R.id.w50, R.id.w100, R.id.w150, R.id.w200};
    private RadioGroup player;
    private boolean positive;
    private boolean fremdweis = false;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.molotowweis, container);

        Bundle bundle = getArguments();
        String[] names = bundle.getStringArray("Names");
        positive = bundle.getBoolean("Positiv");
        int players = bundle.getInt("Players");

        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setTitle(getTag());
        if (positive) {
            toolbar.inflateMenu(R.menu.molotowweis);
            toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.action_fremdweis:
                            String prefix = "";
                            if (!fremdweis) {
                                prefix = "2x";
                                fremdweis = true;
                            } else {
                                fremdweis = false;
                            }

                            for (int resId : weisId) {
                                Button button = view.findViewById(resId);
                                button.setText(String.format("%1$s %2$s", prefix, button.getTag()));
                            }
                            break;
                    }
                    return true;
                }
            });
        }

        for (int i = 0; i < MolotowFragment.MAX_PLAYERS; i++) {
            RadioButton button = view.findViewById(resouceId[i]);
            button.setText(names[i]);
            if (i >= players)
                button.setVisibility(View.GONE);
        }

        for (int resId : weisId) {
            view.findViewById(resId).setOnClickListener(this);
        }

        player = view.findViewById(R.id.player);

        return view;
    }

    @Override
    public void onClick(View view) {
        int player = -1;
        switch (this.player.getCheckedRadioButtonId()) {
            case R.id.player1:
                player = 0;
                break;
            case R.id.player2:
                player = 1;
                break;
            case R.id.player3:
                player = 2;
                break;
            case R.id.player4:
                player = 3;
                break;
            case R.id.player5:
                player = 4;
                break;
            case R.id.player6:
                player = 5;
                break;
            case R.id.player7:
                player = 6;
                break;
            case R.id.player8:
                player = 7;
                break;
        }

        if (player >= 0) {
            int points = Integer.parseInt((String) view.getTag());
            if (!positive)
                points = -points;
            if (fremdweis)
                points = points * 2;

            ((MolotowInterface) getTargetFragment()).addPoints(player, points);
            dismiss();
        }
    }
}
