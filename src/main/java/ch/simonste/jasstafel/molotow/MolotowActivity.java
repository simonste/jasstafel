package ch.simonste.jasstafel.molotow;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;

import ch.simonste.helpers.SimpleInputDialog;
import ch.simonste.helpers.Utils;
import ch.simonste.jasstafel.R;
import ch.simonste.jasstafel.common.NFCUtils;

public class MolotowActivity extends AppCompatActivity implements SimpleInputDialog.SimpleDialogListener {
    private MolotowFragment fragment;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.setCommonPreferences(this);
        setTitle(R.string.molotowtitle);
        setContentView(R.layout.molotow);
        fragment = (MolotowFragment) getFragmentManager().findFragmentById(R.id.fragment);

        NFCUtils.createSender(this, "Molotow");
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        NFCUtils.handleIntent(this, intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.setCommonPreferences(this);
        onNewIntent(getIntent());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.tafelmenu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_reset:
                fragment.reset();
                break;
            case R.id.action_settings:
                startActivity(new Intent(this, MolotowPreferences.class));
                break;
        }
        return true;
    }

    @Override
    public void onFinishDialog(int dialogId, String userInput) {
        fragment.onFinishDialog(dialogId, userInput);
    }

    @Override
    public void onFinishDialog(int dialogId, Integer userInput) {
        fragment.onFinishDialog(dialogId, userInput);
    }
}



