package ch.simonste.jasstafel.molotow;

import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;

import ch.simonste.jasstafel.PreferencesActivity;
import ch.simonste.jasstafel.R;
import ch.simonste.jasstafel.common.NFCUtils;
import ch.simonste.jasstafel.common.Profiles;

public class MolotowPreferences extends PreferencesActivity {

    protected PreferenceFragment createFragment() {
        return new MolotowPreferenceFragment();
    }

    public static class MolotowPreferenceFragment extends PreferenceFragment {

        public MolotowPreferenceFragment() {
        }

        private final Preference.OnPreferenceClickListener customListener = new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                if (preference.getKey().equals("profile")) {
                    Intent intent = new Intent(getActivity(), Profiles.class);
                    intent.putExtra("Name", "Molotow");
                    startActivity(intent);
                }
                return true;
            }
        };

        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            getActivity().setTitle(R.string.molotowersettings);
            addPreferencesFromResource(R.xml.molotowpreferences);

            findPreference("PointsPerRound").setOnPreferenceChangeListener(validWatcher);
            findPreference("profile").setOnPreferenceClickListener(customListener);
            findPreference("language").setOnPreferenceChangeListener(languageWatcher);

            NFCUtils.updatePreference(findPreference("nfc"));
        }
    }
}
