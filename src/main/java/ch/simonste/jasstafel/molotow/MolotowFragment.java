package ch.simonste.jasstafel.molotow;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.preference.PreferenceManager;

import ch.simonste.helpers.SimpleInputDialog;
import ch.simonste.helpers.UndoBar;
import ch.simonste.jasstafel.R;
import ch.simonste.jasstafel.StatsDialog;
import ch.simonste.jasstafel.Whobegins;
import ch.simonste.jasstafel.common.ListFragment;

public class MolotowFragment extends ListFragment<MolotowRow> implements MolotowInterface, SimpleInputDialog.SimpleDialogListener {

    @Override
    protected MolotowRow createRow() {
        return new MolotowRow(getActivity());
    }

    @Override
    protected void showStats() {
        int[] handweis = new int[header.getPlayers()];
        int[] tischweis = new int[header.getPlayers()];
        int[] runde = new int[header.getPlayers()];

        for (int i = 0; i < adapter.getCount(); i++) {
            MolotowRow row = adapter.getItem(i);
            for (int j = 0; j < header.getPlayers(); j++) {
                int pts = row.getPoints(j);
                if (row.isRound()) {
                    runde[j] += pts;
                } else if (pts > 0) {
                    tischweis[j] += pts;
                } else {
                    handweis[j] += pts;
                }
            }
        }

        FragmentManager fm = getFragmentManager();
        StatsDialog dialog = new StatsDialog();

        Bundle bundle = new Bundle();
        String[] head = new String[StatsDialog.MAX_COLUMNS];
        head[0] = getString(R.string.handweis);
        head[1] = getString(R.string.tischweis);
        head[2] = getString(R.string.rounds);
        bundle.putStringArray(StatsDialog.Head_Key, head);
        bundle.putStringArray(StatsDialog.Names_Key, header.getNames());
        bundle.putIntArray(StatsDialog.Col1_Key, handweis);
        bundle.putIntArray(StatsDialog.Col2_Key, tischweis);
        bundle.putIntArray(StatsDialog.Col3_Key, runde);
        bundle.putInt(StatsDialog.RoundNo_Key, MolotowRow.roundCounter);
        bundle.putString(Whobegins.Preferences_Key, NAME);
        if (roundDuration.isReasonable()) {
            bundle.putString(StatsDialog.Text_Key, String.format(getString(R.string.duration), roundDuration.getRoundDuration()));
        }
        dialog.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        dialog.setArguments(bundle);
        dialog.show(fm, getString(R.string.stats));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        NAME = "Molotow";
        View mainView = super.onCreateView(inflater, container, R.layout.molotowfragment);

        undoBar = mainView.findViewById(R.id.undoBar);
        undoBar.setListener(new UndoBar.Listener() {
            @Override
            public void onUndo(Bundle bundle) {
                final int player = bundle.getInt("Player");
                final int points = bundle.getInt("Points");
                if (adapter.getLastItem().getPoints(player) != points * factor) {
                    throw new AssertionError("can only undo last points");
                }
                adapter.getLastItem().resetPoints(player);
                if (adapter.getLastItem().isEmpty()) {
                    adapter.remove(adapter.getLastItem());
                }
                adapter.notifyDataSetChanged();
                footer.addPoints(player, (int) -Math.round(factor * points));
                if (!adapter.isEmpty()) {
                    backup(adapter.getCount(), adapter.getCount() - 1, adapter.getLastItem().getString());
                }
            }
        });

        mainView.findViewById(R.id.tischweis).setOnClickListener(this);
        mainView.findViewById(R.id.addButton).setOnClickListener(this);
        mainView.findViewById(R.id.handweis).setOnClickListener(this);
        listView.addContainer(mainView.findViewById(R.id.FABcontainter));

        return mainView;
    }

    @Override
    protected void getSpecificPreferences() {
        MolotowRow.roundCounter = 0;

        final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getActivity());
        factor = settings.getBoolean("den10", true) ? 0.1 : 1;
        String ppr = settings.getString("PointsPerRound", "157");
        try {
            pointsperround = Integer.valueOf(ppr);
        } catch (NumberFormatException e) {
            pointsperround = 157;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.addButton:
                enterRound(-1, "");
                break;
            case R.id.tischweis:
                enterWeis(true);
                break;
            case R.id.handweis:
                enterWeis(false);
                break;
            default:
                break;
        }
    }

    private void enterWeis(boolean positiv) {
        MolotowDialogWeis dialog = new MolotowDialogWeis();
        dialog.setTargetFragment(this, 0);
        dialog.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        Bundle bundle = new Bundle();
        bundle.putStringArray("Names", header.getNames());
        bundle.putInt("Players", header.getPlayers());
        bundle.putBoolean("Positiv", positiv);
        dialog.setArguments(bundle);
        String tag = getResources().getString(R.string.handweis);
        if (positiv) tag = getResources().getString(R.string.tischweis);
        dialog.show(getFragmentManager(), tag);
    }

    @Override
    public void addPoints(int[] pts) {
        if (adapter.isEmpty()) {
            roundDuration.addFirstPoints();
        }
        MolotowRow newRow = createRow();
        newRow.setColumns(header.getPlayers());
        newRow.setFactor(factor);
        newRow.setPoints(pts);
        addRow(newRow);
    }

    @Override
    public void addPoints(int player, int points) {
        if (adapter.isEmpty()) {
            roundDuration.addFirstPoints();
        }
        if (adapter.getLastItem() != null && !adapter.getLastItem().isRound() && adapter.getLastItem().isEmpty(player)) {
            adapter.getLastItem().setPoints(player, points);
            adapter.notifyDataSetChanged();
            backup(adapter.getCount(), adapter.getCount() - 1, adapter.getLastItem().getString());

            footer.addPoints(player, (int) Math.round(factor * points));
            listView.smoothScrollToPosition(adapter.getCount());
        } else {
            MolotowRow newRow = createRow();
            newRow.setColumns(header.getPlayers());
            newRow.setFactor(factor);
            newRow.setPoints(player, points);
            addRow(newRow);
        }

        Bundle bundle = new Bundle();
        bundle.putInt("Player", player);
        bundle.putInt("Points", points);
        String message = String.format(getString(R.string.undoBarText), header.getPlayerName(player), points);
        undoBar.show(message, bundle);
    }

    @Override
    public void onFinishDialog(int dialogId, String userInput) {
        header.onFinishDialog(dialogId, userInput);
    }

    @Override
    public void onFinishDialog(int dialogId, Integer userInput) {
    }
}