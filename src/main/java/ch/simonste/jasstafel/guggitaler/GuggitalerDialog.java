package ch.simonste.jasstafel.guggitaler;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;

import java.util.ArrayList;

import ch.simonste.helpers.SingleDialog;
import ch.simonste.jasstafel.R;

import static ch.simonste.jasstafel.common.ListFragment.MAX_PLAYERS;

public class GuggitalerDialog extends SingleDialog
        implements View.OnClickListener {
    private final int[] resouceId = new int[]{R.id.player1, R.id.player2, R.id.player3, R.id.player4, R.id.player5, R.id.player6, R.id.player7, R.id.player8};
    NumberPicker.OnValueChangeListener numberPickerChangedListener = new NumberPicker.OnValueChangeListener() {
        @Override
        public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
            if (oldVal != newVal) {
                updateSummary();
            }
        }
    };
    private ArrayList<NumberPicker> numberPickers = new ArrayList<NumberPicker>();
    private RadioGroup groupA;
    private RadioGroup groupB;
    private int id;
    private boolean isEdit = false;
    private boolean negative = false;
    private String[] oldData;
    RadioGroup.OnCheckedChangeListener listenerA = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (checkedId != -1) {
                groupB.setOnCheckedChangeListener(null);
                groupB.clearCheck();
                groupB.setOnCheckedChangeListener(listenerB);
                if (isEdit) {
                    restorePlayer(getPlayerNoFromResource(checkedId));
                }
            }
        }
    };
    RadioGroup.OnCheckedChangeListener listenerB = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (checkedId != -1) {
                groupA.setOnCheckedChangeListener(null);
                groupA.clearCheck();
                groupA.setOnCheckedChangeListener(listenerA);
                if (isEdit) {
                    restorePlayer(getPlayerNoFromResource(checkedId));
                }
            }
        }
    };

    public static int getPlayerNoFromResource(int resourceId) {
        int player = -1;
        switch (resourceId) {
            case R.id.player1:
                player = 0;
                break;
            case R.id.player2:
                player = 1;
                break;
            case R.id.player3:
                player = 2;
                break;
            case R.id.player4:
                player = 3;
                break;
            case R.id.player5:
                player = 4;
                break;
            case R.id.player6:
                player = 5;
                break;
            case R.id.player7:
                player = 6;
                break;
            case R.id.player8:
                player = 7;
                break;
        }
        return player;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.guggitalerdialog, container);

        Bundle bundle = getArguments();
        String[] names = bundle.getStringArray("Names");
        int players = bundle.getInt("Players");
        id = bundle.getInt("Item");

        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setTitle(getTag());
        toolbar.inflateMenu(R.menu.guggitalerdialogmenu);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_negative:
                        negative = !negative;
                        updateSummary();
                        break;
                }
                return true;
            }
        });
        for (int i = 0; i < MAX_PLAYERS; i++) {
            Button button = view.findViewById(resouceId[i]);
            button.setText(names[i]);
            if (i >= players)
                button.setVisibility(View.GONE);
        }

        groupA = view.getRootView().findViewById(R.id.playersA);
        groupB = view.getRootView().findViewById(R.id.playersB);
        groupA.clearCheck();
        groupB.clearCheck();
        groupA.setOnCheckedChangeListener(listenerA);
        groupB.setOnCheckedChangeListener(listenerB);

        numberPickers.add((NumberPicker) view.findViewById(R.id.stiche));
        numberPickers.add((NumberPicker) view.findViewById(R.id.schellen));
        numberPickers.add((NumberPicker) view.findViewById(R.id.ober));
        numberPickers.add((NumberPicker) view.findViewById(R.id.rosenkoenig));
        numberPickers.add((NumberPicker) view.findViewById(R.id.laststich));

        for (NumberPicker np : numberPickers) {
            np.setOnValueChangedListener(numberPickerChangedListener);
            np.setMinValue(0);
        }
        numberPickers.get(0).setMaxValue(9);
        numberPickers.get(1).setMaxValue(9);
        numberPickers.get(2).setMaxValue(4);
        numberPickers.get(3).setMaxValue(1);
        numberPickers.get(4).setMaxValue(1);

        view.findViewById(R.id.ok).setOnClickListener(this);


        String oldString = bundle.getString("Points");
        oldData = oldString.split(";");
        if (oldData.length == MAX_PLAYERS) {
            isEdit = true;
            for (int i = 0; i < MAX_PLAYERS; i++) {
                if (hasOldData(i)) {
                    restorePlayer(i);
                    break;
                }
            }
        }

        updateSummary();

        return view;
    }

    private void updateSummary() {
        new SummaryUpdater().execute();
    }

    private boolean hasOldData(int no) {
        return !oldData[no].equals("null");
    }

    private void restorePlayer(int no) {
        GuggitalerPoints pts = new GuggitalerPoints();
        if (hasOldData(no)) {
            pts = new GuggitalerPoints(oldData[no]);
        }
        negative = pts.negative;
        numberPickers.get(0).setValue(pts.stiche);
        numberPickers.get(1).setValue(pts.schellen);
        numberPickers.get(2).setValue(pts.ober);
        numberPickers.get(3).setValue(pts.rosenkoenig);
        numberPickers.get(4).setValue(pts.laststich);
        ((RadioButton) numberPickers.get(0).getRootView().findViewById(resouceId[no])).setChecked(true);
        updateSummary();
    }

    @Override
    public void onClick(View view) {
        final int buttonId = groupA.getCheckedRadioButtonId() + groupB.getCheckedRadioButtonId() + 1;
        final int player = getPlayerNoFromResource(buttonId);

        if (player >= 0) {
            GuggitalerPoints points = calculatePoints();
            if (isEdit) {
                ((GuggitalerInterface) getTargetFragment()).editPoints(id, player, points);
            } else {
                ((GuggitalerInterface) getTargetFragment()).addPoints(player, points);
            }
            dismiss();
        } else {
            Toast.makeText(view.getContext(), R.string.selectplayer, Toast.LENGTH_SHORT).show();
        }
    }

    private GuggitalerPoints calculatePoints() {
        GuggitalerPoints points = new GuggitalerPoints();
        points.negative = negative;
        points.stiche = numberPickers.get(0).getValue();
        points.schellen = numberPickers.get(1).getValue();
        points.ober = numberPickers.get(2).getValue();
        points.rosenkoenig = numberPickers.get(3).getValue();
        points.laststich = numberPickers.get(4).getValue();
        return points;
    }

    private class SummaryUpdater extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {
            return String.format(getString(R.string.guggitalertotal), calculatePoints().sum());
        }

        @Override
        protected void onPostExecute(String result) {
            ((TextView) numberPickers.get(0).getRootView().findViewById(R.id.summary)).setText(result);
        }
    }
}
