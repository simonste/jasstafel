package ch.simonste.jasstafel.guggitaler;

import android.util.Log;

public class GuggitalerPoints {
    public boolean negative = false;
    public int stiche;
    public int schellen;
    public int ober;
    public int rosenkoenig;
    public int laststich;

    GuggitalerPoints() {
    }

    GuggitalerPoints(String str) {
        Log.i("GuggitalerPoints", str);

        String[] strpts = str.split(":");
        try {
            stiche = Integer.parseInt(strpts[0]);
            schellen = Integer.parseInt(strpts[1]);
            ober = Integer.parseInt(strpts[2]);
            rosenkoenig = Integer.parseInt(strpts[3]);
            laststich = Integer.parseInt(strpts[4]);
        } catch (NumberFormatException exception) {
            Log.i("GuggitalerPoints", exception.toString());
        }
        if (sum() < 0) {
            negative = true;
            stiche = Math.abs(stiche);
            schellen = Math.abs(schellen);
            ober = Math.abs(ober);
            rosenkoenig = Math.abs(rosenkoenig);
            laststich = Math.abs(laststich);
        }
    }

    GuggitalerPoints(int points) {
        stiche = points / 5;
    }

    public int sum() {
        final int factor = negative ? -1 : 1;
        return factor * (stiche * 5 + schellen * 10 + ober * 20 + rosenkoenig * 40 + laststich * 50);
    }

    public String getString() {
        String prefix = negative ? "-" : "";
        return prefix + stiche + ":" + prefix + schellen + ":" + prefix + ober + ":" + prefix + rosenkoenig + ":" + prefix + laststich;
    }
}
