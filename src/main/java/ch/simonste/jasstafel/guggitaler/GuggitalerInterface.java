package ch.simonste.jasstafel.guggitaler;

interface GuggitalerInterface {

    void addPoints(int player, GuggitalerPoints points);

    void editPoints(int id, int player, GuggitalerPoints points);
}
