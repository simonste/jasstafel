package ch.simonste.jasstafel.guggitaler;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import ch.simonste.helpers.NumberTextView;
import ch.simonste.helpers.ScreenPct;
import ch.simonste.jasstafel.R;
import ch.simonste.jasstafel.common.ListRow;
import ch.simonste.jasstafel.common.RowInterface;

public class GuggitalerRow extends ListRow implements RowInterface {

    public static int roundCounter;
    private final NumberTextView[] pointsTW = new NumberTextView[MaxPlayers];
    private final GuggitalerPoints[] points = new GuggitalerPoints[MaxPlayers];
    private final GuggitalerPoints carryOver;

    public GuggitalerRow(Context context) {
        super(context);
        carryOver = new GuggitalerPoints();
        create(context);
    }

    public GuggitalerRow(Context context, GuggitalerPoints carryOverPoints) {
        super(context);
        carryOver = carryOverPoints;
        create(context);
    }

    private void create(Context context) {
        LayoutInflater.from(context).inflate(R.layout.listrow, this);

        ScreenPct screenPct = new ScreenPct(((Activity) context).getWindowManager().getDefaultDisplay());
        LinearLayout linearLayout = (LinearLayout) getChildAt(0);
        for (int i = 0; i < pointsTW.length; i++) {
            pointsTW[i] = (NumberTextView) linearLayout.getChildAt(i + 2);
            pointsTW[i].setTextSize(TypedValue.COMPLEX_UNIT_PX, (float) 3.0 * screenPct.getPct());
        }
    }

    @Override
    public void restore(String string) {
        Log.i("Restore", string);

        if (string.length() == 0) return;
        String[] elemstrs = string.split(";");
        if (elemstrs.length != 6 && elemstrs.length != GuggitalerFragment.MAX_PLAYERS) return;

        for (int i = 0; i < elemstrs.length; i++) {
            points[i] = new GuggitalerPoints(elemstrs[i]);
            if ("null".equals(elemstrs[i])) {
                pointsTW[i].setText("");
            } else {
                pointsTW[i].setInt(getPoints(i));
            }
        }
    }

    @Override
    public String getString() {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < MaxPlayers; i++) {
            if (pointsTW[i].getInteger() == null) {
                str.append("null;");
            } else {
                str.append(points[i].getString());
                str.append(";");
            }
        }
        return str.toString();
    }

    @Override
    protected void updateColumns() {
        for (int i = 0; i < MaxPlayers; i++) {
            if (i < columns) {
                pointsTW[i].setVisibility(View.VISIBLE);
            } else {
                pointsTW[i].setVisibility(View.GONE);
            }
        }
        if (roundNo > 0) {
            ((NumberTextView) findViewById(R.id.roundNo)).setInt(roundNo);
        }
    }

    @Override
    public boolean isComplete() {
        return isRound();
    }

    public boolean isRound() {
        final GuggitalerPoints total = getCarryOverPoints();

        boolean isRound = false;
        if (Math.abs(total.laststich) == 1) {
            isRound = true;
        } else if (Math.abs(total.stiche) == 9 && total.schellen == 0 && total.ober == 0 && total.rosenkoenig == 0) {
            isRound = true;
        } else if (total.stiche == 0 && Math.abs(total.schellen) == 9 && total.ober == 0 && total.rosenkoenig == 0) {
            isRound = true;
        } else if (total.stiche == 0 && total.schellen == 0 && Math.abs(total.ober) == 4 && total.rosenkoenig == 0) {
            isRound = true;
        } else if (total.stiche == 0 && total.schellen == 0 && total.ober == 0 && Math.abs(total.rosenkoenig) == 1) {
            isRound = true;
        }

        if (roundNo == 0 && isRound) {
            roundCounter++;
            roundNo = roundCounter;
        }
        Log.i("sts", "is Round? " + isRound + " " + roundCounter + " " + roundNo);
        Log.i("sts", "is Round? total " + total.stiche + " " + total.schellen + " " + total.ober);

        return isRound;
    }

    public void setPoints(int playerNo, GuggitalerPoints points) {
        this.points[playerNo] = points;
        pointsTW[playerNo].setInt(getPoints(playerNo));
        setNo(this.no);
    }

    public void resetPoints(int playerNo) {
        points[playerNo] = new GuggitalerPoints();
        pointsTW[playerNo].setText("");
    }

    public void setPoints(int[] pts) {
        for (int i = 0; i < MaxPlayers; i++) {
            if (i < columns) {
                setPoints(i, new GuggitalerPoints(pts[i]));
            } else {
                pointsTW[i].setText("");
            }
        }
    }

    public int[] updatePoints(Integer[] newPoints) {
        int[] diff = new int[MaxPlayers];
        for (int i = 0; i < columns; i++) {
            GuggitalerPoints oldPts = points[i];

            if (newPoints[i] == null) {
                diff[i] = -oldPts.sum();
                pointsTW[i].setText("");
            } else {
                int newPts = newPoints[i];
                diff[i] = newPts - oldPts.sum();
                setPoints(i, new GuggitalerPoints(newPts));
            }
        }
        return diff;
    }

    @Override
    protected boolean isEmpty(int playerNo) {
        return pointsTW[playerNo].getInteger() == null;
    }

    public GuggitalerPoints[] get() {
        return points;
    }

    public GuggitalerPoints getCarryOverPoints() {
        GuggitalerPoints total = new GuggitalerPoints(carryOver.getString());
        for (int i = 0; i < MaxPlayers; i++) {
            if (points[i] != null) {
                final int factor = points[i].negative ? -1 : 1;
                total.stiche += points[i].stiche * factor;
                total.schellen += points[i].schellen * factor;
                total.ober += points[i].ober * factor;
                total.rosenkoenig += points[i].rosenkoenig * factor;
                total.laststich += points[i].laststich * factor;
            }
        }
        return total;
    }

    @Override
    public int getPoints(int playerNo) {
        if (points[playerNo] == null) {
            return 0;
        }
        return points[playerNo].sum();
    }
}
