package ch.simonste.jasstafel.guggitaler;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import ch.simonste.helpers.SimpleInputDialog;
import ch.simonste.helpers.UndoBar;
import ch.simonste.jasstafel.R;
import ch.simonste.jasstafel.StatsDialog;
import ch.simonste.jasstafel.Whobegins;
import ch.simonste.jasstafel.common.ListFragment;
import ch.simonste.jasstafel.common.ListRow;

public class GuggitalerFragment extends ListFragment<GuggitalerRow> implements GuggitalerInterface, SimpleInputDialog.SimpleDialogListener {

    @Override
    protected GuggitalerRow createRow() {
        GuggitalerPoints carryOverPoints = new GuggitalerPoints();
        if (adapter.getLastItem() != null && !adapter.getLastItem().isRound()) {
            carryOverPoints = adapter.getLastItem().getCarryOverPoints();
        }
        return new GuggitalerRow(getActivity(), carryOverPoints);
    }

    @Override
    protected void showStats() {
        int[] stiche = new int[header.getPlayers()];
        int[] schellen = new int[header.getPlayers()];
        int[] ober = new int[header.getPlayers()];
        int[] rosenkoenig = new int[header.getPlayers()];
        int[] letzter = new int[header.getPlayers()];

        for (int i = 0; i < adapter.getCount(); i++) {
            GuggitalerRow row = adapter.getItem(i);
            for (int j = 0; j < header.getPlayers(); j++) {
                GuggitalerPoints[] points = row.get();
                if (points[j] == null) {
                    points[j] = new GuggitalerPoints();
                }
                stiche[j] += points[j].stiche;
                schellen[j] += points[j].schellen;
                ober[j] += points[j].ober;
                rosenkoenig[j] += points[j].rosenkoenig;
                letzter[j] += points[j].laststich;
            }
        }

        FragmentManager fm = getFragmentManager();
        StatsDialog dialog = new StatsDialog();

        Bundle bundle = new Bundle();
        String[] head = new String[StatsDialog.MAX_COLUMNS];
        head[0] = getString(R.string.stiche);
        head[1] = getString(R.string.schellen);
        head[2] = getString(R.string.ober);
        head[3] = getString(R.string.rosenkonig);
        head[4] = getString(R.string.laststich);
        bundle.putStringArray(StatsDialog.Head_Key, head);
        bundle.putStringArray(StatsDialog.Names_Key, header.getNames());
        bundle.putIntArray(StatsDialog.Col1_Key, stiche);
        bundle.putIntArray(StatsDialog.Col2_Key, schellen);
        bundle.putIntArray(StatsDialog.Col3_Key, ober);
        bundle.putIntArray(StatsDialog.Col4_Key, rosenkoenig);
        bundle.putIntArray(StatsDialog.Col5_Key, letzter);
        bundle.putInt(StatsDialog.RoundNo_Key, GuggitalerRow.roundCounter);
        bundle.putString(Whobegins.Preferences_Key, NAME);
        if (roundDuration.isReasonable()) {
            bundle.putString(StatsDialog.Text_Key, String.format(getString(R.string.duration), roundDuration.getRoundDuration()));
        }
        dialog.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        dialog.setArguments(bundle);
        dialog.show(fm, getString(R.string.stats));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        NAME = "Guggitaler";
        View mainView = super.onCreateView(inflater, container, R.layout.guggitalerfragment);

        undoBar = mainView.findViewById(R.id.undoBar);
        undoBar.setListener(new UndoBar.Listener() {
            @Override
            public void onUndo(Bundle bundle) {
                final int player = bundle.getInt("Player");
                final int points = bundle.getInt("Points");
                if (adapter.getLastItem().getPoints(player) != points * factor) {
                    throw new AssertionError("can only undo last points");
                }
                adapter.getLastItem().resetPoints(player);
                if (adapter.getLastItem().isEmpty()) {
                    adapter.remove(adapter.getLastItem());
                }
                adapter.notifyDataSetChanged();
                footer.addPoints(player, (int) -Math.round(factor * points));
                if (!adapter.isEmpty()) {
                    backup(adapter.getCount(), adapter.getCount() - 1, adapter.getLastItem().getString());
                }
            }
        });

        mainView.findViewById(R.id.addButton).setOnClickListener(this);
        listView.addContainer(mainView.findViewById(R.id.FABcontainter));
        listView.setOnItemLongClickListener(this);

        return mainView;
    }

    @Override
    protected void getSpecificPreferences() {
        GuggitalerRow.roundCounter = 0;
        factor = 1;
        pointsperround = 0; // not used
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.addButton:
                enterGuggitaler(-1, "");
                break;
            default:
                break;
        }
    }

    private void enterGuggitaler(int id, String points) {
        GuggitalerDialog dialog = new GuggitalerDialog();
        dialog.setTargetFragment(this, 0);
        dialog.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        Bundle bundle = new Bundle();
        bundle.putStringArray("Names", header.getNames());
        bundle.putInt("Players", header.getPlayers());
        bundle.putInt("Item", id);
        bundle.putInt("RoundNo", GuggitalerRow.roundCounter);
        bundle.putString("Points", points);
        String title = getString(R.string.guggitaler);

        if (id >= 0) {
            int roundNo = ((ListRow) adapter.getItem(id)).getRoundNo();
            if (roundNo > 0) {
                title = String.format(getString(R.string.roundeditX), roundNo);
            } else if (roundNo == 0) {
                title = getString(R.string.roundedit);
            }
        }
        dialog.setArguments(bundle);
        dialog.show(getFragmentManager(), title);
    }

    @Override
    public void addPoints(int[] pts) {
        if (adapter.isEmpty()) {
            roundDuration.addFirstPoints();
        }
        GuggitalerRow newRow = createRow();
        newRow.setColumns(header.getPlayers());
        newRow.setFactor(factor);
        newRow.setPoints(pts);
        addRow(newRow);
    }

    @Override
    public void addPoints(int player, GuggitalerPoints points) {
        if (adapter.isEmpty()) {
            roundDuration.addFirstPoints();
        }
        if (adapter.getLastItem() != null && !adapter.getLastItem().isRound() && adapter.getLastItem().isEmpty(player)) {
            adapter.getLastItem().setPoints(player, points);
            adapter.notifyDataSetChanged();
            backup(adapter.getCount(), adapter.getCount() - 1, adapter.getLastItem().getString());

            footer.addPoints(player, (int) Math.round(factor * points.sum()));
            listView.smoothScrollToPosition(adapter.getCount());
        } else {
            GuggitalerRow newRow = createRow();
            newRow.setColumns(header.getPlayers());
            newRow.setFactor(factor);
            newRow.setPoints(player, points);
            addRow(newRow);
        }

        Bundle bundle = new Bundle();
        bundle.putInt("Player", player);
        bundle.putInt("Points", points.sum());
        String message = String.format(getString(R.string.undoBarText), header.getPlayerName(player), points.sum());
        undoBar.show(message, bundle);
    }

    @Override
    public void editPoints(int id, int player, GuggitalerPoints points) {
        if (adapter.getItem(id) != null) {
            int oldPoints = adapter.getItem(id).getPoints(player);
            adapter.getItem(id).setPoints(player, points);
            adapter.notifyDataSetChanged();

            backup(adapter.getCount(), id, adapter.getItem(id).getString());

            footer.addPoints(player, (int) Math.round(factor * (points.sum() - oldPoints)));
            listView.smoothScrollToPosition(adapter.getCount());

            GuggitalerRow.roundCounter = 0;
            for (int i = 0; i < adapter.getCount(); i++) {
                adapter.getItem(i).resetRoundNo();
                adapter.getItem(i).isComplete();
            }
        }
    }

    @Override
    public void onFinishDialog(int dialogId, String userInput) {
        header.onFinishDialog(dialogId, userInput);
    }

    @Override
    public void onFinishDialog(int dialogId, Integer userInput) {
    }


    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {

        enterGuggitaler(i, ((ListRow) adapter.getItem(i)).getString());
        return true;
    }
}