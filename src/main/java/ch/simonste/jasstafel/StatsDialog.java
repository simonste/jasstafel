package ch.simonste.jasstafel;


import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import ch.simonste.helpers.SingleDialog;
import ch.simonste.jasstafel.molotow.MolotowFragment;

public class StatsDialog extends SingleDialog {
    public static final int MAX_COLUMNS = 5;
    public static final String Head_Key = "head";
    public static final String Names_Key = "Names";
    public static final String RoundNo_Key = "RoundNo";
    public static final String Col1_Key = "col1";
    public static final String Col2_Key = "col2";
    public static final String Col3_Key = "col3";
    public static final String Col4_Key = "col4";
    public static final String Col5_Key = "col5";
    public static final String Text_Key = "text";

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.statsdialog, container);
        getDialog().getWindow().setWindowAnimations(R.style.DialogAnimationFlip);

        final Bundle bundle = getArguments();
        final String[] header = bundle.getStringArray(Head_Key);
        final String[] names = bundle.getStringArray(Names_Key);
        final int[] col1 = bundle.getIntArray(Col1_Key);
        final float[] col1f = bundle.getFloatArray(Col1_Key);
        final int[] col2 = bundle.getIntArray(Col2_Key);
        final float[] col2f = bundle.getFloatArray(Col2_Key);
        final int[] col3 = bundle.getIntArray(Col3_Key);
        final float[] col3f = bundle.getFloatArray(Col3_Key);
        final int[] col4 = bundle.getIntArray(Col4_Key);
        final float[] col4f = bundle.getFloatArray(Col4_Key);
        final int[] col5 = bundle.getIntArray(Col5_Key);
        final float[] col5f = bundle.getFloatArray(Col5_Key);
        final int roundNo = bundle.getInt(RoundNo_Key);
        final int numPlayers = (col1 == null) ? col1f.length : col1.length;

        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setTitle(getTag());
        toolbar.inflateMenu(R.menu.statsmenu);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                SingleDialog.switchDialogs();
                switch (item.getItemId()) {
                    case R.id.action_whobegins:
                        FragmentManager fm = getFragmentManager();
                        Whobegins dialog = new Whobegins();
                        Bundle whoBeginsBundle = new Bundle();
                        String[] players = new String[numPlayers];
                        System.arraycopy(names, 0, players, 0, numPlayers);
                        whoBeginsBundle.putStringArray(Whobegins.Names_Key, players);
                        whoBeginsBundle.putInt(Whobegins.RoundNo_Key, roundNo);
                        whoBeginsBundle.putString(Whobegins.Preferences_Key, bundle.getString(Whobegins.Preferences_Key));
                        dialog.setArguments(whoBeginsBundle);
                        dialog.show(fm, "whoBegins");
                        break;
                }
                dismiss();
                return true;
            }
        });

        String text = bundle.getString(Text_Key, "");
        if (!text.equals("")) {
            ((TextView) view.findViewById(R.id.text)).setText(text);
        }

        int usedColumns = 0;
        for (String title : header) {
            if (title != null) usedColumns++;
        }

        TableLayout tableLayout = view.findViewById(R.id.table);
        StatsRow headRow = (StatsRow) tableLayout.getChildAt(0);
        headRow.setValues("", header[0], header[1], header[2], header[3], header[4]);
        headRow.hideUnusedColumns(usedColumns);
        for (int i = 0; i < MolotowFragment.MAX_PLAYERS; i++) {
            StatsRow row = (StatsRow) tableLayout.getChildAt(i + 1);
            if (i < numPlayers) {
                row.setName(names[i]);
                if (col1 != null) {
                    row.setValue(R.id.valA, col1[i]);
                } else {
                    row.setFloatValue(R.id.valA, col1f[i]);
                }
                if (col2 != null) {
                    row.setValue(R.id.valB, col2[i]);
                } else if (col2f != null) {
                    row.setFloatValue(R.id.valB, col2f[i]);
                }
                if (col3 != null) {
                    row.setValue(R.id.valC, col3[i]);
                } else if (col3f != null) {
                    row.setFloatValue(R.id.valC, col3f[i]);
                }
                if (col4 != null) {
                    row.setValue(R.id.valD, col4[i]);
                } else if (col4f != null) {
                    row.setFloatValue(R.id.valD, col4f[i]);
                }
                if (col5 != null) {
                    row.setValue(R.id.valE, col5[i]);
                } else if (col5f != null) {
                    row.setFloatValue(R.id.valE, col5f[i]);
                }
            } else {
                row.setVisibility(View.GONE);
            }
            row.hideUnusedColumns(usedColumns);
        }

        return view;
    }
}
