package ch.simonste.jasstafel;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import java.util.HashMap;
import java.util.Map;

import ch.simonste.helpers.SingleDialog;

public class Whobegins extends SingleDialog implements View.OnLongClickListener, View.OnTouchListener {
    public static final String Names_Key = "Names";
    public static final String RoundNo_Key = "RoundNo";
    public static final String Preferences_Key = "Preferences";

    static final int[] RES = new int[]{R.id.r1c1, R.id.r1c2, R.id.r2c1, R.id.r2c2, R.id.r3c1, R.id.r3c2, R.id.r4c1, R.id.r4c2};
    private static final String RoundOffset_Key = "RoundOffset";
    private static final String SwapPlayers_Key = "SwapPlayers";
    final Map<Integer, Integer> swappedMap = new HashMap<>();
    int players;
    private TextView selectedTW;
    private SharedPreferences preferences;
    private Context context;

    private static String[] splitTeamName(String teamName, int limit) {
        String[] playerNames = teamName.split("[-&/+]", limit);
        if (playerNames.length > 1 && playerNames[0].trim().length() > 0) {
            for (int i = 0; i < playerNames.length; ++i) {
                playerNames[i] = playerNames[i].trim();
            }
            return playerNames;
        } else {
            return teamName.split("[ ]+", limit);
        }
    }

    public static String[] guessPlayerNames(String[] teamNames) {
        int playersPerTeam = 2;
        if (teamNames == null || (teamNames.length == 2 && (teamNames[0] == null || teamNames[1] == null))) {
            return teamNames;
        }
        if (teamNames.length == 2 && splitTeamName(teamNames[0], 0).length == 3 && splitTeamName(teamNames[1], 0).length == 3) {
            playersPerTeam = 3;
        }
        final int numPlayers = teamNames.length * playersPerTeam;

        String[] playerNames = new String[numPlayers];

        for (int i = 0; i < teamNames.length; i++) {
            String teamName = teamNames[i].trim();
            String[] splitedName = splitTeamName(teamName, playersPerTeam);

            boolean validNames = (splitedName.length > 1);
            if (validNames && splitedName[0].toLowerCase().equals("team") && playersPerTeam != 3)
                validNames = false;

            int j = i + teamNames.length;
            int k = i + 2 * teamNames.length;
            if (validNames) {
                playerNames[i] = splitedName[0];
                playerNames[j] = splitedName[splitedName.length - 1];
                if (playersPerTeam == 3) {
                    playerNames[j] = splitedName[1];
                    playerNames[k] = splitedName[2];
                }
            } else if (teamName.length() > 0) {
                if (Character.isDigit(teamName.charAt(teamName.length() - 1))) {
                    playerNames[i] = teamName + "a";
                    playerNames[j] = teamName + "b";
                } else {
                    playerNames[i] = teamName + " 1";
                    playerNames[j] = teamName + " 2";
                }
            } else {
                playerNames[i] = "P1";
                playerNames[j] = "P2";
            }

            playerNames[i] = playerNames[i].trim();
            playerNames[j] = playerNames[j].trim();
        }

        return playerNames;
    }

    private int getTextViewNo(int i) {
        if (i % 2 == 0) {
            return i / 2;
        } else {
            return players - (int) Math.ceil((float) i / 2);
        }
    }

    private int getTextViewNo(View view) {
        for (int i = 0; i < players; i++) {
            if (view.getId() == RES[i]) {
                return getTextViewNo(i);
            }
        }
        return 0;
    }

    int getTextViewId(int i) {
        int pNo = getTextViewNo(i);

        if (swappedMap.containsKey(pNo)) {
            return swappedMap.get(pNo);
        }
        return pNo;
    }

    private int getTextViewId(View view) {
        for (int i = 0; i < players; i++) {
            if (view.getId() == RES[i]) {
                return getTextViewId(i);
            }
        }
        return 0;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.whobegins, container);
        getDialog().getWindow().setWindowAnimations(R.style.DialogAnimationFlip);

        context = inflater.getContext();

        Bundle bundle = getArguments();
        String[] names = bundle.getStringArray(Whobegins.Names_Key);
        players = names.length;
        int roundNo = bundle.getInt(Whobegins.RoundNo_Key);

        String title = getString(R.string.whoBegins);
        String subtitle = getResources().getQuantityString(R.plurals.rounds, roundNo, roundNo);
        getDialog().setTitle(title);
        ((TextView) view.findViewById(R.id.subtitle)).setText(subtitle);

        if (players < 7) {
            view.findViewById(R.id.row4).setVisibility(View.GONE);
            view.findViewById(R.id.r4c1).setVisibility(View.GONE);
            view.findViewById(R.id.r4c2).setVisibility(View.GONE);
        }
        if (players < 5) {
            view.findViewById(R.id.row3).setVisibility(View.GONE);
            view.findViewById(R.id.r3c1).setVisibility(View.GONE);
            view.findViewById(R.id.r3c2).setVisibility(View.GONE);
        }
        if (players < 3) {
            view.findViewById(R.id.row2).setVisibility(View.GONE);
            view.findViewById(R.id.r2c1).setVisibility(View.GONE);
            view.findViewById(R.id.r2c2).setVisibility(View.GONE);
        }

        preferences = context.getSharedPreferences(bundle.getString(Preferences_Key), Context.MODE_PRIVATE);
        final int selectedPlayer = (roundNo + preferences.getInt(RoundOffset_Key, 0)) % players;
        final String swapPlayers = preferences.getString(SwapPlayers_Key, "{}");
        getSwappedList(swapPlayers);
        for (int i = 0; i < players; i++) {
            TextView textView = view.findViewById(RES[i]);
            final int plId = getTextViewId(i);
            textView.setText(names[plId]);

            if (getTextViewNo(i) == selectedPlayer) {
                selectView(textView);
            }
            textView.setOnLongClickListener(this);
            textView.setOnTouchListener(this);
        }
        if (players % 2 == 1) {
            view.findViewById(RES[players]).setVisibility(View.GONE);
        }
        return view;
    }

    private void selectView(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            if (selectedTW != null) {
                selectedTW.setBackground(null);
            }
            view.setBackground(ContextCompat.getDrawable(context, R.drawable.border));
        } else {
            if (selectedTW != null) {
                selectedTW.setBackgroundColor(getResources().getColor(R.color.White));
            }
            view.setBackgroundColor(getResources().getColor(R.color.Teal));
        }
        selectedTW = (TextView) view;
    }

    @Override
    public boolean onLongClick(View view) {
        int prevSelectedPlayer = 0;
        if (selectedTW != null) {
            prevSelectedPlayer = getTextViewNo(selectedTW);
        }
        final int selectedPlayer = getTextViewNo(view);

        selectView(view);

        int roundOffset = preferences.getInt(RoundOffset_Key, 0) + selectedPlayer - prevSelectedPlayer;
        roundOffset = (roundOffset + players) % players;

        preferences.edit().putInt(RoundOffset_Key, roundOffset).apply();

        return true;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
            TextView viewB = null;

            View mainView = (View) view.getParent().getParent();
            for (int resId : RES) {
                View pview = mainView.findViewById(resId);
                int[] tl = new int[2];
                pview.getLocationOnScreen(tl);
                Rect rect = new Rect(tl[0], tl[1], tl[0] + 2 * pview.getHeight(), tl[1] + pview.getWidth());
                if (rect.contains((int) motionEvent.getRawX(), (int) motionEvent.getRawY())) {
                    viewB = (TextView) pview;
                }
            }
            if (viewB != null && view != viewB) {
                swapPlayers((TextView) view, viewB);
                preferences.edit().putString(SwapPlayers_Key, swappedMap.toString()).apply();
            }
            view.performClick();
        }
        return false;
    }

    void swapPlayers(TextView viewA, TextView viewB) {
        CharSequence a = viewA.getText();
        viewA.setText(viewB.getText());
        viewB.setText(a);

        final Integer pA = getTextViewId(viewA);
        final Integer pB = getTextViewId(viewB);
        final Map<Integer, Integer> oldMap = new HashMap<>(swappedMap);
        for (Integer[] set : new Integer[][]{{pA, pB}, {pB, pA}}) {
            boolean newValue = true;
            for (Integer key : oldMap.keySet()) {
                if (oldMap.get(key).equals(set[0])) {
                    newValue = false;
                    if (key.equals(set[1])) {
                        swappedMap.remove(key);
                    } else {
                        swappedMap.put(key, set[1]);
                    }
                }
            }
            if (newValue) {
                swappedMap.put(set[0], set[1]);
            }
        }
    }

    private void getSwappedList(String swapPlayers) {
        swappedMap.clear();

        boolean resetMap = false;
        String[] pairs = swapPlayers.substring(1, swapPlayers.length() - 1).split(",");
        for (String pair : pairs) {
            if (pair.contains("=")) {
                String[] player = pair.split("=");
                Integer p0 = Integer.valueOf(player[0].trim());
                Integer p1 = Integer.valueOf(player[1].trim());
                swappedMap.put(p0, p1);

                if (p0 >= players || p1 >= players) {
                    resetMap = true;
                }
            }
        }
        if (resetMap) {
            swappedMap.clear();
            preferences.edit().putString(SwapPlayers_Key, swappedMap.toString()).apply();
        }
    }
}