package ch.simonste.jasstafel;

import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

import ch.simonste.helpers.ScreenPct;
import ch.simonste.helpers.Utils;
import ch.simonste.jasstafel.coiffeur.CoiffeurActivity;
import ch.simonste.jasstafel.differenzler.DifferenzlerActivity;
import ch.simonste.jasstafel.guggitaler.GuggitalerActivity;
import ch.simonste.jasstafel.molotow.MolotowActivity;
import ch.simonste.jasstafel.punktetafel.PunkteTafelActivity;
import ch.simonste.jasstafel.schieber.SchieberActivity;

public class MainActivity extends AppCompatActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.setCommonPreferences(this);
        setTitle(R.string.app_name);
        setContentView(R.layout.main);

        PreferenceConverter.convert(this);
        PreferenceConverterV2.convert(this);

        ArrayList<Integer> startButtons = new ArrayList<>();
        startButtons.add(R.id.startschieber);
        startButtons.add(R.id.startcoiffeur);
        startButtons.add(R.id.startdifferenzler);
        startButtons.add(R.id.startmolotow);
        startButtons.add(R.id.startguggitaler);
        startButtons.add(R.id.startpunktetafel);

        ScreenPct screenPct = new ScreenPct(getWindowManager().getDefaultDisplay());
        float fontsize = screenPct.getPct() * 6;

        for (Integer viewId : startButtons) {
            Button startButton = findViewById(viewId);

            // reduce text size
            TextView tw = new TextView(getApplicationContext());
            tw.setText(startButton.getText());
            tw.setAllCaps(true);
            tw.setTextSize(TypedValue.COMPLEX_UNIT_PX, fontsize);
            tw.measure(0, 0);
            float ratio = screenPct.getPct() * 45 / tw.getMeasuredWidth();
            if (ratio > 1) ratio = 1.0f;
            startButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, ratio * fontsize);
            startButton.setAllCaps(true);
        }
    }

    public void schiebertafel(View view) {
        startActivity(new Intent(this, SchieberActivity.class));
    }

    public void coiffeurtafel(View view) {
        startActivity(new Intent(this, CoiffeurActivity.class));
    }

    public void differenzlertafel(View view) {
        startActivity(new Intent(this, DifferenzlerActivity.class));
    }

    public void molotowtafel(View view) {
        startActivity(new Intent(this, MolotowActivity.class));
    }

    public void guggitalertafel(View view) {
        startActivity(new Intent(this, GuggitalerActivity.class));
    }

    public void punktetafel(View view) {
        startActivity(new Intent(this, PunkteTafelActivity.class));
    }

}