package ch.simonste.jasstafel;

public interface RundenDialogInterface {

    void addPoints(int[] points);

    void editPoints(int id, Integer[] pts);
}
