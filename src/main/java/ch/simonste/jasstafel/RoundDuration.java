package ch.simonste.jasstafel;

import android.content.SharedPreferences;
import android.util.Log;

public class RoundDuration {

    private static final int RESONABLE_DURATION_MIN = 5 * 60;
    private static final int FIRST_ROUND_DURATION_MIN = 10;
    public static int SEC2MIN = 60;
    private final SharedPreferences preferences;
    private long startTime;
    private long endTime;
    private int roundNo = 0;

    public RoundDuration(SharedPreferences preferences) {
        this.preferences = preferences;
        startTime = preferences.getLong("StartTime", 0);
        endTime = preferences.getLong("EndTime", 0);
    }

    public void reset() {
        startTime = getTimestamp();
        endTime = 0;
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong("StartTime", startTime);
        editor.putLong("EndTime", endTime);
        editor.apply();
    }

    public void addFirstPoints() {
        if (getRoundDuration() > FIRST_ROUND_DURATION_MIN) {
            Log.d("RoundDuration", "reset after " + getRoundDuration());
            reset();
        }
    }

    public void roundFinished(boolean finished) {
        if (finished) {
            endTime = getTimestamp();
        } else {
            endTime = 0;
        }
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong("EndTime", endTime);
        editor.apply();
    }

    public int getRoundDuration() {
        long endTime = this.endTime;
        if (endTime == 0) endTime = getTimestamp();
        return (int) ((endTime - startTime) / SEC2MIN);
    }

    public boolean isReasonable() {
        return (getRoundDuration() < RESONABLE_DURATION_MIN);
    }

    private long getTimestamp() {
        return System.currentTimeMillis() / 1000;
    }

    public int getRoundNo() {
        return roundNo;
    }

    public void setRoundNo(int rNo) {
        roundNo = rNo;
    }
}
