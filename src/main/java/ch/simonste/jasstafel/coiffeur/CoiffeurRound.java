package ch.simonste.jasstafel.coiffeur;


import android.app.Activity;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import ch.simonste.helpers.NumberTextView;
import ch.simonste.helpers.SimpleInputDialog;
import ch.simonste.jasstafel.R;

public class CoiffeurRound extends CoiffeurRow implements View.OnClickListener, SimpleInputDialog.SimpleDialogListener {
    private static final int[] defaultTrumpf = {1, 2, 3, 4, 9, 10, 11, 12, 13, 14, 15, 16};

    private static final String MATCHBONUS = "MATCH";
    private static final String MATCHMALUS = "HCTAM";
    private final int rowNo;
    private final NumberTextView factor;
    private final Integer[] pts = new Integer[MAX_TEAMS];
    private final ImageView icon;

    public CoiffeurRound(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.coiffeur_round, this);
        super.initialize();

        rowNo = Integer.parseInt((String) getTag());
        description = findViewById(R.id.description);
        icon = findViewById(R.id.icon);
        factor = findViewById(R.id.factor);

        team[0] = findViewById(R.id.team1);
        team[1] = findViewById(R.id.team2);
        team[2] = findViewById(R.id.team3);

        findViewById(R.id.jasstype).setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                showCoiffeurDialog();
                return false;
            }
        });
    }

    private boolean isScratched(int i) {
        return (pts[i] != null && pts[i] == CoiffeurInputDialog.SCRATCH);
    }

    public Integer[] isOpen() {
        Integer[] val = new Integer[MAX_TEAMS];
        boolean roundScratched = false;
        for (int i = 0; i < MAX_TEAMS; i++) {
            if (isScratched(i)) {
                roundScratched = true;
                break;
            }
        }
        for (int i = 0; i < MAX_TEAMS; i++) {
            if (i >= teams) {
                val[i] = 0;
            } else if (roundScratched && !isScratched(i)) {
                val[i] = -1;
            } else if (pts[i] == null) {
                val[i] = factor.getInt();
            } else {
                val[i] = null;
            }
        }
        return val;
    }

    public int[] getPoints() {
        int[] pts = new int[MAX_TEAMS];
        for (int i = 0; i < MAX_TEAMS; i++) {
            pts[i] = team[i].getInt() * factor.getInt();
            if (team[i].getText() == MATCHBONUS) {
                pts[i] += Math.round(match * displayFactor) * factor.getInt() + matchBonus * displayFactor;
            }
            if (team[i].getText() == MATCHMALUS) {
                pts[i] += matchMalus * displayFactor;
            }
            if (i >= teams) {
                pts[i] = team[i].getInt();
            }
        }
        return pts;
    }

    public void setType(int type, String str, int factor) {
        Integer oldType = (Integer) description.getTag();
        Integer oldFact = this.factor.getInt();
        String oldStr = description.getText().toString();

        TypedArray trumpfdrawable = getResources().obtainTypedArray(R.array.drawable_trumpf);

        description.setTag(type);
        description.setText(str);
        icon.setImageResource(trumpfdrawable.getResourceId(type, -1));
        this.factor.setInt(factor);
        super.invalidate();

        if (oldType == null || type != oldType || factor != oldFact || !oldStr.equals(str)) {
            SharedPreferences.Editor editor = settings.edit();
            editor.putInt("Type" + rowNo, type);
            editor.putInt("Factor" + rowNo, factor);
            editor.putString("Description" + rowNo, description.getText().toString());
            editor.apply();
            evaluateThirdColumn();
        }
    }

    public void restore(SharedPreferences settings) {
        super.restore(settings, null);

        factor.setTextSize(TypedValue.COMPLEX_UNIT_PX, (float) 0.5 * fontSize);
        description.setTextSize(TypedValue.COMPLEX_UNIT_PX, (float) 0.6 * fontSize);
        team[0].setTextSize(TypedValue.COMPLEX_UNIT_PX, fontSize);
        team[1].setTextSize(TypedValue.COMPLEX_UNIT_PX, fontSize);
        team[2].setTextSize(TypedValue.COMPLEX_UNIT_PX, fontSize);
        for (int i = 0; i < MAX_TEAMS; i++) {
            if (i < teams) {
                team[i].setOnClickListener(this);
            } else {
                team[i].setOnClickListener(null);
            }
        }

        int trumpf = settings.getInt("Type" + rowNo, defaultTrumpf[rowNo - 1]);
        int factor = rowNo;
        if (settingsMain.getBoolean("setFactorManually", false)) {
            factor = settings.getInt("Factor" + rowNo, rowNo);
        }
        String[] trumpfstr = getResources().getStringArray(R.array.trumpf_arr);
        setType(trumpf, settings.getString("Description" + rowNo, trumpfstr[trumpf]), factor);

        for (int i = 0; i < teams; i++) {
            try {
                pts[i] = Integer.parseInt(settings.getString("Points" + (i + 1) + "_" + rowNo, ""));
                if (pts[i] != CoiffeurInputDialog.SCRATCH) {
                    team[i].setInt(Math.round(displayFactor * pts[i]));
                }
            } catch (NumberFormatException e) {
                pts[i] = null;
                team[i].setText("");
                team[i].setBackgroundColor(Color.BLACK);
            }
        }
        checkMatchBonus();
        evaluateThirdColumn();
    }

    private void checkMatchBonus() {
        boolean changed = false;
        final boolean withBonus = matchBonus > 0;

        for (int i = 0; i < MAX_TEAMS; i++) {
            if (pts[i] == null) continue;
            final boolean isMatch = (oldmatch > 0 && pts[i] == oldmatch) || (pts[i] > match) || team[i].getText().equals(MATCHBONUS);

            if (isMatch) {
                int p = pts[i];
                if (withBonus) {
                    pts[i] = match + 100;
                } else {
                    pts[i] = match;
                }
                changed = (p != pts[i]);
            }

            if (withBonus && pts[i] > match) {
                team[i].setText(MATCHBONUS);
            } else if (pts[i] == CoiffeurInputDialog.SCRATCH) {
                scratch(i);
            } else if (matchMalus < 0 && pts[i] < 0) {
                pts[i] = -100;
                team[i].setText(MATCHMALUS);
            } else {
                team[i].setInt(Math.round(pts[i] * displayFactor));
            }
        }
        if (changed) {
            backupPoints();
        }
    }

    public void reset() {
        for (int i = 0; i < MAX_TEAMS; i++) {
            team[i].setText("");
            pts[i] = null;
            team[i].setBackgroundColor(Color.BLACK);
        }
        backupPoints();
    }

    private void scratch(int t) {
        team[t].setText("");
        team[t].setBackgroundResource(R.drawable.scratch);
    }

    private void backupPoints() {
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("Points1_" + rowNo, String.valueOf(pts[0]));
        editor.putString("Points2_" + rowNo, String.valueOf(pts[1]));
        if (teams == MAX_TEAMS) editor.putString("Points3_" + rowNo, String.valueOf(pts[2]));
        editor.apply();
        evaluateThirdColumn();
    }

    private void evaluateThirdColumn() {
        if (teams < 3) {
            if (isScratched(0) && isScratched(1)) {
                team[2].setText("");
            } else if (pts[0] != null && pts[1] != null) {
                int[] points = getPoints();
                int diff = points[0] - points[1];
                team[2].setInt(diff);
            } else {
                team[2].setText("");
            }
        }
    }

    @Override
    public void onClick(View v) {
        int tag = 0;
        switch (v.getId()) {
            case R.id.team1:
                tag = TEAM_1;
                break;
            case R.id.team2:
                tag = TEAM_2;
                break;
            case R.id.team3:
                tag = TEAM_3;
                break;
        }

        int points;
        if (pts[tag] == null)
            points = CoiffeurInputDialog.EMPTY_POINTS;
        else
            points = pts[tag];
        showDialogSimple(tag, points);
    }

    private void showDialogSimple(int tag, int value) {
        FragmentManager fm = ((Activity) getContext()).getFragmentManager();
        SimpleInputDialog dialog = new CoiffeurInputDialog();
        dialog.setTarget(this);
        dialog.setArguments(tag + 10 * rowNo, SimpleInputDialog.DIALOG_INPUT_SIGNED, value);
        dialog.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        dialog.show(fm, description.getText().toString());
    }

    private void showCoiffeurDialog() {
        FragmentManager fm = ((Activity) getContext()).getFragmentManager();
        CoiffeurDialog dialog = new CoiffeurDialog();
        Bundle bundle = new Bundle();
        bundle.putString("Selection", description.getTag().toString());
        bundle.putString("Description", description.getText().toString());
        if (settingsMain.getBoolean("setFactorManually", false)) {
            bundle.putInt("Factor", factor.getInt());
        }
        dialog.setTarget(this);
        dialog.setArguments(bundle);
        dialog.show(fm, Integer.toString(rowNo));
    }

    @Override
    public void onFinishDialog(int t, Integer userInput) {
        t = t % 10;
        pts[t] = userInput;
        String str = "";
        if (userInput != null) {
            if (userInput == CoiffeurInputDialog.SCRATCH) {
                scratch(t);
            } else {
                str = String.valueOf(Math.round(userInput * displayFactor));
                if (userInput > match) {
                    if (matchBonus > 0) {
                        str = MATCHBONUS;
                        pts[t] = match + 100;
                    } else {
                        pts[t] = match;
                    }
                }
                if (userInput < 0 && matchMalus < 0) {
                    str = MATCHMALUS;
                    pts[t] = -100;

                }
                team[t].setBackgroundColor(Color.BLACK);
            }
        } else if (team[t].getBackground() != null) {
            pts[t] = null;
            team[t].setBackgroundColor(Color.BLACK);
        }
        team[t].setText(str);
        backupPoints();

        ((CoiffeurActivity) getContext()).evaluate();
    }

    @Override
    public void onFinishDialog(int dialogId, String userInput) {
    }
}
