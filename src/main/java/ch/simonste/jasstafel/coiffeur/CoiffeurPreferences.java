package ch.simonste.jasstafel.coiffeur;

import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;

import ch.simonste.helpers.SimpleAlertDialog;
import ch.simonste.jasstafel.PreferencesActivity;
import ch.simonste.jasstafel.R;
import ch.simonste.jasstafel.common.NFCUtils;
import ch.simonste.jasstafel.common.Profiles;

public class CoiffeurPreferences extends PreferencesActivity {

    protected PreferenceFragment createFragment() {
        return new CoiffeurPreferenceFragment();
    }

    public static class CoiffeurPreferenceFragment extends PreferenceFragment {

        public CoiffeurPreferenceFragment() {
        }

        private final Preference.OnPreferenceClickListener customListener = new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                if (preference.getKey().equals("reset")) {
                    ((PreferencesActivity) getActivity()).resultAction(R.id.action_reset);
                } else if (preference.getKey().equals("profile")) {
                    Intent intent = new Intent(getActivity(), Profiles.class);
                    intent.putExtra("Name", "Coiffeur");
                    startActivity(intent);
                }
                return true;
            }
        };

        private final Preference.OnPreferenceChangeListener matchPointsListener = new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object object) {
                if (preference == null || validWatcher.onPreferenceChange(preference, object)) {
                    String match = (String) object;
                    String info = getString(R.string.matchBonusInfo);
                    findPreference("matchBonus").setSummary(String.format(info, match));
                    if (match.equals("257") && ((CheckBoxPreference) findPreference("matchBonus")).isChecked()) {
                        FragmentManager fm = getFragmentManager();
                        SimpleAlertDialog dialog = new SimpleAlertDialog();
                        String str = getString(R.string.resetMatchBonus);
                        dialog.setMessage(str);
                        dialog.setCallback(new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ((CheckBoxPreference) findPreference("matchBonus")).setChecked(false);
                            }
                        });
                        int title = R.string.activatedBonus;
                        dialog.show(fm, getString(title));
                        return true;

                    }

                    return true;
                }
                return false;
            }
        };
        private final Preference.OnPreferenceClickListener den10Listener = new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                String bonusPoints = ((EditTextPreference) findPreference("matchBonusVal")).getText();
                String malusPoints = ((EditTextPreference) findPreference("matchMalusVal")).getText();
                if (((CheckBoxPreference) preference).isChecked()) {
                    String info = getString(R.string.matchBonusRounded);
                    int mpts = Integer.valueOf(bonusPoints);
                    findPreference("matchBonusVal").setSummary(String.format(info, mpts, mpts / 10));
                    mpts = Integer.valueOf(malusPoints);
                    findPreference("matchMalusVal").setSummary(String.format(info, mpts, mpts / 10));
                } else {
                    findPreference("matchBonusVal").setSummary(bonusPoints);
                    findPreference("matchMalusVal").setSummary(malusPoints);
                }
                return true;
            }
        };
        private final Preference.OnPreferenceChangeListener matchPointsValListener = new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object object) {
                if (preference == null || validWatcher.onPreferenceChange(preference, object)) {
                    den10Listener.onPreferenceClick(findPreference("den10"));
                    return true;
                }
                return false;
            }
        };
        private final Preference.OnPreferenceChangeListener matchBonusListener = new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object object) {
                boolean preferenceEnabled = (boolean) object;
                boolean preferenceChanged = (((CheckBoxPreference) preference).isChecked() != preferenceEnabled);
                EditTextPreference matchPointsPref = ((EditTextPreference) findPreference("matchPoints"));
                if (preferenceChanged) {
                    final String matchPoints = preferenceEnabled ? "157" : "257";
                    if (!matchPointsPref.getText().equals(matchPoints)) {
                        FragmentManager fm = getFragmentManager();
                        SimpleAlertDialog dialog = new SimpleAlertDialog();
                        String str = String.format(getString(R.string.resetMatchPoints), matchPoints);
                        dialog.setMessage(str);
                        dialog.setCallback(new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ((EditTextPreference) findPreference("matchPoints")).setText(matchPoints);
                                String info = getString(R.string.matchBonusInfo);
                                findPreference("matchBonus").setSummary(String.format(info, matchPoints));
                            }
                        });
                        int title = preferenceEnabled ? R.string.activatedBonus : R.string.deactivatedBonus;
                        dialog.show(fm, getString(title));
                        return true;
                    }
                }
                return true;
            }
        };

        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            getActivity().setTitle(R.string.coiffeursettings);
            addPreferencesFromResource(R.xml.coiffeurpreferences);

            findPreference("reset").setOnPreferenceClickListener(customListener);
            findPreference("profile").setOnPreferenceClickListener(customListener);
            findPreference("matchPoints").setOnPreferenceChangeListener(matchPointsListener);
            findPreference("matchBonus").setOnPreferenceChangeListener(matchBonusListener);
            findPreference("matchBonusVal").setOnPreferenceChangeListener(matchPointsValListener);
            findPreference("matchMalusVal").setOnPreferenceChangeListener(matchPointsValListener);
            findPreference("den10").setOnPreferenceClickListener(den10Listener);
            findPreference("language").setOnPreferenceChangeListener(languageWatcher);

            // initialize
            matchPointsListener.onPreferenceChange(null, ((EditTextPreference) findPreference("matchPoints")).getText());
            den10Listener.onPreferenceClick(findPreference("den10"));

            NFCUtils.updatePreference(findPreference("nfc"));
        }
    }
}
