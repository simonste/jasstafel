package ch.simonste.jasstafel.coiffeur;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import androidx.preference.PreferenceManager;

import java.util.ArrayList;

import ch.simonste.helpers.ScreenPct;
import ch.simonste.helpers.SimpleInputDialog;
import ch.simonste.jasstafel.PreferenceConverterV2;
import ch.simonste.jasstafel.R;
import ch.simonste.jasstafel.Whobegins;

public class CoiffeurFragment extends Fragment implements OnClickListener, SimpleInputDialog.SimpleDialogListener {
    private static final int MAX_ROWS = 12;

    private int rows;
    private CoiffeurHeader header;
    private CoiffeurRound[] rounds;
    private CoiffeurFooter footer;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.coiffeurfragment, container, false);

        header = mainView.findViewById(R.id.header);
        footer = mainView.findViewById(R.id.footer);
        header.setOnClickListener(this);
        footer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                footer.showPointsSummary();
            }
        });

        rounds = new CoiffeurRound[MAX_ROWS];
        for (int i = 0; i < MAX_ROWS; i++) {
            rounds[i] = (CoiffeurRound) ((ViewGroup) mainView).getChildAt(i + 1); // first child is header
        }
        return mainView;
    }

    final public void onResume() {
        super.onResume();
        getPreferences();
        evaluate();
    }

    private float getFontSize() {
        ScreenPct screenPct = new ScreenPct(getActivity().getWindowManager().getDefaultDisplay());
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            return 2.5f * screenPct.getPct();
        }
        return 3.5f * screenPct.getPct();
    }

    private void getPreferences() {
        Log.d("CoifFrag", "getPreferences");

        SharedPreferences settingsMain = PreferenceManager.getDefaultSharedPreferences(getActivity());
        rows = Integer.parseInt(settingsMain.getString("rows", "11"));

        SharedPreferences settings = getActivity().getSharedPreferences("Coiffeur", 0);
        header.restore(settings, settingsMain, getFontSize());
        for (int i = 0; i < MAX_ROWS; i++) {
            rounds[i].restore(settings);
            if (i < rows) {
                rounds[i].setVisibility(View.VISIBLE);
            } else {
                rounds[i].setVisibility(View.GONE);
            }
        }
        footer.restore(null, null);
        evaluate();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.action_settings:
                getActivity().startActivityForResult(new Intent(getActivity(), CoiffeurPreferences.class), 2);
                break;
            case R.id.action_reset:
                reset();
                break;
            case R.id.roundNumberContainer:
                whoBegins();
                break;
        }
    }

    private void whoBegins() {
        FragmentManager fm = getFragmentManager();
        Whobegins dialog = new Whobegins();
        String[] teamNames = header.getTeamNames();
        String[] playerNames = Whobegins.guessPlayerNames(teamNames);

        Bundle bundle = new Bundle();
        bundle.putStringArray(Whobegins.Names_Key, playerNames);
        bundle.putInt(Whobegins.RoundNo_Key, header.getRoundNo());
        bundle.putString(Whobegins.Preferences_Key, "Coiffeur");
        dialog.setArguments(bundle);
        dialog.show(fm, "whoBegins");
    }

    public void evaluate() {
        int[] pts = new int[3];
        ArrayList<Integer>[] factors = new ArrayList[3];
        factors[0] = new ArrayList<>();
        factors[1] = new ArrayList<>();
        factors[2] = new ArrayList<>();

        int maxRounds = rows * 3;
        for (CoiffeurRound round : rounds) {
            if (round.getVisibility() == View.VISIBLE) {
                int[] pots = round.getPoints();
                Integer[] fact = round.isOpen();
                for (int i = 0; i < 3; i++) {
                    pts[i] += pots[i];
                    if (fact[i] != null) {
                        if (fact[i] == -1) {
                            maxRounds--;
                        } else {
                            factors[i].add(fact[i]);
                        }
                    }
                }
            }
        }

        Bundle bundle = new Bundle();
        bundle.putStringArray("names", header.getTeamNames());
        bundle.putIntArray("pts", pts);
        bundle.putIntegerArrayList("factors1", factors[0]);
        bundle.putIntegerArrayList("factors2", factors[1]);
        bundle.putIntegerArrayList("factors3", factors[2]);
        footer.update(bundle);

        int roundNo = maxRounds - factors[0].size() - factors[1].size() - factors[2].size();
        header.setRoundNo(roundNo);
        if (roundNo == 0) {
            header.startClock();
        }
        if (factors[0].size() + factors[1].size() + ((header.getTeamNames().length == 3) ? factors[2].size() : 0) == 0) {
            header.stopClock(true);
        } else {
            header.stopClock(false);
        }

        // update all settings
        PreferenceConverterV2.convert(getContext());
    }

    public void reset() {
        AlertDialog.Builder confirmreset = new AlertDialog.Builder(getActivity());
        confirmreset.setTitle(R.string.reset);
        confirmreset.setMessage(getString(R.string.resetConfirm));
        confirmreset.setPositiveButton(android.R.string.yes,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        for (CoiffeurRound round : rounds) {
                            round.reset();
                        }
                        footer.reset();
                        header.resetClock();
                        evaluate();
                    }
                }
        );
        confirmreset.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        confirmreset.show();
    }

    @Override
    public void onFinishDialog(int t, Integer userInput) {
        int row = (t / 10) - 1;
        rounds[row].onFinishDialog(t, userInput);
    }

    @Override
    public void onFinishDialog(int dialogId, String userInput) {
        header.onFinishDialog(dialogId, userInput);
    }
}