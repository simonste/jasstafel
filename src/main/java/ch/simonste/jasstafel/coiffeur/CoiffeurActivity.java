package ch.simonste.jasstafel.coiffeur;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import ch.simonste.helpers.SimpleInputDialog;
import ch.simonste.helpers.Utils;
import ch.simonste.jasstafel.R;
import ch.simonste.jasstafel.common.NFCUtils;

public class CoiffeurActivity extends AppCompatActivity implements SimpleInputDialog.SimpleDialogListener {
    private static final int MENU_RESET = 1;
    private static final int MENU_SETTINGS = 2;
    private CoiffeurFragment fragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.setCommonPreferences(this);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) getSupportActionBar().hide();

        setContentView(R.layout.coiffeur);
        fragment = (CoiffeurFragment) getFragmentManager().findFragmentById(R.id.fragment);

        NFCUtils.createSender(this, "Coiffeur");
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        NFCUtils.handleIntent(this, intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (data.getExtras().getInt("Action") == R.id.action_reset) {
                fragment.reset();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.setCommonPreferences(this);
        onNewIntent(getIntent());
    }

    public void evaluate() {
        fragment.evaluate();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, MENU_RESET, 0, R.string.reset);
        menu.add(0, MENU_SETTINGS, 0, R.string.settings);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case MENU_RESET:
                fragment.reset();
                break;
            case MENU_SETTINGS:
                startActivityForResult(new Intent(this, CoiffeurPreferences.class), MENU_SETTINGS);
                break;
        }
        return true;
    }

    @Override
    public void onFinishDialog(int t, Integer userInput) {
        fragment.onFinishDialog(t, userInput);
    }

    @Override
    public void onFinishDialog(int dialogId, String userInput) {
        fragment.onFinishDialog(dialogId, userInput);
    }
}