package ch.simonste.jasstafel.coiffeur;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;

import androidx.appcompat.view.menu.ActionMenuItemView;
import androidx.appcompat.widget.Toolbar;

import ch.simonste.helpers.SimpleInputDialog;
import ch.simonste.jasstafel.R;

public class CoiffeurInputDialog extends SimpleInputDialog {
    static final public int EMPTY_POINTS = -3512;
    static final public int SCRATCH = -3513;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        toolbar.inflateMenu(R.menu.coiffeurinput);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_157_x:
                        Integer input = -100;
                        try {
                            input = Integer.valueOf(editText.getText().toString());
                        } catch (NumberFormatException ignored) {
                        }
                        int points = 157 - input;
                        editText.setText(String.valueOf(points));
                        break;
                    case R.id.action_scratch:
                        editText.setText(String.valueOf(SCRATCH));
                        onEditorAction(null, EditorInfo.IME_ACTION_DONE, null);
                }
                return true;
            }
        });

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @SuppressLint("RestrictedApi")
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    ActionMenuItemView opponent = toolbar.findViewById(R.id.action_157_x);
                    opponent.setIcon(getResources().getDrawable(R.drawable.ic_action_157_x));
                    opponent.setTitle(getResources().getString(R.string.opponent));
                } else {
                    ActionMenuItemView opponent = toolbar.findViewById(R.id.action_157_x);
                    opponent.setIcon(getResources().getDrawable(R.drawable.ic_action_match));
                    opponent.setTitle(getResources().getString(R.string.opponent));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        String str = "";
        if (defInt != EMPTY_POINTS && defInt != SCRATCH) str = Integer.toString(defInt);
        editText.setText(str);
        editText.setSelection(editText.getText().length());
        return view;
    }
}
