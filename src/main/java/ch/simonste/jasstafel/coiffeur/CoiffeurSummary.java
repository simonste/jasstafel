package ch.simonste.jasstafel.coiffeur;

import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import java.util.ArrayList;

import ch.simonste.helpers.NumberTextView;
import ch.simonste.helpers.SingleDialog;
import ch.simonste.jasstafel.R;

public class CoiffeurSummary extends SingleDialog {
    private static final Data data = new Data();
    private final String TAG = "Summary";
    private int highestMin;
    private int highestPts;
    private int highestMax;
    private int secondHighestMax;

    public static CoiffeurSummaryResult getResult(Bundle bundle) {
        data.names = bundle.getStringArray("names");
        data.displayFactor = bundle.getFloat("displayFactor", 1.0f);
        data.match = Math.round(data.displayFactor * bundle.getInt("match"));
        data.bonus = Math.round(data.displayFactor * bundle.getInt("bonus"));
        data.malus = Math.round(data.displayFactor * bundle.getInt("malus"));
        data.noMatch = Math.round(data.displayFactor * (int) Math.max(Math.round(data.match * (157.0 / 257.0)), 157));

        data.factors = new ArrayList[3];
        data.factors[0] = bundle.getIntegerArrayList("factors1");
        data.factors[1] = bundle.getIntegerArrayList("factors2");
        data.factors[2] = bundle.getIntegerArrayList("factors3");

        CoiffeurSummaryResult result = new CoiffeurSummaryResult();
        result.pts = bundle.getIntArray("pts");
        result.min = new int[3];
        result.max = new int[3];
        for (int i = 0; i < data.names.length; i++) {
            result.min[i] = getMinPossiblePoints(result.pts[i], data.factors[i]);
            result.max[i] = getMaxPossiblePoints(result.pts[i], data.factors[i]);
        }
        return result;
    }

    private static int accumulateFactors(ArrayList<Integer> factors) {
        int factorSum = 0;
        for (int factor : factors) {
            factorSum += factor;
        }
        return factorSum;
    }

    private static int getMaxPossiblePoints(int points, ArrayList<Integer> factors) {
        return points + accumulateFactors(factors) * data.match + factors.size() * data.bonus;
    }

    private static int getMinPossiblePoints(int points, ArrayList<Integer> factors) {
        return points - accumulateFactors(factors) * (data.match - data.noMatch) + (factors.size() * data.malus);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.coiffeursummary, container);
        getDialog().getWindow().setWindowAnimations(R.style.DialogAnimationFlip);

        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setTitle(getTag());

        Bundle bundle = getArguments();
        CoiffeurSummaryResult result = getResult(bundle);


        ArrayList<Hint> hints = calculate(result);
        String hint = convertHintList(hints);

        final int[] player = new int[]{R.id.player0, R.id.player1, R.id.player2};
        final int[] sum = new int[]{R.id.sum0, R.id.sum1, R.id.sum2};
        final int[] min = new int[]{R.id.min0, R.id.min1, R.id.min2};
        final int[] max = new int[]{R.id.max0, R.id.max1, R.id.max2};
        for (int i = 0; i < data.names.length; i++) {
            ((TextView) view.findViewById(player[i])).setText(data.names[i]);
            ((NumberTextView) view.findViewById(sum[i])).setInt(result.pts[i]);
            if (result.pts[i] == highestPts)
                ((NumberTextView) view.findViewById(sum[i])).setTypeface(null, Typeface.BOLD);
            ((NumberTextView) view.findViewById(min[i])).setInt(result.min[i]);
            if (result.min[i] == highestMin)
                ((NumberTextView) view.findViewById(min[i])).setTypeface(null, Typeface.BOLD);
            ((NumberTextView) view.findViewById(max[i])).setInt(result.max[i]);
            if (result.max[i] == highestMax)
                ((NumberTextView) view.findViewById(max[i])).setTypeface(null, Typeface.BOLD);
        }

        ((TextView) view.findViewById(R.id.hint)).setText(hint);
        if (data.bonus == 0)
            ((TextView) view.findViewById(R.id.matchinfo)).setText(String.format(getString(R.string.matchinfo), data.match));
        else
            ((TextView) view.findViewById(R.id.matchinfo)).setText(String.format(getString(R.string.matchinfoBonus), data.match, data.bonus));

        return view;
    }

    private String convertHintList(ArrayList<Hint> hints) {
        StringBuilder hintstr = new StringBuilder();
        hintstr.append("\n");
        String team = "";
        if (hints.size() > 0) {
            team = hints.get(0).name;
        }

        for (Hint hint : hints) {
            String newHint = "";

            switch (hint.type) {
                case lost:
                    newHint = String.format(getString(R.string.hintlost), hint.name, hint.par2);
                    break;
                case winPoints:
                    newHint = String.format(getString(R.string.hint2win), hint.name, hint.par2);
                    break;
                case winWithMatch:
                    newHint = String.format(getString(R.string.hint2winmatches), hint.name, hint.par2);
                    break;
                case winPointsSingle:
                    newHint = String.format(getString(R.string.hint2winspecial), hint.name, hint.par2, hint.par3);
                    break;
                case pointsNotLose:
                    newHint = String.format(getString(R.string.hint2lead), hint.name, hint.par2);
                    break;
                case matchNotLose:
                    newHint = String.format(getString(R.string.hint2losematches), hint.name, hint.par2);
                    break;
                case pointsNotLoseOther:
                    newHint = String.format(getString(R.string.hint2loseother), hint.name, hint.par2);
                    break;
                case counterMatch:
                    newHint = String.format(getString(R.string.hintCountermatch), hint.name);
                    break;
            }
            if (!hint.name.equals(team)) {
                hintstr.append("\n");
                team = hint.name;
            }
            hintstr.append(newHint);
            hintstr.append("\n");
        }
        return hintstr.toString();
    }

    public ArrayList<Hint> calculate(Bundle bundle) {
        return calculate(getResult(bundle));
    }

    public ArrayList<Hint> calculate(CoiffeurSummaryResult result) {
        highestMin = Math.max(Math.max(result.min[0], result.min[1]), result.min[2]);
        highestPts = Math.max(Math.max(result.pts[0], result.pts[1]), result.pts[2]);
        highestMax = Math.max(Math.max(result.max[0], result.max[1]), result.max[2]);
        for (int i = 0; i < data.names.length; ++i) {
            if (highestMax == result.max[i]) {
                secondHighestMax = Math.max(result.max[(i + 1) % 3], result.max[(i + 2) % 3]);
            }
        }

        ArrayList<Hint> hints = new ArrayList<>();
        for (int i = 0; i < data.names.length; ++i) {
            hints.addAll(getHint(data.names[i], result.min[i], result.pts[i], result.max[i], data.factors[i]));
        }
        return hints;
    }

    private ArrayList<Hint> getHint(String team, int min, int pts, int max, ArrayList<Integer> factors) {
        final int factorSum = accumulateFactors(factors);

        ArrayList<Hint> hints = new ArrayList<>();

        final boolean canWin = max >= highestMin;
        final boolean canWinSelf = max == highestMax && factors.size() > 0;
        final boolean notWonYet = min < secondHighestMax;
        final boolean leading = pts == highestPts;

        if (canWin && max < highestPts) {
            // need countermatch to pass current max
            hints.add(new Hint(HintType.counterMatch, team, 0));
        }

        // round finished
        if (factorSum == 0) return hints;

        if (!canWin) {
            Log.d(TAG, "lost, can not win anymore");
            int val = (highestMin - pts - factors.size() * data.bonus) / factorSum;
            hints.add(new Hint(HintType.lost, team, val));
        } else if (canWinSelf && notWonYet) {
            Log.d(TAG, "leading, calculate points to win");
            final int avgPtsWin = pointsToWin(pts, factorSum);
            if (factors.size() == 1) {
                if (avgPtsWin > data.match) {
                    hints.add(new Hint(HintType.winWithMatch, team, factors.get(0)));
                } else {
                    hints.add(new Hint(HintType.winPointsSingle, team, factors.get(0), avgPtsWin));
                }
            } else {
                if (avgPtsWin > data.match && data.bonus > 0) {
                    // need match to win
                    calcPointsToLead(hints, pts, team, factors);
                } else {
                    hints.add(new Hint(HintType.winPoints, team, avgPtsWin));

                    for (int f : factors) {
                        int enough = pointsToWin(min, f);
                        if (enough < data.noMatch) {
                            hints.add(new Hint(HintType.winPointsSingle, team, f, enough));
                        } else if ((f * data.match + data.bonus) > (secondHighestMax - pts)) {
                            hints.add(new Hint(HintType.winWithMatch, team, f));
                        }
                    }
                }
            }
        } else if (!leading) {
            Log.d(TAG, "trailing, calculate points to lead");
            calcPointsToLead(hints, pts, team, factors);
        }
        return hints;
    }

    private int pointsToWin(int current, int factor) {
        return (int) Math.max(Math.ceil(((double) secondHighestMax - current) / factor), 0);
    }

    private void calcPointsToLead(ArrayList<Hint> hints, int pts, final String team, ArrayList<Integer> factors) {
        int factorSum = accumulateFactors(factors);
        int avgPtsToLead = (int) Math.ceil(((double) highestPts - pts) / factorSum);
        ArrayList<Integer> facts = new ArrayList<>(factors);
        int requiredMatch = matchRequired(avgPtsToLead, factorSum, facts);
        final boolean requireAtLeastOneMatch = (requiredMatch != 0);

        while (requiredMatch != 0) {
            Log.d(TAG, "require a match in " + requiredMatch);
            hints.add(new Hint(HintType.matchNotLose, team, requiredMatch));

            factorSum -= requiredMatch;
            pts += requiredMatch * data.match + data.bonus;
            avgPtsToLead = (int) Math.ceil(((double) highestPts - pts) / factorSum);
            requiredMatch = matchRequired(avgPtsToLead, factorSum, facts);
        }
        if (facts.size() > 0) {
            if (requireAtLeastOneMatch) {
                hints.add(new Hint(HintType.pointsNotLoseOther, team, avgPtsToLead));
            } else {
                hints.add(new Hint(HintType.pointsNotLose, team, avgPtsToLead));
            }
        }
    }

    private int matchRequired(int avgRequired, int factorSum, ArrayList<Integer> factors) {
        if (avgRequired <= data.noMatch || factors.size() == 0) return 0;
        final int ptsDiff = factorSum * avgRequired;
        final int maxPointsWithoutMatch = factorSum * data.noMatch;
        Log.d(TAG, "matchRequired diff:" + ptsDiff + " " + factors);

        for (int f : factors) {
            final int noMatchDiff = (data.bonus != 0) ? data.bonus : ((data.match - data.noMatch) * f);

            // enough if everywhere else "nomatch" points?
            if (ptsDiff - noMatchDiff <= maxPointsWithoutMatch) {
                int idx = factors.lastIndexOf(f);
                Log.d(TAG, "matchRequired in " + f);
                return factors.remove(idx);
            }
        }
        Log.d(TAG, "requires hightest possible match");
        return factors.remove(factors.size() - 1);
    }

    enum HintType {
        lost, winPoints, winWithMatch, winPointsSingle, pointsNotLose, matchNotLose, pointsNotLoseOther, counterMatch
    }

    private static class Data {
        String[] names;
        int match;
        int bonus;
        int malus;
        int noMatch;
        float displayFactor;
        ArrayList<Integer>[] factors;
    }

    public class Hint {
        public final HintType type;
        public final String name;
        public final int par2;
        public final int par3;

        private Hint(HintType t, String p1, int p2, int p3) {
            type = t;
            name = p1;
            par2 = p2;
            par3 = p3;
        }

        private Hint(HintType t, String p1, int p2) {
            this(t, p1, p2, 0);
        }
    }
}
