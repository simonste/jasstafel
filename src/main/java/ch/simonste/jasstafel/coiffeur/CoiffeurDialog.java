package ch.simonste.jasstafel.coiffeur;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;

import ch.simonste.helpers.SingleDialog;
import ch.simonste.jasstafel.R;

public class CoiffeurDialog extends SingleDialog implements
        android.view.View.OnClickListener {
    private CoiffeurRound target;
    private int id;
    private int factor;
    private int currentSelection;
    private String[] trumpf;
    private EditText selection;
    private NumberPicker numberPicker;

    public CoiffeurDialog() {
    }

    private static String keyString(String input) {
        // ignore whitespaces, - , / and dialect specific n,ä,e. Characters u and o are replacable
        return input.toLowerCase().replaceAll("\\s|-|/|n|ä|e", "").replace('u', 'o');
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Bundle bundle = this.getArguments();
        currentSelection = Integer.parseInt(bundle.getString("Selection"));

        View view = inflater.inflate(R.layout.coiffeurdialog, container);
        id = Integer.valueOf(getTag());
        trumpf = getResources().getStringArray(R.array.trumpf_arr);
        factor = bundle.getInt("Factor", 0);
        if (factor == 0) {
            view.findViewById(R.id.numberPicker).setVisibility(View.INVISIBLE);
            getDialog().setTitle(String.format(getString(R.string.xtimes), id));
        } else {
            numberPicker = view.findViewById(R.id.numberPicker);
            numberPicker.setMinValue(1);
            numberPicker.setMaxValue(15);
            numberPicker.setValue(factor);
            getDialog().setTitle(String.format(getString(R.string.xround), id));
        }

        selection = view.findViewById(R.id.selection);
        selection.setText(bundle.getString("Description"));

        LinearLayout tl = view.findViewById(R.id.tableLayout);
        int id = 1;

        Button ibt = view.findViewById(R.id.buttonEmpty);
        ibt.setTag(0);
        ibt.setOnClickListener(this);

        for (int row = 0; row < tl.getChildCount(); row++) {
            LinearLayout tr = (LinearLayout) tl.getChildAt(row);
            for (int i = 0; i < tr.getChildCount(); i++) {
                Button bt = (Button) tr.getChildAt(i);
                bt.setTag(id);
                bt.setOnClickListener(this);
                id++;
            }
        }

        return view;
    }

    public void setTarget(CoiffeurRound target) {
        this.target = target;
    }

    public void onClick(View v) {
        final int id = (Integer) v.getTag();
        currentSelection = id;
        if (factor == 0 || numberPicker == null) {
            factor = this.id;
        } else {
            factor = numberPicker.getValue();
        }
        String trumpfname = selection.getText().toString();
        if (id == 0) {
            String userInputText = keyString(selection.getText().toString());
            for (int i = 0; i < trumpf.length; i++) {
                String trumpf_i = keyString(trumpf[i]);
                if (trumpf_i.equals(userInputText))
                    currentSelection = i;
            }
        } else {
            trumpfname = trumpf[id];
        }
        target.setType(currentSelection, trumpfname, factor);

        ((CoiffeurActivity) target.getContext()).evaluate();
        dismiss();
    }
}
