package ch.simonste.jasstafel.coiffeur;


import android.content.Context;
import android.content.SharedPreferences;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import ch.simonste.helpers.NumberTextView;
import ch.simonste.jasstafel.R;

public abstract class CoiffeurRow extends LinearLayout {
    static final int TEAM_1 = 0;
    static final int TEAM_2 = 1;
    static final int TEAM_3 = 2;
    static final int MAX_TEAMS = 3;
    static int teams = 2;
    static boolean thirdcolumn = true;
    static int match = 0;
    static int oldmatch = 0;
    static int matchBonus = 0;
    static int matchMalus = 0;
    static float displayFactor = 1.0f;
    static float fontSize;
    static SharedPreferences settings;
    static SharedPreferences settingsMain;
    final NumberTextView[] team = new NumberTextView[MAX_TEAMS];
    TextView description;

    CoiffeurRow(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    void initialize() {
        description = findViewById(R.id.description);
        team[0] = findViewById(R.id.team1);
        team[1] = findViewById(R.id.team2);
        team[2] = findViewById(R.id.team3);
    }

    void restore(SharedPreferences settings, SharedPreferences settingsMain) {
        if (settingsMain != null) {
            teams = settingsMain.getBoolean("3teams", false) ? 3 : 2;
            thirdcolumn = settingsMain.getBoolean("3rd_column", true) || teams == MAX_TEAMS;
            oldmatch = match;
            match = Integer.valueOf(settingsMain.getString("matchPoints", "257"));
            if (match == oldmatch) oldmatch = 0;
            if (settingsMain.getBoolean("matchBonus", false)) {
                matchBonus = Integer.valueOf(settingsMain.getString("matchBonusVal", "100"));
                matchMalus = Integer.valueOf(settingsMain.getString("matchMalusVal", "-100"));
            } else {
                matchBonus = 0;
                matchMalus = 0;
            }
            CoiffeurRow.settingsMain = settingsMain;
        }
        if (settings != null) {
            CoiffeurRow.settings = settings;
        } else {
            oldmatch = 0;
        }

        LayoutParams params = (LayoutParams) team[1].getLayoutParams();
        if (thirdcolumn) {
            team[2].setVisibility(View.VISIBLE);
            params.rightMargin = 2;
            if (teams < MAX_TEAMS) {
                team[2].setOnClickListener(null);
            }
        } else {
            team[2].setVisibility(View.GONE);
            params.rightMargin = 0;
        }
        team[1].setLayoutParams(params);

        if (description != null) {
            description.setTextSize(TypedValue.COMPLEX_UNIT_PX, (float) 0.6 * fontSize);
        }
        for (NumberTextView textView : team) {
            textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, fontSize);
        }
    }

    protected void reset() {
        for (NumberTextView textView : team) {
            textView.setText("");
        }
    }
}
