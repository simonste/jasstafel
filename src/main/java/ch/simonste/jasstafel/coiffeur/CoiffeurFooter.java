package ch.simonste.jasstafel.coiffeur;


import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;

import java.util.Locale;

import ch.simonste.helpers.NumberTextView;
import ch.simonste.jasstafel.R;
import ch.simonste.jasstafel.common.VibraHelper;

public class CoiffeurFooter extends CoiffeurRow {

    private Bundle bundle;
    private int winner;

    public CoiffeurFooter(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.coiffeur_footer, this);
        super.initialize();

        OnClickListener onClickListener = new OnClickListener() {
            @Override
            public void onClick(View v) {
                showPointsSummary();
            }
        };
        for (NumberTextView tw : team) {
            tw.setOnClickListener(onClickListener);
        }
        description.setOnClickListener(onClickListener);
    }

    public int[] getPoints() {
        int[] pts = new int[MAX_TEAMS];
        for (int i = 0; i < MAX_TEAMS; i++) {
            pts[i] = team[i].getInt();
        }
        return pts;
    }

    @Override
    void restore(SharedPreferences settings, SharedPreferences settingsMain) {
        super.restore(settings, settingsMain);

        winner = getContext().getSharedPreferences("Coiffeur", Context.MODE_PRIVATE).getInt("Winner", 0);
    }

    public void reset() {
        for (NumberTextView textView : team) {
            textView.setInt(0);
            textView.setBackgroundColor(Color.BLACK);
        }
    }

    public void update(Bundle bundle) {
        bundle.putInt("match", match);
        bundle.putInt("bonus", matchBonus);
        bundle.putInt("malus", matchMalus);
        bundle.putFloat("displayFactor", displayFactor);
        CoiffeurSummaryResult result = CoiffeurSummary.getResult(bundle);

        ((TextView) findViewById(R.id.description)).setTextSize(TypedValue.COMPLEX_UNIT_PX, fontSize);

        Status[] status = new Status[3];
        for (int i = 0; i < teams; i++) {
            int other1 = (i + 1) % 3;
            int other2 = (i + 2) % 3;
            if (Math.max(result.max[other1], result.max[other2]) < result.min[i]) {
                status[i] = Status.Won;
            } else if (Math.max(result.min[other1], result.min[other2]) > result.max[i]) {
                status[i] = Status.Lost;
            }
        }

        for (int i = 0; i < MAX_TEAMS; i++) {
            team[i].setInt(result.pts[i]);
            team[i].setTextSize(TypedValue.COMPLEX_UNIT_PX, fontSize);

            if (i < teams) {
                setStatus(team[i], status[i], result.max[i]);
            } else {
                setStatus(team[i], Status.Neutral, 0);
            }
        }

        bundle.putIntArray("max", result.max);
        this.bundle = bundle;
    }

    public void showPointsSummary() {
        FragmentManager fm = ((Activity) getContext()).getFragmentManager();
        CoiffeurSummary dialog = new CoiffeurSummary();
        dialog.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        dialog.setArguments(bundle);
        dialog.show(fm, getResources().getString(R.string.coiffeursummary));
    }

    private void setStatus(TextView tw, Status status, int max) {
        tw.setTextSize(TypedValue.COMPLEX_UNIT_PX, fontSize);
        if (status == Status.Won) {
            if (winner == 0) {
                VibraHelper.vibrate(getContext(), 1000);
                winner = team[0].equals(tw) ? 1 : (team[1].equals(tw) ? 2 : 3);
                getContext().getSharedPreferences("Coiffeur", Context.MODE_PRIVATE).edit().putInt("Winner", winner).apply();
            }
            tw.setBackgroundColor(Color.WHITE);
            tw.setTextColor(Color.BLACK);
        } else {
            if (winner > 0 && team[winner - 1].equals(tw)) {
                winner = 0;
                getContext().getSharedPreferences("Coiffeur", Context.MODE_PRIVATE).edit().putInt("Winner", winner).apply();
            }
            tw.setBackgroundColor(Color.BLACK);
            tw.setTextColor(Color.WHITE);
            final String string = tw.getText().toString();
            if (status == Status.Lost && !string.equals(String.valueOf(max))) {
                tw.setTextSize(TypedValue.COMPLEX_UNIT_PX, (float) 0.6 * fontSize);
                tw.setText(String.format(Locale.GERMAN, "%1$s\n max: %2$d", string, max));
            }
        }
    }

    private enum Status {
        Neutral,
        Won,
        Lost
    }
}
