package ch.simonste.jasstafel.coiffeur;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;

import ch.simonste.helpers.NumberTextView;
import ch.simonste.helpers.SimpleInputDialog;
import ch.simonste.jasstafel.R;
import ch.simonste.jasstafel.RoundDuration;

public class CoiffeurHeader extends CoiffeurRow implements SimpleInputDialog.SimpleDialogListener {

    public static int UPDATEINTERVAL = 10;
    private final RoundDuration roundDuration;
    private final Runnable roundRunnable = new Runnable() {
        @Override
        public void run() {
            setRoundNo(roundDuration.getRoundNo());
        }
    };

    public CoiffeurHeader(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.coiffeur_header, this);
        super.initialize();

        roundDuration = new RoundDuration(getContext().getSharedPreferences("Coiffeur", Context.MODE_PRIVATE));
    }

    public void startClock() {
        roundDuration.addFirstPoints();
    }

    public void resetClock() {
        roundDuration.reset();
    }

    public void stopClock(boolean stop) {
        roundDuration.roundFinished(stop);
    }

    public void setOnClickListener(OnClickListener listener) {
        findViewById(R.id.action_settings).setOnClickListener(listener);
        findViewById(R.id.action_reset).setOnClickListener(listener);
        findViewById(R.id.roundNumberContainer).setOnClickListener(listener);
    }

    public void restore(SharedPreferences settings, SharedPreferences settingsMain, float fontSize) {
        CoiffeurRow.fontSize = fontSize;
        super.restore(settings, settingsMain);
        displayFactor = settingsMain.getBoolean("den10", false) ? 0.1f : 1.0f;

        for (NumberTextView textView : team) {
            textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, (float) 0.8 * fontSize);
        }
        setTeamName(settings.getString("TeamName1", getResources().getString(R.string.defteam1)), TEAM_1);
        setTeamName(settings.getString("TeamName2", getResources().getString(R.string.defteam2)), TEAM_2);

        FrameLayout button = findViewById(R.id.action_reset);
        if (thirdcolumn && teams < MAX_TEAMS) {
            button.setVisibility(View.VISIBLE);
            team[2].setVisibility(View.GONE);
        } else {
            if (thirdcolumn) {
                setTeamName(settings.getString("TeamName3", getResources().getString(R.string.defteam3)), TEAM_3);
            }
            button.setVisibility(View.GONE);
        }
        for (int i = 0; i < MAX_TEAMS; i++) {
            final int teamNo = i;
            team[teamNo].setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    showDialogSimple(teamNo, team[teamNo].getText().toString());
                }
            });
        }
    }

    public String[] getTeamNames() {
        String[] names;
        if (teams == 3) {
            names = new String[3];
            names[2] = team[2].getText().toString();
        } else {
            names = new String[2];
        }
        names[0] = team[0].getText().toString();
        names[1] = team[1].getText().toString();
        return names;
    }

    private void setTeamName(String name, int team) {
        SharedPreferences.Editor editor = settings.edit();
        switch (team) {
            case TEAM_1:
                editor.putString("TeamName1", name);
                this.team[0].setText(name);
                break;
            case TEAM_2:
                editor.putString("TeamName2", name);
                this.team[1].setText(name);
                break;
            case TEAM_3:
                editor.putString("TeamName3", name);
                this.team[2].setText(name);
                break;
        }
        editor.apply();
    }

    private void showDialogSimple(int team, String value) {
        FragmentManager fm = ((Activity) getContext()).getFragmentManager();
        SimpleInputDialog dialog = new SimpleInputDialog();
        dialog.setTarget(this);
        dialog.setArguments(team, SimpleInputDialog.DIALOG_INPUT_STRING, value);
        dialog.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        dialog.show(fm, getResources().getString(R.string.enterteamname));
    }

    @Override
    public void onFinishDialog(int team, String userInput) {
        if (userInput.equals("")) {
            switch (team) {
                case TEAM_1:
                    userInput = getResources().getString(R.string.defteam1);
                    break;
                case TEAM_2:
                    userInput = getResources().getString(R.string.defteam2);
                    break;
                case TEAM_3:
                    userInput = getResources().getString(R.string.defteam3);
                    break;
            }
        }
        setTeamName(userInput, team);
    }

    @Override
    public void onFinishDialog(int dialogId, Integer userInput) {

    }

    public int getRoundNo() {
        return roundDuration.getRoundNo();
    }

    public void setRoundNo(int roundNo) {
        TextView view = findViewById(R.id.roundNumber);
        String text = getResources().getQuantityString(R.plurals.rounds, roundNo, roundNo);
        view.removeCallbacks(roundRunnable);

        if (roundDuration.isReasonable()) {
            text += " - " + roundDuration.getRoundDuration() + "\"";
        }
        view.setText(text);

        roundDuration.setRoundNo(roundNo);
        view.postDelayed(roundRunnable, UPDATEINTERVAL * 1000);
    }
}
