package ch.simonste.jasstafel;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import ch.simonste.helpers.NumberTextView;

public class StatsRow extends TableRow {
    public StatsRow(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater.from(context).inflate(R.layout.statsrow, this);

        setLayoutParams(new LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.WRAP_CONTENT, 1.0f));
    }

    public void setName(String name) {
        ((TextView) findViewById(R.id.name)).setText(name);
    }

    public void setValues(String name, String a, String b, String c, String d, String e) {
        ((TextView) findViewById(R.id.name)).setText(name);
        ((NumberTextView) findViewById(R.id.valA)).setText(a);
        ((NumberTextView) findViewById(R.id.valB)).setText(b);
        ((NumberTextView) findViewById(R.id.valC)).setText(c);
        ((NumberTextView) findViewById(R.id.valD)).setText(d);
        ((NumberTextView) findViewById(R.id.valE)).setText(e);
    }

    public void setValue(int id, int val) {
        ((NumberTextView) findViewById(id)).setInt(val);
    }

    public void setFloatValue(int id, float val) {
        ((NumberTextView) findViewById(id)).setText(String.format("%.1f", val));
    }

    public void hideUnusedColumns(int usedColumns) {
        switch (usedColumns) {
            case 0:
                findViewById(R.id.valA).setVisibility(View.GONE);
            case 1:
                findViewById(R.id.valB).setVisibility(View.GONE);
            case 2:
                findViewById(R.id.valC).setVisibility(View.GONE);
            case 3:
                findViewById(R.id.valD).setVisibility(View.GONE);
            case 4:
                findViewById(R.id.valE).setVisibility(View.GONE);
            default:
        }
    }
}
