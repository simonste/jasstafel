package ch.simonste.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.view.WindowManager;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import java.util.Locale;

public class Utils {
    public static void setCommonPreferences(Activity activity) {
        setLanguage(activity);
        setScreenOrientation(activity);
    }

    private static void setScreenOrientation(Activity activity) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(activity);
        String orientationString = settings.getString("screenOrientation", "0");
        int orientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR;
        if (Integer.parseInt(orientationString) == ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE) {
            orientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE;
        } else if (Integer.parseInt(orientationString) == ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT) {
            orientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT;
        }

        activity.setRequestedOrientation(orientation);

        if (settings.getBoolean("keepScreenOn", false)) {
            activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        } else {
            activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
        AppCompatActivity appCompatActivity = (AppCompatActivity) activity;
        ActionBar actionBar = appCompatActivity.getSupportActionBar();
        if (actionBar != null) actionBar.setDisplayHomeAsUpEnabled(false);
    }

    private static void setLanguage(Activity activity) {
        Context baseContext = activity.getBaseContext();
        Configuration old_config = baseContext.getResources().getConfiguration();

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(activity);
        String languageString = settings.getString("language", old_config.locale.getLanguage());
        if (!old_config.locale.getLanguage().equals(languageString)) {
            Locale locale = new Locale(languageString);
            Locale.setDefault(locale);
            Configuration configuration = new Configuration(old_config);
            configuration.locale = locale;
            baseContext.getResources().updateConfiguration(configuration, baseContext.getResources().getDisplayMetrics());
        } else {
            settings.edit().putString("language", languageString).apply();
        }
    }
}
