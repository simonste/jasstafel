package ch.simonste.helpers;

import android.graphics.Point;
import android.view.Display;

public class ScreenPct {

    private final float fontPct;

    public ScreenPct(Display display) {
        Point size = new Point();
        display.getSize(size);
        fontPct = (float) (Math.max(size.x, size.y) / 100.0);
    }

    public float getPct() {
        return fontPct;
    }
}
