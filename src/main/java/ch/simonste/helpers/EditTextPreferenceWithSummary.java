package ch.simonste.helpers;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.preference.EditTextPreference;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

class EditTextPreferenceWithSummary extends EditTextPreference {
    public EditTextPreferenceWithSummary(Context context) {
        super(context);
    }

    public EditTextPreferenceWithSummary(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EditTextPreferenceWithSummary(Context context, AttributeSet attrs,
                                         int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setText(String text) {
        super.setText(text);
        this.setSummary(text);
    }

    public void setTitle(int id) {
        super.setTitle(id);
        setDialogTitle(id);
    }

    @Override
    protected View onCreateView(ViewGroup parent) {
        this.setSummary(this.getText());
        return super.onCreateView(parent);
    }

    @Override
    protected void showDialog(Bundle state) {
        super.showDialog(state);
        Handler delayedRun = new Handler();
        delayedRun.post(new Runnable() {
            @Override
            public void run() {
                EditText textBox = getEditText();
                textBox.setSelection(textBox.getText().length());
            }
        });
    }
}
