package ch.simonste.helpers;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

public class SimpleAlertDialog extends SingleDialog {
    private static final String Message = "Message";

    private DialogInterface.OnClickListener callback;

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        Bundle bundle = this.getArguments();
        String message = bundle.getString(Message);

        builder.setTitle(getTag());
        builder.setMessage(message);
        builder.setPositiveButton(android.R.string.ok, callback);
        if (callback != null) {
            builder.setNegativeButton(android.R.string.cancel, null);
        }

        return builder.create();
    }

    public void setMessage(String message) {
        Bundle bundle = new Bundle();
        bundle.putString(Message, message);
        super.setArguments(bundle);
    }

    public void setCallback(DialogInterface.OnClickListener callback) {
        this.callback = callback;
    }

    public interface OKCallback {
        void onOKPressed();
    }
}
