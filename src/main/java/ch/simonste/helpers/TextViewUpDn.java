package ch.simonste.helpers;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;

public class TextViewUpDn extends AppCompatTextView {

    public TextViewUpDn(Context context) {
        super(context);
    }

    public TextViewUpDn(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TextViewUpDn(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onDraw(@NonNull Canvas canvas) {
        canvas.save();
        canvas.rotate(180, getWidth() / 2.0f, getHeight() / 2.0f);
        super.onDraw(canvas);
        canvas.restore();
    }
}
