package ch.simonste.helpers;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;

public class RegressBar extends ProgressBar {

    public RegressBar(Context context) {
        super(context);
    }

    public RegressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RegressBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onDraw(@NonNull Canvas canvas) {
        canvas.save();
        canvas.rotate(180, getWidth() / 2.0f, getHeight() / 2.0f);
        super.onDraw(canvas);
        canvas.restore();
    }
}
