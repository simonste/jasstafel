package ch.simonste.helpers;

import android.content.Context;
import android.preference.ListPreference;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

class ListPreferenceWithSummary extends ListPreference {

    public ListPreferenceWithSummary(Context context) {
        super(context);
    }

    public ListPreferenceWithSummary(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected View onCreateView(ViewGroup parent) {
        this.setSummary(this.getEntry());
        return super.onCreateView(parent);
    }

    @Override
    public void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);
        this.setSummary(this.getEntry());
    }
}
