package ch.simonste.helpers;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.TextView;

import ch.simonste.jasstafel.R;

import static ch.simonste.jasstafel.R.anim.slide_in_top;

public class UndoBar extends FrameLayout {

    public static long DURATION = 3000;
    private final Handler handler;
    private final Runnable hideRunnable;
    private UndoBar.Listener undoListener;
    private Bundle bundle;

    public UndoBar(Context context) {
        this(context, null);
    }

    public UndoBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        View.inflate(context, R.layout.undobar, this);
        handler = new Handler();
        hideRunnable = new Runnable() {
            @Override
            public void run() {
                startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.slide_out_top));
                hide();
            }
        };

        findViewById(R.id.undoAction).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (undoListener != null && bundle != null) {
                    undoListener.onUndo(bundle);
                }
                hide();
            }
        });
        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                hide();
            }
        });
    }

    public void setListener(Listener listener) {
        undoListener = listener;
    }

    public void show(String message, final Bundle bundle) {
        hide(); // hide previous message
        this.bundle = bundle;
        ((TextView) findViewById(R.id.undoText)).setText(message);
        setVisibility(View.VISIBLE);
        startAnimation(AnimationUtils.loadAnimation(getContext(), slide_in_top));
        handler.postDelayed(hideRunnable, DURATION);
    }

    public void hide() {
        setVisibility(View.INVISIBLE);
        bundle = null;
        handler.removeCallbacks(hideRunnable);
    }

    public interface Listener {
        void onUndo(Bundle bundle);
    }
}
