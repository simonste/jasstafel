package ch.simonste.helpers;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.DialogInterface;

// only one of all dialogs based on this type is active at once
public class SingleDialog extends DialogFragment {
    private static boolean active = false;

    public static void switchDialogs() {
        active = false;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        active = false;
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        if (!active) {
            active = true;
            super.show(manager, tag);
        }
    }
}
