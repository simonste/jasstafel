package ch.simonste.helpers;

import android.text.Editable;
import android.text.TextWatcher;

public class SimpleTextWatcher implements TextWatcher {
    private final NumberTextView textView;
    private final double factor;
    private final Listener listener;
    private final int id;

    public SimpleTextWatcher(NumberTextView view, double factor) {
        textView = view;
        this.factor = factor;
        listener = null;
        id = 0;
    }

    public SimpleTextWatcher(NumberTextView view, double factor, Listener listener, int id) {
        textView = view;
        this.factor = factor;
        this.listener = listener;
        this.id = id;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        try {
            int ptsOrig = Integer.parseInt(charSequence.toString());
            int pts = (int) Math.round((factor * ptsOrig));
            if (factor != 1.0) textView.setInt(pts);
            if (listener != null) listener.textChanged(id, ptsOrig);
        } catch (NumberFormatException e) {
            textView.setText("");
            if (listener != null) listener.textChanged(id, 0);
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {
    }

    public interface Listener {
        void textChanged(int id, int pts);
    }
}
