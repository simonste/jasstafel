package ch.simonste.helpers;

import android.os.Bundle;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import androidx.appcompat.widget.Toolbar;

import ch.simonste.jasstafel.R;


public class SimpleInputDialog extends SingleDialog
        implements OnEditorActionListener {
    public static final int DIALOG_INPUT_STRING = 0;
    public static final int DIALOG_INPUT_NUMBER = 1;
    public static final int DIALOG_INPUT_SIGNED = 2;
    protected static SimpleDialogListener target; // static, to keep it, onRotate,..
    protected int tag;
    protected int defInt;
    protected EditText editText;
    protected Toolbar toolbar;
    private int inputType;
    private String defStr;

    public SimpleInputDialog() {
        target = null; // do not keep target of another SimpleInputDialog
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle bundle = this.getArguments();
        inputType = bundle.getInt("Type");
        tag = bundle.getInt("Tag");

        switch (inputType) {
            case DIALOG_INPUT_NUMBER:
            case DIALOG_INPUT_SIGNED:
                defInt = bundle.getInt("Value");
                break;
            case DIALOG_INPUT_STRING:
                defStr = bundle.getString("Value");
                break;
        }

        View view = inflater.inflate(R.layout.simpledialog, container);
        getDialog().setTitle(getTag());
        toolbar = view.findViewById(R.id.toolbar);
        toolbar.setTitle(getTag());
        editText = view.findViewById(R.id.inputField);

        if (inputType == DIALOG_INPUT_STRING) {
            editText.setText(defStr);
        } else {
            if (inputType == DIALOG_INPUT_NUMBER) {
                editText.setInputType(InputType.TYPE_CLASS_NUMBER);
            } else {
                editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED);
            }
            String str = "";
            if (defInt != 0) str = Integer.toString(defInt);
            editText.setText(str);
        }

        editText.requestFocus();
        editText.setHint(getTag());
        getDialog().getWindow().setSoftInputMode(
                LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        editText.setOnEditorActionListener(this);
        editText.setSelection(editText.getText().length());
        return view;
    }

    public void setTarget(SimpleDialogListener target) {
        SimpleInputDialog.target = target;
    }

    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (EditorInfo.IME_ACTION_DONE == actionId) {
            if (target == null) {
                if (getTargetFragment() != null) {
                    target = (SimpleDialogListener) getTargetFragment();
                } else {
                    target = (SimpleDialogListener) getActivity();
                }
            }
            // Return input text to activity
            if (inputType == DIALOG_INPUT_NUMBER | inputType == DIALOG_INPUT_SIGNED) {
                Integer input = null;
                try {
                    input = Integer.valueOf(editText.getText().toString());
                } catch (NumberFormatException ignored) {
                }
                target.onFinishDialog(tag, input);
            } else {
                target.onFinishDialog(tag, editText.getText().toString());
            }
            target = null;
            this.dismiss();
            return true;
        }
        return false;
    }

    public void setArguments(int tag, int type, String value) {
        Bundle bundle = new Bundle();
        bundle.putInt("Type", type);
        bundle.putInt("Tag", tag);
        bundle.putString("Value", value);
        super.setArguments(bundle);
    }

    public void setArguments(int tag, int type, int value) {
        Bundle bundle = new Bundle();
        bundle.putInt("Type", type);
        bundle.putInt("Tag", tag);
        bundle.putInt("Value", value);
        super.setArguments(bundle);
    }

    public interface SimpleDialogListener {
        void onFinishDialog(int dialogId, String userInput);

        void onFinishDialog(int dialogId, Integer userInput);
    }
}
