package ch.simonste.helpers;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ListView;

import ch.simonste.jasstafel.R;

public class FabListView extends ListView implements View.OnTouchListener {

    private View fabContainer;
    private HideOnScrollTop translator;
    private boolean buttonOnTop;
    private boolean scrollable;
    private OnSizeChangedListener listener;
    private int parentHeight;

    public FabListView(Context context, AttributeSet attrs) {
        super(context, attrs);

        buttonOnTop = false;
        scrollable = false;
        parentHeight = 0;

        this.setOnTouchListener(this);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (isInEditMode()) return;

        smoothScrollToPosition(getAdapter().getCount());

        int pxMargin = (int) getResources().getDimension(R.dimen.fab_height);
        int parHeight = ((View) getParent()).getHeight();
        if (parentHeight < parHeight) parentHeight = parHeight;

        buttonOnTop = (h + pxMargin) > parentHeight;
        scrollable = h == parentHeight;

        if (listener != null) {
            //boolean scroll = (h + 0.4*pxMargin) > parentHeight;
            listener.notify(scrollable);
        }

        setFabPosition();
    }

    public void addListener(OnSizeChangedListener listener) {
        this.listener = listener;
    }

    public void addContainer(View view) {
        fabContainer = view;
        translator = new HideOnScrollTop(view, R.anim.floating_action_button_show_top, R.anim.floating_action_button_hide_top);
    }

    private void setFabPosition() {
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) fabContainer.getLayoutParams();
        if (buttonOnTop) {
            params.gravity = Gravity.TOP | Gravity.RIGHT;
        } else {
            params.gravity = Gravity.BOTTOM | Gravity.RIGHT;
            if (fabContainer.getVisibility() == View.GONE) {
                fabContainer.setVisibility(View.VISIBLE);
            }
        }
        fabContainer.setLayoutParams(params);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        // no animation if FAB is invisible
        if (fabContainer.getVisibility() == INVISIBLE) return false;

        if (buttonOnTop) {
            if (scrollable) {
                translator.onTouch(v, event);
            } else if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
                if (fabContainer.getVisibility() == GONE) {
                    translator.onScrollUp();
                } else {
                    translator.onScrollDown();
                }
            }
        }
        return false;
    }

    public interface OnSizeChangedListener {
        void notify(boolean scrollable);
    }
}
