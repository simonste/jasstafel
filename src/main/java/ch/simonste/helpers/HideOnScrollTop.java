package ch.simonste.helpers;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

/**
 * Implementation of {@link View.OnTouchListener} which shows/hides specified view
 * when {@link #onScrollUp()} and {@link #onScrollDown()} events recognized.
 */
class HideOnScrollTop extends com.shamanland.fab.ScrollDetector implements Animation.AnimationListener {
    private final View view;
    private final int show;
    private final int hide;

    /**
     * Construct object with custom animations.
     *
     * @param view     null not allowed
     * @param animShow anim resource id
     * @param animHide anim resource id
     */
    public HideOnScrollTop(View view, int animShow, int animHide) {
        super(view.getContext());
        this.view = view;
        show = animShow;
        hide = animHide;
    }

    @Override
    public void onScrollDown() {
        if (view.getVisibility() == View.VISIBLE) {
            view.setVisibility(View.GONE);
            animate(hide);
        }
    }

    @Override
    public void onScrollUp() {
        if (view.getVisibility() != View.VISIBLE) {
            view.setVisibility(View.VISIBLE);
            animate(show);
        }
    }

    private void animate(int anim) {
        if (anim != 0) {
            Animation a = AnimationUtils.loadAnimation(view.getContext(), anim);
            a.setAnimationListener(this);

            view.startAnimation(a);
            setIgnore(true);
        }
    }

    @Override
    public void onAnimationStart(Animation animation) {
        // empty
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        setIgnore(false);
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
        // empty
    }
}
