package ch.simonste.helpers;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

import java.util.Locale;

public class NumberTextView extends AppCompatTextView {

    public NumberTextView(Context context) {
        super(context);
    }

    public NumberTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NumberTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public Integer getInteger() {
        try {
            return Integer.parseInt(this.getText().toString());
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public int getInt() {
        try {
            return Integer.parseInt(this.getText().toString());
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public void setInt(Integer value) {
        if (value == null)
            setText("");
        else
            setText(String.format(Locale.GERMAN, "%1$d", value));
    }

    private float getFloat() {
        try {
            return Float.parseFloat(this.getText().toString());
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    private void setFloat(float value) {
        this.setText(String.format(Locale.GERMAN, "%1$f", value));
    }

    public void add(int value) {
        this.setInt(this.getInt() + value);
    }

    public void add(float value) {
        this.setFloat(this.getFloat() + value);
    }
}
